/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_Interface_Queue.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2018年5月8日
  最近修改   :
  功能描述   : RF的接口操作-RF发送队列操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2018年5月8日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
RF_ItfRfExeTypedef RfExtQueueBuff[ITF_RF_EXT_QUEUE_SIZE];

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : ItfQueue_ElementClr
 功能描述  : 清除元素内容
 输入参数  : RF_ItfRfExeTypedef *clrExt  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfQueue_ElementClr(RF_ItfRfExeTypedef *clrExt)
{
    // 不激活
    clrExt->IsActvie = RESET;

    // 默认不转发AG地址
    clrExt->AgAddr.App = 0xFF;
    clrExt->AgAddr.Group = 0xFF;

    // 默认控制事件
    clrExt->Event = RF_EVENT_CNL;

    // 清零重复次数和重复计数
    clrExt->RepeatNum = 0;
    clrExt->RepeatCnt = 0;

    // 载入默认的最大和最小间隔时间
    clrExt->IntervalMax = RF_SEND_REPEAT_MS_MAX;
    clrExt->IntervalMin = RF_SEND_REPEAT_MS_MIN;

    // 清零数据长度
    clrExt->CmdLen = 0;

    // 清除定时器计时
    Tmm_Clr(clrExt->TimId);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfQueue_Init
 功能描述  : 初始化队列
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfQueue_Init(void)
{
    u8 i;

    // 循环清除队列元素
    for(i=0; i<ITF_RF_EXT_QUEUE_SIZE; i++)
    {   
        // 载入TmmID
        RfExtQueueBuff[i].TimId = (Tmm_ID)(TMM_RF_SEND_QUEUE_START + i);

        // 清除元素内容
        ItfQueue_ElementClr(&RfExtQueueBuff[i]);
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfQueue_GetEmptyIndex
 功能描述  : 获取空的元素下标
 输入参数  : u8 *gIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfQueue_GetEmptyIndex(u8 *gIndex)
{
    u8 i;
    
    for(i=0; i<ITF_RF_EXT_QUEUE_SIZE; i++)
    {
        if(RfExtQueueBuff[i].IsActvie != SET)
        {
            *gIndex = i;

            return SUCCESS;
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfQueue_GetAddrIndex
 功能描述  : 获取地址对应的下标
 输入参数  : RunAddrTypedef sAddr  
             u8 *gIndex            
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年12月14日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfQueue_GetAddrIndex(RunAddrTypedef sAddr, u8 *gIndex)
{
    u8 i;
    
    // 轮询所有的接口队列
    for(i=0; i<ITF_RF_EXT_QUEUE_SIZE; i++)
    {
        // 判断是否激活启用
        if(RfExtQueueBuff[i].IsActvie == SET)
        {
            // 判断AG地址是否匹配
            if((RfExtQueueBuff[i].AgAddr.App == sAddr.App)
            && (RfExtQueueBuff[i].AgAddr.Group == sAddr.Group))
            {
                // 载入下标号
                *gIndex = i;

                // 返回成功
                return SUCCESS;
            }
        }
    }

    // 返回失败
    return ERROR;
}


/*****************************************************************************
 函 数 名  : ItfQueue_CheckAddr
 功能描述  : 检查地址合法性
 输入参数  : RunAddrTypedef *cAddr  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus ItfQueue_CheckAddr(RunAddrTypedef *cAddr)
{
    if((cAddr->App == 0xFF)
    || (cAddr->App == 0x00)
    || (cAddr->Group == 0xFF)
    || (cAddr->Group == 0x00))
    {
        return RESET;
    }

    return SET;
}

/*****************************************************************************
 函 数 名  : ItfQueue_CheckRepeat
 功能描述  : 检查重发次数
 输入参数  : uc8 rNum  
             uc8 rCnt  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus ItfQueue_CheckRepeat(uc8 rNum, uc8 rCnt)
{
    if(rCnt >= rNum)
    {
        return RESET;
    }

    return SET;
}

/*****************************************************************************
 函 数 名  : ItfQueue_ElementPoll
 功能描述  : 单个元素发送轮询
 输入参数  : uc8 elmIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfQueue_ElementPoll(uc8 elmIndex)
{
    u32 gRandomTick = 0;
    u32 gMaxMs = 0, gMinMs = 0;


    // 判断是否启动
    if(RfExtQueueBuff[elmIndex].IsActvie == SET)
    {
        // 判断定时器是否已经计时完毕
        if(Tmm_IsON(RfExtQueueBuff[elmIndex].TimId) != SET)
        {
            // 检查发送地址是否合法
            if(ItfQueue_CheckAddr(&RfExtQueueBuff[elmIndex].AgAddr) == SET)
            {
                // 判断发送次数是否足够
                if(ItfQueue_CheckRepeat(RfExtQueueBuff[elmIndex].RepeatNum, RfExtQueueBuff[elmIndex].RepeatCnt) == SET)
                {
                    // 载入发送 - 并判断是否发送成功
                    if(RfApp_LoadElementToSend(&RfExtQueueBuff[elmIndex]) == SUCCESS)
                    {
                        // 发送次数+1
                        RfExtQueueBuff[elmIndex].RepeatCnt += 1;

                        // 判断发送次数是否足够
                        if(RfExtQueueBuff[elmIndex].RepeatCnt >= RfExtQueueBuff[elmIndex].RepeatNum)
                        {
                            // 清空元素内容
                            ItfQueue_ElementClr(&RfExtQueueBuff[elmIndex]);
                        }
                        else
                        {
                            // 获取最大最小毫秒数
                            gMaxMs = ItfLogic_GetExtDelayMs(RfExtQueueBuff[elmIndex].IntervalMax);
                            gMinMs = ItfLogic_GetExtDelayMs(RfExtQueueBuff[elmIndex].IntervalMin);
                            
                            // 获取随机数节拍
                            gRandomTick = Ramdom_GetRandom(gMaxMs*100, gMinMs*100);

                            // 载入延时节拍
                            Tmm_LoadBaseTick(RfExtQueueBuff[elmIndex].TimId, gRandomTick);
                        }
                    }
                    else
                    {
                        // 清空元素内容
                        ItfQueue_ElementClr(&RfExtQueueBuff[elmIndex]);
                    }
                }
                else
                {
                    // 清空元素内容
                    ItfQueue_ElementClr(&RfExtQueueBuff[elmIndex]);
                }
            }
            // 发送地址非法
            else
            {
                // 清空元素内容
                ItfQueue_ElementClr(&RfExtQueueBuff[elmIndex]);
            }
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfQueue_Poll
 功能描述  : 接口队列发送轮询
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfQueue_Poll(void)
{
    u8 i;

    // 轮巡所有的执行队列
    for(i=0; i<ITF_RF_EXT_QUEUE_SIZE; i++)
    {
        // 单个元素执行
        ItfQueue_ElementPoll(i);
    }

    return SUCCESS;
}

