/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_Interface_Mmy.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2018年5月2日
  最近修改   :
  功能描述   : RF接口的存储操作层
  函数列表   :
  修改历史   :
  1.日    期   : 2018年5月2日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static RF_ItfMmyOptGroupTypedef ItfMmyOptAttr;

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

// CAN 转发到RF的接口参数 默认参数
static uc8  ItfCanMmyParaDefault[RF_ITF_CAN_CFG_NUM][ITF_CAN_MMY_PARA_LEN] __attribute__((at(
MCU_ITF_CAN_DEFAULT_MMY_ADDR))) =
{
    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },

    // CNL_EVENT、3次重发、min=100ms,max=200;
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00, 0x03, RF_SEND_REPEAT_MS_MIN, RF_SEND_REPEAT_MS_MAX
    },
};

// RF 转发到CAN的接口参数 默认参数
static uc8  ItfRfMmyParaDefault[RF_ITF_RF_CFG_NUM][ITF_RF_MMY_PARA_LEN] __attribute__((at(MCU_ITF_RF_DEFAULT_MMY_ADDR)
)) =
{
    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00   
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },

    {
        0x00, 0xFF, 0xFF,                                  \
        0x00, 0x00    
    },
};

// 默认配置的安全层
static u8 ItfSecurityDefaultBuff[ITF_RF_SECURITY_PARA_LEN] = {0x55, 0xAA, 0x33, 0xCC, 0x5A, 0x3C};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : ItfMmy_IsMmyType
 功能描述  : 判断是否为接口存储类型
 输入参数  : RF_ItfMmyTypedef mmyType  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus ItfMmy_IsMmyType(RF_ItfMmyTypedef mmyType)
{
    if(mmyType < RF_ITF_MMY_TYPE_MAX)
    {
     return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : ItfMmy_IsCanIndex
 功能描述  : 判断是否为CAN的接口下标
 输入参数  : u8 tIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  ItfMmy_IsCanIndex(u8 tIndex)
{
    if(tIndex < RF_ITF_CAN_CFG_NUM)
    {
        return SET;
    }

    return RESET;  
}

/*****************************************************************************
 函 数 名  : ItfMmy_IsRfIndex
 功能描述  : 判断是否为RF的接口下标
 输入参数  : u8 tIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  ItfMmy_IsRfIndex(u8 tIndex)
{
    if(tIndex < RF_ITF_RF_CFG_NUM)
    {
        return SET;
    }

    return RESET;  
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetCanIndex
 功能描述  : 获取CAN的存储下标
 输入参数  : u8 sIndex   
             u8 *gIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetCanIndex(u8 sIndex, u8 *gIndex)
{
    // 判断下标是否合法
    if(ItfMmy_IsCanIndex(sIndex) == SET)
    {
        *gIndex = (RF_ITF_MMY_TYPE_CAN_START + sIndex);

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetRfIndex
 功能描述  : 获取RF的存储下标
 输入参数  : u8 sIndex   
             u8 *gIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetRfIndex(u8 sIndex, u8 *gIndex)
{
    // 判断下标是否合法
    if(ItfMmy_IsRfIndex(sIndex) == SET)
    {
        *gIndex = (RF_ITF_MMY_TYPE_RF_START + sIndex);

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetRfSecIndex
 功能描述  : 获取RF安全层存储下标
 输入参数  : u8 *gIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2019年1月2日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetRfSecIndex(u8 *gIndex)
{
    *gIndex = RF_ITF_MMY_TYPE_SECURITY;

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : ItfMmy_CanOrganizeInit
 功能描述  : CAN的接口存储架构初始化
 输入参数  : uc8 sIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_CanOrganizeInit(uc8 sIndex)
{
    u8 gIndex = 0;
    
    if(ItfMmy_GetCanIndex(sIndex, &gIndex) == SUCCESS)
    {
        // FLASH起始地址
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyStartAddr = (ITF_CAN_MMY_ADDR_START + ((gIndex - RF_ITF_MMY_TYPE_CAN_START
) * ITF_CAN_MMY_CFG_MSG_SIZE));
        // 存储类型
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyDatType = ITF_CAN_MMY_DATA_TYPE;
        // 数据长度
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyOptParaLen = ITF_CAN_MMY_PARA_LEN;
        // 存储大小
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyOptParaSize = ITF_CAN_MMY_CFG_MSG_SIZE;
        // 默认参数的数据指针
        ItfMmyOptAttr.OptMmyPara[gIndex].DefaultRamAddr = ItfCanMmyParaDefault[gIndex - RF_ITF_MMY_TYPE_CAN_START];
        
        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfMmy_RfOrganizeInit
 功能描述  : RF的接口存储架构初始化
 输入参数  : uc8 sIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_RfOrganizeInit(uc8 sIndex)
{
    u8 gIndex = 0;

    if(ItfMmy_GetRfIndex(sIndex, &gIndex) == SUCCESS)
    {
        // FLASH起始地址
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyStartAddr = (ITF_RF_MMY_ADDR_START + ((gIndex - RF_ITF_MMY_TYPE_RF_START) 
* ITF_RF_MMY_CFG_MSG_SIZE));
        // 存储类型
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyDatType = ITF_RF_MMY_DATA_TYPE;
        // 数据长度
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyOptParaLen = ITF_RF_MMY_PARA_LEN;
        // 存储大小
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyOptParaSize = ITF_RF_MMY_CFG_MSG_SIZE;
        // 默认参数的数据指针
        ItfMmyOptAttr.OptMmyPara[gIndex].DefaultRamAddr = ItfRfMmyParaDefault[gIndex - RF_ITF_MMY_TYPE_RF_START];

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfMmy_RfSecOrganizeInit
 功能描述  : RF安全层存储架构初始化
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2019年1月2日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_RfSecOrganizeInit(void)
{
    u8 gIndex;

    // 获取下标
    if(ItfMmy_GetRfSecIndex(&gIndex) == SUCCESS)
    {
        // FLASH存储地址
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyStartAddr = ITF_RF_SECURITY_ADDR;
        // 存储类型
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyDatType = ITF_RF_SECURITY_DATA_TYPE;
        // 数据长度
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyOptParaLen = ITF_RF_SECURITY_PARA_LEN;

        // 存储大小
        ItfMmyOptAttr.OptMmyPara[gIndex].MmyOptParaSize = ITF_RF_SECURITY_CFG_MSG_SIZE;
        // 默认参数的数据指针
        ItfMmyOptAttr.OptMmyPara[gIndex].DefaultRamAddr = ItfSecurityDefaultBuff;

        return SUCCESS;
    }

    return ERROR;
}


/*****************************************************************************
 函 数 名  : ItfMmy_OrganizeInit
 功能描述  : 接口存储的架构初始化
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void ItfMmy_OrganizeInit(void)
{
    u8 tmpIndex;

    // 轮询所有CAN_ID
    for(tmpIndex=0; tmpIndex<RF_ITF_CAN_CFG_NUM; tmpIndex++)
    {
        // CAN组织架构初始化
        ItfMmy_CanOrganizeInit(tmpIndex);
    }

    // 轮询所有RF_ID
    for(tmpIndex=0; tmpIndex<RF_ITF_RF_CFG_NUM; tmpIndex++)
    {
        // RF组织架构初始化
        ItfMmy_RfOrganizeInit(tmpIndex);
    }

    // 初始化RF安全层架构
    ItfMmy_RfSecOrganizeInit();
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetParaFromMMY
 功能描述  : 获取存储参数
 输入参数  : RF_ItfMmyTypedef mmyType  
             u8 gParaBuff[]            
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetParaFromMMY(RF_ItfMmyTypedef mmyType, u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr;
    u8          gParaLen;
    u8          i;

    assert_app_param(IS_ITF_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));


    // 获取 存储类型
    gMmyDat.Type = ItfMmyOptAttr.OptMmyPara[mmyType].MmyDatType;

    // 获取起始地址
    gMmyAddr = ItfMmyOptAttr.OptMmyPara[mmyType].MmyStartAddr;

    // 获取FLASH数据
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    // 获取数据长度
    gParaLen = ItfMmyOptAttr.OptMmyPara[mmyType].MmyOptParaLen;

    // 复制数据
    for(i = 0; i<gParaLen; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfMmy_SetParaToMMY
 功能描述  : 设置存储参数
 输入参数  : RF_ItfMmyTypedef mmyType  
             uc8 sParaBuff[]           
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_SetParaToMMY(RF_ItfMmyTypedef mmyType, uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr;
    u8          sParaLen;
    u8          sParaSize;
    u8          i;

    assert_app_param(IS_ITF_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));


    // 获取 存储类型
    sMmyDat.Type = ItfMmyOptAttr.OptMmyPara[mmyType].MmyDatType;

    // 获取起始地址
    sMmyAddr = ItfMmyOptAttr.OptMmyPara[mmyType].MmyStartAddr;


    // 获取数据容量大小
    sParaSize = ItfMmyOptAttr.OptMmyPara[mmyType].MmyOptParaSize;

    // 获取写入的数据长度
    sParaLen = ItfMmyOptAttr.OptMmyPara[mmyType].MmyOptParaLen;

    // 载入数据
    for(i = 0; i<sParaSize; i++)
    {
        if(i < sParaLen)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    // 将数据载入到FLASH内
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetDefaultPara
 功能描述  : 获取默认参数
 输入参数  : RF_ItfMmyTypedef mmyType  
             u8 gParaBuff[]            
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetDefaultPara(RF_ItfMmyTypedef mmyType, u8 gParaBuff[])
{
    u8 i;
    u8 gLen;

    assert_app_param(IS_ITF_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));

    // 获取数据长度
    gLen = ItfMmyOptAttr.OptMmyPara[mmyType].MmyOptParaLen;

    // 载入数据
    for(i = 0; i<gLen; i++)
    {
        gParaBuff[i] = ItfMmyOptAttr.OptMmyPara[mmyType].DefaultRamAddr[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfMmy_SetDefaultToMMY
 功能描述  : 设置默认参数
 输入参数  : RF_ItfMmyTypedef mmyType  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_SetDefaultToMMY(RF_ItfMmyTypedef mmyType)
{
    assert_app_param(IS_ITF_MMY_TYPE(mmyType));

    // 设置默认数据到RAM中
    return ItfMmy_SetParaToMMY(mmyType, ItfMmyOptAttr.OptMmyPara[mmyType].DefaultRamAddr);
}

/*****************************************************************************
 函 数 名  : ItfMmy_WriteCanParaToMmy
 功能描述  : 写入CAN接口参数到存储内
 输入参数  : RF_ItfMmyTypedef mmyType  
             u8 sBuff[]                
             u8 sLen                   
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_WriteCanParaToMmy(RF_ItfMmyTypedef mmyType, u8 sBuff[], u8 sLen)
{
    u8 gIndex = 0;
    u8 reIndex = 0;

    // 长度字节是否 大于等于 内容字节+1 且 内容字节等于配置内容长度
    if(((sLen - 1) >= sBuff[0])     &&
    ((sBuff[0]) == ITF_CAN_MMY_PARA_LEN))
    {
        // 越过数据长度
        gIndex++;

        // 设置参数到存储里面
        if(ItfMmy_SetParaToMMY(mmyType, &(sBuff[gIndex])) == SUCCESS)
        {
            // 获取CAN下标
            reIndex =  (mmyType - RF_ITF_MMY_TYPE_CAN_START);

            // 判断CAN下标的合法性
            if(IS_RF_ITF_CAN_ID(reIndex) == SET)
            {
                // 重新解析CAN接口的触发参数
                return ItfMmy_GetTriggerPara(&(sBuff[gIndex]), &CanItfTriAttr[reIndex]);
            }
        }
    }

    // 返回错误
    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfMmy_WriteRfParaToMmy
 功能描述  : 写入RF接口参数到存储内
 输入参数  : RF_ItfMmyTypedef mmyType  
             u8 sBuff[]                
             u8 sLen                   
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_WriteRfParaToMmy(RF_ItfMmyTypedef mmyType, u8 sBuff[], u8 sLen)
{
    u8 gIndex = 0;
    u8 reIndex = 0;

    // 长度字节是否 大于等于 内容字节+1 且 内容字节等于配置内容长度
    if(((sLen - 1) >= sBuff[0])     &&
       ((sBuff[0]) == ITF_RF_MMY_PARA_LEN))
    {
        // 越过数据长度
        gIndex++;

        // 设置参数到存储里面
        if(ItfMmy_SetParaToMMY(mmyType, &(sBuff[gIndex])) == SUCCESS)
        {
            // 获取RF下标
            reIndex =  (mmyType - RF_ITF_MMY_TYPE_RF_START);

            // 判断RF下标的合法性
            if(IS_RF_ITF_RF_ID (reIndex) == SET)
            {
                // 重新解析RF接口的触发参数
                return ItfMmy_GetTriggerPara(&(sBuff[gIndex]), &RfItfTriAttr[reIndex]);
            }
        }
    }

    // 返回错误
    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfMmy_WriteRfSecParaToMmy
 功能描述  : 写入RF安全层到存储区
 输入参数  : u8 sBuff[]  
             u8 sLen     
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2019年1月2日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_WriteRfSecParaToMmy(u8 sBuff[], u8 sLen)
{
    u8 gIndex = 0;

    // 长度字节是否 大于等于 内容字节+1 且 内容字节等于配置内容长度
    if(((sLen - 1) >= sBuff[0])     &&
       ((sBuff[0]) == ITF_RF_SECURITY_PARA_LEN))
    {
        gIndex++;

         // 设置参数到存储里面
        if(ItfMmy_SetParaToMMY(RF_ITF_MMY_TYPE_SECURITY, &(sBuff[gIndex])) == SUCCESS)
        {
            // 重新刷新安全层
            return ItfMmy_GetRfSecPara(&(sBuff[gIndex]), RfSecBuff);
        }
    }
    
    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetTriggerPara
 功能描述  : 获取触发参数
 输入参数  : uc8 gParaBuff[]            
             RF_ItfTriTypedef *triPara  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetTriggerPara(uc8 gParaBuff[], RF_ItfTriTypedef *triPara)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(triPara));

    // 获取转发状态是否激活
    if((gParaBuff[0] & 0x80) == 0x80)
    {
        triPara->IsActive = SET;
    }
    else
    {
        triPara->IsActive = RESET;
    }

    // 获取转发的AG地址
    triPara->AgAddr.App = gParaBuff[1];
    triPara->AgAddr.Group = gParaBuff[2];
  
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetCanExtPara
 功能描述  : 获取CAN接口的执行参数
 输入参数  : uc8 gParaBuff[]                  
             RF_ItfCanTriExtTypedef *extPara  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetCanExtPara(uc8 gParaBuff[], RF_ItfCanTriExtTypedef *extPara)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(extPara));

    // 载入AG地址
    extPara->ExtAddr.App = gParaBuff[0];
    extPara->ExtAddr.Group = gParaBuff[1];

    // 判断事件类型是否为 控制类型
    if((gParaBuff[2] & 0x10) == 0x00)
    {
        extPara->Event = RF_EVENT_CNL;
    }
    // 判断事件类型是否为 场景类型
    else
    {
        extPara->Event = RF_EVENT_SCENE;
    }

    // 载入重发次数
    extPara->RepeatNum = (gParaBuff[2] & RF_SEND_MAX_REPEAT_NUM);

    // 载入最小的间隔时间
    extPara->IntervalMin = gParaBuff[3];
    // 载入最大的间隔时间
    extPara->IntervalMax = gParaBuff[4];

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetRfExtPara
 功能描述  : 获取RF执行的参数 - 执行地址
 输入参数  : uc8 gParaBuff[]          
             RunAddrTypedef *extAddr  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetRfExtPara(uc8 gParaBuff[], RunAddrTypedef *extAddr)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(extAddr));

    extAddr->App = gParaBuff[0];
    extAddr->Group = gParaBuff[1];

    return SUCCESS;    
}

/*****************************************************************************
 函 数 名  : ItfMmy_GetRfSecPara
 功能描述  : 获取RF安全层参数
 输入参数  : uc8 sPara[]  
             u8 gPara[]   
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2019年1月2日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfMmy_GetRfSecPara(uc8 sPara[], u8 gPara[])
{

    u8 i;
    
    for(i=0; i<ITF_RF_SECURITY_PARA_LEN; i++)
    {
        gPara[i] = sPara[i];
    }

    return SUCCESS;
}

