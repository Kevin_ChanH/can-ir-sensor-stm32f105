/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_Interface_Mmy.h
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2018年5月2日
  最近修改   :
  功能描述   : RF_Interface_Mmy.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2018年5月2日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/
#ifndef __RF_INTERFACE_MMY_H__
#define __RF_INTERFACE_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


typedef enum
{
    /* 存储类型 - CANTORF - 起始条数 */
    RF_ITF_MMY_TYPE_CAN_START,         

    /* 存储类型 - RFTOCAN 起始条数 */
    RF_ITF_MMY_TYPE_RF_START = (RF_ITF_MMY_TYPE_CAN_START + MCU_CAN_TO_RF_LOGIC_NUM),

    /* 存储类型 - RF_SECURITY */
    RF_ITF_MMY_TYPE_SECURITY = (RF_ITF_MMY_TYPE_RF_START + MCU_RF_TO_CAN_LOGIC_NUM),
    
    /* 存储最大条数 */
    RF_ITF_MMY_TYPE_MAX,
}RF_ItfMmyTypedef;


#define IS_ITF_CAN_MMY_TYPE(PARAM)              ((((PARAM) >= RF_ITF_MMY_TYPE_CAN_START) &&  \
                                             ((PARAM) <  (RF_ITF_MMY_TYPE_RF_START))))

#define IS_ITF_RF_MMY_TYPE(PARAM)               (((PARAM) >= RF_ITF_MMY_TYPE_RF_START) &&  \
                                             ((PARAM) <  RF_ITF_MMY_TYPE_MAX))

#define IS_ITF_RF_SEC_TYPE(PARAM)               ((PARAM) == RF_ITF_MMY_TYPE_SECURITY)

#define IS_ITF_MMY_TYPE(PARAM)                  (((PARAM) >= RF_ITF_MMY_TYPE_CAN_START) &&  \
                                              ((PARAM) <  RF_ITF_MMY_TYPE_MAX))

// 存储数据架构
typedef struct
{
    u32         MmyStartAddr;               /* Flash起始地址 */
    MmyDataType  MmyDatType;                /* 存储的数据类型 */
    u8          MmyOptParaLen;              /* 操作数据长度 */
    u8          MmyOptParaSize;             /* 操作数据尺寸 */
    uc8         *DefaultRamAddr;            /* 默认数据的操作地址 */
}RF_ItfMmyOptTypedef;

// 存储操作架构
typedef struct
{
    RF_ItfMmyOptTypedef OptMmyPara[RF_ITF_MMY_TYPE_MAX];
}RF_ItfMmyOptGroupTypedef;

// RF 事件
typedef enum
{
    RF_EVENT_CNL = 0,
    RF_EVENT_SCENE,
}RF_ItfRfEventTypedef;

// 接口触发参数
typedef struct
{
    FlagStatus      IsActive;
    RunAddrTypedef  AgAddr;
}RF_ItfTriTypedef;

typedef struct
{
    RunAddrTypedef          ExtAddr;            // 执行地址
    RF_ItfRfEventTypedef    Event;              // 执行事件
    u8                    RepeatNum;            // 重发次数
    u8                    IntervalMin;          // 重发最小间隔时间
    u8                    IntervalMax;          // 重发最大间隔时间
}RF_ItfCanTriExtTypedef;

// RF发送执行参数
typedef struct
{
    FlagStatus             IsActvie;            // 是否激活
    RunAddrTypedef          AgAddr;             // 执行地址
    RF_ItfRfEventTypedef    Event;              // 执行事件
    u8                    RepeatNum;            // 重发次数
    u8                    RepeatCnt;            // 
    u8                    IntervalMin;          // 重发最小间隔时间
    u8                    IntervalMax;          // 重发最大间隔时间
    u8                    CmdBuff[CNL_CMD_LONG_BYTE_LEN];   // 命令存储
    u8                    CmdLen;                           // 命令长度
    Tmm_ID                TimId;                            // 定时器ID
}RF_ItfRfExeTypedef;





#define     RF_ITF_CAN_MAX_NUM          MCU_CAN_TO_RF_LOGIC_NUM
#define     RF_ITF_CAN_CFG_NUM          MCU_CAN_TO_RF_LOGIC_NUM
#define     RF_ITF_CAN_LIMIT_START      1
#define     RF_ITF_CAN_LIMIT_END        RF_ITF_CAN_CFG_NUM
#define     IS_RF_ITF_CAN_ID(PARAM)    ((PARAM) < RF_ITF_CAN_CFG_NUM)

#define     RF_ITF_RF_MAX_NUM           MCU_RF_TO_CAN_LOGIC_NUM
#define     RF_ITF_RF_CFG_NUM           MCU_RF_TO_CAN_LOGIC_NUM
#define     RF_ITF_RF_LIMIT_START       1
#define     RF_ITF_RF_LIMIT_END         RF_ITF_RF_CFG_NUM
#define     IS_RF_ITF_RF_ID(PARAM)     ((PARAM) < RF_ITF_RF_CFG_NUM)



// ITF CAN 存储参数 - 起始
#define     ITF_CAN_MMY_ADDR_START              MCU_ITF_CAN_ADDR            /* 存储的起始地址 */
#define     ITF_CAN_MMY_PARA_LEN                8                         /* 占用8Byte */
#define     ITF_CAN_MMY_DATA_TYPE               MMY_DATA_TYPE_MID           /* 存储数据类型为中等类型 */
#define     ITF_CAN_MMY_CFG_MSG_SIZE            MCU_MMY_CFG_MSG_MID_SIZE     /* 存储最大字节数为32Byte */



// ITF RF 存储参数 - 起始
#define     ITF_RF_MMY_ADDR_START               MCU_ITF_RF_ADDR             /* 存储的起始地址 */
#define     ITF_RF_MMY_PARA_LEN                 5                         /* 占用5Byte */
#define     ITF_RF_MMY_DATA_TYPE                MMY_DATA_TYPE_MID           /* 存储数据类型为中等类型 */
#define     ITF_RF_MMY_CFG_MSG_SIZE             MCU_MMY_CFG_MSG_MID_SIZE     /* 存储最大字节数为32Byte */


/* 安全层使用空间 */
#define     ITF_RF_SECURITY_SIZE                6
// ITF RF SECURITYT 存储参数
#define     ITF_RF_SECURITY_ADDR                MCU_ITF_RF_SEC_ADDR             /* 存储地址 */
#define     ITF_RF_SECURITY_PARA_LEN            ITF_RF_SECURITY_SIZE            /* 占用6Byte */
#define     ITF_RF_SECURITY_DATA_TYPE           MMY_DATA_TYPE_MID               /* 存储数据类型为中等类型 */
#define     ITF_RF_SECURITY_CFG_MSG_SIZE        MCU_MMY_CFG_MSG_MID_SIZE        /* 存储最大字节数为32Byte */


/* ITF CAN 存储的执行参数长度 5Bytes */
#define     ITF_CAN_MMY_EXECUTION_PARA_LEN           5
/* ITF CAN 执行起始下标 3 */
#define     ITF_CAN_MMY_EXECUTION_START_INDEX        3

/* ITF CAN 存储的执行参数长度 2Bytes */
#define     ITF_RF_MMY_EXECUTION_PARA_LEN           2
/* ITF CAN 执行起始下标 3 */
#define     ITF_RF_MMY_EXECUTION_START_INDEX        3

/* RF接口 -RF执行的队列大小 */
#define     ITF_RF_EXT_QUEUE_SIZE                   MCU_ITF_RF_EXT_QUEUE_SIZE

// 无线发送间隔时间 - 最小时间 - 100ms
#define     RF_SEND_REPEAT_MS_MIN                   0x8A
// 无线发送间隔时间 - 最大时间 - 200ms
#define     RF_SEND_REPEAT_MS_MAX                   0x94

// 最大重发7次
#define     RF_SEND_MAX_REPEAT_NUM                  7


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  FlagStatus      ItfMmy_IsMmyType            (RF_ItfMmyTypedef mmyType);
extern  FlagStatus      ItfMmy_IsCanIndex           (u8 tIndex);
extern  FlagStatus      ItfMmy_IsRfIndex            (u8 tIndex);


extern  ErrorStatus     ItfMmy_GetCanIndex          (u8 sIndex, u8 *gIndex);
extern  ErrorStatus     ItfMmy_GetRfIndex           (u8 sIndex, u8 *gIndex);
extern  ErrorStatus     ItfMmy_GetRfSecIndex        (u8 *gIndex);

extern  ErrorStatus     ItfMmy_CanOrganizeInit      (uc8 sIndex);
extern  ErrorStatus     ItfMmy_RfOrganizeInit       (uc8 sIndex);
extern  ErrorStatus     ItfMmy_RfSecOrganizeInit(void);
extern  void            ItfMmy_OrganizeInit         (void);

extern  ErrorStatus     ItfMmy_GetParaFromMMY       (RF_ItfMmyTypedef mmyType, u8 gParaBuff[]);
extern  ErrorStatus     ItfMmy_SetParaToMMY         (RF_ItfMmyTypedef mmyType, uc8 sParaBuff[]);
extern  ErrorStatus     ItfMmy_GetDefaultPara       (RF_ItfMmyTypedef mmyType, u8 gParaBuff[]);
extern  ErrorStatus     ItfMmy_SetDefaultToMMY      (RF_ItfMmyTypedef mmyType);

extern  ErrorStatus     ItfMmy_WriteCanParaToMmy    (RF_ItfMmyTypedef mmyType, u8 sBuff[], u8 sLen);
extern  ErrorStatus     ItfMmy_WriteRfParaToMmy     (RF_ItfMmyTypedef mmyType, u8 sBuff[], u8 sLen);
extern  ErrorStatus     ItfMmy_WriteRfSecParaToMmy(u8 sBuff[], u8 sLen);

extern  ErrorStatus     ItfMmy_GetTriggerPara       (uc8 gParaBuff[], RF_ItfTriTypedef *triPara);
extern  ErrorStatus     ItfMmy_GetCanExtPara        (uc8 gParaBuff[], RF_ItfCanTriExtTypedef *extPara);

extern  ErrorStatus     ItfMmy_GetRfExtPara         (uc8 gParaBuff[], RunAddrTypedef *extAddr);
extern  ErrorStatus     ItfMmy_GetRfSecPara(uc8 sPara[], u8 gPara[]);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RF_INTERFACE_MMY_H__ */
