/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_Interface_Queue.h
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2018年5月8日
  最近修改   :
  功能描述   : RF_Interface_Queue.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2018年5月8日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/
#ifndef __RF_INTERFACE_QUEUE_H__
#define __RF_INTERFACE_QUEUE_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

extern  RF_ItfRfExeTypedef RfExtQueueBuff[ITF_RF_EXT_QUEUE_SIZE];

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus     ItfQueue_ElementClr             (RF_ItfRfExeTypedef *clrExt);
extern  ErrorStatus     ItfQueue_Init                   (void);
extern  ErrorStatus     ItfQueue_GetEmptyIndex          (u8 *gIndex);
extern  ErrorStatus     ItfQueue_GetAddrIndex           (RunAddrTypedef sAddr, u8 *gIndex);
extern  FlagStatus      ItfQueue_CheckAddr              (RunAddrTypedef *cAddr);
extern  FlagStatus      ItfQueue_CheckRepeat            (uc8 rNum, uc8 rCnt);
extern  ErrorStatus     ItfQueue_ElementPoll            (uc8 elmIndex);
extern  ErrorStatus     ItfQueue_Poll                   (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RF_INTERFACE_QUEUE_H__ */
