

/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_Interface_Logic.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2018年5月3日
  最近修改   :
  功能描述   : RF接口逻辑转发层
  函数列表   :
  修改历史   :
  1.日    期   : 2018年5月3日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
RF_ItfTriTypedef CanItfTriAttr[RF_ITF_CAN_CFG_NUM];
RF_ItfTriTypedef RfItfTriAttr[RF_ITF_RF_CFG_NUM];

u8 RfSecBuff[ITF_RF_SECURITY_PARA_LEN];

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
 
/*****************************************************************************
 函 数 名  : ItfLogic_Init
 功能描述  : 初始化转发接口
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void ItfLogic_Init (void)
{
    u8              i;
    u8              canItfMmyBuff[ITF_CAN_MMY_PARA_LEN];
    u8              rfItfMmyBuff[ITF_RF_MMY_PARA_LEN];
    u8              rfSecMmyBuff[ITF_RF_SECURITY_PARA_LEN];
    
    // 初始化存储架构
    ItfMmy_OrganizeInit ();
    // 初始化队列架构
    ItfQueue_Init();

    // 初始化CAN接口触发参数
    for (i = RF_ITF_MMY_TYPE_CAN_START; i < RF_ITF_MMY_TYPE_RF_START; i++)
    {
        // 获取CAN接口的存储参数
        if (ItfMmy_GetParaFromMMY ((RF_ItfMmyTypedef) i, canItfMmyBuff) != SUCCESS)
        {
            // 获取默认配置参数
            ItfMmy_GetDefaultPara ((RF_ItfMmyTypedef) i, canItfMmyBuff);

            // 将默认配置参数写入到FLASH中
            ItfMmy_SetDefaultToMMY ((RF_ItfMmyTypedef) i);
        }

        // 获取CAN接口的触发参数
        ItfMmy_GetTriggerPara (canItfMmyBuff, &CanItfTriAttr[i]);
    }

    // 初始化RF接口触发参数
    for (i = RF_ITF_MMY_TYPE_RF_START; i < RF_ITF_MMY_TYPE_SECURITY; i++)
    {
        // 获取CAN接口的存储参数
        if (ItfMmy_GetParaFromMMY ((RF_ItfMmyTypedef) i, rfItfMmyBuff) != SUCCESS)
        {
            // 获取默认配置参数
            ItfMmy_GetDefaultPara ((RF_ItfMmyTypedef) i, rfItfMmyBuff);

            // 将默认配置参数写入到FLASH中
            ItfMmy_SetDefaultToMMY ((RF_ItfMmyTypedef) i);
        }

        // 获取CAN接口的触发参数
        ItfMmy_GetTriggerPara (rfItfMmyBuff, &RfItfTriAttr[i-RF_ITF_MMY_TYPE_RF_START]);
    }
    
    // 获取RF安全层参数
    if(ItfMmy_GetParaFromMMY(RF_ITF_MMY_TYPE_SECURITY, rfSecMmyBuff) != SUCCESS)
    {
        // 获取默认参数
        ItfMmy_GetDefaultPara(RF_ITF_MMY_TYPE_SECURITY, rfSecMmyBuff);

        // 将默认配置参数写入到FLASH中
        ItfMmy_SetDefaultToMMY(RF_ITF_MMY_TYPE_SECURITY);
    }

    // 获取安全层参数
    ItfMmy_GetRfSecPara(rfSecMmyBuff, RfSecBuff);
}

/*****************************************************************************
 函 数 名  : ItfLogic_GetRfSec
 功能描述  : 获取安全层数据
 输入参数  : u8 secBuff[]  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2019年1月2日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfLogic_GetRfSec(u8 secBuff[])
{
    u8 i;
    
    // 获取安全层数据
    for(i=0; i<ITF_RF_SECURITY_PARA_LEN; i++)
    {
        secBuff[i] = RfSecBuff[i];
    }
    
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfLogic_IsRfSecMatch
 功能描述  : 判断安全层是否匹配
 输入参数  : u8 secBuff[]  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2019年1月2日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus ItfLogic_IsRfSecMatch(u8 secBuff[])
{
    u8 i;
    u8 defSecBuff[ITF_RF_SECURITY_PARA_LEN];
    
    // 判断当前是否匹配
    for(i=0; i<ITF_RF_SECURITY_PARA_LEN; i++)
    {
        if(RfSecBuff[i] != secBuff[i])
        {
            break;
        }
    }
    
    // 判断当前是否匹    配
    if(i >= ITF_RF_SECURITY_PARA_LEN)
    {
        // 返回匹配
        return SET;
    }
    
    // 获取默认参数
    ItfMmy_GetDefaultPara(RF_ITF_MMY_TYPE_SECURITY, defSecBuff);
    // 判断当前是否匹配
    for(i=0; i<ITF_RF_SECURITY_PARA_LEN; i++)
    {
        if(defSecBuff[i] != secBuff[i])
        {
            break;
        }
    }

    // 判断当前是否匹配
    if(i >= ITF_RF_SECURITY_PARA_LEN)
    {
        // 返回匹配
        return SET;
    }

    // 返回不匹配
    return RESET;
}


/*****************************************************************************
 函 数 名  : ItfLogic_TriIsActive
 功能描述  : 判断触发接口是否激活
 输入参数  : RF_ItfTriTypedef * tri  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus ItfLogic_TriIsActive (RF_ItfTriTypedef * tri)
{
    // 判断接口是否激活
    if (tri->IsActive == SET)
    {
        // 判断App是否明确化 且 Group是否不为0xFF
        if ((tri->AgAddr.App != 0x00) && (tri->AgAddr.App != 0xFF) && (tri->AgAddr.Group != 0xFF))
        {
            return SET;
        }
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : ItfLogic_GetExtDelayMs
 功能描述  : 获取执行的延迟时间
 输入参数  : uc8 timByte  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
u16 ItfLogic_GetExtDelayMs (uc8 timByte)
{
    u16             timValue = 0;

    // 判断是否为1ms密度
    if ((timByte & 0xC0) == 0x00)
    {
        timValue            = (u16) (timByte & 0x3F);
    }

    // 判断是否为5ms密度
    else if ((timByte & 0xC0) == 0x40)
    {
        timValue            = (u16) ((timByte & 0x3F) * 5);
    }

    // 判断是否为10ms密度
    else if ((timByte & 0xC0) == 0x80)
    {
        timValue            = (u16) ((timByte & 0x3F) * 10);
    }

    // 判断是否50ms密度
    else 
    {
        timValue            = (u16) ((timByte & 0x3F) * 50);
    }

    return timValue;
}

/*****************************************************************************
 函 数 名  : ItfLogic_LoadRfCmd
 功能描述  : 载入RF命令
 输入参数  : RFSFrame * gRFsFrame  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfLogic_LoadRfCmd (RFSFrame * gRFsFrame)
{
    u8              i   = 0;
    u8              gIndex = 0;
    u8              tmpCmd;
//    u8              sCmdType = 0;

    FrameTypedef    sFrame;
//    CAN_DataBase    sDb;
    AddrAttrTypedef desAddr;
    u8              cnlCmdBuff[CNL_CMD_LONG_BYTE_LEN] =
    {
        0
    };
    u8              cnlCmdLen = 0;
    u8              tmpLevel1;
//    u8              tmpTim1;

    // 判断长度是否不足
    if (gRFsFrame->len < (gIndex + ITF_RF_SECURITY_PARA_LEN))
    {
        return ERROR;
    }

    // 判断安全层是否不匹配
    if(ItfLogic_IsRfSecMatch(&(gRFsFrame->buff[gIndex])) != SET)
    {
        return ERROR;
    }
    
    // 载入安全层长度
    gIndex              += ITF_RF_SECURITY_PARA_LEN;

    // 判断长度是否足够
    if (gRFsFrame->len < (gIndex + 2))
    {
        return ERROR;
    }

    // 判断传输层是否不等于0x11
    if (gRFsFrame->buff[gIndex] != 0x11)
    {
        return ERROR;
    }

    gIndex++;

    // 获取CMD
    tmpCmd              = gRFsFrame->buff[gIndex++];

    // 判断DIR是否为 UP
    if ((tmpCmd & 0x40) != 0x00)
    {
        return ERROR;
    }

    // 判断CNL是否为 CONFIG
    if ((tmpCmd & 0x20) != 0x00)
    {
        return ERROR;
    }

    // 判断OPT是否为 READ
    if ((tmpCmd & 0x10) != 0x00)
    {
        return ERROR;
    }

    // 判断SRC是否为 NONE
    if ((tmpCmd & 0x0C) == 0x00)
    {
    }

    // 判断SRC是否为 MAC
    else if ((tmpCmd & 0x0C) == 0x04)
    {
        return ERROR;
    }

    // 判断SRC是否为 UNIT
    else if ((tmpCmd & 0x0C) == 0x08)
    {
        gIndex              += 2;
    }

    // 判断SRC是否为 APP
    else if ((tmpCmd & 0x0C) == 0x0C)
    {
        gIndex              += 2;
    }
    else 
    {
        return ERROR;
    }

    // 判断DES是否为 NONE
    if ((tmpCmd & 0x03) == 0x00)
    {
        return ERROR;
    }

    // 判断DES是否为 MAC
    else if ((tmpCmd & 0x03) == 0x01)
    {
        return ERROR;
    }

    // 判断DES是否为 UNIT
    else if ((tmpCmd & 0x03) == 0x02)
    {
        return ERROR;
    }

    // 判断DES是否为 APP
    else if ((tmpCmd & 0x03) == 0x03)
    {
        // 载入目的地址为APP
        desAddr.Type        = CMD_ADDR_TYPE_APP;

        // 获取AG地址
        desAddr.AddrBuff[0] = gRFsFrame->buff[gIndex++];
        desAddr.AddrBuff[1] = gRFsFrame->buff[gIndex++];
    }
    else 
    {
        return ERROR;
    }

    // 判断数据内容是否足够
    if (gRFsFrame->len < (gIndex + 1))
    {
        return ERROR;
    }

    // 获取CMD TYPE
    tmpCmd              = gRFsFrame->buff[gIndex++];

    // 判断是否为 停止命令
    if (tmpCmd == RF_REG_CONTROL_STOP_RAMP)
    {
        // 获取STOP的命令
        CnlCmd_GetCnlBuff (CNL_CMD_TYPE_RAMP_STOP, cnlCmdBuff, &cnlCmdLen);
    }

    // 判断是否为 控制命令
    else if (tmpCmd == RF_REG_CONTROL_CONTROL)
    {
        // 判断数据长度是否足够
        if (gRFsFrame->len < (gIndex + CNL_CMD_LONG_BYTE_LEN))
        {
            return ERROR;
        }

        // 获取命令内容
        for (i = 0; i < CNL_CMD_LONG_BYTE_LEN; i++)
        {
            cnlCmdBuff[i]       = gRFsFrame->buff[gIndex++];
        }

        // 获取命令长度
        cnlCmdLen           = CNL_CMD_LONG_BYTE_LEN;
    }

    // 判断是否为 场景命令
    else if (tmpCmd == RF_REG_CONTROL_SCENE)
    {
        if (gRFsFrame->len < (gIndex + 1))
        {
            return ERROR;
        }

        // 获取Level1
        tmpLevel1           = gRFsFrame->buff[gIndex++];

        // 获取绝对控制命令
        CnlCmd_GetAbsCnlBuff (tmpLevel1, cnlCmdBuff, &cnlCmdLen);
    }
    else 
    {
        return ERROR;
    }

    // 载入控制命令 获取控制帧
    if (Frame_GetSingleCnlFrame (desAddr, cnlCmdBuff, cnlCmdLen, &sFrame) == SUCCESS)
    {
        // 匹配RF命令
        ItfLogic_RfCmdMatch (&sFrame);
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfLogic_RfCmdMatch
 功能描述  : RF命令匹配
 输入参数  : FrameTypedef * sFrame  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfLogic_RfCmdMatch (FrameTypedef * sFrame)
{
    u8              i   = 0;

    // 判断否为 向下写控制命令 且 目的地址是AG地址
    if ((sFrame->CmdAttr.Dir == CMD_DIR_DOWN) && (sFrame->CmdAttr.Reg == CMD_REG_CONTROL) &&
         (sFrame->CmdAttr.Opt == CMD_OPT_WIRTE) && (sFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP))
    {
        // 轮询所有RF接口配置
        for (i = 0; i < RF_ITF_RF_CFG_NUM; i++)
        {
            // 判断转换命令是否激活
            if (ItfLogic_TriIsActive (&RfItfTriAttr[i]) == SET)
            {
                // 载入RF命令转换
                ItfLogic_RfCmdConvert (i, sFrame);
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfLogic_RfCmdConvert
 功能描述  : RF命令转换
 输入参数  : u8 tIndex             
             FrameTypedef *sFrame  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfLogic_RfCmdConvert (u8 tIndex, FrameTypedef *sFrame)
{
    u8 gMmyBuff[ITF_RF_MMY_PARA_LEN] =
    {
        0
    };
    RunAddrTypedef  rfExtAddr;
    CAN_DataBase    sDb;
        
    // 判断ID是否不在范围内
    if (IS_RF_ITF_RF_ID (tIndex) != SET)
    {
        return ERROR;
    }

    // 判断APP是否相等
    if(RfItfTriAttr[tIndex].AgAddr.App == sFrame->CmdAttr.DesAddr.AddrBuff[0])
    {
        // 判断是否不为任意Group
        if(RfItfTriAttr[tIndex].AgAddr.Group != 0)
        {
            // 判断Group是否不匹配
            if(RfItfTriAttr[tIndex].AgAddr.Group != sFrame->CmdAttr.DesAddr.AddrBuff[1])
            {
                return ERROR;
            }
        }

        // 判断Group是否非法
        if((sFrame->CmdAttr.DesAddr.AddrBuff[1] == 0x00)
        || (sFrame->CmdAttr.DesAddr.AddrBuff[1] == 0xFF))
        {
            return ERROR;
        }

        // 获取RF接口的参数
        if(ItfMmy_GetParaFromMMY((RF_ItfMmyTypedef)(RF_ITF_MMY_TYPE_RF_START+tIndex), gMmyBuff) != SUCCESS)
        {
            return ERROR;
        }

        // 获取RF的执行地址
        if(ItfMmy_GetRfExtPara(&gMmyBuff[ITF_RF_MMY_EXECUTION_START_INDEX], &rfExtAddr) != SUCCESS)
        {
            return ERROR;
        }

        // 判断目标AG地址是否无效
        if((rfExtAddr.App == 0xFF)
        || (rfExtAddr.Group == 0xFF))
        {
            return ERROR;
        }

        // 判断App是否不是跟随命令的APP -> 载入配置自身的APP
        if(rfExtAddr.App != 0x00)
        {
            sFrame->CmdAttr.DesAddr.AddrBuff[0] = rfExtAddr.App;
        }

        // 判断Group是否不是跟随命令的Group -> 载入配置自身的Group
        if(rfExtAddr.Group != 0x00)
        {
            sFrame->CmdAttr.DesAddr.AddrBuff[1]  = rfExtAddr.Group;
        }

        // 载入AG地址
        rfExtAddr.App = sFrame->CmdAttr.DesAddr.AddrBuff[0];
        rfExtAddr.Group = sFrame->CmdAttr.DesAddr.AddrBuff[1];
        // 载入控制命令到本模块的虚拟地址命令内
        VirtualAddr_VaLoadCmd(rfExtAddr, sFrame->Data, sFrame->DataLen);
        
        // 获取CAN控制帧
        if(Frame_SingleFrameGetCanDb(*sFrame, &sDb) == SUCCESS)
        {
            // 载入控制帧到发送队列
            CAN_SendDbMsg(&sDb);
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfLogic_LoadCanCmd
 功能描述  : CAN接口载入命令
 输入参数  : CAN_DataBase * loadcmd  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfLogic_LoadCanCmd (CAN_DataBase * loadcmd)
{
    FrameTypedef    gFrame;
    u8              i;

    // 获取单个控制帧
    if (Frame_CanDbGetSingleFrame (*loadcmd, &gFrame) == SUCCESS)
    {
        // 目的地址必须为AG地址 且 为下发写入控制命令 数据内容大于0
        if ((gFrame.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP) && (gFrame.CmdAttr.Reg == CMD_REG_CONTROL) &&
             (gFrame.CmdAttr.Dir == CMD_DIR_DOWN) && (gFrame.CmdAttr.Opt == CMD_OPT_WIRTE) && ((gFrame.DataLen > 0) &&
             (gFrame.DataLen <= CNL_CMD_LONG_BYTE_LEN)))
        {
            // 轮询所有CAN接口配置
            for (i = 0; i < RF_ITF_CAN_CFG_NUM; i++)
            {
                // 判断转换命令是否激活
                if (ItfLogic_TriIsActive (&CanItfTriAttr[i]) == SET)
                {
                    // CAN接口转换
                    ItfLogic_CanCmdConvert (i, &gFrame);
                }
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ItfLogic_CanCheckIntervalTim
 功能描述  : 检查间隔时间
 输入参数  : u8 * intervalMax  
             u8 * intervalMin  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfLogic_CanCheckIntervalTim (u8 * intervalMax, u8 * intervalMin)
{
    u16             maxMs = 0;
    u16             minMs = 0;

    // 获取最大毫秒数
    maxMs               = ItfLogic_GetExtDelayMs (*intervalMax);

    // 获取最小毫秒数
    minMs               = ItfLogic_GetExtDelayMs (*intervalMin);

    // 判断间隔是否不符合规则
    if (maxMs <= minMs)
    {
        // 载入默认的最大最小时间
        *intervalMax        = RF_SEND_REPEAT_MS_MAX;
        *intervalMin        = RF_SEND_REPEAT_MS_MIN;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : ItfLogic_CanCmdConvert
 功能描述  : CAN命令转发
 输入参数  : uc8 tIndex             
             FrameTypedef * gFrame  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ItfLogic_CanCmdConvert (uc8 tIndex, FrameTypedef * gFrame)
{
    u8              gMmyBuff[ITF_CAN_MMY_PARA_LEN] =
    {
        0
    };
    RF_ItfCanTriExtTypedef extPara;
    u8              i;
    u8              gIndex = 0;
    RunAddrTypedef  sAddr;

    // 判断ID是否不在范围内
    if (IS_RF_ITF_CAN_ID (tIndex) != SET)
    {
        return ERROR;
    }

    // 判断APP是否匹配
    if (CanItfTriAttr[tIndex].AgAddr.App == gFrame->CmdAttr.DesAddr.AddrBuff[0])
    {
        // 判断Group是否不为任意Group转发
        if (CanItfTriAttr[tIndex].AgAddr.Group != 0x00)
        {
            // 判断Group是否不匹配
            if (CanItfTriAttr[tIndex].AgAddr.Group != gFrame->CmdAttr.DesAddr.AddrBuff[1])
            {
                return ERROR;
            }
        }

        // 判断Group是否非法
        if ((gFrame->CmdAttr.DesAddr.AddrBuff[1] == 0x00) || (gFrame->CmdAttr.DesAddr.AddrBuff[1] == 0xFF))
        {
            return ERROR;
        }

        // 获取CAN接口的存储参数
        if (ItfMmy_GetParaFromMMY ((RF_ItfMmyTypedef)(RF_ITF_MMY_TYPE_CAN_START + tIndex), gMmyBuff) != SUCCESS)
        {
            return ERROR;
        }

        // 获取CAN执行参数
        if (ItfMmy_GetCanExtPara (&gMmyBuff[ITF_CAN_MMY_EXECUTION_START_INDEX], &extPara) != SUCCESS)
        {
            return ERROR;
        }

        // 判断目标AG地址是否无效
        if ((extPara.ExtAddr.App == 0xFF) || (extPara.ExtAddr.Group == 0xFF))
        {
            return ERROR;
        }

        // 判断重发次数是否为0
        if (extPara.RepeatNum == 0)
        {
            return ERROR;
        }

        // 判断APP是否跟随命令的APP
        if (extPara.ExtAddr.App == 0x00)
        {
            // 载入被跟随的APP
            sAddr.App = gFrame->CmdAttr.DesAddr.AddrBuff[0];
        }
        else 
        {
            // 载入固定的APP
            sAddr.App = extPara.ExtAddr.App;
        }

        // 判断Group是否跟随命令的Group
        if (extPara.ExtAddr.Group == 0x00)
        {
            // 载入被跟随的GROUP
            sAddr.Group = gFrame->CmdAttr.DesAddr.AddrBuff[1];
        }
        else 
        {
            // 载入固定的GROUP
            sAddr.Group = extPara.ExtAddr.Group;
        }

        // 先判断该地址是否已经有消息正在发送，并且返回该消息的下标号
        if(ItfQueue_GetAddrIndex(sAddr, &gIndex) != SUCCESS)
        {
            // 获取执行下标 - 并判断是否成功
            if (ItfQueue_GetEmptyIndex (&gIndex) != SUCCESS)
            {
                return ERROR;
            }
        }

        // 设置RF执行
        RfExtQueueBuff[gIndex].IsActvie = SET;

        // 载入AG地址
        RfExtQueueBuff[gIndex].AgAddr.App = sAddr.App;
        RfExtQueueBuff[gIndex].AgAddr.Group = sAddr.Group;
        
        // 载入事件
        RfExtQueueBuff[gIndex].Event = extPara.Event;

        // 载入重发次数
        RfExtQueueBuff[gIndex].RepeatNum = extPara.RepeatNum;
        RfExtQueueBuff[gIndex].RepeatCnt = 0;


        // 载入间隔时间
        RfExtQueueBuff[gIndex].IntervalMax = extPara.IntervalMax;
        RfExtQueueBuff[gIndex].IntervalMin = extPara.IntervalMin;

        // 检查时间 并自动纠正
        ItfLogic_CanCheckIntervalTim (&RfExtQueueBuff[gIndex].IntervalMax, &RfExtQueueBuff[gIndex].IntervalMin);

        // 复制数据内容
        for (i = 0; i < gFrame->DataLen; i++)
        {
            RfExtQueueBuff[gIndex].CmdBuff[i] = gFrame->Data[i];
        }

        // 复制数据长度
        RfExtQueueBuff[gIndex].CmdLen = gFrame->DataLen;

        return SUCCESS;
    }

    return ERROR;
}

