/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_Interface_Logic.h
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2018年5月3日
  最近修改   :
  功能描述   : RF_Interface_Logic.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2018年5月3日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/ 
#ifndef __RF_INTERFACE_LOGIC_H__
#define __RF_INTERFACE_LOGIC_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

extern  RF_ItfTriTypedef CanItfTriAttr[RF_ITF_CAN_CFG_NUM];
extern  RF_ItfTriTypedef RfItfTriAttr[RF_ITF_RF_CFG_NUM];
extern  u8 RfSecBuff[ITF_RF_SECURITY_PARA_LEN];

#ifdef __cplusplus
#if __cplusplus
extern "C"{ 
#endif
#endif /* __cplusplus */

extern  void            ItfLogic_Init                       (void);

extern  ErrorStatus     ItfLogic_GetRfSec                   (u8 secBuff[]);

extern  FlagStatus      ItfLogic_TriIsActive                (RF_ItfTriTypedef *tri);
extern  u16             ItfLogic_GetExtDelayMs              (uc8 timByte);

extern  ErrorStatus     ItfLogic_LoadRfCmd                  (RFSFrame * gRFsFrame);
extern  ErrorStatus     ItfLogic_RfCmdMatch                 (FrameTypedef * sFrame);
extern  ErrorStatus     ItfLogic_RfCmdConvert               (u8 tIndex, FrameTypedef *sFrame);

extern  ErrorStatus     ItfLogic_LoadCanCmd                 (CAN_DataBase *loadcmd);
extern  ErrorStatus     ItfLogic_CanCheckIntervalTim        (u8 *intervalMax, u8 *intervalMin);
extern  ErrorStatus     ItfLogic_CanCmdConvert              (uc8 tIndex, FrameTypedef *gFrame);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RF_INTERFACE_LOGIC_H__ */
