/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Receive.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月13日
  最近修改   :
  功能描述   : 命令接收文档
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月13日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

// CAN 状态参数
extern  CAN_StatParaTypedef     CanStatPara;
/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


ErrorStatus Receive_WriteDownCnlFrameAnalyze(FrameTypedef cnlFrame)
{
    RunAddrTypedef gAppAddr;

    // 判断目的地址是否为 单元地址
    if(cnlFrame.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        // 载入控制 单元地址

        // 暂时还没有 设计到
        return ERROR;
    }
    // 判断目的地址是否为 APP+GROUP
    else if(cnlFrame.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP)
    {
        // 载入App + Group 地址
        gAppAddr.App    = cnlFrame.CmdAttr.DesAddr.AddrBuff[0];
        gAppAddr.Group  = cnlFrame.CmdAttr.DesAddr.AddrBuff[1];

        // 载入控制 APP+GROUP
        VirtualAddr_VaLoadCmd(gAppAddr, cnlFrame.Data, cnlFrame.DataLen);

        // 载入控制 APP+GROUP
        IrsLogic_LoadFormulaTriggetCmd(gAppAddr, cnlFrame.Data, cnlFrame.DataLen);

        // 载入命令到RF中
        // RF_CANToRF(gAppAddr, cnlFrame.Data, cnlFrame.DataLen);
    }
    else
    {
        return ERROR;
    }

    return ERROR;
}


ErrorStatus Receive_TrslFrameAnalyzeData(FrameTypedef gFrame)
{
    u8 gIndex = 0;
    u8 trslReg = 0;
    u8 detailedReg = 0;
    u8 tmpLen = 0;

    // 判断是否还有数据
    if(gFrame.DataLen > gIndex)
    {
        // 获取传输层寄存器
        trslReg = gFrame.Data[gIndex++];

        // 判断是否还有数据
        if(gFrame.DataLen > gIndex)
        {
            // 获取详细寄存器
            detailedReg = gFrame.Data[gIndex++];

            // 传输层 - 状态寄存器
            if(trslReg == CAN_TRSL_REG_STAT)
            {
                // 总线区域状态
                if(detailedReg == CAN_TRSL_REG_STAT_AREA)
                {
                    // 判断是否有其余数据
                    if(gFrame.DataLen > gIndex)
                    {
                        // 获取数据长度
                        tmpLen = gFrame.DataLen - gIndex;

                        // 解析区域状态码
                        return CAN_ReceiveTrslStatPoll(gFrame.CmdAttr ,&(gFrame.Data[gIndex]), tmpLen);
                    }
                }
            }
            // 传输层 - 数据包寄存器
            else if(trslReg == CAN_TRSL_REG_PACKET)
            {
                // 数据包状态
                if(detailedReg == CAN_TRSL_REG_PACKET_STAT)
                {
                    // 判断是否有其余数据
                    if(gFrame.DataLen >= gIndex)
                    {
                        // 获取数据长度
                        tmpLen = gFrame.DataLen - gIndex;

                        // 解析 数据包状态码
                        return CAN_ReceivePacketStatPoll(gFrame.CmdAttr ,&(gFrame.Data[gIndex]), tmpLen);
                    }
                }
                // 数据包RNG
                else if(detailedReg == CAN_TRSL_REG_PACKET_RNG)
                {
                    // 判断是否有其余数据
                    if(gFrame.DataLen >= gIndex)
                    {
                        // 获取数据长度
                        tmpLen = gFrame.DataLen - gIndex;

                        // 解析 数据包的RTS状态
                        return CAN_ReceivePacketRtsPoll(gFrame.CmdAttr ,&(gFrame.Data[gIndex]), tmpLen);
                    }
                }
                else
                {
                }
            }
            // 传输层 - 安全组寄存器
            else if(trslReg == CAN_TRSL_REG_SEC)
            {
                return SUCCESS;
            }
            else
            {
            }
        }
    }

    return ERROR;
}


ErrorStatus Receive_TrslFrameAnalyze(FrameTypedef gFrame)
{
    // 目前 只能支持旧协议
    if(gFrame.CmdAttr.Version != CMD_VERSION_OLD)
    {
        return ERROR;
    }

    // 目前 只能支持控制协议
    if(gFrame.CmdAttr.Reg != CMD_REG_CONTROL)
    {
        return ERROR;
    }

    // 目前源地址仅能支持 单元地址
    if(gFrame.CmdAttr.SrcAddr.Type != CMD_ADDR_TYPE_UNIT)
    {
        return ERROR;
    }

    // 目前目的地址仅能支持 无地址-广播
    if(gFrame.CmdAttr.DesAddr.Type != CMD_ADDR_TYPE_NONE)
    {
        return ERROR;
    }

    return Receive_TrslFrameAnalyzeData(gFrame);
}

ErrorStatus Receive_WriteCnlFrameAnalyze(FrameTypedef cnlFrame)
{
    // 下发控制命令
    if(cnlFrame.CmdAttr.Dir == CMD_DIR_DOWN)
    {
        return Receive_WriteDownCnlFrameAnalyze(cnlFrame);
    }

    return ERROR;
}

ErrorStatus Receive_ReadDownStatFrameAnalyze(FrameTypedef cnlFrame)
{
    AddrAttrTypedef gAddr;

    // 判断目的地址是否为 单元地址
    if(cnlFrame.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        // 载入控制 单元地址

        // 暂时还没有 设计到
        return ERROR;
    }
    // 判断目的地址是否为 APP+GROUP
    else if(cnlFrame.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP)
    {
        // 载入地址类型
        gAddr.Type = CMD_ADDR_TYPE_APP;

        // 载入App + Group 地址
        gAddr.AddrBuff[0] = cnlFrame.CmdAttr.DesAddr.AddrBuff[0];
        gAddr.AddrBuff[1] = cnlFrame.CmdAttr.DesAddr.AddrBuff[1];

        // 获取 APP+GROUP 地址的状态
        StatLogic_ResponseAgStat(gAddr);
    }
    else
    {
        return ERROR;
    }

    return ERROR;
}

ErrorStatus Receive_StatUpdateFrameAnalyze(FrameTypedef cnlFrame)
{
    AddrAttrTypedef      gAddr;
    MtcStatAttrTypedef   mtcStat;

    // 判断目的地址是否为 单元地址
    if(cnlFrame.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        // 载入控制 单元地址

        // 暂时还没有 设计到
        return ERROR;
    }
    // 判断目的地址是否为 APP+GROUP
    else if(cnlFrame.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP)
    {
        if((cnlFrame.CmdAttr.DesAddr.AddrBuff[0] != 0)
           && (cnlFrame.CmdAttr.DesAddr.AddrBuff[1] != 0)
           && (cnlFrame.DataLen >= 2))
        {
            // 载入地址类型
            gAddr.Type = CMD_ADDR_TYPE_APP;

            // 载入App + Group 地址
            gAddr.AddrBuff[0] = cnlFrame.CmdAttr.DesAddr.AddrBuff[0];
            gAddr.AddrBuff[1] = cnlFrame.CmdAttr.DesAddr.AddrBuff[1];

            // 载入更新的状态
            mtcStat.Version     = cnlFrame.Data[0];
            mtcStat.StableLevel = cnlFrame.Data[1];

            // 获取 APP+GROUP 地址的状态
            StatLogic_UpdateOneAgStat(gAddr, mtcStat);
        }
    }
    else
    {
        return ERROR;
    }

    return ERROR;
}


ErrorStatus Receive_ReadStatFrameAnalyze(FrameTypedef cnlFrame)
{
    // 向下读取状态命令
    if(cnlFrame.CmdAttr.Dir == CMD_DIR_DOWN)
    {
        Receive_ReadDownStatFrameAnalyze(cnlFrame);
    }
    // 向上反馈状态命令
    else
    {
        Receive_StatUpdateFrameAnalyze(cnlFrame);
    }

    return ERROR;
}


ErrorStatus Receive_CnlRegFrameAnalyze(FrameTypedef cnlFrame)
{
    // 控制命令
    if(cnlFrame.CmdAttr.Opt == CMD_OPT_WIRTE)
    {
        return Receive_WriteCnlFrameAnalyze(cnlFrame);
    }
    // 状态命令
    else
    {
        return Receive_ReadStatFrameAnalyze(cnlFrame);
    }
}

ErrorStatus Receive_CheckReadCfgDesAddr(AddrAttrTypedef addrPort)
{
    u16 tmpUnitAddr;

    // 目的地址 - 广播
    if(addrPort.Type == CMD_ADDR_TYPE_NONE)
    {
        return SUCCESS;
    }
    // 目的地址 - SN
    else if(addrPort.Type == CMD_ADDR_TYPE_MAC)
    {
        if(Addr_IsSnMatch(addrPort.AddrBuff) == SET)
        {
            return SUCCESS;
        }
    }
    // 目的地址 - 单元地址
    else if(addrPort.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpUnitAddr = (u16)(addrPort.AddrBuff[0] << 8);
        tmpUnitAddr |= (u16)(addrPort.AddrBuff[1] & 0xFF);
        tmpUnitAddr >>= 7;

        if(Addr_IsUnitMatch(tmpUnitAddr) == SET)
        {
            return SUCCESS;
        }
    }
    else
    {
    }

    return ERROR;
}

ErrorStatus Receive_ResponseAddr(FrameCfgTypedef *gCfgFrame)
{
    CAN_PacketBase rAddrMsg;
    u8 sIndex = 0;
    u32 partakeRegs = 0;
    u16 gTmpUnit = 0;
    u32 sId = 0;
    u8 tmpByte = 0;
    u8 i = 0;

    // 获取CMD字节
    tmpByte = (FRAME_CMD_BIT_FRAME | FRAME_CMD_BIT_DIR | FRAME_CMD_BIT_REG | FRAME_CMD_BIT_OPT | FRAME_CMD_SRC_MAC);

    // 载入SN地址
    Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));
    // 偏移数据下标
    sIndex += SN_MMY_PARA_LEN;

    if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;

        for(i=0; i<SYSTEM_SN_APP_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;

        for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_NONE)
    {
        tmpByte |= FRAME_CMD_DES_NONE;
    }
    else
    {
        return ERROR;
    }

    sId |= (u32)(tmpByte << 16);
    // 载入数据包ID
    rAddrMsg.PacketID = sId;

    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.TypeReg);
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.DetailedReg);

    // 获取所有的地址信息
    if(gCfgFrame->RegAttr.DetailedReg == CFG_ADDR_REG_DETAILED_ALL)
    {
        // 载入参与地址
        partakeRegs |= 0x07;

        rAddrMsg.PacketBuff[sIndex++] = (u8)(partakeRegs >> 24);
        rAddrMsg.PacketBuff[sIndex++] = (u8)(partakeRegs >> 16);
        rAddrMsg.PacketBuff[sIndex++] = (u8)(partakeRegs >> 8);
        rAddrMsg.PacketBuff[sIndex++] = (u8)(partakeRegs & 0xFF);

        // 载入SN地址长度
        rAddrMsg.PacketBuff[sIndex++] = (u8)(SN_MMY_PARA_LEN);

        // 载入SN地址
        Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));

        // 偏移数据下标
        sIndex += SN_MMY_PARA_LEN;


        // 载入UNIT地址长度
        rAddrMsg.PacketBuff[sIndex++] = (u8)(UNIT_MMY_PARA_LEN);

        // 获取Unit地址
        Addr_GetUnitAddr(&gTmpUnit);

        // 载入Unit地址
        rAddrMsg.PacketBuff[sIndex++] = (u8)(gTmpUnit >> 8);
        rAddrMsg.PacketBuff[sIndex++] = (u8)(gTmpUnit & 0xFF);

        // 获取版本号长度
        rAddrMsg.PacketBuff[sIndex++] = sizeof(FirmwareVersion);

        // 获取版本号内容
        Firmware_GetVersion(&(rAddrMsg.PacketBuff[sIndex]));

        // 偏移数据下标
        sIndex += sizeof(FirmwareVersion);
    }
    // 获取SN地址
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_ADDR_REG_DETAILED_SN)
    {
        // 载入SN地址长度
        rAddrMsg.PacketBuff[sIndex++] = (u8)(SN_MMY_PARA_LEN);

        // 载入SN地址
        Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));

        // 偏移数据下标
        sIndex += SN_MMY_PARA_LEN;
    }
    // 获取单元地址
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_ADDR_REG_DETAILED_UNIT)
    {
        // 载入UNIT地址长度
        rAddrMsg.PacketBuff[sIndex++] = (u8)(UNIT_MMY_PARA_LEN);

        // 获取Unit地址
        Addr_GetUnitAddr(&gTmpUnit);

        // 载入Unit地址
        rAddrMsg.PacketBuff[sIndex++] = (u8)(gTmpUnit >> 8);
        rAddrMsg.PacketBuff[sIndex++] = (u8)(gTmpUnit & 0xFF);
    }
    else
    {
        return ERROR;
    }

    // 载入数据长度
    rAddrMsg.PacketLen = sIndex;

    // 反馈读取信息
    return CAN_PacketSend(&CanStatPara, &rAddrMsg);
}


ErrorStatus Receive_DownReadAddrFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    // 检查目的地址是否正确
    if(Receive_CheckReadCfgDesAddr(gCfgFrame->CmdAttr.DesAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取地址信息必须通过MAC地址或者单元地址寻址或者广播寻址才可以
    if((gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_NONE))
    {
    }
    else
    {
        return ERROR;
    }

    return Receive_ResponseAddr(gCfgFrame);
}

ErrorStatus Receive_DownReadFirmwareFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    // 检查目的地址是否正确
    if(Receive_CheckReadCfgDesAddr(gCfgFrame->CmdAttr.DesAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取输出寄存器 必须先配置好SN地址
    if(Addr_IsSnNotCfg() == SET)
    {
        return ERROR;
    }

    // 固件信息必须通过MAC地址或者单元地址寻址才可以
    if((gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    // 固件信息必须使用自身的源地址读取
    if((gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    return FirmwareResponse(gCfgFrame);
}

ErrorStatus Receive_ResponseInput(FrameCfgTypedef *gCfgFrame)
{
    CAN_PacketBase rAddrMsg;
    u8 sIndex = 0;
    u8 gIndex = 0;
    u32 sId = 0;
    u16 tmpValue = 0;
    u8 tmpByte = 0;
    u8 i = 0;
    IrsCfgRegTypedef cfgRegAttr;
    IllCfgRegTypedef cfgIllRegAttr;

    // 获取CMD字节
    tmpByte = (FRAME_CMD_BIT_FRAME | FRAME_CMD_BIT_DIR | FRAME_CMD_BIT_REG | FRAME_CMD_BIT_OPT | FRAME_CMD_SRC_MAC);

    // 载入SN地址
    Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));
    // 偏移数据下标
    sIndex += SN_MMY_PARA_LEN;

    if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;
        for(i=0; i<SYSTEM_SN_APP_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;
        for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    sId |= (u32)(tmpByte << 16);
    // 载入数据包ID
    rAddrMsg.PacketID = sId;

    // 载入对应的寄存器 - 输入寄存器&人体红外输入寄存器
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.TypeReg);
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.DetailedReg);

    
    // 获取人体红外输入寄存器
    if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_IR_SENSOR)
    {
        // 判断数据是否足够 - 2字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 2))
        {
            // 获取红外ID字节
            cfgRegAttr.CfgIrId = gCfgFrame->CfgAttrBuff[gIndex++];
            // 获取红外参数字节
            cfgRegAttr.CfgPara = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的红外ID字节
        rAddrMsg.PacketBuff[sIndex++] = cfgRegAttr.CfgIrId;
        // 载入需要反馈的红外参数字节
        rAddrMsg.PacketBuff[sIndex++] = cfgRegAttr.CfgPara;

        // 判断红外ID是否大于0
        if(cfgRegAttr.CfgIrId > 0)
        {
            // 内部红外ID号减一, 从0开始算起           
            cfgRegAttr.CfgIrId -= 1;

            // 判断ID是否非法
            if(IS_IRS_ID(cfgRegAttr.CfgIrId) != SET)
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }
        
        // 判断是否为读取 人体红外捕获参数
        if(cfgRegAttr.CfgPara == CFG_INPUT_REG_IR_SENSOR_SUB_CATCH)
        {
            // 载入Catch数据长度
            rAddrMsg.PacketBuff[sIndex++] = IRS_CATCH_MMY_PARA_LEN;

            // 判断获取配置数据是否成功
            if(IrsMmy_GetParaFromMMY(cfgRegAttr.CfgIrId, IRS_MMY_TYPE_CATCH, &(rAddrMsg.PacketBuff[sIndex])) != SUCCESS)
            {
                return ERROR;
            }

            // 加上获取到的数据长度
            sIndex += IRS_CATCH_MMY_PARA_LEN;
        }
        // 判断是否为读取 人体红外逻辑参数
        else if(cfgRegAttr.CfgPara == CFG_INPUT_REG_IR_SENSOR_SUB_LOGIC)
        {
            // 判断数据是否足够 - 1字节
            if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
            {
                // 获取逻辑参数下的 - 状态逻辑
                tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

                // 载入逻辑参数下的逻辑状态
                rAddrMsg.PacketBuff[sIndex++] = tmpByte;
                
                // 判断红外逻辑事件是否非法
                if(IrsMmy_IsMmyLogicEvent(tmpByte) != SET)
                {
                    return ERROR;
                }

                // 载入逻辑事件的配置长度
                rAddrMsg.PacketBuff[sIndex++] = IRS_LOGIC_EVENT_PARA_LEN;

                 // 判断获取配置数据是否成功
                if(IrsMmy_GetParaFromMMY(cfgRegAttr.CfgIrId, (IrsMmyType)tmpByte, &(rAddrMsg.PacketBuff[sIndex])) != SUCCESS)
                {
                    return ERROR;
                }

                // 加上获取到的数据长度
                sIndex += IRS_LOGIC_EVENT_PARA_LEN;
            }
            else
            {
                return ERROR;
            }
        }
        // 判断是否为读取 人体红外复合公式
        else if(cfgRegAttr.CfgPara == CFG_INPUT_REG_IR_SENSOR_SUB_FORMULA)
        {
            // 判断数据是否足够 - 1字节
            if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
            {
                // 获取复合公式下的 - 公式下标
                tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

                // 载入复合公式下的 - 公式下标
                rAddrMsg.PacketBuff[sIndex++] = tmpByte;
                
                // 判断复合公式下的 - 公式下标是否非法
                if(IrsMmy_IsFormulaIndex(tmpByte) != SET)
                {
                    return ERROR;
                }

                // 载入复合公式的配置长度
                rAddrMsg.PacketBuff[sIndex++] = IRS_FORMULA_MMY_PARA_LEN;

                // 将下标调整到存储复合公式的对应下标
                tmpByte += (ISR_MMY_TYPE_FORMULA_START - 1);
                
                 // 判断获取配置数据是否成功
                if(IrsMmy_GetParaFromMMY(cfgRegAttr.CfgIrId, (IrsMmyType)tmpByte, &(rAddrMsg.PacketBuff[sIndex])) != SUCCESS)
                {
                    return ERROR;
                }

                // 加上获取到的数据长度
                sIndex += IRS_FORMULA_MMY_PARA_LEN;
            }
            else
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }
    }
    // 获取照度传感器输入寄存器
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_ILLUMINANCE)
    {
        // 判断数据是否足够 - 2字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 2))
        {
            // 获取照度ID字节
            cfgIllRegAttr.CfgIllId = gCfgFrame->CfgAttrBuff[gIndex++];
            // 获取照度参数字节
            cfgIllRegAttr.CfgPara = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的照度ID字节
        rAddrMsg.PacketBuff[sIndex++] = cfgIllRegAttr.CfgIllId;
        // 载入需要反馈的照度参数字节
        rAddrMsg.PacketBuff[sIndex++] = cfgIllRegAttr.CfgPara;

        // 判断照度ID是否不等于1
        if(cfgIllRegAttr.CfgIllId != 1)
        {
            return ERROR;
        }

        // 判断参数是否为照度传感器的触发参数
        if(cfgIllRegAttr.CfgPara == CFG_INPUT_REG_ILLUMINANCE_SUB_TRIGGET)
        {
            // 判断数据是否足够 - 1字节
            if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
            {
                // 获取触发公式下的 - 公式下标
                tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

                // 载入触发公式下的 - 公式下标
                rAddrMsg.PacketBuff[sIndex++] = tmpByte;

                // 判断触发公式下标是否在范围内
                if(IS_BEAM_FORMULA_TRIGGET_ID(tmpByte) != SET)
                {
                    return ERROR;
                }

                // 载入触发公式的配置长度
                rAddrMsg.PacketBuff[sIndex++] = BEAM_FORMULA_MMY_PARA_LEN;

                // 将下标调整到存储触发公式的对应下标
                tmpByte = ((tmpByte - 1) + BEAM_MMY_TYPE_FORMULA_START);

                // 获取触发公式
                if(BeamMmy_GetParaFromMMY((BeamMmyType)tmpByte, &(rAddrMsg.PacketBuff[sIndex])) != SUCCESS)
                {
                    return ERROR;
                }

                // 加上获取到的数据长度
                sIndex += BEAM_FORMULA_MMY_PARA_LEN;
            }
        }
        // 判断参数是否为获取照度值参数
        else if(cfgIllRegAttr.CfgPara == CFG_INPUT_REG_ILLUMINANCE_SUB_GET_VALUE)
        {
            // 载入照度值的数据长度
            rAddrMsg.PacketBuff[sIndex++] = BEAM_ILLUMINANCE_VALUE_SIZE;
            
            // 获取照度值
            tmpValue = BeamApp_GetIlluminanceVaule();

            // 载入照度值
            rAddrMsg.PacketBuff[sIndex++] = (u8)(tmpValue >> 8);
            rAddrMsg.PacketBuff[sIndex++] = (u8)(tmpValue & 0xFF);
        }
        else
        {
            return ERROR;
        }
    }
    else
    {
        return ERROR;
    }

    // 载入数据长度
    rAddrMsg.PacketLen = sIndex;

    // 反馈读取信息
    return CAN_PacketSend(&CanStatPara, &rAddrMsg);
}


ErrorStatus Receive_DownReadInputFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    // 检查目的地址是否正确
    if(Receive_CheckReadCfgDesAddr(gCfgFrame->CmdAttr.DesAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取输出寄存器 必须先配置好SN地址
    if(Addr_IsSnNotCfg() == SET)
    {
        return ERROR;
    }

    // 输出寄存器必须通过MAC地址或者单元地址寻址才可以
    if((gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    // 输出寄存器必须使用自身的源地址读取
    if((gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    return Receive_ResponseInput(gCfgFrame);
}

ErrorStatus Receive_ResponseInterface(FrameCfgTypedef *gCfgFrame)
{
    CAN_PacketBase rAddrMsg;
    u8 sIndex = 0;
    u8 gIndex = 0;
    u32 sId = 0;
    u8 tmpByte = 0;
    u8 i = 0;
    u8 readId = 0;
    u8 gMmyIndex = 0;
    
    // 获取CMD字节
    tmpByte = (FRAME_CMD_BIT_FRAME | FRAME_CMD_BIT_DIR | FRAME_CMD_BIT_REG | FRAME_CMD_BIT_OPT | FRAME_CMD_SRC_MAC);

    // 载入SN地址
    Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));
    // 偏移数据下标
    sIndex += SN_MMY_PARA_LEN;

    if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;
        for(i=0; i<SYSTEM_SN_APP_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;
        for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    sId |= (u32)(tmpByte << 16);
    // 载入数据包ID
    rAddrMsg.PacketID = sId;

    // 载入对应的寄存器 - 接口寄存器 & 对应接口(CAN/RF)寄存器
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.TypeReg);
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.DetailedReg);

    
    // CAN接口寄存器
    if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_CAN)
    {
        // 判断数据是否足够 - 1字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
        {
            // 获取ID
            readId = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的ID字节
        rAddrMsg.PacketBuff[sIndex++] = readId;


        // 判断读取的ID是否大于0
        if(readId > 0)
        {
            // 内部读取ID号-1, 从0开始算起           
            readId -= 1;

            // 判断ID是否非法
            if(IS_RF_ITF_CAN_ID(readId) != SET)
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }

        // 获取CAN在存储上的INDEX
        if(ItfMmy_GetCanIndex(readId, &gMmyIndex) != SUCCESS)
        {
            return ERROR;
        }

        // 载入CAN的存储数据长度
        rAddrMsg.PacketBuff[sIndex++] = ITF_CAN_MMY_PARA_LEN;

        if(ItfMmy_GetParaFromMMY((RF_ItfMmyTypedef)gMmyIndex, &(rAddrMsg.PacketBuff[sIndex])) != SUCCESS)
        {
            return ERROR;
        }

        // 加上获取到的数据长度
        sIndex += ITF_CAN_MMY_PARA_LEN;
    }
    // 获取照度传感器输入寄存器
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_RF)
    {
        // 判断数据是否足够 - 1字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
        {
            // 获取ID
            readId = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的ID字节
        rAddrMsg.PacketBuff[sIndex++] = readId;


        // 判断读取的ID是否大于0
        if(readId > 0)
        {
            // 内部读取ID号-1, 从0开始算起           
            readId -= 1;

            // 判断ID是否非法
            if(IS_RF_ITF_RF_ID(readId) != SET)
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }

        // 获取RF在存储上的INDEX
        if(ItfMmy_GetRfIndex(readId, &gMmyIndex) != SUCCESS)
        {
            return ERROR;
        }

        // 载入RF的存储数据长度
        rAddrMsg.PacketBuff[sIndex++] = ITF_RF_MMY_PARA_LEN;

        if(ItfMmy_GetParaFromMMY((RF_ItfMmyTypedef)gMmyIndex, &(rAddrMsg.PacketBuff[sIndex])) != SUCCESS)
        {
            return ERROR;
        }

        // 加上获取到的数据长度
        sIndex += ITF_RF_MMY_PARA_LEN;
    }
    // 获取RF安全层数据
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_RF_SEC)
    {
        // 载入安全层长度
        rAddrMsg.PacketBuff[sIndex++] = ITF_RF_SECURITY_PARA_LEN;

        // 获取安全层数据
        if(ItfMmy_GetParaFromMMY(RF_ITF_MMY_TYPE_SECURITY, &(rAddrMsg.PacketBuff[sIndex])) != SUCCESS)
        {
            return ERROR;
        }

        // 加上安全层的数据长度
        sIndex += ITF_RF_SECURITY_PARA_LEN;
    }
    else
    {
        return ERROR;
    }

    // 载入数据长度
    rAddrMsg.PacketLen = sIndex;

    // 反馈读取信息
    return CAN_PacketSend(&CanStatPara, &rAddrMsg);
}

ErrorStatus Receive_DownReadInterfaceFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    // 检查目的地址是否正确
    if(Receive_CheckReadCfgDesAddr(gCfgFrame->CmdAttr.DesAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取输出寄存器 必须先配置好SN地址
    if(Addr_IsSnNotCfg() == SET)
    {
        return ERROR;
    }

    // 输出寄存器必须通过MAC地址或者单元地址寻址才可以
    if((gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    // 输出寄存器必须使用自身的源地址读取
    if((gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    return Receive_ResponseInterface(gCfgFrame);
}


ErrorStatus Receive_DownReadCfgFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gCfgFrame));

    // 判断是否为配置地址信息
    if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_ADDR)
    {
        return Receive_DownReadAddrFrameAnalyze(gCfgFrame);
    }
    // 判断是否为获取固件信息
    else if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_FIRMWARE)
    {
        return  Receive_DownReadFirmwareFrameAnalyze(gCfgFrame);
    }
    // 判断是否为 - 输入模块寄存器
    else if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_INPUT)
    {
        return Receive_DownReadInputFrameAnalyze(gCfgFrame);
    }
    // 判断是否为 - 接口信息寄存器
    else if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_INTERFACE)
    {
        return Receive_DownReadInterfaceFrameAnalyze(gCfgFrame);
    }
    else
    {
    }

    return ERROR;
}

ErrorStatus Receive_WirteAddr(FrameCfgTypedef *gCfgFrame)
{
    CAN_PacketBase rAddrMsg;
    u8 sIndex = 0;
    u8 gIndex = 0;
    u8 tmpByte = 0;
    u8 i = 0;
    FlagStatus wirteStat = RESET;
    u32 sId = 0;

    // 配置SN
    if(gCfgFrame->RegAttr.DetailedReg == CFG_ADDR_REG_DETAILED_SN)
    {
        // 判断是否能够获取到数据
        if(gCfgFrame->CfgBuffLen > gIndex)
        {
            // 获取数据
            tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

            // 判断长度字节不为 SN长度字节
            if(SN_MMY_PARA_LEN != tmpByte)
            {
                return ERROR;
            }

            // 判断是否能够获取到数据
            if(gCfgFrame->CfgBuffLen >= (gIndex + SN_MMY_PARA_LEN))
            {
                // 未配置 或 目标地址为SN
                if((Addr_IsSnNotCfg() == SET)
                   || (gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC))
                {
                    // 配置SN
                    if(Addr_SetSnAddr(&(gCfgFrame->CfgAttrBuff[gIndex])) == SUCCESS)
                    {
                        // 配置成功
                        wirteStat = SET;
                    }
                    else
                    {
                        // 配置失败
                        wirteStat = RESET;
                    }
                }
                else
                {
                    return ERROR;
                }
            }
            else
            {
                return ERROR;
            }
        }
    }
    // 配置单元地址
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_ADDR_REG_DETAILED_UNIT)
    {
        // 配置单元地址必须用SN寻址
        if(gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
        {
            // 判断是否能够获取到数据
            if(gCfgFrame->CfgBuffLen > gIndex)
            {
                // 获取数据
                tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

                // 判断长度字节不为 SN长度
                if(UNIT_MMY_PARA_LEN != tmpByte)
                {
                    return ERROR;
                }

                // 判断是否能够获取到数据
                if(gCfgFrame->CfgBuffLen >= (gIndex + UNIT_MMY_PARA_LEN))
                {
                    // 配置UNIT
                    if(Addr_SetUnitAddr(&(gCfgFrame->CfgAttrBuff[gIndex])) == SUCCESS)
                    {
                        // 配置成功
                        wirteStat = SET;
                    }
                    else
                    {
                        // 配置失败
                        wirteStat = RESET;
                    }
                }
                else
                {
                    return ERROR;
                }
            }
        }
        else
        {
            return ERROR;
        }
    }
    else
    {
        return ERROR;
    }

    tmpByte = 0;

    tmpByte = (FRAME_CMD_BIT_FRAME | FRAME_CMD_BIT_DIR | FRAME_CMD_BIT_REG | FRAME_CMD_SRC_MAC);


    // 载入SN地址
    Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));

    // 偏移数据下标
    sIndex += SN_MMY_PARA_LEN;

    // 判断目标地址是否为MAC地址
    if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;

        // 载入目标地址 - SN地址
        for(i=0; i<SN_MMY_PARA_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    // 判断目标地址是否为UNIT地址
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;

        // 载入目标地址 - 单元地址
        for(i=0; i<UNIT_MMY_PARA_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    sId |= (u32)(tmpByte << 16);

    // 载入数据包ID
    rAddrMsg.PacketID = sId;


    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.TypeReg);
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.DetailedReg);

    rAddrMsg.PacketBuff[sIndex++] = 0;

    if(wirteStat == SET)
    {
        rAddrMsg.PacketBuff[sIndex++] = 0;
    }
    else
    {
        rAddrMsg.PacketBuff[sIndex++] = 1;
    }

    // 载入数据长度
    rAddrMsg.PacketLen = sIndex;

    // 读取地址帧
    return CAN_PacketSend(&CanStatPara, &rAddrMsg);
}


ErrorStatus Receive_DownWirteAddrFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gCfgFrame));

    // 检查读配置的目的地址是否合法
    if(Receive_CheckReadCfgDesAddr(gCfgFrame->CmdAttr.DesAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 检查目的地址类型是否正确 - 配置必须使用MAC地址
    if(gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
    {
    }
    else
    {
        return ERROR;
    }

    // 检查源地址类型是否正确 - 配置必须使用MAC地址或者单元地址
    if((gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    return Receive_WirteAddr(gCfgFrame);
}

ErrorStatus Receive_WirteInput(FrameCfgTypedef *gCfgFrame)
{
    CAN_PacketBase rAddrMsg;
    u8 sIndex = 0;
    u8 gIndex = 0;
    u32 sId = 0;
    u8 tmpByte = 0;
    u8 i = 0;

    IrsCfgRegTypedef cfgRegAttr;
    IllCfgRegTypedef cfgIllRegAttr;
    FlagStatus resStat = RESET;

    // 获取CMD字节
    tmpByte = (FRAME_CMD_BIT_FRAME | FRAME_CMD_BIT_DIR | FRAME_CMD_BIT_REG | FRAME_CMD_SRC_MAC);

    // 载入SN地址
    Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));
    // 偏移数据下标
    sIndex += SN_MMY_PARA_LEN;

    if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;
        for(i=0; i<SYSTEM_SN_APP_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;
        for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    sId |= (u32)(tmpByte << 16);
    // 载入数据包ID
    rAddrMsg.PacketID = sId;



    // 载入对应的寄存器 - 输入寄存器&人体红外输入寄存器
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.TypeReg);
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.DetailedReg);


    // 获取人体红外输入寄存器
    if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_IR_SENSOR)
    {
        // 判断数据是否足够 - 2字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 2))
        {
            // 获取红外ID字节
            cfgRegAttr.CfgIrId = gCfgFrame->CfgAttrBuff[gIndex++];
            // 获取红外参数字节
            cfgRegAttr.CfgPara = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的红外ID字节
        rAddrMsg.PacketBuff[sIndex++] = cfgRegAttr.CfgIrId;
        // 载入需要反馈的红外参数字节
        rAddrMsg.PacketBuff[sIndex++] = cfgRegAttr.CfgPara;

        // 判断红外ID是否大于0
        if(cfgRegAttr.CfgIrId > 0)
        {
            // 内部红外ID号减一, 从0开始算起           
            cfgRegAttr.CfgIrId -= 1;

            // 判断ID是否非法
            if(IS_IRS_ID(cfgRegAttr.CfgIrId) != SET)
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }
        
        // 判断是否为读取 人体红外捕获参数
        if(cfgRegAttr.CfgPara == CFG_INPUT_REG_IR_SENSOR_SUB_CATCH)
        {
            // 写入捕获数据到FLASH中
            if(IrsMmy_WriteCatchDataToFlash(cfgRegAttr.CfgIrId, &(gCfgFrame->CfgAttrBuff[gIndex]), (gCfgFrame->CfgBuffLen - gIndex)) == SUCCESS)
            {
                // 返回成功
                resStat = SET;
            }
            else
            {
                // 返回失败
                resStat = RESET;
            }
        }
        // 判断是否为读取 人体红外逻辑参数
        else if(cfgRegAttr.CfgPara == CFG_INPUT_REG_IR_SENSOR_SUB_LOGIC)
        {
            // 判断数据是否足够 - 1字节
            if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
            {
                // 获取逻辑参数下的 - 状态逻辑
                tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

                // 载入逻辑参数下的逻辑状态
                rAddrMsg.PacketBuff[sIndex++] = tmpByte;
                
                // 判断红外逻辑事件是否非法
                if(IrsMmy_IsMmyLogicEvent(tmpByte) != SET)
                {
                    // 返回失败
                    resStat = RESET;
                }
                else
                {
                    // 写入逻辑数据到FLASH中
                    if(IrsMmy_WriteLogicDataToFlash(cfgRegAttr.CfgIrId, tmpByte, &(gCfgFrame->CfgAttrBuff[gIndex]), (gCfgFrame->CfgBuffLen - gIndex)) == SUCCESS)
                    {
                        // 返回成功
                        resStat = SET;
                    }
                    else
                    {
                        // 返回失败
                        resStat = RESET;
                    }
                }
            }
            else
            {
                return ERROR;
            }
        }
        // 判断是否为读取 人体红外复合公式
        else if(cfgRegAttr.CfgPara == CFG_INPUT_REG_IR_SENSOR_SUB_FORMULA)
        {
            // 判断数据是否足够 - 1字节
            if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
            {
                // 获取复合公式下的 - 公式下标
                tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

                // 载入复合公式下的 - 公式下标
                rAddrMsg.PacketBuff[sIndex++] = tmpByte;
                
                // 判断复合公式下的 - 公式下标是否非法
                if(IrsMmy_IsFormulaIndex(tmpByte) != SET)
                {
                    // 返回失败
                    resStat = RESET;
                }
                else
                {
                    // 写入复合公式到FLASH中
                    if(IrsMmy_WriteFormulaDataToFlash(cfgRegAttr.CfgIrId, tmpByte, &(gCfgFrame->CfgAttrBuff[gIndex]), (gCfgFrame->CfgBuffLen - gIndex)) == SUCCESS)
                    {
                        // 返回成功
                        resStat = SET;
                    }
                    else
                    {
                        // 返回失败
                        resStat = RESET;
                    }
                }
            }
            else
            {
                return ERROR;
            }
        }
        else
        {
            // 返回失败
            resStat = RESET;
        }
    }
    // 获取照度传感器输入寄存器
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_ILLUMINANCE)
    {
        // 判断数据是否足够 - 2字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 2))
        {
            // 获取照度ID字节
            cfgIllRegAttr.CfgIllId = gCfgFrame->CfgAttrBuff[gIndex++];
            // 获取照度参数字节
            cfgIllRegAttr.CfgPara = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的照度ID字节
        rAddrMsg.PacketBuff[sIndex++] = cfgIllRegAttr.CfgIllId;
        // 载入需要反馈的照度参数字节
        rAddrMsg.PacketBuff[sIndex++] = cfgIllRegAttr.CfgPara;

        // 判断照度ID是否不等于1
        if(cfgIllRegAttr.CfgIllId != 1)
        {
            return ERROR;
        }

        // 判断参数是否为照度传感器的触发参数
        if(cfgIllRegAttr.CfgPara == CFG_INPUT_REG_ILLUMINANCE_SUB_TRIGGET)
        {
            // 判断数据是否足够 - 1字节
            if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
            {
                // 获取触发公式下的 - 公式下标
                tmpByte = gCfgFrame->CfgAttrBuff[gIndex++];

                // 载入触发公式下的 - 公式下标
                rAddrMsg.PacketBuff[sIndex++] = tmpByte;

                // 判断触发公式下标是否在范围内
                if(IS_BEAM_FORMULA_TRIGGET_ID(tmpByte) != SET)
                {
                    // 返回失败
                    resStat = RESET;
                }
                else
                {
                    // 将下标调整到存储触发公式的对应下标
                    tmpByte = ((tmpByte - 1) + BEAM_MMY_TYPE_FORMULA_START);

                    // 获取触发公式
                    if(BeamMmy_WriteFormulaParaToMmy((BeamMmyType)tmpByte, &(gCfgFrame->CfgAttrBuff[gIndex]), (gCfgFrame->CfgBuffLen - gIndex)) == SUCCESS)
                    {
                        // 返回成功
                        resStat = SET;
                    }
                    else 
                    {
                        // 返回失败
                        resStat = RESET;
                    }
                }
            }
        }
        else
        {
            return ERROR;
        }
    }
    else
    {
        return ERROR;
    }    

    rAddrMsg.PacketBuff[sIndex++] = 0x00;
    if(resStat == SET)
    {
        rAddrMsg.PacketBuff[sIndex++] = 0x00;
    }
    else
    {
        rAddrMsg.PacketBuff[sIndex++] = 0x01;
    }

    // 载入数据长度
    rAddrMsg.PacketLen = sIndex;

    // 反馈读取信息
    return CAN_PacketSend(&CanStatPara, &rAddrMsg);
}


ErrorStatus Receive_DownWirteInputFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    // 检查目的地址是否正确
    if(Receive_CheckReadCfgDesAddr(gCfgFrame->CmdAttr.DesAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取输出寄存器 必须先配置好SN地址
    if(Addr_IsSnNotCfg() == SET)
    {
        return ERROR;
    }

    // 输出寄存器必须通过MAC地址或者单元地址寻址才可以
    if((gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    // 输出寄存器必须通过MAC地址或者单元地址寻址才可以
    if((gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    return Receive_WirteInput(gCfgFrame);
}

ErrorStatus Receive_WirteInterface(FrameCfgTypedef *gCfgFrame)
{
    CAN_PacketBase rAddrMsg;
    u8 sIndex = 0;
    u8 gIndex = 0;
    u32 sId = 0;
    u8 tmpByte = 0;
    u8 i = 0;
    u8 writeId = 0;
    u8 sMmyIndex = 0;
    
    FlagStatus resStat = RESET;

    // 获取CMD字节
    tmpByte = (FRAME_CMD_BIT_FRAME | FRAME_CMD_BIT_DIR | FRAME_CMD_BIT_REG | FRAME_CMD_SRC_MAC);

    // 载入SN地址
    Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));
    // 偏移数据下标
    sIndex += SN_MMY_PARA_LEN;

    if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;
        for(i=0; i<SYSTEM_SN_APP_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;
        for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    sId |= (u32)(tmpByte << 16);
    // 载入数据包ID
    rAddrMsg.PacketID = sId;



    // 载入对应的寄存器 - 接口寄存器&对应的(CAN/RF)寄存器
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.TypeReg);
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.DetailedReg);


    // 配置CAN接口寄存器
    if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_CAN)
    {
        // 判断数据是否足够 - 1字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
        {
            writeId = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的写入ID
        rAddrMsg.PacketBuff[sIndex++] = writeId;

        // 判断红外ID是否大于0
        if(writeId > 0)
        {
            // 内部写入ID号减一, 从0开始算起           
            writeId -= 1;

            // 判断ID是否非法
            if(IS_RF_ITF_CAN_ID(writeId) != SET)
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }

        // 获取CAN在存储上的INDEX
        if(ItfMmy_GetCanIndex(writeId, &sMmyIndex) != SUCCESS)
        {
            return ERROR;
        }
        
        // 写入捕获数据到FLASH中
        if(ItfMmy_WriteCanParaToMmy((RF_ItfMmyTypedef)sMmyIndex, &(gCfgFrame->CfgAttrBuff[gIndex]), (gCfgFrame->CfgBuffLen - gIndex)) == SUCCESS)
        {
            // 返回成功
            resStat = SET;
        }
        else
        {
            // 返回失败
            resStat = RESET;
        }

    }
    // 配置RF接口寄存器
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_RF)
    {
        // 判断数据是否足够 - 1字节
        if(gCfgFrame->CfgBuffLen >= (gIndex + 1))
        {
            writeId = gCfgFrame->CfgAttrBuff[gIndex++];
        }
        else
        {
            return ERROR;
        }

        // 载入需要反馈的写入ID
        rAddrMsg.PacketBuff[sIndex++] = writeId;

        // 判断红外ID是否大于0
        if(writeId > 0)
        {
            // 内部写入ID号减一, 从0开始算起           
            writeId -= 1;

            // 判断ID是否非法
            if(IS_RF_ITF_RF_ID(writeId) != SET)
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }

        // 获取RF在存储上的INDEX
        if(ItfMmy_GetRfIndex(writeId, &sMmyIndex) != SUCCESS)
        {
            return ERROR;
        }
        
        // 写入捕获数据到FLASH中
        if(ItfMmy_WriteRfParaToMmy((RF_ItfMmyTypedef)sMmyIndex, &(gCfgFrame->CfgAttrBuff[gIndex]), (gCfgFrame->CfgBuffLen - gIndex)) == SUCCESS)
        {
            // 返回成功
            resStat = SET;
        }
        else
        {
            // 返回失败
            resStat = RESET;
        }
    }
    // 配置RF安全层接口寄存器
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_RF_SEC)
    {
        // 写入安全层参数到FLASH中
        if(ItfMmy_WriteRfSecParaToMmy(&(gCfgFrame->CfgAttrBuff[gIndex]), (gCfgFrame->CfgBuffLen - gIndex)) == SUCCESS)
        {
            // 返回成功
            resStat = SET;
        }
        else
        {
            // 返回失败
            resStat = RESET;
        }
    }
    else
    {
        return ERROR;
    }    

    rAddrMsg.PacketBuff[sIndex++] = 0x00;
    if(resStat == SET)
    {
        rAddrMsg.PacketBuff[sIndex++] = 0x00;
    }
    else
    {
        rAddrMsg.PacketBuff[sIndex++] = 0x01;
    }

    // 载入数据长度
    rAddrMsg.PacketLen = sIndex;

    // 反馈读取信息
    return CAN_PacketSend(&CanStatPara, &rAddrMsg);
}


ErrorStatus Receive_DownWirteInterfaceFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    // 检查目的地址是否正确
    if(Receive_CheckReadCfgDesAddr(gCfgFrame->CmdAttr.DesAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取输出寄存器 必须先配置好SN地址
    if(Addr_IsSnNotCfg() == SET)
    {
        return ERROR;
    }

    // 输出寄存器必须通过MAC地址或者单元地址寻址才可以
    if((gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    // 输出寄存器必须通过MAC地址或者单元地址寻址才可以
    if((gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
       || (gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT))
    {
    }
    else
    {
        return ERROR;
    }

    return Receive_WirteInterface(gCfgFrame);
}

ErrorStatus Receive_DownWirteCfgFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gCfgFrame));

    // 判断是否为配置地址信息
    if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_ADDR)
    {
        return Receive_DownWirteAddrFrameAnalyze(gCfgFrame);
    }
    // 判断是否为配置调光器的AG地址
    else if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_INPUT)
    {
        return Receive_DownWirteInputFrameAnalyze(gCfgFrame);
    }
    // 判断是否为 - 接口信息寄存器
    else if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_INTERFACE)
    {
        return Receive_DownWirteInterfaceFrameAnalyze(gCfgFrame);
    }
//    // 判断是否为 - 场景寄存器
//    else if(gCfgFrame->RegAttr.TypeReg == FRAME_CFG_REG_TYPE_SCENE)
//    {
//        return Receive_DownWirteSceneFrameAnalyze(gCfgFrame);
//    }
    else
    {
    }

    return ERROR;
}


ErrorStatus Receive_DownCfgFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gCfgFrame));

    // 向下读操作
    if(gCfgFrame->CmdAttr.Opt == CMD_OPT_READ)
    {
        Receive_DownReadCfgFrameAnalyze(gCfgFrame);
    }
    // 向下写操作
    else if(gCfgFrame->CmdAttr.Opt == CMD_OPT_WIRTE)
    {
        Receive_DownWirteCfgFrameAnalyze(gCfgFrame);
    }
    else
    {
    }

    return ERROR;
}


ErrorStatus Receive_CfgFrameAnalyze(FrameCfgTypedef *gCfgFrame)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gCfgFrame));

    // 判断帧类型是否不为新的配置帧
    if(gCfgFrame->CmdAttr.Version != CMD_VERSION_NEW)
    {
        return ERROR;
    }

    // 判断寄存器是否不为配置寄存器
    if(gCfgFrame->CmdAttr.Reg != CMD_REG_CONFIG)
    {
        return ERROR;
    }

    // 判断是否为向下配置帧
    if(gCfgFrame->CmdAttr.Dir == CMD_DIR_DOWN)
    {
        // 解析向下配置帧
        return Receive_DownCfgFrameAnalyze(gCfgFrame);
    }
    // 向上配置帧
    else
    {
        // 解析向上配置帧无需解析
    }

    return ERROR;
}


ErrorStatus Receive_AppFrameAnalyze(FrameTypedef cnlFrame)
{
    FrameCfgTypedef gCfgFrame;

    // 控制命令
    if(cnlFrame.CmdAttr.Reg == CMD_REG_CONTROL)
    {
        return Receive_CnlRegFrameAnalyze(cnlFrame);
    }
    // 配置命令
    else
    {
        // 获取控制帧
        if(Frame_FrameGetCfgFrame(cnlFrame, &gCfgFrame) == SUCCESS)
        {
            return Receive_CfgFrameAnalyze(&gCfgFrame);
        }
    }

    return ERROR;
}


ErrorStatus Receive_CanDbPoll(CAN_DataBase rxDb)
{
    FrameTypedef gFrame;
    u32 rxFrameId = 0;

    // 获取帧ID
    rxFrameId = rxDb.FrameID;

    // 判断是否为大数据包子帧
    if((rxFrameId & CAN_FRAME_TYPE_PACKET) == CAN_FRAME_TYPE_PACKET)
    {
        return  ERROR;
    }

    // 传输层识别位 - 传输层数据
    if((rxFrameId & CAN_FRAME_TYPE_TRSL) == CAN_FRAME_TYPE_TRSL)
    {
        // 将CAN 接收数据 转换为应用数据
        if(Frame_CanDbGetSingleFrame(rxDb, &gFrame) == SUCCESS)
        {
            // 解析传输层
            Receive_TrslFrameAnalyze(gFrame);
        }
    }
    // 应用数据帧
    else
    {
        // 将CAN 接收数据 转换为应用数据
        if(Frame_CanDbGetSingleFrame(rxDb, &gFrame) == SUCCESS)
        {
            // 解析控制帧
            Receive_AppFrameAnalyze(gFrame);

            // 载入控制命令
            ItfLogic_LoadCanCmd(&rxDb);
        }
    } 

    return SUCCESS;
}

ErrorStatus Receive_CanPacketPoll(CAN_PacketBase rxPacket)
{
    FrameCfgTypedef gCfgFrame;

    // 获取配置帧
    if(Frame_PacketGetCfgFrame(rxPacket, &gCfgFrame) == SUCCESS)
    {
        return Receive_CfgFrameAnalyze(&gCfgFrame);
    }

    return ERROR;
}


/*****************************************************************************
 函 数 名  : Receive_Poll
 功能描述  : 接收处理函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Receive_Poll(void)
{
    CAN_DataBase    rxDB;
    CAN_PacketBase  gPacket;

    // 获取CAN接收队列数据
    if(CAN_QueuePull(&(CanStatPara.DbAtttr.RecDB), &rxDB) == SUCCESS)
    {
        // 通讯灯闪烁一下
        LedIndicator_BlinkHoldSet(LED_ID_CAN);

        // 执行 CAN基础数据接收解析
        Receive_CanDbPoll(rxDB);
    }

    // 获取CAN接收数据包
    if(CAN_PacketRec(&(CanStatPara), &gPacket) == SUCCESS)
    {
        // 通讯灯闪烁一下
        LedIndicator_BlinkHoldSet(LED_ID_CAN);

        // 接收数据包解析
        Receive_CanPacketPoll(gPacket);
    }
}




