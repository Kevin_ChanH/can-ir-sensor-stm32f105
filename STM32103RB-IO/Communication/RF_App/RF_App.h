/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_App.h
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月21日
  最近修改   :
  功能描述   : RF_App.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月21日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/
#ifndef __RF_APP_H__
#define __RF_APP_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef struct
{
    FlagStatus Active;
    u8  Level1;
    u8  Time1;
    u8  RepeatCnt;
    RunAddrTypedef DesAddr;
} RFSendCmdAttr;

// 无线重发3次
#define     RF_SEND_REPEAT_NUM              3

// 无线发送间隔时间 - 最小时间
#define     RF_SEND_REPEAT_TICK_MIN         10000
// 无线发送间隔时间 - 最大时间
#define     RF_SEND_REPEAT_TICK_MAX         25000



/* RF 寄存器标识 */
// 控制寄存器
#define RF_REG_CONTROL_CONTROL              0x01
// 监控寄存器
#define RF_REG_CONTROL_MONITOR              0x03
// 实际寄存器
#define RF_REG_CONTROL_REAL                 0x02
// 停止调光寄存器
#define RF_REG_CONTROL_STOP_RAMP            0x04
// 场景寄存器
#define RF_REG_CONTROL_SCENE                0x05


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus         RfApp_LoadElementToSend         (RF_ItfRfExeTypedef *lElement);

// extern  ErrorStatus         RF_CmdInit                      (void);
// extern  ErrorStatus         RF_LoadCmd                      (RunAddrTypedef irsAddr, u8 sLevel);

// extern  ErrorStatus         RF_CANToRF                      (RunAddrTypedef gAddr, uc8 *cmdBuff, uc8 cmdLen);

// extern  ErrorStatus         RF_GetRFcmd                     (RunAddrTypedef irsAddr, u8 sLevel, u8 sTim1, u8 sBuff[], u8 *sLen);

// extern  ErrorStatus         RF_CmdTxPoll                    (void);

extern  ErrorStatus         RF_CmdPoll                      (void);
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RF_APP_H__ */
