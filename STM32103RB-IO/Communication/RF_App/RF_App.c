/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : RF_App.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月21日
  最近修改   :
  功能描述   : RF应用实例
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月21日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"
/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

// 只针对一个AG地址
//RFSendCmdAttr RFSendCmd;
//

//
// RF APP 转换CMD
//#define RF_APP_SEND_CMD_TYPE                RF_REG_CONTROL_SCENE
//
// 继电器1 APP
//#define RF_CAN_TO_RF_APP_RELAY1             0x39
// 继电器2 APP
//#define RF_CAN_TO_RF_APP_RELAY2             0x38
// 调光器1 APP
//#define RF_CAN_TO_RF_APP_DIMMER1            0x3A
// 窗帘1 APP
//#define RF_CAN_TO_RF_APP_CURTAIN1           0x3C
// 场景1 APP
//#define RF_CAN_TO_RF_SCENE                  0xCA
//
// 是否转换场景
//#define RF_CONGIG_CAN_TO_RF_SCENE_ENABLE     1
//#define RF_CONGIG_CAN_TO_RF_SCENE_DISABLE    0
//
// 转换场景开关
//#define RF_CONFIG_CAN_TO_RF_SCENE           RF_CONGIG_CAN_TO_RF_SCENE_ENABLE

// 默认的安全层
//uc8 SecurityBuffDefault[SECURITY_DATA_LEN] = 
//{
//    0x00, 0x00, 0x00, 0x01, 0x10, 0x21
//};


// RF_315的发射功率表
CC1101_TransmitPowerStruct TransmitPowerAttr[RF_SEND_MAX_REPEAT_NUM] = 
{
    CC1101_TRANSMIT_POWER_NORMAL,           // +8.5dB
    CC1101_TRANSMIT_POWER_AVERAGE,          // +0.1dB
    CC1101_TRANSMIT_POWER_MINOR,            // -9.9dB
    CC1101_TRANSMIT_POWER_MID,              // +5.0dB
    CC1101_TRANSMIT_POWER_FLAT,             // -5.5dB
    CC1101_TRANSMIT_POWER_HIGHT,            // +10.6dB
    CC1101_TRANSMIT_POWER_LOW,              // -20.0dB
};


/*****************************************************************************
 函 数 名  : RfApp_LoadElementToSend
 功能描述  : 载入待发送的元素 -> 将其转换为发送命令
                 并发送
 输入参数  : RF_ItfRfExeTypedef *lElement  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年5月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RfApp_LoadElementToSend(RF_ItfRfExeTypedef *lElement)
{
    u8 sendControlBuff[CC1101_TX_BUFF_SIZE];
    RFSFrame sendControlFrame = {0,
                               0,
                               CC1101_TX_BUFF_SIZE,
                               (u8 *)0,
                               CC1101_TRANSMIT_POWER_NORMAL,
                              };
    u16 tmpUnitAddr = SystemUnitDefaultValue;
    u8 sIndex = 0;
    u8 i;
    
    // 指向发送Buff
    sendControlFrame.buff = sendControlBuff;

    // 载入安全层数据
    ItfLogic_GetRfSec(&sendControlFrame.buff[sIndex]);
    // 载入安全层长度
    sIndex += ITF_RF_SECURITY_PARA_LEN;

    
    // 载入传输层
    sendControlFrame.buff[sIndex++] = 0x11;

    // 载入CMD
    sendControlFrame.buff[sIndex++] = 0x0B;

    
    // 获取单元地址
    Addr_GetUnitAddr(&tmpUnitAddr);    
    // 载入 源地址 - UNIT地址
    sendControlFrame.buff[sIndex++] = (u8)(tmpUnitAddr >> 1);
    sendControlFrame.buff[sIndex++] = (u8)((tmpUnitAddr << 7) & 0xFF);
        
    // 载入 目的地址 - AG地址
    sendControlFrame.buff[sIndex++] = lElement->AgAddr.App;
    sendControlFrame.buff[sIndex++] = lElement->AgAddr.Group;

    // 判断转发事件是否为控制事件
    if(lElement->Event == RF_EVENT_CNL)
    {
        // 6字节长码控制命令不做识别 -> 一律当做控制命令转发
        if(lElement->CmdLen == CNL_CMD_LONG_BYTE_LEN)
        {
            // 载入控制命令类型
            sendControlFrame.buff[sIndex++] = RF_REG_CONTROL_CONTROL;

            // 循环载入长命令
            for(i=0; i<CNL_CMD_LONG_BYTE_LEN; i++)
            {
                sendControlFrame.buff[sIndex++] = lElement->CmdBuff[i];
            }
        }
        // 2字节短码控制命令需要做命令识别
        else if(lElement->CmdLen == CNL_CMD_SHORT_BYTE_LEN)
        {
            // 判断T1是否非法
            if(VirtualAddr_DimmerChkT1(lElement->CmdBuff[1]) != SUCCESS)
            {
                return ERROR;
            }

            // 检查T1是否为STOP命令
            if(VirtualAddr_DimmerT1IsStop(lElement->CmdBuff[1]) == SET)
            {
                // 载入命令类型 - 停止调光
                sendControlFrame.buff[sIndex++] = RF_REG_CONTROL_STOP_RAMP;
            }
            // 检查T1是否为进入自动模式
            else if(VirtualAddr_DimmerT1IsEnterStopMode(lElement->CmdBuff[1]) == SET)
            {
                return ERROR;
            }
            // 短码自动补齐为长码命令发送
            else
            {
                // 载入命令类型 
                sendControlFrame.buff[sIndex++] = RF_REG_CONTROL_CONTROL;
                
                // 载入L1
                sendControlFrame.buff[sIndex++] = lElement->CmdBuff[0];
                // 载入L2
                sendControlFrame.buff[sIndex++] = 0;

                // 载入T1
                sendControlFrame.buff[sIndex++] = lElement->CmdBuff[1];
                
                // 载入T2
                sendControlFrame.buff[sIndex++] = 0xFF;
                sendControlFrame.buff[sIndex++] = 0xFF;

                // 载入T3
                sendControlFrame.buff[sIndex++] = 0;
            }
        }
        else 
        {
            return ERROR;
        }
    }
    // 判断转换事件是否为场景事件
    else if(lElement->Event == RF_EVENT_SCENE)
    {
        // 载入场景事件
        sendControlFrame.buff[sIndex++] = RF_REG_CONTROL_SCENE;

        // 载入场景号
        sendControlFrame.buff[sIndex++] = lElement->CmdBuff[0];
    }
    else
    {
        return ERROR;
    }
    
    // 载入数据长度
    sendControlFrame.len = sIndex;

    // 载入发送速率
    sendControlFrame.trmitPower = TransmitPowerAttr[(lElement->RepeatCnt)&RF_SEND_MAX_REPEAT_NUM];

    // 将命令载入到RF的发送队列
    return RFQueue_Push(&CC1101TxQ, &sendControlFrame);
}


// 初始化发送CMD
//ErrorStatus RF_CmdInit(void)
//{
//    RFSendCmd.Active = RESET;
//    RFSendCmd.DesAddr.App = 0xFF;
//    RFSendCmd.DesAddr.Group = 0xFF;
//    RFSendCmd.Level1 = 0;
//    RFSendCmd.Time1 = 0;
//    RFSendCmd.RepeatCnt = 0;
//
//    return SUCCESS;
//}

// RF载入命令
//ErrorStatus RF_LoadCmd(RunAddrTypedef irsAddr, u8 sLevel)
//{
//    u8 i;
//    
//    // 遍历整个红外地址
//    for(i=0; i<IRS_CFG_USER_NUM; i++)
//    {
//        // 判断地址是否匹配
//        if((irsAddr.App) == (IrsLogicAttr[i].IrsAddr.App)
//           &&(irsAddr.Group) == (IrsLogicAttr[i].IrsAddr.Group))
//        {
//            break;
//        }
//    }
//
//    // 判断地址是否不匹配
//    if(i == IRS_CFG_USER_NUM)
//    {
//        return ERROR;
//    }
//
//    if((sLevel == IRS_LOGIC_STAT_AE)
//    || (sLevel == IRS_LOGIC_STAT_E) 
//    || (sLevel == IRS_LOGIC_STAT_AO)
//    || (sLevel == IRS_LOGIC_STAT_O))
//    {
//        /*
//        if(sLevel == IRS_LOGIC_STAT_AE)
//        {
//            sLevel = 0;
//        }
//        else if(sLevel == IRS_LOGIC_STAT_AO)
//        {
//            sLevel = 0xFF;
//        }
//        else
//        {
//            return ERROR;
//        }
//        */
//    }
//    else
//    {
//        return ERROR;
//    }
//
//    // 发送命令设置为激活
//    RFSendCmd.Active = SET;
//    RFSendCmd.DesAddr.App = 0x37;
//    RFSendCmd.DesAddr.Group = 0x01;
//    RFSendCmd.DesAddr.App = irsAddr.App;
//    RFSendCmd.DesAddr.Group = irsAddr.Group;
//    RFSendCmd.Level1 = sLevel;
//    RFSendCmd.Time1 = 0;
//    RFSendCmd.RepeatCnt = RF_SEND_REPEAT_NUM;
//
//    return SUCCESS;
//}

// CAN转RF 协议
//ErrorStatus RF_CANToRF(RunAddrTypedef gAddr, uc8 *cmdBuff, uc8 cmdLen)
//{
//    u8 sendControlBuff[CC1101_TX_BUFF_SIZE];
//    RFSFrame sendControlFrame = {0,
//                               0,
//                               CC1101_TX_BUFF_SIZE,
//                               (u8 *)0,
//                               CC1101_TRANSMIT_POWER_NORMAL,
//                              };
//    u16 tmpUnitAddr = SystemUnitDefaultValue;
//    u8 sIndex = 0;
//    u8 i;
//    u8 sCmdType = 0;
//    
//    // 指向发送Buff
//    sendControlFrame.buff = sendControlBuff;
//    
//    // 判断APP 是否是 允许转发的APP   Relay1/Realy2/Dimmer1/Curtain1
//    if((gAddr.App == RF_CAN_TO_RF_APP_RELAY1)
//    || (gAddr.App == RF_CAN_TO_RF_APP_RELAY2)
//    || (gAddr.App == RF_CAN_TO_RF_APP_DIMMER1)
//    || (gAddr.App == RF_CAN_TO_RF_APP_CURTAIN1))
//    {
//        // 载入控制命令字
//        sCmdType = RF_REG_CONTROL_CONTROL;
//    }
//    // 判断配置是否允许转发场景
//    #if (RF_CONFIG_CAN_TO_RF_SCENE == RF_CONGIG_CAN_TO_RF_SCENE_ENABLE)
//    // 判断APP 是否是 允许转发的场景
//    else if(gAddr.App == RF_CAN_TO_RF_SCENE)
//    {
//        // 载入场景命令字
//        sCmdType = RF_REG_CONTROL_SCENE;
//    }
//    #endif
//    else
//    {
//        return ERROR;
//    }
//
//    
//    
//    // 载入安全层
//    for(i=0; i<SECURITY_DATA_LEN; i++)
//    {
//        sendControlFrame.buff[sIndex++] = SecurityBuffDefault[i];
//    }
//
//    // 载入安全层
//    sendControlFrame.buff[sIndex++] = 0x11;
//
//    // 载入CMD
//    sendControlFrame.buff[sIndex++] = 0x0B;
//
//    
//    // 获取单元地址
//    Addr_GetUnitAddr(&tmpUnitAddr);    
//    // 载入 源地址 - UNIT地址
//    sendControlFrame.buff[sIndex++] = (u8)(tmpUnitAddr >> 1);
//    sendControlFrame.buff[sIndex++] = (u8)((tmpUnitAddr << 7) & 0xFF);
//        
//    // 载入 目的地址 - AG地址
//    sendControlFrame.buff[sIndex++] = gAddr.App;
//    sendControlFrame.buff[sIndex++] = gAddr.Group;
//
//    // 判断数据长度是否为 长数据类型
//    if(cmdLen == CNL_CMD_LONG_BYTE_LEN)
//    {
//        // 载入命令类型 
//        sendControlFrame.buff[sIndex++] = sCmdType;
//        
//        // 循环载入长命令
//        for(i=0; i<CNL_CMD_LONG_BYTE_LEN; i++)
//        {
//            sendControlFrame.buff[sIndex++] = cmdBuff[i];
//        }
//    }
//    // 判断数据长度是否为 短数据类型
//    else if(cmdLen == CNL_CMD_SHORT_BYTE_LEN)
//    {
//        if(VirtualAddr_DimmerChkT1(cmdBuff[1]) != SUCCESS)
//        {
//            return ERROR;
//        }
//
//        // 检查T1是否为STOP命令
//        if(VirtualAddr_DimmerT1IsStop(cmdBuff[1]) == SET)
//        {
//            // 载入命令类型 - 停止调光
//            sendControlFrame.buff[sIndex++] = RF_REG_CONTROL_STOP_RAMP;
//        }
//        // 检查T1是否为进入自动模式
//        else if(VirtualAddr_DimmerT1IsEnterStopMode(cmdBuff[1]) == SET)
//        {
//            return ERROR;
//        }
//        else
//        {
//            // 载入命令类型 
//            sendControlFrame.buff[sIndex++] = sCmdType;
//            
//            // 载入L1
//            sendControlFrame.buff[sIndex++] = cmdBuff[0];
//            // 载入L2
//            sendControlFrame.buff[sIndex++] = 0;
//
//            // 载入T1
//            sendControlFrame.buff[sIndex++] = cmdBuff[1];
//            
//            // 载入T2
//            sendControlFrame.buff[sIndex++] = 0xFF;
//            sendControlFrame.buff[sIndex++] = 0xFF;
//
//            // 载入T3
//            sendControlFrame.buff[sIndex++] = 0;
//        }
//    }
//    else
//    {
//        return ERROR;
//    }
//
//    // 载入数据长度
//    sendControlFrame.len = sIndex;
//
//    // 将命令载入到RF的发送队列
//    return RFQueue_Push(&CC1101TxQ, &sendControlFrame);
//}

// 获取RF命令
//ErrorStatus RF_GetRFcmd(RunAddrTypedef irsAddr, u8 sLevel, u8 sTim1, u8 sBuff[], u8 *sLen)
//{
//    u8 i = 0;
//    u8 sIndex = 0;
//    u16 tmpUnitAddr = SystemUnitDefaultValue;    
//
//    // 载入安全层
//    for(i=0; i<SECURITY_DATA_LEN; i++)
//    {
//        sBuff[sIndex++] = SecurityBuffDefault[i];
//    }
//
//    // 载入安全层
//    sBuff[sIndex++] = 0x11;
//
//    // 载入CMD
//    sBuff[sIndex++] = 0x0B;
//
//    // 获取单元地址
//    Addr_GetUnitAddr(&tmpUnitAddr);    
//    // 载入 源地址 - UNIT地址
//    sBuff[sIndex++] = (u8)(tmpUnitAddr >> 1);
//    sBuff[sIndex++] = (u8)((tmpUnitAddr << 7) & 0xFF);
//    
//    // 载入 目的地址 - AG地址
//    sBuff[sIndex++] = irsAddr.App;
//    sBuff[sIndex++] = irsAddr.Group;
//
//    // 载入命令类型 
//    sBuff[sIndex++] = RF_APP_SEND_CMD_TYPE;
//
//    // 载入L1
//    sBuff[sIndex++] = sLevel;
//    // 载入L2
//    sBuff[sIndex++] = 0;
//
//    // 载入T1
//    sBuff[sIndex++] = sTim1;
//
//    // 载入T2
//    sBuff[sIndex++] = 0xFF;
//    sBuff[sIndex++] = 0xFF;
//
//    // 载入T3
//    sBuff[sIndex++] = 0;
//
//    // 载入命令长度
//    *sLen = sIndex;
//
//    return SUCCESS;
//}

// RF 发送轮询
//ErrorStatus RF_CmdTxPoll(void)
//{
//    u8 sendControlBuff[CC1101_TX_BUFF_SIZE];
//    RFSFrame sendControlFrame = {0,
//                               0,
//                               CC1101_TX_BUFF_SIZE,
//                               (u8 *)0,
//                               CC1101_TRANSMIT_POWER_NORMAL,
//                              };
//    u32 reLoadTick = 0;
//    u8 gLen;
//
//    // 指向发送Buff
//    sendControlFrame.buff = sendControlBuff;
//    
//    // 判断定时是否完毕
//    if(Tmm_IsON(TMM_RF_APP_SEND) != SET)
//    {
//        // 判断是否激活
//        if(RFSendCmd.Active == SET)
//        {
//            // 判断是否有重发次数
//            if(RFSendCmd.RepeatCnt > 0)
//            {
//                // 获取命令
//                RF_GetRFcmd(RFSendCmd.DesAddr, RFSendCmd.Level1, RFSendCmd.Time1,   \
//                sendControlFrame.buff, &gLen);
//
//                // 载入数据长度
//                sendControlFrame.len = gLen;
//
//                if(RFSendCmd.RepeatCnt >= 3)
//                {
//                    // 高分贝发送
//                    sendControlFrame.trmitPower = CC1101_TRANSMIT_POWER_HIGHT;
//                }
//                else if(RFSendCmd.RepeatCnt == 2)
//                {
//                    // 中分贝发送
//                    sendControlFrame.trmitPower = CC1101_TRANSMIT_POWER_NORMAL;
//                }
//                else if(RFSendCmd.RepeatCnt == 1)
//                {
//                    // 低分贝发送
//                    sendControlFrame.trmitPower = CC1101_TRANSMIT_POWER_AVERAGE;
//                }
//                else
//                {
//                    RFSendCmd.Active = RESET;
//                    return ERROR;
//                }
//            
//                // 将命令载入到
//                RFQueue_Push(&CC1101TxQ, &sendControlFrame);
//
//                // 重发次数-1
//                RFSendCmd.RepeatCnt--;    
//
//                // 重发次数大于0
//                if(RFSendCmd.RepeatCnt > 0)
//                {
//                    // 获取随机数
//                    reLoadTick = Ramdom_GetRandom(RF_SEND_REPEAT_TICK_MAX, RF_SEND_REPEAT_TICK_MIN);
//
//                    // 载入随机时间
//                    Tmm_LoadBaseTick (TMM_RF_APP_SEND, reLoadTick);
//                }
//                else
//                {
//                    // 发送完成 - 
//                    RFSendCmd.Active = RESET;
//                }
//            }
//            else
//            {
//                RFSendCmd.Active = RESET;
//            }
//        }
//    }
//
//    return SUCCESS;
//}

// RF 接收载入命令
//ErrorStatus RF_ReceiveCmdLoad(RFSFrame *gRFsFrame)
//{
//    u8 i = 0;
//    u8 gIndex = 0;
//    u8 tmpCmd;
//    u8 sCmdType = 0;
//
//    FrameTypedef sFrame;
//    CAN_DataBase    sDb;
//    AddrAttrTypedef desAddr;
//    u8 cnlCmdBuff[CNL_CMD_LONG_BYTE_LEN] = {0};
//    u8 cnlCmdLen = 0;
//    u8 tmpLevel1;
//    u8 tmpTim1;
//
//    // 判断长度是否不足
//    if(gRFsFrame->len < (gIndex + SECURITY_DATA_LEN))
//    {
//        return ERROR;
//    }
//    
//    // 判断安全层
//    for(i=0; i<SECURITY_DATA_LEN; i++)
//    {
//        // 判断长度是否为 不匹配
//        if(SecurityBuffDefault[i] != gRFsFrame->buff[i])
//        {
//            return ERROR;
//        }
//    }
//
//    // 载入安全层长度
//    gIndex += SECURITY_DATA_LEN;
//
//    if(gRFsFrame->len < (gIndex + 2))
//    {
//        return ERROR;
//    }
//
//    // 判断传输层是否不等于0x11
//    if(gRFsFrame->buff[gIndex] != 0x11)
//    {
//        return ERROR;
//    }
//    gIndex++;
//
//    // 获取CMD
//    tmpCmd = gRFsFrame->buff[gIndex++];
//
//    // 判断DIR是否为 UP
//    if((tmpCmd & 0x40) != 0x00)
//    {
//        return ERROR;
//    }
//
//    // 判断CNL是否为 CONFIG
//    if((tmpCmd & 0x20) != 0x00)
//    {
//        return ERROR;
//    }
//
//    // 判断OPT是否为 READ
//    if((tmpCmd & 0x10) != 0x00)
//    {
//        return ERROR;
//    }
//
//    // 判断SRC是否为 NONE
//    if((tmpCmd & 0x0C) == 0x00)
//    {
//    }
//    // 判断SRC是否为 MAC
//    else if((tmpCmd & 0x0C) == 0x04)
//    {
//        return ERROR;
//    }
//    // 判断SRC是否为 UNIT
//    else if((tmpCmd & 0x0C) == 0x08)
//    {
//        gIndex += 2;
//    }
//    // 判断SRC是否为 APP
//    else if((tmpCmd & 0x0C) == 0x0C)
//    {
//        gIndex += 2;
//    }
//    else
//    {
//        return ERROR;
//    }
//
//    // 判断DES是否为 NONE
//    if((tmpCmd & 0x03) == 0x00)
//    {
//        return ERROR;
//    }
//    // 判断DES是否为 MAC
//    else if((tmpCmd & 0x03) == 0x01)
//    {
//        return ERROR;
//    }
//    // 判断DES是否为 UNIT
//    else if((tmpCmd & 0x03) == 0x02)
//    {
//        return ERROR;
//    }
//    // 判断DES是否为 APP
//    else if((tmpCmd & 0x03) == 0x03)
//    {
//        // 载入目的地址为APP
//        desAddr.Type = CMD_ADDR_TYPE_APP;
//        desAddr.AddrBuff[0] = gRFsFrame->buff[gIndex++];
//        desAddr.AddrBuff[1] = gRFsFrame->buff[gIndex++];
//
//        // 判断APP 是否是 允许转发的APP  Relay1/Realy2/Dimmer1/Curtain1
//        if((desAddr.AddrBuff[0] == RF_CAN_TO_RF_APP_RELAY1)
//        || (desAddr.AddrBuff[0] == RF_CAN_TO_RF_APP_RELAY2)
//        || (desAddr.AddrBuff[0] == RF_CAN_TO_RF_APP_DIMMER1)
//        || (desAddr.AddrBuff[0] == RF_CAN_TO_RF_APP_CURTAIN1))
//        {
//            // 载入控制命令字
//            sCmdType = RF_REG_CONTROL_CONTROL;
//        }
//        // 判断配置是否允许转发场景
//        #if (RF_CONFIG_CAN_TO_RF_SCENE == RF_CONGIG_CAN_TO_RF_SCENE_ENABLE)
//        // 判断APP 是否是 允许转发的场景
//        else if(desAddr.AddrBuff[0] == RF_CAN_TO_RF_SCENE)
//        {
//            // 载入场景命令字
//            sCmdType = RF_REG_CONTROL_SCENE;
//        }
//        #endif
//        else
//        {
//            return ERROR;
//        }
//    }
//    else
//    {
//        return ERROR;
//    }
//
//    if(gRFsFrame->len < (gIndex + 1))
//    {
//        return ERROR;
//    }
//
//    // 获取CMD TYPE
//    tmpCmd = gRFsFrame->buff[gIndex++];
//
//    // 判断是否为 停止命令
//    if(tmpCmd == RF_REG_CONTROL_STOP_RAMP)
//    {
//        // 获取STOP的命令
//        CnlCmd_GetCnlBuff(CNL_CMD_TYPE_RAMP_STOP, cnlCmdBuff, &cnlCmdLen);
//    }
//    // 判断命令是否与对应的AG地址匹配
//    else if(tmpCmd == sCmdType)
//    {
//        if(sCmdType == RF_REG_CONTROL_CONTROL)
//        {
//            if(gRFsFrame->len < (gIndex + CNL_CMD_LONG_BYTE_LEN))
//            {
//                return ERROR;
//            }
//
//            // 获取Level1
//            tmpLevel1 = gRFsFrame->buff[gIndex++];
//            // 跨过Level2
//            gIndex++;
//            // 获取Time1
//            tmpTim1 = gRFsFrame->buff[gIndex++];
//
//            // 判断Level2是否没禁用
//            if((gRFsFrame->buff[gIndex] != 0xFF) || (gRFsFrame->buff[gIndex+1] != 0xFF))
//            {
//                return ERROR;
//            }
//
//            // 获取调光命令
//            CnlCmd_GetRampBuff(tmpLevel1, tmpTim1, cnlCmdBuff, &cnlCmdLen);
//        }
//        else if(sCmdType == RF_REG_CONTROL_SCENE)
//        {
//            if(gRFsFrame->len < (gIndex + 1))
//            {
//                return ERROR;
//            }
//
//            // 获取Level1
//            tmpLevel1 = gRFsFrame->buff[gIndex++];
//
//            // 获取绝对控制命令
//            CnlCmd_GetAbsCnlBuff(tmpLevel1, cnlCmdBuff, &cnlCmdLen);
//        }
//        else
//        {
//            return ERROR;
//        }
//    }
//    else
//    {
//        return ERROR;
//    }
//
//    // 载入控制命令 获取控制帧
//    if(Frame_GetSingleCnlFrame(desAddr, cnlCmdBuff, cnlCmdLen, &sFrame) == SUCCESS)
//    {
//        // 获取CAN控制帧
//        if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
//        {
//            // 载入控制帧到发送队列
//            CAN_SendDbMsg(&sDb);
//        }
//    }
//    
//    return SUCCESS;
//}

// RF接收轮询
ErrorStatus RF_CmdRxPoll(void)
{
    static u8 rfFrameBuff[CC1101_RX_BUFF_SIZE];
    RFSFrame rfFrame = {0,
                      0,
                      CC1101_RX_BUFF_SIZE,
                      rfFrameBuff,
                      CC1101_TRANSMIT_POWER_NORMAL};

    if(RFQueue_IsNotEmpty(&CC1101RxQ) == SET)
    {
        if(RFQueue_Pull(&CC1101RxQ, &rfFrame) == SUCCESS)
        { 
            // 载入接收命令
            ItfLogic_LoadRfCmd(&rfFrame);
        }
    }

    return SUCCESS;
}

// 命令轮询
ErrorStatus RF_CmdPoll(void)
{
    // RF_CmdTxPoll();
    RF_CmdRxPoll();

    return SUCCESS;
}




