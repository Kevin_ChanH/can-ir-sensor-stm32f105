/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Frame.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月13日
  最近修改   :
  功能描述   : 通讯帧操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月13日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

ErrorStatus Frame_CopyCmdAttr(CmdAttrTypedef sCmdAttr, CmdAttrTypedef *dCmdAttr)
{
    u8 i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(dCmdAttr));

    dCmdAttr->Version   = sCmdAttr.Version;
    dCmdAttr->Dir       = sCmdAttr.Dir;
    dCmdAttr->Reg       = sCmdAttr.Reg;
    dCmdAttr->Opt       = sCmdAttr.Opt;

    // 获取源地址
    dCmdAttr->SrcAddr.Type = sCmdAttr.SrcAddr.Type;
    for(i=0; i<SYSTEM_SN_APP_LEN; i++)
    {
        dCmdAttr->SrcAddr.AddrBuff[i] = sCmdAttr.SrcAddr.AddrBuff[i];
    }

    // 获取目的地址
    dCmdAttr->DesAddr.Type = sCmdAttr.DesAddr.Type;
    for(i=0; i<SYSTEM_SN_APP_LEN; i++)
    {
        dCmdAttr->DesAddr.AddrBuff[i] = sCmdAttr.DesAddr.AddrBuff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Frame_GetCmdAttr
 功能描述  : 根据Cmd字节解析Cmd内容
 输入参数  : u8 cmdByte
             CmdAttrTypedef *cmdAttr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_GetCmdAttr(u8 cmdByte, CmdAttrTypedef *cmdAttr)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(cmdAttr));

    // 版本号
    if((cmdByte & FRAME_CMD_BIT_FRAME) == 0)
    {
        cmdAttr->Version = CMD_VERSION_OLD;
    }
    else
    {
        cmdAttr->Version = CMD_VERSION_NEW;
    }

    // 方向
    if((cmdByte & FRAME_CMD_BIT_DIR) == 0)
    {
        cmdAttr->Dir = CMD_DIR_DOWN;
    }
    else
    {
        cmdAttr->Dir = CMD_DIR_UP;
    }

    // 寄存器
    if((cmdByte & FRAME_CMD_BIT_REG) == 0)
    {
        cmdAttr->Reg = CMD_REG_CONTROL;
    }
    else
    {
        cmdAttr->Reg = CMD_REG_CONFIG;
    }

    // 操作
    if((cmdByte & FRAME_CMD_BIT_OPT) == 0)
    {
        cmdAttr->Opt = CMD_OPT_WIRTE;
    }
    else
    {
        cmdAttr->Opt = CMD_OPT_READ;
    }

    // 源地址
    if((cmdByte & FRAME_CMD_BIT_ADDR_SRC) == FRAME_CMD_SRC_NONE)
    {
        cmdAttr->SrcAddr.Type = CMD_ADDR_TYPE_NONE;
    }
    else if((cmdByte & FRAME_CMD_BIT_ADDR_SRC) == FRAME_CMD_SRC_MAC)
    {
        cmdAttr->SrcAddr.Type = CMD_ADDR_TYPE_MAC;
    }
    else if((cmdByte & FRAME_CMD_BIT_ADDR_SRC) == FRAME_CMD_SRC_UNIT)
    {
        cmdAttr->SrcAddr.Type = CMD_ADDR_TYPE_UNIT;
    }
    else if((cmdByte & FRAME_CMD_BIT_ADDR_SRC) == FRAME_CMD_SRC_APP)
    {
        cmdAttr->SrcAddr.Type = CMD_ADDR_TYPE_APP;
    }
    else
    {
        return ERROR;
    }

    // 目的地址
    if((cmdByte & FRAME_CMD_BIT_ADDR_DES) == FRAME_CMD_DES_NONE)
    {
        cmdAttr->DesAddr.Type = CMD_ADDR_TYPE_NONE;
    }
    else if((cmdByte & FRAME_CMD_BIT_ADDR_DES) == FRAME_CMD_DES_MAC)
    {
        cmdAttr->DesAddr.Type = CMD_ADDR_TYPE_MAC;
    }
    else if((cmdByte & FRAME_CMD_BIT_ADDR_DES) == FRAME_CMD_DES_UNIT)
    {
        cmdAttr->DesAddr.Type = CMD_ADDR_TYPE_UNIT;
    }
    else if((cmdByte & FRAME_CMD_BIT_ADDR_DES) == FRAME_CMD_DES_APP)
    {
        cmdAttr->DesAddr.Type = CMD_ADDR_TYPE_APP;
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Frame_GetCmdByte
 功能描述  : 根据Cmd内容解析为Cmd字节
 输入参数  : CmdAttrTypedef cmdAttr
             u8 *cmdByte
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_GetCmdByte(CmdAttrTypedef cmdAttr, u8 *cmdByte)
{
    u8 tmpByte = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(cmdByte));

    // 版本
    if(cmdAttr.Version == CMD_VERSION_NEW)
    {
        tmpByte |= FRAME_CMD_BIT_FRAME;
    }

    // 方向
    if(cmdAttr.Dir == CMD_DIR_UP)
    {
        tmpByte |= FRAME_CMD_BIT_DIR;
    }

    // 寄存器
    if(cmdAttr.Reg == CMD_REG_CONFIG)
    {
        tmpByte |= FRAME_CMD_BIT_REG;
    }

    // 操作
    if(cmdAttr.Opt == CMD_OPT_READ)
    {
        tmpByte |= FRAME_CMD_BIT_OPT;
    }

    // 源地址
    if(cmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_NONE)
    {
        tmpByte |= FRAME_CMD_SRC_NONE;
    }
    else if(cmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_SRC_MAC;
    }
    else if(cmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_SRC_UNIT;
    }
    else if(cmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_APP)
    {
        tmpByte |= FRAME_CMD_SRC_APP;
    }
    else
    {
        return ERROR;
    }

    // 目的地址
    if(cmdAttr.DesAddr.Type == CMD_ADDR_TYPE_NONE)
    {
        tmpByte |= FRAME_CMD_DES_NONE;
    }
    else if(cmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;
    }
    else if(cmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;
    }
    else if(cmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP)
    {
        tmpByte |= FRAME_CMD_DES_APP;
    }
    else
    {
        return ERROR;
    }

    // 载入控制字
    *cmdByte = tmpByte;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Frame_GetSingleCnlFrame
 功能描述  : 根据控制命令 获取帧数据
 输入参数  : AddrAttrTypedef desAddr
             u8 cmdBuff[]
             uc8 cmdLen
             FrameTypedef *userDat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_GetSingleCnlFrame(AddrAttrTypedef desAddr, u8 cmdBuff[], uc8 cmdLen, FrameTypedef *userDat)
{
    u8 i;
    u8 tmpLen = 0;
    u16 tmpUnitAddr = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(cmdBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(userDat));

    // 命令版本为旧版本
    userDat->CmdAttr.Version    = CMD_VERSION_OLD;
    // 方向设为下发
    userDat->CmdAttr.Dir        = CMD_DIR_DOWN;
    // 寄存器类型设为控制
    userDat->CmdAttr.Reg        = CMD_REG_CONTROL;
    // 操作设为写操作
    userDat->CmdAttr.Opt        = CMD_OPT_WIRTE;


    // 源地址 - 单元地址
    userDat->CmdAttr.SrcAddr.Type = CMD_ADDR_TYPE_UNIT;

    if(Addr_GetUnitAddr(&tmpUnitAddr) == SUCCESS)
    {
        userDat->CmdAttr.SrcAddr.AddrBuff[0] = (u8)(tmpUnitAddr >> 1);
        userDat->CmdAttr.SrcAddr.AddrBuff[1] = (u8)((tmpUnitAddr << 7) & 0xFF);
    }
    else
    {
        userDat->CmdAttr.SrcAddr.AddrBuff[0] = (u8)(SystemUnitDefaultValue >> 1);
        userDat->CmdAttr.SrcAddr.AddrBuff[1] = (u8)((SystemUnitDefaultValue << 7) & 0xFF);
    }

    // 目的地址为 单元地址 或 AG地址
    if((desAddr.Type == CMD_ADDR_TYPE_UNIT)
       ||(desAddr.Type == CMD_ADDR_TYPE_APP))
    {
        // 载入目的地址类型
        userDat->CmdAttr.DesAddr.Type = desAddr.Type;

        // 载入目的地址数据
        userDat->CmdAttr.DesAddr.AddrBuff[tmpLen++] = desAddr.AddrBuff[0];
        userDat->CmdAttr.DesAddr.AddrBuff[tmpLen++] = desAddr.AddrBuff[1];
    }
    else if(desAddr.Type == CMD_ADDR_TYPE_NONE)
    {
        // 载入目的地址类型
        userDat->CmdAttr.DesAddr.Type = desAddr.Type;
    }
    else
    {
        return ERROR;
    }

    // 需要载入的数据是否小于最大的容量
    if((tmpLen + cmdLen) <= FRAME_SINGLE_DATA_LEN)
    {
        // 载入发送数据
        for(i=0; i<cmdLen; i++)
        {
            userDat->Data[i] = cmdBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    // 载入数据区长度
    userDat->DataLen = cmdLen;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Frame_GetSingleReadCnlFrame
 功能描述  : 获取单帧读取控制寄存器帧
 输入参数  : AddrAttrTypedef desAddr
             u8 cmdBuff[]
             uc8 cmdLen
             FrameTypedef *userDat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_GetSingleReadCnlFrame(AddrAttrTypedef desAddr, u8 cmdBuff[], uc8 cmdLen, FrameTypedef *userDat)
{
    u8 i;
    u8 tmpLen = 0;
    u16 tmpUnitAddr = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(cmdBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(userDat));

    // 命令版本为旧版本
    userDat->CmdAttr.Version    = CMD_VERSION_OLD;
    // 方向设为下发
    userDat->CmdAttr.Dir        = CMD_DIR_DOWN;
    // 寄存器类型设为控制
    userDat->CmdAttr.Reg        = CMD_REG_CONTROL;
    // 操作设为读操作
    userDat->CmdAttr.Opt        = CMD_OPT_READ;


    // 源地址 - 单元地址
    userDat->CmdAttr.SrcAddr.Type = CMD_ADDR_TYPE_UNIT;
    if(Addr_GetUnitAddr(&tmpUnitAddr) == SUCCESS)
    {
        userDat->CmdAttr.SrcAddr.AddrBuff[0] = (u8)(tmpUnitAddr >> 1);
        userDat->CmdAttr.SrcAddr.AddrBuff[1] = (u8)((tmpUnitAddr << 7) & 0xFF);
    }
    else
    {

        userDat->CmdAttr.SrcAddr.AddrBuff[0] = (u8)(SystemUnitDefaultValue >> 1);
        userDat->CmdAttr.SrcAddr.AddrBuff[1] = (u8)((SystemUnitDefaultValue << 7) & 0xFF);
    }

    // 目的地址为 单元地址 或 AG地址
    if((desAddr.Type == CMD_ADDR_TYPE_UNIT)
       ||(desAddr.Type == CMD_ADDR_TYPE_APP))
    {
        // 载入目的地址类型
        userDat->CmdAttr.DesAddr.Type = desAddr.Type;

        // 载入目的地址数据
        userDat->CmdAttr.DesAddr.AddrBuff[tmpLen++] = desAddr.AddrBuff[0];
        userDat->CmdAttr.DesAddr.AddrBuff[tmpLen++] = desAddr.AddrBuff[1];
    }
    else if(desAddr.Type == CMD_ADDR_TYPE_NONE)
    {
        // 载入目的地址类型
        userDat->CmdAttr.DesAddr.Type = desAddr.Type;
    }
    else
    {
        return ERROR;
    }

    // 需要载入的数据是否小于最大的容量
    if((tmpLen + cmdLen) <= FRAME_SINGLE_DATA_LEN)
    {
        // 载入发送数据
        for(i=0; i<cmdLen; i++)
        {
            userDat->Data[i] = cmdBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    // 载入数据区长度
    userDat->DataLen = cmdLen;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Frame_GetSingleFeedbackCnlFrame
 功能描述  : 获取单帧度反馈的状态帧
 输入参数  : AddrAttrTypedef desAddr
             u8 cmdBuff[]
             uc8 cmdLen
             FrameTypedef *userDat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_GetSingleFeedbackCnlFrame(AddrAttrTypedef desAddr, u8 cmdBuff[], uc8 cmdLen, FrameTypedef *userDat)
{
    u8 i;
    u8 tmpLen = 0;
    u16 tmpUnitAddr = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(cmdBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(userDat));

    // 命令版本为旧版本
    userDat->CmdAttr.Version    = CMD_VERSION_OLD;
    // 方向设为下发
    userDat->CmdAttr.Dir        = CMD_DIR_UP;
    // 寄存器类型设为控制
    userDat->CmdAttr.Reg        = CMD_REG_CONTROL;
    // 操作设为读操作
    userDat->CmdAttr.Opt        = CMD_OPT_READ;


    // 源地址 - 单元地址
    userDat->CmdAttr.SrcAddr.Type = CMD_ADDR_TYPE_UNIT;

    if(Addr_GetUnitAddr(&tmpUnitAddr) == SUCCESS)
    {
        userDat->CmdAttr.SrcAddr.AddrBuff[0] = (u8)(tmpUnitAddr >> 1);
        userDat->CmdAttr.SrcAddr.AddrBuff[1] = (u8)((tmpUnitAddr << 7) & 0xFF);
    }
    else
    {
        userDat->CmdAttr.SrcAddr.AddrBuff[0] = (u8)(SystemUnitDefaultValue >> 1);
        userDat->CmdAttr.SrcAddr.AddrBuff[1] = (u8)((SystemUnitDefaultValue << 7) & 0xFF);
    }

    // 目的地址为 单元地址 或 AG地址
    if((desAddr.Type == CMD_ADDR_TYPE_UNIT)
       ||(desAddr.Type == CMD_ADDR_TYPE_APP))
    {
        // 载入目的地址类型
        userDat->CmdAttr.DesAddr.Type = desAddr.Type;

        // 载入目的地址数据
        userDat->CmdAttr.DesAddr.AddrBuff[tmpLen++] = desAddr.AddrBuff[0];
        userDat->CmdAttr.DesAddr.AddrBuff[tmpLen++] = desAddr.AddrBuff[1];
    }
    else if(desAddr.Type == CMD_ADDR_TYPE_NONE)
    {
        // 载入目的地址类型
        userDat->CmdAttr.DesAddr.Type = desAddr.Type;
    }
    else
    {
        return ERROR;
    }

    // 需要载入的数据是否小于最大的容量
    if((tmpLen + cmdLen) <= FRAME_SINGLE_DATA_LEN)
    {
        // 载入发送数据
        for(i=0; i<cmdLen; i++)
        {
            userDat->Data[i] = cmdBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    // 载入数据区长度
    userDat->DataLen = cmdLen;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Frame_SingleFrameGetCanDb
 功能描述  : 用户数据转换为控制数据帧
 输入参数  : FrameTypedef userDat
             CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月13日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_SingleFrameGetCanDb(FrameTypedef userDat, CAN_DataBase *sDB)
{
    u32 tmpId  = 0;
    u8 tmpByte = 0;
    u8 sIndex  = 0;
    u8  i      = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sDB));

    // 获取控制字节
    if(Frame_GetCmdByte(userDat.CmdAttr, &tmpByte) != SUCCESS)
    {
        return ERROR;
    }
    // 载入CMD
    tmpId |= (u32)(tmpByte << 16);


    // 载入源地址
    if((userDat.CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
       ||(userDat.CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_APP))
    {
        // 载入源地址
        tmpId |= (u32)((userDat.CmdAttr.SrcAddr.AddrBuff[0]) << 8);
        tmpId |= (u32)(userDat.CmdAttr.SrcAddr.AddrBuff[1]);
    }
    else if(userDat.CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_NONE)
    {
    }
    else
    {
        return ERROR;
    }
    // 载入帧ID
    sDB->FrameID = tmpId;


    // 载入目的地址
    if((userDat.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT)
       ||(userDat.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP))
    {
        // 载入目的地址
        sDB->DataBuff[sIndex++] = userDat.CmdAttr.DesAddr.AddrBuff[0];
        sDB->DataBuff[sIndex++] = userDat.CmdAttr.DesAddr.AddrBuff[1];
    }
    else if(userDat.CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_NONE)
    {
    }
    else
    {
        return ERROR;
    }


    // 判断剩余数据是否超出最大容量
    if((sIndex + userDat.DataLen) <= CAN_FRAME_MAX_LEN)
    {
        // 载入控制数据
        for(i=0; i<userDat.DataLen; i++)
        {
            sDB->DataBuff[sIndex++] = userDat.Data[i];
        }

        // 载入帧长度
        sDB->FrameLen = sIndex;
    }
    else
    {
        return ERROR;
    }

    // 返回转换成功
    return SUCCESS;

}



/*****************************************************************************
 函 数 名  : Frame_CanDbGetSingleFrame
 功能描述  : 控制数据转换为用户数据
 输入参数  : CAN_DataBase cDB
             FrameTypedef *userDat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月13日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_CanDbGetSingleFrame(CAN_DataBase cDB, FrameTypedef *userDat)
{
    u32 tmpId   = 0;
    u8  tmpByte = 0;
    u8  sIndex  = 0;
    u8  i       = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(userDat));
    assert_app_param(IS_CAN_DB_DATA_LEN(cDB.FrameLen));


    // 获取帧ID
    tmpId = cDB.FrameID;

    // 判断帧是否为大数据包 子帧
    if((tmpId & CAN_FRAME_TYPE_PACKET) == CAN_FRAME_TYPE_PACKET)
    {
        return ERROR;
    }

    // 获取控制字节
    tmpByte = (u8)((tmpId >> 16) & 0xFF);

    // 获取控制字节内容
    if(Frame_GetCmdAttr(tmpByte, &(userDat->CmdAttr)) != SUCCESS)
    {
        return ERROR;
    }

    // 短帧源地址只接受 UNIT APP NONE
    if((userDat->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
       ||(userDat->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_APP))
    {
        userDat->CmdAttr.SrcAddr.AddrBuff[0] = (u8)((tmpId >> 8) & 0xFF);
        userDat->CmdAttr.SrcAddr.AddrBuff[1] = (u8)(tmpId & 0xFF);
    }
    else if(userDat->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_NONE)
    {
    }
    else
    {
        return ERROR;
    }

    // 短帧目的地址只接受 UNIT APP NONE
    if((userDat->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT)
       ||(userDat->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_APP))
    {
        // 判断是否接收够足够的目的地址数据
        if(cDB.FrameLen >= 2)
        {
            userDat->CmdAttr.DesAddr.AddrBuff[0] = cDB.DataBuff[sIndex++];
            userDat->CmdAttr.DesAddr.AddrBuff[1] = cDB.DataBuff[sIndex++];
        }
        else
        {
            return ERROR;
        }
    }
    else if(userDat->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_NONE)
    {
    }
    else
    {
        return ERROR;
    }


    // 判断帧长度是否大于等于下标
    if(cDB.FrameLen >= sIndex)
    {
        // 获取目前的数据下标
        tmpByte = sIndex;
        // 获取数据内容
        for(i=0; i<(cDB.FrameLen-tmpByte); i++)
        {
            userDat->Data[i] = cDB.DataBuff[sIndex++];
        }
        // 载入数据长度
        userDat->DataLen = i;
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}


ErrorStatus Frame_AnalyzeDetailedReg(u8 gBuff[], uc16 gLen, u16 *gIndex, FrameCfgTypedef *gCfgFrame)
{
    u16 tmpIndex = *gIndex;
    u32 tmpDoubleWord;
    u8  tmpByte;
    ErrorStatus retStat = ERROR;


    /* 遍历类型寄存器 */
    switch(gCfgFrame->RegAttr.TypeReg)
    {
        /* 地址信息 */
        case FRAME_CFG_REG_TYPE_ADDR:

            /* 地址信息 不存在通道 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* 判断是否为所有地址信息 */
            if(gCfgFrame->RegAttr.DetailedReg == CFG_ADDR_REG_DETAILED_ALL)
            {
                /* 所有的地址信息仅为读有效 */
                if(gCfgFrame->CmdAttr.Opt == CMD_OPT_READ)
                {
                    /* 判断是否为读反馈 */
                    if(gCfgFrame->CmdAttr.Dir == CMD_DIR_UP)
                    {
                        /* 判断长度是否足够 */
                        if(gLen >= (tmpIndex + 4))
                        {
                            tmpDoubleWord  = (u32)(gBuff[tmpIndex++] << 24);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 16);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 8);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++]);

                            /* 更新下标号 */
                            *gIndex = tmpIndex;

                            /* 载入参与寄存器 并标识有效 */
                            gCfgFrame->RegAttr.PartakeRegs.Regs    = tmpDoubleWord;
                            gCfgFrame->RegAttr.PartakeRegs.HasMean = SET;

                            /* 返回正确 */
                            retStat = SUCCESS;
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        /* 返回正确 */
                        retStat = SUCCESS;
                    }
                }
                else
                {
                }
            }
            /* 判断是否为有效的单一地址信息 */
            else if(gCfgFrame->RegAttr.DetailedReg < CFG_ADDR_REG_DETAILED_LIMIT)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            else
            {
            }
            break;

        /* 标签信息 */
        case FRAME_CFG_REG_TYPE_TAG:

            /* 标签信息 不存在通道 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* 判断是否为预留的标签信息 */
            if(gCfgFrame->RegAttr.DetailedReg == CFG_TAG_REG_DETAILED_RESERVED)
            {
            }
            /* 判断是否为有效的单一标签信息 */
            else if(gCfgFrame->RegAttr.DetailedReg < CFG_TAG_REG_DETAILED_LIMIT)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            else
            {
            }

            break;

        /* Bootloader信息 */
        case FRAME_CFG_REG_TYPE_BOOTLOADER:
            /* Bootloader 不存在通道 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* Bootloader信息仅为读有效 */
            if(gCfgFrame->CmdAttr.Opt == CMD_OPT_READ)
            {
                /* 判断是否为所有的Bootloader信息 */
                if(gCfgFrame->RegAttr.DetailedReg == CFG_BOOTLOADER_REG_DETAILED_ALL)
                {
                    /* 判断是否为读反馈 */
                    if(gCfgFrame->CmdAttr.Dir == CMD_DIR_UP)
                    {
                        /* 判断长度是否足够 */
                        if(gLen >= (tmpIndex + 4))
                        {
                            tmpDoubleWord  = (u32)(gBuff[tmpIndex++] << 24);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 16);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 8);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++]);

                            /* 更新下标号 */
                            *gIndex = tmpIndex;

                            /* 载入参与寄存器 并标识有效 */
                            gCfgFrame->RegAttr.PartakeRegs.Regs    = tmpDoubleWord;
                            gCfgFrame->RegAttr.PartakeRegs.HasMean = SET;

                            /* 返回正确 */
                            retStat = SUCCESS;
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        /* 返回正确 */
                        retStat = SUCCESS;
                    }
                }
                /* 判断是否为单一有效的Bootloader信息 */
                else if(gCfgFrame->RegAttr.DetailedReg < CFG_BOOTLOADER_REG_DETAILED_LIMIT)
                {
                    /* 返回正确 */
                    retStat = SUCCESS;
                }
                else
                {
                }
            }
            else
            {
            }

            break;

        /* Firmware信息 */
        case FRAME_CFG_REG_TYPE_FIRMWARE:

            /* Firmware 不存在通道 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* Firmware信息仅为读有效 */
            if(gCfgFrame->CmdAttr.Opt ==  CMD_OPT_READ)
            {
                /* 判断是否为所有的Firmware信息 */
                if(gCfgFrame->RegAttr.DetailedReg == CFG_FIRMWARE_REG_DETAILED_ALL)
                {
                    /* 判断是否为读反馈 */
                    if(gCfgFrame->CmdAttr.Dir == CMD_DIR_UP)
                    {
                        /* 判断长度是否足够 */
                        if(gLen >= (tmpIndex + 4))
                        {
                            tmpDoubleWord  = (u32)(gBuff[tmpIndex++] << 24);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 16);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 8);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++]);

                            /* 更新下标号 */
                            *gIndex = tmpIndex;

                            /* 载入参与寄存器 并标识有效 */
                            gCfgFrame->RegAttr.PartakeRegs.Regs    = tmpDoubleWord;
                            gCfgFrame->RegAttr.PartakeRegs.HasMean = SET;

                            /* 返回正确 */
                            retStat = SUCCESS;
                        }
                        else
                        {
                        }
                    }
                    else
                    {
                        /* 返回正确 */
                        retStat = SUCCESS;
                    }
                }
                /* 判断是否为单一有效的Firmware信息 */
                else if(gCfgFrame->RegAttr.DetailedReg < CFG_FIRMWARE_REG_DETAILED_LIMIT)
                {
                    /* 返回正确 */
                    retStat = SUCCESS;
                }
                else
                {
                }
            }
            else
            {
            }

            break;

        /* Hardware信息 */
        case FRAME_CFG_REG_TYPE_HARDWARE:

            /* Hardware 不存在通道 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* 判断是否为所有的Hardware信息 */
            if(gCfgFrame->RegAttr.DetailedReg == CFG_HARDWARE_REG_DETAILED_ALL)
            {
                /* 向下读取所有寄存器的时候 不需要指明参与寄存器 */
                if((gCfgFrame->CmdAttr.Opt == CMD_OPT_READ)
                   &&(gCfgFrame->CmdAttr.Dir == CMD_DIR_DOWN))
                {
                    /* 返回正确 */
                    retStat = SUCCESS;
                }

                /* 判断长度是否足够 */
                if(gLen >= (tmpIndex + 4))
                {
                    tmpDoubleWord  = (u32)(gBuff[tmpIndex++] << 24);
                    tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 16);
                    tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 8);
                    tmpDoubleWord |= (u32)(gBuff[tmpIndex++]);

                    /* 更新下标号 */
                    *gIndex = tmpIndex;

                    /* 载入参与寄存器 并标识有效 */
                    gCfgFrame->RegAttr.PartakeRegs.Regs    = tmpDoubleWord;
                    gCfgFrame->RegAttr.PartakeRegs.HasMean = SET;

                    /* 返回正确 */
                    retStat = SUCCESS;
                }
                else
                {
                }
            }
            /* 判断是否为单一有效的Hardware信息 */
            else if(gCfgFrame->RegAttr.DetailedReg < CFG_HARDWARD_REG_DETAILED_LIMIT)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            else
            {
            }

            break;

        /* 输入信息 */
        case FRAME_CFG_REG_TYPE_INPUT:

            /* Input 设置通道为无效 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* 判断是否为预留的输入信息 */
            if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_RESERVED)
            {
            }
            /* 判断是否为按键输入信息 */
            else if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_KEY)
            {
                if(gLen > tmpIndex)
                {
                    tmpByte = gBuff[tmpIndex++];
                    /* 更新下标号 */
                    *gIndex = tmpIndex;

                    /* 载入通道 */
                    gCfgFrame->RegAttr.ChReg.ChNum = tmpByte;
                    gCfgFrame->RegAttr.ChReg.HasMean = SET;

                    /* 判断是否为单输入类型的所有通道 */
                    if(tmpByte == CFG_INPUT_REG_CHANNEL_ALL)
                    {
                        /* 向下读取所有寄存器的时候 不需要指明参与寄存器 */
                        if((gCfgFrame->CmdAttr.Opt == CMD_OPT_READ)
                           &&(gCfgFrame->CmdAttr.Dir == CMD_DIR_DOWN))
                        {
                            /* 返回正确 */
                            retStat = SUCCESS;
                        }

                        /* 判断长度是否足够 */
                        if(gLen >= (tmpIndex + 4))
                        {
                            tmpDoubleWord  = (u32)(gBuff[tmpIndex++] << 24);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 16);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 8);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++]);

                            /* 更新下标号 */
                            *gIndex = tmpIndex;

                            /* 载入参与寄存器 并标识有效 */
                            gCfgFrame->RegAttr.PartakeRegs.Regs    = tmpDoubleWord;
                            gCfgFrame->RegAttr.PartakeRegs.HasMean = SET;

                            /* 返回正确 */
                            retStat = SUCCESS;
                        }
                        else
                        {
                        }
                    }
                    else if((tmpByte >= CFG_INPUT_REG_CHANNEL_MIN)
                            && (tmpByte <= CFG_INPUT_REG_CHANNEL_MAX))
                    {
                        /* 返回正确 */
                        retStat = SUCCESS;
                    }
                    else
                    {
                    }
                }
                else
                {
                }
            }
            /* 判断是否为照度传感器输入信息 */
            else if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_ILLUMINANCE)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            /* 判断是否为红外传感器输入信息 */
            else if(gCfgFrame->RegAttr.DetailedReg == CFG_INPUT_REG_DETAILED_IR_SENSOR)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            else
            {
            }

            break;

        /* 输出信息 */
        case FRAME_CFG_REG_TYPE_OUTPUT:

            /* Output 设置通道为无效 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* 判断是否为预留的输出信息 */
            if(gCfgFrame->RegAttr.DetailedReg == CFG_OUTPUT_REG_DETAILED_RESERVED)
            {
            }
            /* 判断是否为单一的输出信息 */
            else if(gCfgFrame->RegAttr.DetailedReg < CFG_OUTPUT_REG_DETAILED_LIMIT)
            {
                if(gLen > tmpIndex)
                {
                    tmpByte = gBuff[tmpIndex++];
                    /* 更新下标号 */
                    *gIndex = tmpIndex;

                    /* 载入通道 */
                    gCfgFrame->RegAttr.ChReg.ChNum = tmpByte;
                    gCfgFrame->RegAttr.ChReg.HasMean = SET;

                    /* 判断是否为单输出类型的所有通道 */
                    if(tmpByte == CFG_OUTPUT_REG_CHANNEL_ALL)
                    {
                        /* 向下读取所有寄存器的时候 不需要指明参与寄存器 */
                        if((gCfgFrame->CmdAttr.Opt == CMD_OPT_READ)
                           &&(gCfgFrame->CmdAttr.Dir == CMD_DIR_DOWN))
                        {
                            /* 返回正确 */
                            retStat = SUCCESS;
                        }

                        /* 判断长度是否足够 */
                        if(gLen >= (tmpIndex + 4))
                        {
                            tmpDoubleWord  = (u32)(gBuff[tmpIndex++] << 24);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 16);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++] << 8);
                            tmpDoubleWord |= (u32)(gBuff[tmpIndex++]);

                            /* 更新下标号 */
                            *gIndex = tmpIndex;

                            /* 载入参与寄存器 并标识有效 */
                            gCfgFrame->RegAttr.PartakeRegs.Regs    = tmpDoubleWord;
                            gCfgFrame->RegAttr.PartakeRegs.HasMean = SET;

                            /* 返回正确 */
                            retStat = SUCCESS;
                        }
                        else
                        {
                        }
                    }
                    else if((tmpByte >= CFG_OUTPUT_REG_CHANNEL_MIN)
                            && (tmpByte <= CFG_OUTPUT_REG_CHANNEL_MAX))
                    {
                        /* 返回正确 */
                        retStat = SUCCESS;
                    }
                    else
                    {
                    }
                }
                else
                {
                }
            }
            else
            {
            }

            break;

        /* 场景信息 */
        case FRAME_CFG_REG_TYPE_SCENE:

            /* Input 设置通道为无效 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* 判断是否为预留的场景信息 */
            if(gCfgFrame->RegAttr.DetailedReg == CFG_SCENE_REG_DETAILED_RESERVED)
            {
            }
            else if((gCfgFrame->RegAttr.DetailedReg >= CFG_SCENE_REG_DETAILED_MIN)
                    && (gCfgFrame->RegAttr.DetailedReg <= CFG_SCENE_REG_DETAILED_MAX))
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            else
            {
            }

            break;

        case FRAME_CFG_REG_TYPE_INTERFACE:

            /* Input 设置通道为无效 */
            gCfgFrame->RegAttr.ChReg.HasMean = RESET;
            gCfgFrame->RegAttr.ChReg.ChNum   = 0;

            /* 参与寄存器标识无效 */
            gCfgFrame->RegAttr.PartakeRegs.HasMean = RESET;
            gCfgFrame->RegAttr.PartakeRegs.Regs    = 0;

            /* 判断是否为预留的场景信息 */
            if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_RESERVED)
            {
            }
            /* 判断是否为配置CAN信息 */
            else if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_CAN)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            /* 判断是否为配置RF信息 */
            else if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_RF)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            // 判断是否为配置RF安全层信息
            else if(gCfgFrame->RegAttr.DetailedReg == CFG_INTERFACE_REG_DETAILED_RF_SEC)
            {
                /* 返回正确 */
                retStat = SUCCESS;
            }
            else
            {
            }
            
            break;
        default:
            break;
    }

    return retStat;

}

ErrorStatus Frame_AnalyzeCfgReg(u8 gBuff[], uc16 gLen, u16 *gIndex, FrameCfgTypedef *gCfgFrame)
{
    u16 tmpIndex = *gIndex;
    u8  tmpByte = 0;

    /* 判断长度是否足够 */
    if(gLen > tmpIndex)
    {
        /* 获取字节暂存器 */
        tmpByte = gBuff[tmpIndex++];

        /* 更新下标号 */
        *gIndex = tmpIndex;

        /* 判断寄存器类型有效范围 */
        if((tmpByte > CFG_REG_TYPE_RESERVED)
           && (tmpByte < CFG_REG_TYPE_MAX))
        {
            /* 载入类型寄存器 */
            gCfgFrame->RegAttr.TypeReg = (FrameCfgRegType)(tmpByte);
        }
        else
        {
            return ERROR;
        }
    }
    else
    {
        return ERROR;
    }


    /* 判断长度是否足够 */
    if(gLen > tmpIndex)
    {
        /* 载入详细寄存器 */
        gCfgFrame->RegAttr.DetailedReg = gBuff[tmpIndex++];

        /* 更新下标号 */
        *gIndex = tmpIndex;
    }
    else
    {
        return ERROR;
    }

    /* 解析详细的寄存器 */
    return Frame_AnalyzeDetailedReg(gBuff, gLen, gIndex, gCfgFrame);
}

/*****************************************************************************
 函 数 名  : Frame_FrameGetCfgFrame
 功能描述  : 单帧获取配置数据帧
 输入参数  : FrameTypedef singleFrame
             FrameCfgTypedef *cfgFrame
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月28日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Frame_FrameGetCfgFrame(FrameTypedef singleFrame, FrameCfgTypedef *cfgFrame)
{
    u16 sIndex = 0;
    u8  gDataLen = 0;
    u8  i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(cfgFrame));

    // 复制CmdAttr
    Frame_CopyCmdAttr(singleFrame.CmdAttr, &(cfgFrame->CmdAttr));

    // 解析配置字节
    if(Frame_AnalyzeCfgReg(singleFrame.Data, singleFrame.DataLen, &sIndex, cfgFrame) != SUCCESS)
    {
        return ERROR;
    }

    // 判断是否存在数据
    if(singleFrame.DataLen >= sIndex)
    {
        gDataLen = singleFrame.DataLen - sIndex;

        // 循环复制数据
        for(i=0; i<gDataLen; i++)
        {
            cfgFrame->CfgAttrBuff[i] = singleFrame.Data[sIndex++];
        }

        // 载入数据长度
        cfgFrame->CfgBuffLen = gDataLen;

        return SUCCESS;
    }

    return ERROR;
}

ErrorStatus Frame_PacketGetCfgFrame(CAN_PacketBase gpacket,FrameCfgTypedef *cfgFrame)
{
    u8 tmpByte;
    u8 i = 0;
    u16 sIndex = 0;
    u16 gDataLen = 0;

    tmpByte = (u8)((gpacket.PacketID >> 16) & 0xFF);

    // 转换为CmdAttr
    if(Frame_GetCmdAttr(tmpByte, &(cfgFrame->CmdAttr)) != SUCCESS)
    {
        return ERROR;
    }

    // 源地址为MAC地址
    if(cfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        // 数据包长度是否猪狗
        if(gpacket.PacketLen > (sIndex + SYSTEM_SN_APP_LEN))
        {
            for(i=0; i<SYSTEM_SN_APP_LEN; i++)
            {
                cfgFrame->CmdAttr.SrcAddr.AddrBuff[i] = gpacket.PacketBuff[sIndex++];
            }
        }
        else
        {
            return ERROR;
        }
    }
    // 源地址为UNIT地址
    else if(cfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        // 数据包长度是否猪狗
        if(gpacket.PacketLen > (sIndex + SYSTEM_UNIT_ADDR_LEN))
        {
            for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
            {
                cfgFrame->CmdAttr.SrcAddr.AddrBuff[i] = gpacket.PacketBuff[sIndex++];
            }
        }
        else
        {
            return ERROR;
        }
    }
    // 源地址为NONE
    else if(cfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_NONE)
    {

    }
    else
    {
        return ERROR;
    }


    // 源地址为MAC地址
    if(cfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        // 数据包长度是否猪狗
        if(gpacket.PacketLen > (sIndex + SYSTEM_SN_APP_LEN))
        {
            for(i=0; i<SYSTEM_SN_APP_LEN; i++)
            {
                cfgFrame->CmdAttr.DesAddr.AddrBuff[i] = gpacket.PacketBuff[sIndex++];
            }
        }
        else
        {
            return ERROR;
        }
    }
    // 源地址为UNIT地址
    else if(cfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        // 数据包长度是否猪狗
        if(gpacket.PacketLen > (sIndex + SYSTEM_UNIT_ADDR_LEN))
        {
            for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
            {
                cfgFrame->CmdAttr.DesAddr.AddrBuff[i] = gpacket.PacketBuff[sIndex++];
            }
        }
        else
        {
            return ERROR;
        }
    }
    // 源地址为NONE
    else if(cfgFrame->CmdAttr.DesAddr.Type == CMD_ADDR_TYPE_NONE)
    {

    }
    else
    {
        return ERROR;
    }

    // 解析配置字节
    if(Frame_AnalyzeCfgReg(gpacket.PacketBuff, gpacket.PacketLen, &sIndex, cfgFrame) != SUCCESS)
    {
        return ERROR;
    }

    // 判断是否存在数据
    if(gpacket.PacketLen >= sIndex)
    {
        gDataLen = gpacket.PacketLen - sIndex;

        // 循环复制数据
        for(i=0; i<gDataLen; i++)
        {
            cfgFrame->CfgAttrBuff[i] = gpacket.PacketBuff[sIndex++];
        }

        // 载入数据长度
        cfgFrame->CfgBuffLen = gDataLen;

        return SUCCESS;
    }


    return ERROR;
}

