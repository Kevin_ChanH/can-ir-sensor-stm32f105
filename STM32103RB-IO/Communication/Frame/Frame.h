/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Frame.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月13日
  最近修改   :
  功能描述   : Frame.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月13日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __FRAME_H__
#define __FRAME_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/* 单帧数据最大为8Byte */
#define     FRAME_SINGLE_DATA_LEN       8
#define     FRAME_CFG_DATA_LEN          540

/* 控制字的版本 */
typedef enum
{
    CMD_VERSION_OLD = 0,
    CMD_VERSION_NEW,
} CmdVersionTypedef;

/* 帧的方向 */
typedef enum
{
    CMD_DIR_DOWN = 0,
    CMD_DIR_UP,
} CmdDirTypedef;

/* 帧的类型 */
typedef enum
{
    CMD_REG_CONTROL = 0,
    CMD_REG_CONFIG,
} CmdRegTypedef;

/* 帧的操作方式 */
typedef enum
{
    CMD_OPT_WIRTE = 0,
    CMD_OPT_READ,
} CmdOptTypedef;

/* 地址类型 */
typedef enum
{
    CMD_ADDR_TYPE_NONE = 0,
    CMD_ADDR_TYPE_MAC,
    CMD_ADDR_TYPE_UNIT,
    CMD_ADDR_TYPE_APP,
} CmdAddrTypedef;

/* 地址结构 */
typedef struct
{
    CmdAddrTypedef      Type;
    u8                  AddrBuff[SYSTEM_SN_APP_LEN];
} AddrAttrTypedef;

/* 控制字结构 */
typedef struct
{
    CmdVersionTypedef       Version;        /* 版本号 */
    CmdDirTypedef           Dir;            /* 方向 */
    CmdRegTypedef           Reg;            /* 寄存器 */
    CmdOptTypedef           Opt;            /* 操作 */

    AddrAttrTypedef         SrcAddr;        /* 源地址 */
    AddrAttrTypedef         DesAddr;        /* 目的地址 */
} CmdAttrTypedef;

/* 控制命令结构 */
typedef struct
{
    CmdAttrTypedef          CmdAttr;
    u8                      Data[FRAME_SINGLE_DATA_LEN];
    u8                      DataLen;
} FrameTypedef;



/* 通道号格式内容 */
typedef struct
{
    FlagStatus              HasMean;        /* 有含义的标识 */
    u8                      ChNum;          /* 通道号 */
} FrameCfgChTypedef;

/* 寄存器参与反馈格式内容 */
typedef struct
{
    FlagStatus              HasMean;        /* 有意义的标识 */
    u32                     Regs;           /* 参与的寄存器 */
} FrameCfgPartakeTypedef;

/* 配置类型寄存器 */
typedef enum
{
    FRAME_CFG_REG_TYPE_RESERVED = 0,        /* 预留 */
    FRAME_CFG_REG_TYPE_ADDR,                /* 地址信息 */
    FRAME_CFG_REG_TYPE_TAG,                 /* 标签信息 */
    FRAME_CFG_REG_TYPE_BOOTLOADER,          /* Bootloader */
    FRAME_CFG_REG_TYPE_FIRMWARE,            /* 固件信息 */
    FRAME_CFG_REG_TYPE_HARDWARE,            /* 硬件信息 */
    FRAME_CFG_REG_TYPE_INPUT,               /* 输入信息 */
    FRAME_CFG_REG_TYPE_OUTPUT,              /* 输出信息 */
    FRAME_CFG_REG_TYPE_SCENE,               /* 场景信息 */
    FRAME_CFG_REG_TYPE_INTERFACE,           /* 接口信息 */
    FRAME_CFG_REG_TYPE_MAX,                 /* 最大标识 */
} FrameCfgRegType;

/* 配置寄存器类型 */
typedef struct
{
    FrameCfgRegType         TypeReg;        /* 类型寄存器 */
    u8                      DetailedReg;    /* 详细寄存器 */
    FrameCfgChTypedef       ChReg;          /* 通道寄存器 */
    FrameCfgPartakeTypedef  PartakeRegs;    /* 参与反馈的寄存器 */
} FrameCfgRegTypedef;


/* 配置命令结构 */
typedef struct
{
    CmdAttrTypedef          CmdAttr;                            /* 控制字 */
    FrameCfgRegTypedef      RegAttr;                            /* 寄存器 */
    u8                      CfgAttrBuff[FRAME_CFG_DATA_LEN];    /* 配置参数 */
    u16                     CfgBuffLen;                         /* 数据长度 */
} FrameCfgTypedef;



#define         CAN_FRAME_BIT_VERSION       0x00800000      /* 版本号 */
#define         CAN_FRAME_BIT_DIR           0x00400000      /* 方向 */
#define         CAN_FRAME_BIT_REG           0x00200000      /* 类型 */
#define         CAN_FRAME_BIT_OPT           0x00100000      /* 操作 */
#define         CAN_FRAME_BIT_SRC           0x000C0000      /* 源地址 */
#define         CAN_FRAME_BIT_DES           0x00030000      /* 目的地址 */


#define         CAN_FRAME_BIT_SRC_NONE      0x00000000      /* 帧源地址-空 */
#define         CAN_FRAME_BIT_SRC_MAC       0x00040000      /* 帧源地址-MAC */
#define         CAN_FRAME_BIT_SRC_UNIT      0x00080000      /* 帧源地址-UNIT */
#define         CAN_FRAME_BIT_SRC_APP       0x000C0000      /* 帧源地址-App */

#define         CAN_FRAME_BIT_DES_NONE      0x00000000      /* 帧目的地址-空 */
#define         CAN_FRAME_BIT_DES_MAC       0x00010000      /* 帧目的地址-MAC */
#define         CAN_FRAME_BIT_DES_UNIT      0x00020000      /* 帧目的地址-UNIT */
#define         CAN_FRAME_BIT_DES_APP       0x00030000      /* 帧目的地址-APP */


#define         FRAME_CMD_BIT_FRAME         0x80            /* 控制字 帧类型 */
#define         FRAME_CMD_BIT_DIR           0x40            /* 控制字 方向 */
#define         FRAME_CMD_BIT_REG           0x20            /* 控制字 寄存器类型 */
#define         FRAME_CMD_BIT_OPT           0x10            /* 控制字 操作类型 */
#define         FRAME_CMD_BIT_ADDR_SRC      0x0C            /* 控制字 源地址类型 */
#define         FRAME_CMD_BIT_ADDR_DES      0x03            /* 控制字 目的地址类型 */

#define         FRAME_CMD_SRC_NONE          0x00            /* 源地址 空 */
#define         FRAME_CMD_SRC_MAC           0x04            /* 源地址 MAC地址 */
#define         FRAME_CMD_SRC_UNIT          0x08            /* 源地址 UNIT地址 */
#define         FRAME_CMD_SRC_APP           0x0C            /* 源地址 APP地址 */

#define         FRAME_CMD_DES_NONE          0x00            /* 目的地址 空 */
#define         FRAME_CMD_DES_MAC           0x01            /* 目的地址 MAC地址 */
#define         FRAME_CMD_DES_UNIT          0x02            /* 目的地址 UNIT地址 */
#define         FRAME_CMD_DES_APP           0x03            /* 目的地址 APP地址 */


#define     CAN_DB_DATA_LEN_MIN     1
#define     CAN_DB_DATA_LEN_MAX     8

#define     IS_CAN_DB_DATA_LEN(PARAM)               (((PARAM) >= CAN_DB_DATA_LEN_MIN)       &&  \
                                                     ((PARAM) <= CAN_DB_DATA_LEN_MAX))
#define     IS_SINGLE_CNL_DATA_LEN(PARAM)           (((PARAM) <= CNL_CMD_LONG_BYTE_LEN)     &&   \
                                                     ((PARAM) >= CNL_CMD_SHORT_BYTE_LEN))

#define     IS_SINGLE_CNL_DATA_LEN_LIMIT(PARAM)     (((PARAM) <= CNL_CMD_LONG_BYTE_LEN))


#define     APP_CNL_CMD_LONG_BYTE_LEN       6
#define     APP_CNL_CMD_SHORT_BYTE_LEN      2


#define CFG_REG_TYPE_RESERVED       0           /* 预留 */
#define CFG_REG_TYPE_ADDR           1           /* 地址信息 */
#define CFG_REG_TYPE_TAG            2           /* 标签信息 */
#define CFG_REG_TYPE_BOOTLOADER     3           /* Bootloader */
#define CFG_REG_TYPE_FIRMWARE       4           /* 固件信息 */
#define CFG_REG_TYPE_HARDWARE       5           /* 硬件信息 */
#define CFG_REG_TYPE_INPUT          6           /* 输入信息 */
#define CFG_REG_TYPE_OUTPUT         7           /* 输出信息 */
#define CFG_REG_TYPE_SCENE          8           /* 场景信息 */
#define CFG_REG_TYPE_INTERFACE      9           /* 接口信息 */
#define CFG_REG_TYPE_MAX            10           /* 最大标识 */



/* 地址寄存器详细标识 */
#define     CFG_ADDR_REG_DETAILED_ALL                   0
#define     CFG_ADDR_REG_DETAILED_SN                    1
#define     CFG_ADDR_REG_DETAILED_UNIT                  2
#define     CFG_ADDR_REG_DETAILED_LIMIT                 3

/* 标签寄存器详细标识 */
#define     CFG_TAG_REG_DETAILED_RESERVED               0
#define     CFG_TAG_REG_DETAILED_DEVICE                 1
#define     CFG_TAG_REG_DETAILED_LIMIT                  2

/* Bootloader寄存器详细标识 */
#define     CFG_BOOTLOADER_REG_DETAILED_ALL             0
#define     CFG_BOOTLOADER_REG_DETAILED_NAME            1
#define     CFG_BOOTLOADER_REG_DETAILED_VERSION         2
#define     CFG_BOOTLOADER_REG_DETAILED_TIME            3
#define     CFG_BOOTLOADER_REG_DETAILED_AUTHOR          4
#define     CFG_BOOTLOADER_REG_DETAILED_LIMIT           5

/* Firmware寄存器详细标识 */
#define     CFG_FIRMWARE_REG_DETAILED_ALL               0
#define     CFG_FIRMWARE_REG_DETAILED_NAME              1
#define     CFG_FIRMWARE_REG_DETAILED_VERSION           2
#define     CFG_FIRMWARE_REG_DETAILED_TIME              3
#define     CFG_FIRMWARE_REG_DETAILED_AUTHOR            4
#define     CFG_FIRMWARE_REG_DETAILED_SN                5
#define     CFG_FIRMWARE_REG_DETAILED_LIMIT             6

/* Hardware寄存器详细标识 */
#define     CFG_HARDWARE_REG_DETAILED_ALL               0
#define     CFG_HARDWARE_REG_DETAILED_NAME              1
#define     CFG_HARDWARE_REG_DETAILED_VERSION           2
#define     CFG_HARDWARD_REG_DETAILED_LIMIT             6

/* 输入寄存器详细标识 */
#define     CFG_INPUT_REG_DETAILED_RESERVED             0
#define     CFG_INPUT_REG_DETAILED_KEY                  1
#define     CFG_INPUT_REG_DETAILED_ILLUMINANCE          0x0E
#define     CFG_INPUT_REG_DETAILED_IR_SENSOR            0x0F
#define     CFG_INPUT_REG_DETAILED_LIMIT                2

/* 输入寄存器通道标识 */
#define     CFG_INPUT_REG_CHANNEL_ALL                   0
#define     CFG_INPUT_REG_CHANNEL_MIN                   1
#define     CFG_INPUT_REG_CHANNEL_MAX                   32
/* 红外传感器寄存器标识 */
#define     CFG_INPUT_REG_IR_SENSOR_SUB_CATCH           1
#define     CFG_INPUT_REG_IR_SENSOR_SUB_LOGIC           2
#define     CFG_INPUT_REG_IR_SENSOR_SUB_FORMULA         3
/* 红外传感器复合公式通道标识 */
#define     CFG_INPUT_REG_IR_SENSOR_FORMULA_CH_RESERVED 0
#define     CFG_INPUT_REG_IR_SENSOR_FORMULA_CH_MIN      1
#define     CFG_INPUT_REG_IR_SENSOR_FORMULA_CH_MAX      16

/* 照度传感器寄存器标识 */
#define     CFG_INPUT_REG_ILLUMINANCE_SUB_RESERVED      0
#define     CFG_INPUT_REG_ILLUMINANCE_SUB_TRIGGET       1
#define     CFG_INPUT_REG_ILLUMINANCE_SUB_GET_VALUE     2

/* 照度传感器触发参数通道号 */
#define     CFG_INPUT_REG_ILLUMINANCE_TRIGGET_CH_RESERVED   0
#define     CFG_INPUT_REG_ILLUMINANCE_TRIGGET_CH_MIN        1
#define     CFG_INPUT_REG_ILLUMINANCE_TRIGGET_CH_MAX        2

/* 输出寄存器详细标识 */
#define     CFG_OUTPUT_REG_DETAILED_RESERVED            0
#define     CFG_OUTPUT_REG_DETAILED_DIMMER              1
#define     CFG_OUTPUT_REG_DETAILED_RELAY               2
#define     CFG_OUTPUT_REG_DETAILED_CURTAIN             3
#define     CFG_OUTPUT_REG_DETAILED_LIMIT               4

/* 输出寄存器通道标识 */
#define     CFG_OUTPUT_REG_CHANNEL_ALL                  0
#define     CFG_OUTPUT_REG_CHANNEL_MIN                  1
#define     CFG_OUTPUT_REG_CHANNEL_MAX                  32

/* 场景寄存器通道标识 */
#define     CFG_SCENE_REG_DETAILED_RESERVED             0
#define     CFG_SCENE_REG_DETAILED_MIN                  1
#define     CFG_SCENE_REG_DETAILED_MAX                  255

/* 接口寄存器通道标识 */
#define     CFG_INTERFACE_REG_DETAILED_RESERVED             0
#define     CFG_INTERFACE_REG_DETAILED_CAN                  1
#define     CFG_INTERFACE_REG_DETAILED_RF                   2
#define     CFG_INTERFACE_REG_DETAILED_RF_SEC               3

#include "CAN_Driver.h"
#include "CAN_Packet.h"


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */


extern  ErrorStatus             Frame_GetCmdAttr                            (u8 cmdByte, CmdAttrTypedef *cmdAttr);
extern  ErrorStatus             Frame_GetCmdByte                            (CmdAttrTypedef cmdAttr, u8 *cmdByte);

extern  ErrorStatus             Frame_GetSingleCnlFrame                     (AddrAttrTypedef desAddr, u8 cmdBuff[], uc8 cmdLen, FrameTypedef *userDat);
extern  ErrorStatus             Frame_GetSingleReadCnlFrame                 (AddrAttrTypedef desAddr, u8 cmdBuff[], uc8 cmdLen, FrameTypedef *userDat);
extern  ErrorStatus             Frame_GetSingleFeedbackCnlFrame             (AddrAttrTypedef desAddr, u8 cmdBuff[], uc8 cmdLen, FrameTypedef *userDat);

extern  ErrorStatus             Frame_SingleFrameGetCanDb                   (FrameTypedef userDat, CAN_DataBase *sDB);
extern  ErrorStatus             Frame_CanDbGetSingleFrame                   (CAN_DataBase cDB, FrameTypedef *userDat);


extern  ErrorStatus             Frame_FrameGetCfgFrame                      (FrameTypedef singleFrame, FrameCfgTypedef *cfgFrame);
extern  ErrorStatus             Frame_PacketGetCfgFrame                     (CAN_PacketBase gpacket,FrameCfgTypedef *cfgFrame);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __FRAME_H__ */
