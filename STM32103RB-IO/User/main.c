/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : main.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : 主文件
  函数列表   :
              main
              Mcu_ApplicationPoll
              Mcu_DeviceInit
              Mcu_DevicePoll
              Mcu_DriverInit
              Mcu_DriverPoll
              Mcu_Init
              Mcu_NvicInit
              Mcu_SendCanDat
              Mcu_Statistics
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/
// CAN 状态参数
extern  CAN_StatParaTypedef     CanStatPara;

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
 
//  设置默认的单元地址
uc16 SystemUnitDefaultValue __attribute__((at(MCU_UNIT_ADDR_DEFAULT_MMY_ADDR))) = SYSTEM_UNIT_DEFAULT_VALUE;
    
//  设置默认的CAN波特率
uc8   SystemCan1BtrDefauleValue __attribute__((at(MCU_CAN1_BTR_DEFAULT_MMY_ADDR))) = SYSTEM_CAN1_USER_BTR;


/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : assert_app_failed
 功能描述  : 打印错误
 输入参数  : uint8_t* file
             uint32_t line
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月17日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
#ifdef USE_APP_FULL_ASSERT

void assert_app_failed(uint8_t* file, uint32_t line)
{
    printf("Wrong App parameters value: file %s on line %d\r\n", file, line);
    while(1);
}

#endif

void Delay_us(uc32 us)
{
    vu32 dly_Tick = 0;

    if(us == 0)
    {
        return;
    }

    dly_Tick = us*72;

    while(--dly_Tick);
}

/*****************************************************************************
 函 数 名  : Mcu_NvicInit
 功能描述  : 初始化MCU的NVIC
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月16日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Mcu_NvicInit(void)
{
    /* NVIC向量表 映射到FLASH的0x00处 */
    NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0);
    /* NVIC的中断优先级组选择组1 */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
}

/*****************************************************************************
 函 数 名  : Mcu_Init
 功能描述  : 初始化MCU运行环境
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月16日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Mcu_Init(void)
{
    /* 初始化NVIC的向量表 */
    Mcu_NvicInit();
    /* 使能AFIO时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    /* 使能SWD 关闭JTAG */
    GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
    /* 使能CRC计算的时钟线 */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
}

/*****************************************************************************
 函 数 名  : Mcu_DriverInit
 功能描述  : 初始化MCU的外设驱动
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月16日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Mcu_DriverInit(void)
{
    /* 初始化整个MCU的运行环境 */
    Mcu_Init();
    /* 初始化MSystick */
    Systick_Init();

    /* 初始化随机数发生器 */
    Random_Init();

    /* 初始化CAN */
    CAN_DriverInit();
    /* 设置CAN的波特率为500K */
    CAN_SetBtr(CAN_1 , (CAN_BtrType)SystemCan1BtrDefauleValue);

    /* 初始化UART3_485 */
    //UART3_Init();


    /* 初始化PWM 的 GPIO */
    //PWM_GpioInit();
    /* 初始化PWM的 运行参数 */
    //PWM_RunParaInit();

    /* 初始化继电器 的 GPIO */
    //Relay_GpioInit();
    /* 初始化继电器的 运行参数 */
    //Relay_InitRunPara();

    /* 初始化KEY 的GPIO */
    //KEY_GpioInit();
    /* 初始化KEY 的运行参数 */
    //KEY_RunParaInit();


    /* 初始化LED 的GPIO */
    //LED_GpioInit();
    /* 初始化LED 的运行参数 */
    //LED_InitRunPara();

    /* 初始化LED 指示的GPIO */
    LedIndicator_GpioInit();
    /* 初始化LED 指示的运行参数 */
    LedIndicator_InitRunPara();

    /* 初始化 IRS 的 GPIO */
    Irs_GpioInit();
}



/*****************************************************************************
 函 数 名  : Mcu_DeviceInit
 功能描述  : 初始化外部设备和应用组件
 输入参数  : 无
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月16日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Mcu_DeviceInit(void)
{
    /* 初始化Tmm的运行参数 */
    Tmm_Init();

    /* 初始化计时器的运行参数 */
    TmmCnt_Init();

    /* 初始化TmmExp的运行参数 */
    TmmExp_Init();

    /* 初始化CAN的运行参数 */
    CAN_InitRunPara();

    /* 初始化虚拟地址运行参数 */
    VirtualAddr_Init();

    /* 初始化PWM 输出逻辑 */
    //PwmLogic_Init();

    /* 初始化继电器 输出逻辑 */
    //RelayLogic_Init();

    /* 初始化按键 捕获逻辑 */
    //KeyLogic_Init();

    /* 初始化LED 指示逻辑 */
    //LedLogic_Init();

    /* 初始化 红外捕获的 运行参数 */
    Irs_InitRunPara();

    /* 初始化 红外逻辑的 运行参数  */
    IrsLogic_Init();

    /* 初始化CC1101 */
    CC1101_Init();
    /* 初始化CSMA */
    CSMA_Init();

    // 初始化接口逻辑
    ItfLogic_Init();

    /* 初始化地址运行参数 */
    AddrMmy_RunParaInit();

    /* 初始化BH1621 */
    BH1621_Init();

    /* 初始化照度计应用 */
    BeamApp_Init();
}

/*****************************************************************************
 函 数 名  : Mcu_DriverPoll
 功能描述  : MCU外设驱动执行函数
 输入参数  : 无
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月16日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Mcu_DriverPoll(void)
{
    /* 执行CAN消息管理 */
    CAN_MsgPoll();

    /* 执行UART3消息管理 */
    //UART3_Poll();

    /* 执行按键事件捕获 */
    //KEY_Poll();

    /* 执行LED输出 */
    //LED_Poll();

    /* 执行继电器输出 */
    //Relay_OutPoll();

    /* 执行LED指示输出 */
    LedIndicator_Poll();

    /* 执行红外状态捕获逻辑 */
    Irs_CatchPoll();
}

/*****************************************************************************
 函 数 名  : Mcu_DevicePoll
 功能描述  : MCU外部设备和应用组件执行函数
 输入参数  : 无
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月16日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Mcu_DevicePoll(void)
{
    /* 执行Tmm组件 */
    Tmm_Poll();
    /* 执行TmmExp组件 */
    TmmExp_Poll();

    /* 执行接收命令 */
    Receive_Poll();

    /* 执行CC1101 */
    CC1101_Impl();
    /* 执行CSMA */
    CSMA_Impl();

    /* 执行虚拟地址 */
    VirtualAddr_Poll();

    /* 执行PWM输出逻辑 */
    //PwmLogic_UpdataLogicPoll();

    /* 执行继电器输出逻辑 */
    //RelayLogic_OutLogicPoll();

    /* 执行按键解析逻辑 */
    //KeyLogic_AnalyzePoll();

    /* 执行LED输出逻辑 */
    //LedLogic_OutLogicPoll();

    /* 执行红外传感器逻辑控制 */
    IrsLogic_Poll();

    // 执行RF命令
    RF_CmdPoll();
    // 执行队列发送任务
    ItfQueue_Poll();

    /* 执行状态获取 */
    StatLogic_StatReGetPoll();

    // 执行BH1621
    BH1621_Poll();

    /* 执行照度计应用 */
    BeamApp_Poll();
}

// MCU统计参数
u32 Mcu_MinCnt = 0xFFFFFFFF;
u32 Mcu_MaxCnt = 0;
u32 Mcu_CpuCnt = 0;

/*****************************************************************************
 函 数 名  : Mcu_Statistics
 功能描述  : MCU统计函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Mcu_Statistics(void)
{
    if(Tmm_IsON(TMM_TONGJI) == RESET)
    {
        /* 载入延时1000ms */
        Tmm_LoadMs(TMM_TONGJI, 1000);

        if(Mcu_CpuCnt < Mcu_MinCnt)
        {
            Mcu_MinCnt = Mcu_CpuCnt;
        }

        if(Mcu_CpuCnt > Mcu_MaxCnt)
        {
            Mcu_MaxCnt = Mcu_CpuCnt;
        }

        Mcu_CpuCnt = 0;
    }
    else
    {
        Mcu_CpuCnt++;
    }
}

RCC_ClocksTypeDef SystemClock;

/*****************************************************************************
 函 数 名  : main
 功能描述  : 主函数
 输入参数  : 无
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月16日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
int main(void)
{
    // 定义寄存器状态 变量
    CPU_SR_ALLOC();

    // 关闭所有中断
    CPU_INT_DIS();

    /* 初始化MCU的运行环境和外设驱动 */
    Mcu_DriverInit();
    /* 初始化外部设备和应用组件 */
    Mcu_DeviceInit();

    /* 载入延时1000ms */
    Tmm_LoadMs(TMM_TONGJI, 1000);

    RCC_GetClocksFreq(&SystemClock);

    // 打开所有中断
    CPU_INT_EN();

    while(1)
    {
        /* 执行MCU外设驱动 */
        Mcu_DriverPoll();
        /* 执行MCU外部设备和应用组件 */
        Mcu_DevicePoll();

        /* 执行MCU统计 */
        Mcu_Statistics();
    }
}

