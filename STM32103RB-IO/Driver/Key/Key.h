/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Key.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月2日
  最近修改   :
  功能描述   : Key.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __KEY_H__
#define __KEY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    KEY_STATE_IDLE    =   0,        /* 空闲模式 */
    KEY_STATE_BOUNCE     ,          /* 去抖动模式 *///1
    KEY_STATE_RISE       ,          /* 获取到按键按下 */
    KEY_STATE_HIGH       ,          /* 等待按键释放 */
    KEY_STATE_SHORTINGL  ,          /* 等待按键再次按下 */
    KEY_STATE_SHORTINGH  ,          /* 等待按键再次释放 */
    KEY_STATE_SHORTEND   ,          /* 按键短按释放 */
    KEY_STATE_LONGING    ,          /* 按键长按持续 */
    KEY_STATE_LONGEND    ,          /* 按键长按释放 */
    KEY_STATE_LLONGING   ,          /* 按键超长按持续 */
    KEY_STATE_LLONGEND   ,          /* 按键超长按结束 */
} KeyStatTypedef;

typedef enum
{
    KEY_PARA_TIME_BOUNCE = 0,
    KEY_PARA_T1,
    KEY_PARA_T2,
    KEY_PARA_T3,
} KeyParaTypedef;

typedef struct
{
    FlagStatus      GetIn;
    KeyStatTypedef  KeyStat;
    Tmm_ID          TmmId;
    u8 KeyShortNum;
}KeyAttrTypedef;


/* 按键最大的使用 */
#define     KEY_CFG_MAX_NUM         MCU_KEY_CHANNEL_NUM
#define     KEY_CFG_USER_NUM        MCU_KEY_CHANNEL_NUM
#define     KEY_ID_LIMIT_START      1
#define     KEY_ID_LIMIT_END        KEY_CFG_USER_NUM

#define     KEY_PARA_VALUE_LEN      7


/* 按键ID */
#define     KEY1                    1
#define     KEY2                    2
#define     KEY3                    3


#define     KEY_Pin_1               PA_3
#define     KEY_Pin_2               PA_2
#define     KEY_Pin_3               PA_0



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */


extern  void                KEY_GpioInit                        (void);
extern  void                KEY_RunParaInit                     (void);
extern  u32                 KEY_GetPara                         (KeyParaTypedef keyParaId);
extern  FlagStatus          KEY_IsKeyId                         (uc8 keyId);
extern  ErrorStatus         KEY_SetSimulantStat                 (uc8 sKeyId, KeyStatTypedef sStat);
extern  ErrorStatus         KEY_Clear                           (uc8 sKeyId);
extern  KeyStatTypedef      KEY_GetStat                         (uc8 gKeyId, u8 *gShortNum);
extern  u8                  KEY_GetShortNum                     (uc8 gKeyId);
extern  FlagStatus          KEY_IsRelease                       (KeyStatTypedef kStat);
extern  FlagStatus          KEY_IsPress                         (KeyStatTypedef kStat);
extern  FlagStatus          KEY_IsShortRelease                  (KeyStatTypedef kStat);
extern  FlagStatus          KEY_IsPressRelease                  (KeyStatTypedef kStat);
extern  void                KEY_Poll                            (void);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __KEY_H__ */
