/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Key_Logic.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月10日
  最近修改   :
  功能描述   : 按键逻辑操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/
// CAN 状态参数
extern  CAN_StatParaTypedef     CanStatPara;

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static KeyRunAttrTypedef    KeyRunAttr[KEY_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : KeyLogic_Init
 功能描述  : 按键 逻辑参数初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void KeyLogic_Init(void)
{
    u8 i;

    // 初始化存储参数
    KeyMmy_RunParaInit();

    // 循环获取运行参数
    for(i=0; i<KEY_CFG_USER_NUM; i++)
    {
        // 获取回路的运行参数
        if(KeyLogic_GetMmyAttr(i) == SUCCESS)
        {
            // 加入地址解析
            KeyLogic_AddrAnalyzeInit(i);
        }

        // 设置之前的状态为 空闲状态
        KeyRunAttr[i].BeforeKeyStat = KEY_STATE_IDLE;

        // 实际值初始化为0
        KeyRunAttr[i].RealLevel = 0;

        // 设置按键的调光方向为未知
        KeyRunAttr[i].DimDir    = DIMMER_DIR_UNKOWN;
    }
}

/*****************************************************************************
 函 数 名  : KeyLogic_AddrAnalyzeInit
 功能描述  : Key地址逻辑 加入地址（APP+GROUP）
 输入参数  : uc8 keyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月13日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_AddrAnalyzeInit(uc8 keyId)
{
    // 判断ID是否为App+Group
    if(KeyRunAttr[keyId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        // 加入地址
        VirtualAddr_Join(KeyRunAttr[keyId].portAddr);
    }

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : KeyLogic_GetMmyAttr
 功能描述  : 获取按键逻辑参数
 输入参数  : uc8 keyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GetMmyAttr(uc8 keyId)
{
    // 判断ID是否符合范围
    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取地址类型
    if(KeyMmy_GetAddrCatch(keyId, &KeyRunAttr[keyId].AddrFrom) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路存储的地址
    if(KeyMmy_GetAddr(keyId, &KeyRunAttr[keyId].portAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取短按按下 按键功能
    if(KeyMmy_GetShortPressFtn(keyId, &KeyRunAttr[keyId].ShortPressFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].ShortPressFtn = KEY_CNL_FTN_IDEL;
    }
    // 获取短按释放 按键功能
    if(KeyMmy_GetShortReleaseFtn(keyId, &KeyRunAttr[keyId].ShortReleaseFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].ShortReleaseFtn = KEY_CNL_FTN_IDEL;
    }
    // 获取长按按下 按键功能
    if(KeyMmy_GetLongPressFtn(keyId, &KeyRunAttr[keyId].LongPressFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].LongPressFtn = KEY_CNL_FTN_IDEL;
    }
    // 获取长按释放 按键功能
    if(KeyMmy_GetLongReleaseFtn(keyId, &KeyRunAttr[keyId].LongReleaseFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].LongReleaseFtn = KEY_CNL_FTN_IDEL;
    }
    // 获取超长按按下 按键功能
    if(KeyMmy_GetLongLongPressFtn(keyId, &KeyRunAttr[keyId].LongLongPressFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].LongLongPressFtn = KEY_CNL_FTN_IDEL;
    }
    // 获取超长按释放 按键功能
    if(KeyMmy_GetLongLongReleaseFtn(keyId, &KeyRunAttr[keyId].LongLongReleaseFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].LongLongReleaseFtn = KEY_CNL_FTN_IDEL;
    }

    // 获取多次短按 触发次数
    if(KeyMmy_GetShortCntNum(keyId, &KeyRunAttr[keyId].ShortCntNum) != SUCCESS)
    {
        KeyRunAttr[keyId].ShortCntNum = 0;
    }

    // 获取多次短按 按键功能
    if(KeyMmy_GetShortCntFtn(keyId, &KeyRunAttr[keyId].ShortCntFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].ShortCntFtn = KEY_CNL_FTN_IDEL;
    }

    // 获取定时器 超时功能
    if(KeyMmy_GetExpiryTimeFtn(keyId, &KeyRunAttr[keyId].ExpiryTimeFtn) != SUCCESS)
    {
        KeyRunAttr[keyId].ExpiryTimeFtn = KEY_CNL_FTN_IDEL;
    }

    // 获取Recall值 1
    if(KeyMmy_GetRecall1Value(keyId, &KeyRunAttr[keyId].Recall1) != SUCCESS)
    {
        KeyRunAttr[keyId].Recall1 = 255;
    }
    // 获取Recall值 2
    if(KeyMmy_GetRecall2Value(keyId, &KeyRunAttr[keyId].Recall2) != SUCCESS)
    {
        KeyRunAttr[keyId].Recall2 = 255;
    }
    // 获取Recall值 3
    if(KeyMmy_GetRecall3Value(keyId, &KeyRunAttr[keyId].Recall3) != SUCCESS)
    {
        KeyRunAttr[keyId].Recall3 = 255;
    }
    // 获取Recall值 4
    if(KeyMmy_GetRecall4Value(keyId, &KeyRunAttr[keyId].Recall4) != SUCCESS)
    {
        KeyRunAttr[keyId].Recall4 = 255;
    }

    // 获取 按键触发列表
    if(KeyMmy_GetKeyTriggerMap(keyId, &KeyRunAttr[keyId].KeyTriggerMap) != SUCCESS)
    {
        KeyRunAttr[keyId].KeyTriggerMap = (1 << keyId);
    }
    // 获取 定时器设置时间
    if(KeyMmy_GetExpiryTimeValue(keyId, &KeyRunAttr[keyId].ExpiryTime) != SUCCESS)
    {
        KeyRunAttr[keyId].ExpiryTime = 0;
    }
    // 获取 调光定时时间
    if(KeyMmy_GetRampTimeValue(keyId, &KeyRunAttr[keyId].RampTime) != SUCCESS)
    {
        KeyRunAttr[keyId].RampTime = 5;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyLogic_UpdateRealLevel
 功能描述  : 按键逻辑 更新实际值
 输入参数  : uc8 keyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_UpdateRealLevel(uc8 keyId)
{
    u8 tmpLevel;

    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    // 判断地址类型是否为自身的
    if(KeyRunAttr[keyId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        // 获取自身回路存储的地址 的状态
        if(VirtualAddr_GetVaRealLevel(KeyRunAttr[keyId].portAddr, &tmpLevel) == SUCCESS)
        {
            KeyRunAttr[keyId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(KeyRunAttr[keyId].AddrFrom.CatchFrom == ADDR_CATCH_PWM)
    {
        // 获取PWM实际值

        return ERROR;
    }
    else if(KeyRunAttr[keyId].AddrFrom.CatchFrom == ADDR_CATCH_RELAY)
    {
        // 获取继电器的实际值

        return ERROR;
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : KeyLogic_GetRealLevel
 功能描述  : 按键逻辑 获取按键实际值
 输入参数  : uc8 keyId
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GetRealLevel(uc8 keyId, u8 *gLevel)
{
    if(gLevel == NULL)
    {
        return ERROR;
    }

    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取实际值
    *gLevel = KeyRunAttr[keyId].RealLevel;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyLogic_LoadCnlCmd
 功能描述  : Key逻辑 载入控制命令
 输入参数  : uc8 keyId
             u8 sBuff[]
             u8 sLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月13日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_LoadCnlCmd(uc8 keyId, u8 sBuff[], u8 sLen)
{
    FrameCnlTypedef sFrame;
    CAN_DataBase    sDb;
    FrameAddrAttrTypedef desAddr;

    if(sBuff == NULL)
    {
        return ERROR;
    }

    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    if(KeyRunAttr[keyId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        /* 寻找自身地址 发送 */
        // 载入控制命令到本模块的虚拟地址命令内
        VirtualAddr_VaLoadCmd(KeyRunAttr[keyId].portAddr, sBuff, sLen);

        if(sLen <= FRAME_CNL_DATA_LEN)
        {
            // 地址类型设为 APP+GROUP
            desAddr.Type = FRAME_ADDR_TYPE_APP;
            // 载入目的地址 APP
            desAddr.AddrBuff[0] = KeyRunAttr[keyId].portAddr.App;
            // 载入目的地址 GROUP
            desAddr.AddrBuff[1] = KeyRunAttr[keyId].portAddr.Group;

            // 载入控制命令 获取控制帧
            if(Frame_CmdGetUserData(desAddr, sBuff, sLen, &sFrame) == SUCCESS)
            {
                // 获取CAN控制帧
                if(Frame_UserGetCnlData(sFrame, &sDb) == SUCCESS)
                {
                    // 载入控制帧到发送队列
                    return CAN_QueuePush(&(CanStatPara.SendDB), &sDb);
                }
            }
        }
        else
        {
        }

        return ERROR;
    }
    else if(KeyRunAttr[keyId].AddrFrom.CatchFrom == ADDR_CATCH_PWM)
    {
        // 载入控制命令到本地PWM回路

        return ERROR;
    }
    else if(KeyRunAttr[keyId].AddrFrom.CatchFrom == ADDR_CATCH_RELAY)
    {
        // 载入控制命令到本地RELAY回路

        return ERROR;
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_GenerateAbsCnlCmd
 功能描述  : 按键逻辑 产生绝对控制命令
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef keyCnlCmd
             u8 cmdBuff[]
             u8 *cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GenerateAbsCnlCmd(uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen)
{
    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    if((cmdBuff == NULL) || (cmdLen == NULL))
    {
        return ERROR;
    }

    // 获取 ON KEY 的 CMD
    if(keyCnlCmd == KEY_CNL_FTN_ON_KEY)
    {
        return CnlCmd_GetCnlBuff(CNL_CMD_TYPE_ON, cmdBuff, cmdLen);
    }
    // 获取 OFF KEY 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_OFF_KEY)
    {
        return CnlCmd_GetCnlBuff(CNL_CMD_TYPE_OFF, cmdBuff, cmdLen);
    }
    // 获取 Toggle KEY 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_TOGGLE)
    {
        if(KeyRunAttr[keyId].RealLevel > 0)
        {
            return CnlCmd_GetCnlBuff(CNL_CMD_TYPE_OFF, cmdBuff, cmdLen);
        }
        else
        {
            return CnlCmd_GetCnlBuff(CNL_CMD_TYPE_ON, cmdBuff, cmdLen);
        }
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_GenerateGmRampCnlCmd
 功能描述  : 按键逻辑 产生通用调光命令
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef keyCnlCmd
             u8 cmdBuff[]
             u8 *cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GenerateGmRampCnlCmd(uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen)
{
    u8 tim;

    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    if((cmdBuff == NULL) || (cmdLen == NULL))
    {
        return ERROR;
    }

    // 获取 设置的调光时间
    tim = KeyRunAttr[keyId].RampTime;

    // 判断调光时间是否为0
    if(tim == 0)
    {
        // 载入系统默认的调光时间
        tim = CNL_CMD_DEFAULT_RAMP_TIM;
    }

    // 获取 RAMP UP 的 CMD
    if(keyCnlCmd == KEY_CNL_FTN_RAMP_UP)
    {
        // 载入向上调光命令
        return CnlCmd_GetRampBuff(0xFF, tim, cmdBuff, cmdLen);
    }
    // 获取 RAMP DOWN 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RAMP_DOWN)
    {
        // 载入向下调光命令
        return CnlCmd_GetRampBuff(0x00, tim, cmdBuff, cmdLen);
    }
    // 获取 RAMP END 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RAMP_END)
    {
        return CnlCmd_GetCnlBuff(CNL_CMD_TYPE_RAMP_STOP, cmdBuff, cmdLen);
    }
    else if(keyCnlCmd == KEY_CNL_FTN_RAMP_CYCLE)
    {
        if(KeyRunAttr[keyId].RealLevel == 0x00)
        {
            // 调光方向为向上调光
            KeyRunAttr[keyId].DimDir = DIMMER_DIR_UP;

            // 载入向上调光命令
            return CnlCmd_GetRampBuff(0xFF, tim, cmdBuff, cmdLen);
        }
        else if(KeyRunAttr[keyId].RealLevel == 0xFF)
        {
            // 调光方向为向下调光
            KeyRunAttr[keyId].DimDir = DIMMER_DIR_DOWN;

            // 载入向下调光命令
            return CnlCmd_GetRampBuff(0x00, tim, cmdBuff, cmdLen);
        }
        else
        {
            // 判断之前是否为 向上调光模块
            if(KeyRunAttr[keyId].DimDir == DIMMER_DIR_UP)
            {
                // 调光方向为向下调光
                KeyRunAttr[keyId].DimDir = DIMMER_DIR_DOWN;

                // 载入向下调光命令
                return CnlCmd_GetRampBuff(0x00, tim, cmdBuff, cmdLen);
            }
            else
            {
                // 调光方向为向上调光
                KeyRunAttr[keyId].DimDir = DIMMER_DIR_UP;

                // 载入向上调光命令
                return CnlCmd_GetRampBuff(0xFF, tim, cmdBuff, cmdLen);
            }
        }
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_GenerateAbsParaCnlCmd
 功能描述  : 按键逻辑  产生 带参数绝对控制命令
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef keyCnlCmd
             u8 cmdBuff[]
             u8 *cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GenerateAbsParaCnlCmd(uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen)
{
    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    if((cmdBuff == NULL) || (cmdLen == NULL))
    {
        return ERROR;
    }

    // 获取 RECALL1 的 CMD
    if(keyCnlCmd == KEY_CNL_FTN_RECALL_1)
    {
        return CnlCmd_GetAbsCnlBuff(KeyRunAttr[keyId].Recall1, cmdBuff, cmdLen);
    }
    // 获取 RECALL2 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RECALL_2)
    {
        return CnlCmd_GetAbsCnlBuff(KeyRunAttr[keyId].Recall2, cmdBuff, cmdLen);
    }
    // 获取 RECALL3 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RECALL_3)
    {
        return CnlCmd_GetAbsCnlBuff(KeyRunAttr[keyId].Recall3, cmdBuff, cmdLen);
    }
    // 获取 RECALL4 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RECALL_4)
    {
        return CnlCmd_GetAbsCnlBuff(KeyRunAttr[keyId].Recall4, cmdBuff, cmdLen);
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_GenerateRampParaCnlCmd
 功能描述  : 按键逻辑 产生带参数的调光命令
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef keyCnlCmd
             u8 cmdBuff[]
             u8 *cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GenerateRampParaCnlCmd(uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen)
{
    u8 tim;

    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    if((cmdBuff == NULL) || (cmdLen == NULL))
    {
        return ERROR;
    }

    // 获取 设置的调光时间
    tim = KeyRunAttr[keyId].RampTime;

    // 判断调光时间是否为0
    if(tim == 0)
    {
        // 载入系统默认的调光时间
        tim = CNL_CMD_DEFAULT_RAMP_TIM;
    }

    // 获取 RECALL1 的 CMD
    if(keyCnlCmd == KEY_CNL_FTN_RAMP_RECALL_1)
    {
        return CnlCmd_GetRampBuff(KeyRunAttr[keyId].Recall1, tim, cmdBuff, cmdLen);
    }
    // 获取 RECALL2 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RAMP_RECALL_2)
    {
        return CnlCmd_GetRampBuff(KeyRunAttr[keyId].Recall2, tim, cmdBuff, cmdLen);
    }
    // 获取 RECALL3 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RAMP_RECALL_3)
    {
        return CnlCmd_GetRampBuff(KeyRunAttr[keyId].Recall3, tim, cmdBuff, cmdLen);
    }
    // 获取 RECALL4 的 CMD
    else if(keyCnlCmd == KEY_CNL_FTN_RAMP_RECALL_4)
    {
        return CnlCmd_GetRampBuff(KeyRunAttr[keyId].Recall4, tim, cmdBuff, cmdLen);
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_GenerateSpecialCmd
 功能描述  : 按键逻辑 产生特殊控制命令
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef keyCnlCmd
             u8 cmdBuff[]
             u8 *cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GenerateSpecialCmd(uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen)
{
    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    if((cmdBuff == NULL) || (cmdLen == NULL))
    {
        return ERROR;
    }

    if(keyCnlCmd == KEY_CNL_FTN_ENTER_AUTO_MODE)
    {
        return CnlCmd_GetCnlBuff(CNL_CMD_TYPE_ENTER_AUTO, cmdBuff, cmdLen);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_GenerateCnlCmd
 功能描述  : 按键逻辑 产生控制命令
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef keyCnlCmd
             u8 cmdBuff[]
             u8 *cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GenerateCnlCmd(uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen)
{
    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    if((cmdBuff == NULL) || (cmdLen == NULL))
    {
        return ERROR;
    }

    // 绝对控制命令
    if((keyCnlCmd >= KEY_CNL_FTN_ON_KEY) && (keyCnlCmd <= KEY_CNL_FTN_TOGGLE))
    {
        return KeyLogic_GenerateAbsCnlCmd(keyId, keyCnlCmd, cmdBuff, cmdLen);
    }
    // 通用调光命令
    else if((keyCnlCmd >= KEY_CNL_FTN_RAMP_UP) && (keyCnlCmd <= KEY_CNL_FTN_RAMP_CYCLE))
    {
        return KeyLogic_GenerateGmRampCnlCmd(keyId, keyCnlCmd, cmdBuff, cmdLen);
    }
    // 参数绝对控制命令
    else if((keyCnlCmd >= KEY_CNL_FTN_RECALL_1) && (keyCnlCmd <= KEY_CNL_FTN_RECALL_4))
    {
        return KeyLogic_GenerateAbsParaCnlCmd(keyId, keyCnlCmd, cmdBuff, cmdLen);
    }
    // 参数调光命令
    else if((keyCnlCmd >= KEY_CNL_FTN_RAMP_RECALL_1) && (keyCnlCmd <= KEY_CNL_FTN_RAMP_RECALL_4))
    {
        return KeyLogic_GenerateRampParaCnlCmd(keyId, keyCnlCmd, cmdBuff, cmdLen);
    }
    // 特殊命令
    else if(keyCnlCmd == KEY_CNL_FTN_ENTER_AUTO_MODE)
    {
        return KeyLogic_GenerateSpecialCmd(keyId, keyCnlCmd, cmdBuff, cmdLen);
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_GenerateCnl
 功能描述  : 按键逻辑 生成控制命令
 输入参数  : uc8 keyId
             KeyStatTypedef keyStat
             uc8 shortCnt
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_GenerateCnl(uc8 keyId, KeyStatTypedef keyStat, uc8 shortCnt)
{
    ErrorStatus             retStat = SUCCESS;
    KeyCnlFunctionTypedef   gKeyFtn = KEY_CNL_FTN_IDEL;
    u8 cmdBuff[CNL_CMD_LONG_BYTE_LEN] = {0};
    u8 cmdLen;

    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    // 保存当前的状态 以备下一个时刻查询状态
    KeyRunAttr[keyId].BeforeKeyStat = keyStat;

    // 根据按键状态 -
    switch(keyStat)
    {
        // 短按按下事件 - 任何状态必经
        case KEY_STATE_HIGH:

            // 获取按键短按按下功能
            gKeyFtn = KeyRunAttr[keyId].ShortPressFtn;
            break;

        // 短按释放事件 - 多次短按释放也在此执行
        case KEY_STATE_SHORTEND:

            // 判断短按次数是否已配置 且 按键按下次数>=配置的短按次数
            if((KeyRunAttr[keyId].ShortCntNum >= 2) && (shortCnt >= KeyRunAttr[keyId].ShortCntNum))
            {
                // 获取按键多次短按次数功能
                gKeyFtn = KeyRunAttr[keyId].ShortCntFtn;
            }
            else
            {
                // 获取按键多次短按释放功能
                gKeyFtn = KeyRunAttr[keyId].ShortReleaseFtn;
            }
            break;

        // 长按按下事件 - 从短按按下事件演变
        case KEY_STATE_LONGING:

            // 获取按键长按按下功能
            gKeyFtn = KeyRunAttr[keyId].LongPressFtn;
            break;

        // 长按释放事件 - 从长按按下事件演变
        case KEY_STATE_LONGEND:

            // 获取按键长按释放功能
            gKeyFtn = KeyRunAttr[keyId].LongReleaseFtn;
            break;

        // 超长按按下事件 - 从长按按下事件演变
        case KEY_STATE_LLONGING:

            // 获取按键超长按按下功能
            gKeyFtn = KeyRunAttr[keyId].LongLongPressFtn;
            break;

        // 超长按释放事件 - 从超长按按下事件演变
        case KEY_STATE_LLONGEND:

            // 获取按键长按释放功能
            gKeyFtn = KeyRunAttr[keyId].LongLongReleaseFtn;
            break;

        default:
            retStat = ERROR;
            break;
    }

    // 判断获取状态是否成功
    if(retStat == SUCCESS)
    {
        // 获取控制命令
        if(KeyLogic_GenerateCnlCmd(keyId, gKeyFtn, cmdBuff, &cmdLen) == SUCCESS)
        {
            // 载入命令
            retStat = KeyLogic_LoadCnlCmd(keyId, cmdBuff, cmdLen);
        }
        else
        {
            retStat = ERROR;
        }
    }

    return retStat;
}

/*****************************************************************************
 函 数 名  : KeyLogic_Analyze
 功能描述  : 按键逻辑 按键解析函数
 输入参数  : uc8 keyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyLogic_Analyze(uc8 keyId)
{
    KeyStatTypedef tmpKeyStat = KEY_STATE_IDLE;
    u8 tmpShortNum  = 0;

    if(KEY_IsKeyId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取按键状态 以及 按键短按次数
    tmpKeyStat = KEY_GetStat(keyId, &tmpShortNum);

    // 判断之前的状态是否不等于 新获取的状态
    if(KeyRunAttr[keyId].BeforeKeyStat != tmpKeyStat)
    {
        // 获取实际值是否成功
        if(KeyLogic_UpdateRealLevel(keyId) == SUCCESS)
        {
            // 产生控制命令
            return KeyLogic_GenerateCnl(keyId, tmpKeyStat, tmpShortNum);
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyLogic_AnalyzePoll
 功能描述  : 按键逻辑 按键解析逻辑函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void KeyLogic_AnalyzePoll(void)
{
    u8 i = 0;

    for(i=0; i<KEY_CFG_USER_NUM; i++)
    {
        KeyLogic_Analyze(i);
    }
}


