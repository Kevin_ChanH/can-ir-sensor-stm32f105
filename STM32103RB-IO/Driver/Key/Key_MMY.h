/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Key_MMY.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月9日
  最近修改   :
  功能描述   : Key_MMY.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __KEY_MMY_H__
#define __KEY_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    DIMMER_DIR_UP = 0,
    DIMMER_DIR_DOWN,
    DIMMER_DIR_UNKOWN,
} DimmerDirTypedef;


typedef enum
{
    KEY_CNL_FTN_IDEL = 0,
    KEY_CNL_FTN_ON_KEY,
    KEY_CNL_FTN_OFF_KEY,
    KEY_CNL_FTN_TOGGLE,

    KEY_CNL_FTN_RAMP_UP = 10,
    KEY_CNL_FTN_RAMP_DOWN,
    KEY_CNL_FTN_RAMP_END,
    KEY_CNL_FTN_RAMP_CYCLE,

    KEY_CNL_FTN_MEM_TOGGLE = 20,
    KEY_CNL_FTN_RECALL_1,
    KEY_CNL_FTN_RECALL_2,
    KEY_CNL_FTN_RECALL_3,
    KEY_CNL_FTN_RECALL_4,

    KEY_CNL_FTN_RAMP_RECALL_1 = 30,
    KEY_CNL_FTN_RAMP_RECALL_2,
    KEY_CNL_FTN_RAMP_RECALL_3,
    KEY_CNL_FTN_RAMP_RECALL_4,

    KEY_CNL_FTN_TIME_RETRIGGER = 40,

    KEY_CNL_FTN_ENTER_AUTO_MODE = 240,
} KeyCnlFunctionTypedef;

typedef struct
{
    AddrFromTypedef         AddrFrom;                   /* 地址跟随类型 */
    RunAddrTypedef          portAddr;                   /* 端口存储地址 */

    KeyCnlFunctionTypedef   ShortPressFtn;              /* 短按按下 功能 */
    KeyCnlFunctionTypedef   ShortReleaseFtn;            /* 短按释放 功能 */
    KeyCnlFunctionTypedef   LongPressFtn;               /* 长按按下 功能 */
    KeyCnlFunctionTypedef   LongReleaseFtn;             /* 长按释放 功能 */
    KeyCnlFunctionTypedef   LongLongPressFtn;           /* 超长按按下 功能 */
    KeyCnlFunctionTypedef   LongLongReleaseFtn;         /* 超长按释放 功能 */
    KeyCnlFunctionTypedef   ShortCntFtn;                /* 多次短按 功能 */
    KeyCnlFunctionTypedef   ExpiryTimeFtn;              /* 定时器超时 功能 */

    u8                      ShortCntNum;                /* 短按次数 功能 */
    u8                      Recall1;                    /* 绝对参数值1 */
    u8                      Recall2;                    /* 绝对参数值2 */
    u8                      Recall3;                    /* 绝对参数值3 */
    u8                      Recall4;                    /* 绝对参数值4 */
    u8                      RampTime;                   /* 调光的执行时间 */
    u16                     ExpiryTime;                 /* 定时器 重载时间 */
    u32                     KeyTriggerMap;              /* 按键 触发按键的地图 */

    KeyStatTypedef          BeforeKeyStat;              /* 上一个按键的状态 */
    u8                      RealLevel;                  /* 实际值 */
    DimmerDirTypedef        DimDir;                     /* 调光方向 */
} KeyRunAttrTypedef;


/* KEY 存储参数 占23Byte */
#define     KEY_MMY_PARA_LEN            23                              /* 占用23Bytes配置字节 */
#define     KEY_MMY_DATA_TYPE           MMY_DATA_TYPE_MID               /* 存储数据类型为中等类型 */
#define     KEY_MMY_CFG_MSG_SIZE        MCU_MMY_CFG_MSG_MID_SIZE        /* 存储最大字节数为32Byte */




#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

extern  FlagStatus              KeyMmy_IsId                                     (u8 keyId);
extern  ErrorStatus             KeyMmy_GetParaFromMMY                           (uc8 keyId, u8 gParaBuff[]);
extern  ErrorStatus             KeyMmy_SetParaToMMY                             (uc8 keyId, uc8 sParaBuff[]);
extern  ErrorStatus             KeyMmy_GetParaFromRam                           (uc8 keyId, u8 gParaBuff[]);
extern  ErrorStatus             KeyMmy_SetParaToRam                             (uc8 keyId, uc8 sParaBuff[]);
extern  void                    KeyMmy_RunParaInit                              (void);

extern  FlagStatus              KeyMmy_FtnIsTrue                                (KeyCnlFunctionTypedef keyFtn);
extern  ErrorStatus             KeyMmy_GetFtn                                   (uc8 sValue, KeyCnlFunctionTypedef *gFtn);

extern  ErrorStatus             KeyMmy_GetAddrCatch                             (u8 keyId, AddrFromTypedef *addrFrom);
extern  ErrorStatus             KeyMmy_GetAddr                                  (uc8 keyId, RunAddrTypedef *gAddr);

extern  ErrorStatus             KeyMmy_GetShortPressFtn                         (uc8 keyId, KeyCnlFunctionTypedef *gFtn);
extern  ErrorStatus             KeyMmy_GetShortReleaseFtn                       (uc8 keyId, KeyCnlFunctionTypedef *gFtn);
extern  ErrorStatus             KeyMmy_GetLongPressFtn                          (uc8 keyId, KeyCnlFunctionTypedef *gFtn);
extern  ErrorStatus             KeyMmy_GetLongReleaseFtn                        (uc8 keyId, KeyCnlFunctionTypedef *gFtn);
extern  ErrorStatus             KeyMmy_GetLongLongPressFtn                      (uc8 keyId, KeyCnlFunctionTypedef *gFtn);
extern  ErrorStatus             KeyMmy_GetLongLongReleaseFtn                    (uc8 keyId, KeyCnlFunctionTypedef *gFtn);

extern  ErrorStatus             KeyMmy_GetShortCntNum                           (uc8 keyId, u8 *gNum);
extern  ErrorStatus             KeyMmy_GetShortCntFtn                           (uc8 keyId, KeyCnlFunctionTypedef *gFtn);

extern  ErrorStatus             KeyMmy_GetExpiryTimeFtn                         (uc8 keyId, KeyCnlFunctionTypedef *gFtn);

extern  ErrorStatus             KeyMmy_GetRecall1Value                          (uc8 keyId, u8 *gValue);
extern  ErrorStatus             KeyMmy_GetRecall2Value                          (uc8 keyId, u8 *gValue);
extern  ErrorStatus             KeyMmy_GetRecall3Value                          (uc8 keyId, u8 *gValue);
extern  ErrorStatus             KeyMmy_GetRecall4Value                          (uc8 keyId, u8 *gValue);

extern  ErrorStatus             KeyMmy_GetKeyTriggerMap                         (uc8 keyId, u32 *gMap);

extern  ErrorStatus             KeyMmy_GetExpiryTimeValue                       (uc8 keyId, u16 *gTime);
extern  ErrorStatus             KeyMmy_GetRampTimeValue                         (uc8 keyId, u8 *gTime);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __KEY_MMY_H__ */
