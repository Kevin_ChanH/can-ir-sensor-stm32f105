/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Key_MMY.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月9日
  最近修改   :
  功能描述   : 按键的存储操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
/* KEY 存储的运行参数 */
static u8  KeyMmyPara[KEY_CFG_USER_NUM][KEY_MMY_PARA_LEN] = {0};

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
/* KEY 存储的默认运行参数 */
static uc8 KeyMmyParaDefault[KEY_CFG_USER_NUM][KEY_MMY_PARA_LEN] =
{
    // 按键存储1
    {0x00, 100, 1,                                                      \
     (u8)KEY_CNL_FTN_IDEL, (u8)KEY_CNL_FTN_TOGGLE,                      \
     (u8)KEY_CNL_FTN_RAMP_CYCLE, (u8)KEY_CNL_FTN_RAMP_END,              \
     (u8)KEY_CNL_FTN_IDEL, (u8)KEY_CNL_FTN_RAMP_END,                    \
     3, (u8)KEY_CNL_FTN_RECALL_1, (u8)KEY_CNL_FTN_IDEL,                 \
     0x80, 0x40, 0x20, 0x10,                                            \
     0x00, 0x00, 0x00, 0x00,                                            \
     0x00, 0x00,0x05},

    // 按键存储2
    {0x00, 100, 2,                                                      \
     (u8)KEY_CNL_FTN_IDEL, (u8)KEY_CNL_FTN_TOGGLE,                      \
     (u8)KEY_CNL_FTN_RAMP_CYCLE, (u8)KEY_CNL_FTN_RAMP_END,              \
     (u8)KEY_CNL_FTN_IDEL, (u8)KEY_CNL_FTN_RAMP_END,                    \
     3, (u8)KEY_CNL_FTN_RECALL_2, (u8)KEY_CNL_FTN_IDEL,                 \
     0x80, 0x40, 0x20, 0x10,                                            \
     0x00, 0x00, 0x00, 0x00,                                            \
     0x00, 0x00,0x05},

    // 按键存储3
    {0x00, 100, 3,                                                      \
     (u8)KEY_CNL_FTN_IDEL, (u8)KEY_CNL_FTN_TOGGLE,                      \
     (u8)KEY_CNL_FTN_RAMP_CYCLE, (u8)KEY_CNL_FTN_RAMP_END,              \
     (u8)KEY_CNL_FTN_IDEL, (u8)KEY_CNL_FTN_RAMP_END,                    \
     3, (u8)KEY_CNL_FTN_RAMP_RECALL_3, (u8)KEY_CNL_FTN_IDEL,            \
     0x80, 0x40, 0x20, 0x10,                                            \
     0x00, 0x00, 0x00, 0x00,                                            \
     0x00, 0x00,0x05},
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : KeyMmy_IsId
 功能描述  : 判断KEY ID是否合法
 输入参数  : u8 keyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus KeyMmy_IsId(u8 keyId)
{
    /* ID 判断范围为 0~Key个数-1 */
    if(keyId < KEY_CFG_USER_NUM)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetParaFromMMY
 功能描述  : 从FLASH中获取KEY参数
 输入参数  : uc8 keyId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetParaFromMMY(uc8 keyId, u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr = MCU_KEY_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID号 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    gMmyDat.Type = KEY_MMY_DATA_TYPE;

    /* 获取地址下标 */
    gMmyAddr += ((keyId) * KEY_MMY_CFG_MSG_SIZE);

    /* 获取FLASH数据 */
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<KEY_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_SetParaToMMY
 功能描述  : 设置KEY参数到FLASH中
 输入参数  : uc8 keyId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_SetParaToMMY(uc8 keyId, uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr = MCU_KEY_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID号 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    sMmyDat.Type = KEY_MMY_DATA_TYPE;

    /* 获取地址下标 */
    sMmyAddr += ((keyId) * KEY_MMY_CFG_MSG_SIZE);

    /* 载入数据 */
    for(i = 0; i<KEY_MMY_CFG_MSG_SIZE; i++)
    {
        if(i < KEY_MMY_PARA_LEN)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    /* 将数据载入到FLASH内 */
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetParaFromRam
 功能描述  : 从RAM中获取KEY的参数
 输入参数  : uc8 keyId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetParaFromRam(uc8 keyId, u8 gParaBuff[])
{
    u8 i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID号 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<KEY_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = KeyMmyPara[keyId][i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_SetParaToRam
 功能描述  : 将KEY的参数写入到RAM中
 输入参数  : uc8 keyId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_SetParaToRam(uc8 keyId, uc8 sParaBuff[])
{
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID号 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    /* 载入数据 */
    for(i = 0; i<KEY_MMY_PARA_LEN; i++)
    {
        KeyMmyPara[keyId][i] = sParaBuff[i];
    }

    return KeyMmy_SetParaToMMY(keyId, sParaBuff);
}

/*****************************************************************************
 函 数 名  : KeyMmy_RunParaInit
 功能描述  : 初始化KEY的运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void    KeyMmy_RunParaInit(void)
{
    u8  i = 0;
    u8  j = 0;

    for(i = 0; i<KEY_CFG_USER_NUM; i++)
    {
        /* 获取数据 判断获取是否失败 */
        if(KeyMmy_GetParaFromMMY(i, &KeyMmyPara[i][0]) != SUCCESS)
        {
            /* 获取回路运行参数失败 载入默认数据 */
            for(j = 0; j < KEY_MMY_PARA_LEN; j++)
            {
                KeyMmyPara[i][j] = KeyMmyParaDefault[i][j];
            }
        }
    }
}

/*****************************************************************************
 函 数 名  : KeyMmy_FtnIsTrue
 功能描述  : 判断参数是否为真实能用的
 输入参数  : KeyCnlFunctionTypedef keyFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus KeyMmy_FtnIsTrue(KeyCnlFunctionTypedef keyFtn)
{
    // 判断功能是否为绝对控制功能
    if(keyFtn <= KEY_CNL_FTN_TOGGLE)
    {
        return SET;
    }
    // 判断功能是否为调光功能
    else if((keyFtn >= KEY_CNL_FTN_RAMP_UP) && (keyFtn <= KEY_CNL_FTN_RAMP_CYCLE))
    {
        return SET;
    }
    // 判断功能是否为调用参数绝对控制功能
    else if((keyFtn >= KEY_CNL_FTN_MEM_TOGGLE) && (keyFtn <= KEY_CNL_FTN_RECALL_4))
    {
        return SET;
    }
    // 判断功能是否为调用参数调光功能
    else if((keyFtn >= KEY_CNL_FTN_RAMP_RECALL_1) && (keyFtn <= KEY_CNL_FTN_RAMP_RECALL_4))
    {
        return SET;
    }
    // 判断功能是否为进入自动模式
    else if(keyFtn == KEY_CNL_FTN_ENTER_AUTO_MODE)
    {
        return SET;
    }
    else
    {
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetFtn
 功能描述  : 根据数值 获取按键功能
 输入参数  : uc8 sValue
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetFtn(uc8 sValue, KeyCnlFunctionTypedef *gFtn)
{
    if(gFtn == NULL)
    {
        return ERROR;
    }

    // 判断按键功能是否存在
    if(KeyMmy_FtnIsTrue((KeyCnlFunctionTypedef)(sValue)) == SET)
    {
        *gFtn = (KeyCnlFunctionTypedef)(sValue);

        return SUCCESS;
    }
    else
    {
        *gFtn = KEY_CNL_FTN_IDEL;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetAddrCatch
 功能描述  : 获取按键索引号
 输入参数  : u8 keyId
             AddrFromTypedef *addrFrom
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetAddrCatch(u8 keyId, AddrFromTypedef *addrFrom)
{
    u8 addrCatch = 0;
    u8 addrIndex = 0;

    /* 判断指针是否为空 */
    if(addrFrom == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    /* 获取地址跟随码 */
    addrCatch = (KeyMmyPara[keyId][0] >> 5);
    /* 获取序号下标 */
    addrIndex = (KeyMmyPara[keyId][0] & 0x1F);


    /* 跟随自身地址 自身存储的APP+GROUP */
    if(addrCatch == 0)
    {
        addrFrom->CatchFrom = ADDR_CATCH_SELF;
    }
    /* 跟随PWM值 - 实际值 */
    else if(addrCatch == 1)
    {
        addrFrom->CatchFrom = ADDR_CATCH_PWM;
    }
    /* 跟随继电器值 - 实际值 */
    else if(addrCatch == 2)
    {
        addrFrom->CatchFrom = ADDR_CATCH_RELAY;
    }
    /* 跟随按键值 - 按键的地址的实际值 */
    else if(addrCatch == 3)
    {
        addrFrom->CatchFrom = ADDR_CATCH_KEY;
    }
    else
    {
        return ERROR;
    }

    /* 载入下标 */
    addrFrom ->Index = addrIndex;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetAddr
 功能描述  : 获取按键的存储地址
 输入参数  : uc8 keyId
             RunAddrTypedef *gAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetAddr(uc8 keyId, RunAddrTypedef *gAddr)
{
    /* 判断指针是否为空 */
    if(gAddr == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    /* 载入APP */
    gAddr->App      = KeyMmyPara[keyId][1];
    /* 载入GROUP */
    gAddr->Group    = KeyMmyPara[keyId][2];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetShortPressFtn
 功能描述  : 获取短按按下功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetShortPressFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][3];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetShortReleaseFtn
 功能描述  : 获取按键短按释放功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetShortReleaseFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][4];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetLongPressFtn
 功能描述  : 获取按键长按按下功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetLongPressFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][5];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetLongReleaseFtn
 功能描述  : 获取按键长按释放功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetLongReleaseFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][6];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetLongLongPressFtn
 功能描述  : 获取按键超长按按下功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetLongLongPressFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][7];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetLongLongReleaseFtn
 功能描述  : 获取按键超长按释放功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetLongLongReleaseFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][8];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetShortCntNum
 功能描述  : 获取按键短按次数
 输入参数  : uc8 keyId
             u8 *gNum
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetShortCntNum(uc8 keyId, u8 *gNum)
{
    /* 判断指针是否为空 */
    if(gNum == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    *gNum = KeyMmyPara[keyId][9];

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetShortCntFtn
 功能描述  : 获取按键多次短按的功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetShortCntFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][10];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetExpiryTimeFtn
 功能描述  : 获取定时器超时触发功能
 输入参数  : uc8 keyId
             KeyCnlFunctionTypedef *gFtn
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetExpiryTimeFtn(uc8 keyId, KeyCnlFunctionTypedef *gFtn)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gFtn == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    gValue = KeyMmyPara[keyId][11];

    return KeyMmy_GetFtn(gValue, gFtn);
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetRecall1Value
 功能描述  : 获取按键的Recall1值
 输入参数  : uc8 keyId
             u8 *gValue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/

ErrorStatus KeyMmy_GetRecall1Value(uc8 keyId, u8 *gValue)
{
    /* 判断指针是否为空 */
    if(gValue == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    *gValue = KeyMmyPara[keyId][12];

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetRecall2Value
 功能描述  : 获取按键的Recall2值
 输入参数  : uc8 keyId
             u8 *gValue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetRecall2Value(uc8 keyId, u8 *gValue)
{
    /* 判断指针是否为空 */
    if(gValue == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    *gValue = KeyMmyPara[keyId][13];

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetRecall3Value
 功能描述  : 获取按键的Recall3值
 输入参数  : uc8 keyId
             u8 *gValue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetRecall3Value(uc8 keyId, u8 *gValue)
{
    /* 判断指针是否为空 */
    if(gValue == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    *gValue = KeyMmyPara[keyId][14];

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetRecall4Value
 功能描述  : 获取按键的Recall4值
 输入参数  : uc8 keyId
             u8 *gValue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetRecall4Value(uc8 keyId, u8 *gValue)
{
    /* 判断指针是否为空 */
    if(gValue == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    // 获取 存储数据下标
    *gValue = KeyMmyPara[keyId][15];

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetKeyTriggerMap
 功能描述  : 获取按键的定时时间
 输入参数  : uc8 keyId
             u32 *gMap
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetKeyTriggerMap(uc8 keyId, u32 *gMap)
{
    u32 tmpKeyMap;

    /* 判断指针是否为空 */
    if(gMap == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    tmpKeyMap  = (u32)KeyMmyPara[keyId][16];
    tmpKeyMap |= (u32)(KeyMmyPara[keyId][17] << 8);
    tmpKeyMap |= (u32)(KeyMmyPara[keyId][18] << 16);
    tmpKeyMap |= (u32)(KeyMmyPara[keyId][19] << 24);

    *gMap = tmpKeyMap;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetExpiryTimeValue
 功能描述  : 获取按键的定时器定时时间
 输入参数  : uc8 keyId
             u16 *gTime
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetExpiryTimeValue(uc8 keyId, u16 *gTime)
{
    u16 tmpSetTime;

    /* 判断指针是否为空 */
    if(gTime == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    tmpSetTime  = (u16)KeyMmyPara[keyId][20];
    tmpSetTime |= (u16)(KeyMmyPara[keyId][21] << 8);

    *gTime = tmpSetTime;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : KeyMmy_GetRampTimeValue
 功能描述  : 获取按键的调光时间值
 输入参数  : uc8 keyId
             u8 *gTime
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KeyMmy_GetRampTimeValue(uc8 keyId, u8 *gTime)
{
    u8 tmpSetTime;

    /* 判断指针是否为空 */
    if(gTime == NULL)
    {
        return ERROR;
    }

    /* 判断KEY ID是否合法 */
    if(KeyMmy_IsId(keyId) != SET)
    {
        return ERROR;
    }

    tmpSetTime  = KeyMmyPara[keyId][22];

    *gTime = tmpSetTime;

    return SUCCESS;
}



