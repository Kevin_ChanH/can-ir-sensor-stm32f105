/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Key.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月1日
  最近修改   :
  功能描述   : 按键驱动文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static  KeyAttrTypedef      KeyAttr[KEY_CFG_USER_NUM];
static  u8                  KeyParaValue[KEY_PARA_VALUE_LEN] = {0};


/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
uc8 KeyParaDefaultVaule[KEY_PARA_VALUE_LEN] =
{
    20,
    0x01,
    0xF4,

    0x00,
    0x64,

    0x13,
    0x88,
};

const PinNames KeyGpioMap[KEY_CFG_USER_NUM] =
{
    KEY_Pin_1,
    KEY_Pin_2,
    KEY_Pin_3,
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : KEY_GpioInit
 功能描述  : 按键GPIO初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void KEY_GpioInit(void)
{
    u8 i = 0;

    for(i=0; i<KEY_CFG_USER_NUM; i++)
    {
        Gpio_Init(KeyGpioMap[i], GPIO_Mode_IPU, Bit_SET);
    }
}

/*****************************************************************************
 函 数 名  : KEY_RunParaInit
 功能描述  : 按键运行参数初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void KEY_RunParaInit(void)
{
    u8 i = 0;

    for(i=0; i<KEY_PARA_VALUE_LEN; i++)
    {
        KeyParaValue[i] = KeyParaDefaultVaule[i];
    }


    for(i=0; i<KEY_CFG_USER_NUM; i++)
    {
        KeyAttr[i].KeyStat      = KEY_STATE_IDLE;
        KeyAttr[i].GetIn        = SET;
        KeyAttr[i].KeyShortNum  = 0;
        KeyAttr[i].TmmId        = (Tmm_ID)(TMM_KEY_START + i);
    }
}

/*****************************************************************************
 函 数 名  : KEY_GetPara
 功能描述  : 按键获取参数
 输入参数  : KeyParaTypedef keyParaId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u32 KEY_GetPara(KeyParaTypedef keyParaId)
{
    u32 result = 0;

    if(keyParaId == KEY_PARA_TIME_BOUNCE)
    {
        result = KeyParaValue[0];
    }
    else if(keyParaId == KEY_PARA_T1)
    {
        result = KeyParaValue[1];
        result <<= 8;
        result += KeyParaValue[2];
    }
    else if(keyParaId == KEY_PARA_T2)
    {
        result = KeyParaValue[3];
        result <<= 8;
        result += KeyParaValue[4];
    }
    else if(keyParaId == KEY_PARA_T3)
    {
        result = KeyParaValue[5];
        result <<= 8;
        result += KeyParaValue[6];
    }
    else
    {
    }

    return result;
}

/*****************************************************************************
 函 数 名  : KEY_IsKeyId
 功能描述  : 判断是否为按键ID
 输入参数  : uc8 keyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus KEY_IsKeyId(uc8 keyId)
{
    if(keyId < KEY_CFG_USER_NUM)
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : KEY_SetSimulantStat
 功能描述  : 按键 设置虚拟状态
 输入参数  : uc8 sKeyId
             KeyStatTypedef sStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KEY_SetSimulantStat(uc8 sKeyId, KeyStatTypedef sStat)
{
    if(KEY_IsKeyId(sKeyId) == SET)
    {
        KeyAttr[sKeyId].KeyStat = sStat;
        return SUCCESS;
    }
    else
    {
        return ERROR;
    }
}



/*****************************************************************************
 函 数 名  : KEY_Clear
 功能描述  : 按键 清除状态
 输入参数  : uc8 sKeyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus KEY_Clear(uc8 sKeyId)
{
    if(KEY_IsKeyId(sKeyId) == SET)
    {
        KeyAttr[sKeyId].KeyStat     = KEY_STATE_IDLE;
        KeyAttr[sKeyId].GetIn       = SET;
        KeyAttr[sKeyId].KeyShortNum = 0;

        return SUCCESS;
    }
    else
    {
        return ERROR;
    }
}

/*****************************************************************************
 函 数 名  : KEY_GetStat
 功能描述  : 按键 获取状态
 输入参数  : uc8 gKeyId
             u8 *gShortNum
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
KeyStatTypedef KEY_GetStat(uc8 gKeyId, u8 *gShortNum)
{
    KeyStatTypedef resultStat;

    if((KEY_IsKeyId(gKeyId) == SET) && (gShortNum != NULL))
    {
        // 载入状态
        resultStat = KeyAttr[gKeyId].KeyStat;
        // 载入短按次数
        *gShortNum = KeyAttr[gKeyId].KeyShortNum;

        if((resultStat == KEY_STATE_SHORTEND)
           ||(resultStat == KEY_STATE_LONGEND)
           ||(resultStat == KEY_STATE_LLONGEND))
        {
            KEY_Clear(gKeyId);
        }

        return resultStat;
    }
    else
    {
        return KEY_STATE_IDLE;
    }
}

/*****************************************************************************
 函 数 名  : KEY_GetShortNum
 功能描述  : 按键 获取按键短按次数
 输入参数  : uc8 gKeyId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u8 KEY_GetShortNum(uc8 gKeyId)
{
    if(KEY_IsKeyId(gKeyId) == SET)
    {
        return KeyAttr[gKeyId].KeyShortNum;
    }
    else
    {
        return 0;
    }
}

/*****************************************************************************
 函 数 名  : KEY_IsRelease
 功能描述  : 判断状态是否为 按键释放状态
 输入参数  : KeyStatTypedef kStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus KEY_IsRelease(KeyStatTypedef kStat)
{
    if((kStat == KEY_STATE_SHORTEND)
       ||(kStat == KEY_STATE_LONGEND)
       ||(kStat == KEY_STATE_LLONGEND))
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : KEY_IsPress
 功能描述  : 判断状态是否为按键按下状态
 输入参数  : KeyStatTypedef kStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus KEY_IsPress(KeyStatTypedef kStat)
{
    if((kStat == KEY_STATE_LONGING)         /*  */
       ||(kStat == KEY_STATE_LLONGING))
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : KEY_IsShortRelease
 功能描述  : 判断状态是否为短按释放状态
 输入参数  : KeyStatTypedef kStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus KEY_IsShortRelease(KeyStatTypedef kStat)
{
    if(kStat == KEY_STATE_SHORTEND)     /* 判断按键状态是否为 短按是否状态 */
    {
        /* 确定为短按释放状态 */
        return SET;
    }
    else
    {
        /* 否定为短按释放状态 */
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : KEY_IsPressRelease
 功能描述  : 判断状态是否为长按释放状态
 输入参数  : KeyStatTypedef kStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus KEY_IsPressRelease(KeyStatTypedef kStat)
{
    if((kStat == KEY_STATE_LONGEND)     /* 判断按键状态是否为 长按释放状态 */
       ||(kStat == KEY_STATE_LLONGEND)) /* 判断按键状态是否为 超长按释放状态 */
    {
        /* 确定为长按释放状态 */
        return SET;
    }
    else
    {
        /* 否定长按释放状态 */
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : KEY_Poll
 功能描述  : 按键轮询函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void KEY_Poll(void)
{
    u8 i = 0;

    for(i = 0; i<KEY_CFG_USER_NUM; i++)
    {
        switch(KeyAttr[i].KeyStat)
        {

            // 按键 空闲状态
            case KEY_STATE_IDLE:

                // 判断按键状态是否为 按下
                if(Gpio_Read(KeyGpioMap[i]) == RESET)
                {
                    // 判断之前的状态是否为 放开状态
                    if(KeyAttr[i].GetIn == SET)
                    {
                        // 将输入状态设置为 按下
                        KeyAttr[i].GetIn = RESET;
                        // 将按键状态设置为 防抖动状态
                        KeyAttr[i].KeyStat = KEY_STATE_BOUNCE;
                        // 载入防抖动时间
                        Tmm_LoadMs(KeyAttr[i].TmmId, KEY_GetPara(KEY_PARA_TIME_BOUNCE));
                    }
                }
                else
                {
                    // 判断按键状态是否不为 按下状态
                    if(Gpio_Read(KeyGpioMap[i]) != RESET)
                    {
                        // 将输入状态设置为 放开
                        KeyAttr[i].GetIn = SET;
                    }
                }
                break;

            // 按键 防抖动状态
            case KEY_STATE_BOUNCE:

                // 判断防抖动时间 是否到达
                if(Tmm_IsON(KeyAttr[i].TmmId) != SET)
                {
                    // 获取输入状态是否为 按下状态
                    if(Gpio_Read(KeyGpioMap[i]) == RESET)
                    {
                        // 将按键状态设置为 寻找上升沿状态
                        KeyAttr[i].KeyStat = KEY_STATE_RISE;
                    }
                    else
                    {
                        // 将按键状态设置为 空闲状态
                        KeyAttr[i].KeyStat = KEY_STATE_IDLE;
                    }
                }
                else
                {
                    // 获取输入状态是否为 放开状态
                    if(Gpio_Read(KeyGpioMap[i]) != RESET)
                    {
                        // 将按键状态设置为 空闲状态
                        KeyAttr[i].KeyStat = KEY_STATE_IDLE;
                    }
                    else
                    {
                    }
                }
                break;

            // 按键 寻找上升沿状态
            case KEY_STATE_RISE:

                // 载入寻找上升沿定时时间
                Tmm_LoadMs(KeyAttr[i].TmmId, KEY_GetPara(KEY_PARA_T1));
                // 将按键状态设置为 寻找高电平状态
                KeyAttr[i].KeyStat = KEY_STATE_HIGH;
                break;

            // 按键 寻找高电平状态
            case KEY_STATE_HIGH:

                // 判断寻找高电平的定时时间是否到达
                if(Tmm_IsON(KeyAttr[i].TmmId) != SET)
                {
                    // 将按键状态设置为 长按状态
                    KeyAttr[i].KeyStat = KEY_STATE_LONGING;
                    // 载入寻找超长按定时时间
                    Tmm_LoadMs(KeyAttr[i].TmmId, KEY_GetPara(KEY_PARA_T3));
                }
                else
                {
                    // 获取输入状态是否为 放开状态
                    if(Gpio_Read(KeyGpioMap[i]) != RESET)
                    {
                        // 将按键状态设置为 寻找低电平状态
                        KeyAttr[i].KeyStat = KEY_STATE_SHORTINGL;
                        // 按键短按次数设置为0
                        KeyAttr[i].KeyShortNum = 0;
                        // 载入按键放开定时时间
                        Tmm_LoadMs(KeyAttr[i].TmmId, KEY_GetPara(KEY_PARA_T2));
                    }
                    else
                    {
                    }
                }
                break;

            // 按键 寻找低电平状态
            case KEY_STATE_SHORTINGL:

                // 判断寻找低电平的定时时间是否到达
                if(Tmm_IsON(KeyAttr[i].TmmId) != SET)
                {
                    // 将按键短按次数+1
                    KeyAttr[i].KeyShortNum++;
                    // 将按键状态设置为 短按结束状态
                    KeyAttr[i].KeyStat = KEY_STATE_SHORTEND;
                }
                else
                {
                    // 获取输入状态是否为 按下状态
                    if(Gpio_Read(KeyGpioMap[i]) == RESET)
                    {
                        // 将按键短按次数+1
                        KeyAttr[i].KeyShortNum++;
                        // 载入按键寻找低电平的定时时间
                        Tmm_LoadMs(KeyAttr[i].TmmId, KEY_GetPara(KEY_PARA_T1));
                        // 按键 寻找高电平状态
                        KeyAttr[i].KeyStat = KEY_STATE_SHORTINGH;
                    }
                    else
                    {
                    }
                }
                break;

            // 按键 寻找高电平状态
            case KEY_STATE_SHORTINGH:

                // 判断寻找高电平的定时时间是否到达
                if(Tmm_IsON(KeyAttr[i].TmmId) != SET)
                {
                    // 将按键状态设置为 长按按下状态
                    KeyAttr[i].KeyStat = KEY_STATE_LONGING;
                    // 载入按键长按的持续定时时间阀值
                    Tmm_LoadMs(KeyAttr[i].TmmId, KEY_GetPara(KEY_PARA_T3));
                }
                else
                {
                    // 获取输入状态是否为 放开状态
                    if(Gpio_Read(KeyGpioMap[i]) != RESET)
                    {
                        // 将按键状态设置为 寻找低电平状态
                        KeyAttr[i].KeyStat = KEY_STATE_SHORTINGL;
                        // 载入按键寻找高电平的定时时间
                        Tmm_LoadMs(KeyAttr[i].TmmId, KEY_GetPara(KEY_PARA_T2));
                    }
                    else
                    {
                    }
                }
                break;

            // 按键 短按结束状态
            case KEY_STATE_SHORTEND:
                break;

            // 按键 长按按下状态
            case KEY_STATE_LONGING:

                // 判断长按的持续时间是否到达
                if(Tmm_IsON(KeyAttr[i].TmmId) != SET)
                {
                    // 将按键状态设置为 超长按按下状态
                    KeyAttr[i].KeyStat = KEY_STATE_LLONGING;
                }
                else
                {
                    // 获取输入状态是否为 放开状态
                    if(Gpio_Read(KeyGpioMap[i]) != RESET)
                    {
                        // 将按键状态设置为 长按结束状态
                        KeyAttr[i].KeyStat = KEY_STATE_LONGEND;
                    }
                    else
                    {
                    }
                }
                break;

            // 按键 长按结束状态
            case KEY_STATE_LONGEND:
                break;

            // 按键 超长按按下状态
            case KEY_STATE_LLONGING:

                // 获取输入状态是否为 放开状态
                if(Gpio_Read(KeyGpioMap[i]) != RESET)
                {
                    // 将按键状态设置为 超长按结束状态
                    KeyAttr[i].KeyStat = KEY_STATE_LLONGEND;
                }
                else
                {
                }
                break;

            // 按键 超长按结束状态
            case KEY_STATE_LLONGEND:
                break;

            // 其余状态
            default:
                // 将按键状态设置为 空闲状态
                KeyAttr[i].KeyStat = KEY_STATE_IDLE;
                break;
        }
    }
}



