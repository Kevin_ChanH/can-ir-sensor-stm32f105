/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Key_Logic.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月10日
  最近修改   :
  功能描述   : Key_Logic.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __KEY_LOGIC_H__
#define __KEY_LOGIC_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/




#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  void                        KeyLogic_Init                               (void);
extern  ErrorStatus                 KeyLogic_AddrAnalyzeInit                    (uc8 keyId);

extern  ErrorStatus                 KeyLogic_GetMmyAttr                         (uc8 keyId);

extern  ErrorStatus                 KeyLogic_UpdateRealLevel                    (uc8 keyId);
extern  ErrorStatus                 KeyLogic_GetRealLevel                       (uc8 keyId, u8 *gLevel);

extern  ErrorStatus                 KeyLogic_GenerateAbsCnlCmd                  (uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen);
extern  ErrorStatus                 KeyLogic_GenerateGmRampCnlCmd               (uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen);
extern  ErrorStatus                 KeyLogic_GenerateAbsParaCnlCmd              (uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen);
extern  ErrorStatus                 KeyLogic_GenerateRampParaCnlCmd             (uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen);
extern  ErrorStatus                 KeyLogic_GenerateSpecialCmd                 (uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen);
extern  ErrorStatus                 KeyLogic_GenerateCnlCmd                     (uc8 keyId, KeyCnlFunctionTypedef keyCnlCmd, u8 cmdBuff[], u8 *cmdLen);

extern  ErrorStatus                 KeyLogic_LoadCnlCmd                         (uc8 keyId, u8 sBuff[], u8 sLen);

extern  ErrorStatus                 KeyLogic_GenerateCnl                        (uc8 keyId, KeyStatTypedef keyStat, uc8 shortCnt);

extern  ErrorStatus                 KeyLogic_Analyze                            (uc8 keyId);
extern  void                        KeyLogic_AnalyzePoll                        (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __KEY_LOGIC_H__ */
