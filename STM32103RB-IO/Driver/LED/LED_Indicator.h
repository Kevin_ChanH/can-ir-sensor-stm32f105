/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : LED_Indicator.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月26日
  最近修改   :
  功能描述   : LED_Indicator.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月26日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __LED_INDICATOR_H__
#define __LED_INDICATOR_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


typedef enum
{
    LED_ID_POWER = 0,
    LED_ID_CAN,
    LED_ID_RF,
    LED_ID_MAX,
} LedIndicatorIdTypedef;

typedef enum
{
    LED_FLASH_STAT_OFF = 0,
    LED_FLASH_STAT_ON,
    LED_FLASH_STAT_FLIP,
} LedFlashStatTypedef;

typedef enum
{
    LED_STAT_OFF = 0,
    LED_STAT_ON,
    LED_STAT_FLIP,
} LedStatTypedef;


typedef struct
{
    LedFlashStatTypedef LedFlashStat;
    LedStatTypedef      LedStat;
    u8                  LedLevel;
    FlagStatus          BinkHold;
    Tmm_ID              TmmId;
} LedIndicatorAttrTypedef;



/* LED Indicator ON Stat  */
#define     LED_IDC_ON                  Bit_RESET
/* LED Indicator OFF Stat */
#define     LED_IDC_OFF                 Bit_SET



#define     LED_POWER                       PA_8
#define     LED_CAN                         PC_12
#define     LED_RF                          PC_13

#define     LED_INDICATOR_INIT_OUT          LED_IDC_OFF

/* LED闪烁时间 */
#define     LED_INDICATOR_FLASH_TIM_MS      250
#define     LED_INDICATOR_HOLD_TIM_MS       50


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

extern  void                    LedIndicator_GpioInit                           (void);
extern  void                    LedIndicator_InitRunPara                        (void);
extern  FlagStatus              LedIndicator_IsId                               (uc8 ledId);
extern  ErrorStatus             LedIndicator_Out                                (uc8 ledId, LedStatTypedef outStat);
extern  ErrorStatus             LedIndicator_FlashSet                           (uc8 ledId, LedFlashStatTypedef flashStat);
extern  LedFlashStatTypedef     LedIndicator_GetFlashStat                       (uc8 ledId);
extern  ErrorStatus             LedIndicator_BlinkHoldSet                       (uc8 ledId);
extern  void                    LedIndicator_FlashPoll                          (void);
extern  void                    LedIndicator_BlinkHoldPoll                      (void);
extern  void                    LedIndicator_Poll                               (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __LED_INDICATOR_H__ */
