/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : LED_MMY.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月28日
  最近修改   :
  功能描述   : LED_MMY.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月28日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __LED_MMY_H__
#define __LED_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    LED_OPEN_DIRECTION_ABOVE,           /* 开启方向是大于阀值时开启 其余时间关闭 */
    LED_OPEN_DIRECTION_SMEQ,            /* 开启方向是小于等于阀值时开启 其余时间关闭 */
} LedOpenDirection;

typedef struct
{
    AddrFromTypedef     AddrFrom;       /* 地址跟随类型 */
    RunAddrTypedef      portAddr;       /* 端口存储地址 */
    u8                  SwitchTH;       /* LED的开关阀值 */
    u8                  RealLevel;      /* LED端口的实际值 */
    LedOpenDirection    OpenDir;        /* LED的开启方向 大于或小于阀值开启 */
} LedRunAttrTypedef;


/* LED 存储参数 占5Byte */
#define     LED_MMY_PARA_LEN            5                               /* 占用5Bytes配置字节 */
#define     LED_MMY_DATA_TYPE           MMY_DATA_TYPE_MID               /* 存储数据类型为中等类型 */
#define     LED_MMY_CFG_MSG_SIZE        MCU_MMY_CFG_MSG_MID_SIZE        /* 存储最大字节数为32Byte */



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  FlagStatus                  LedMmy_IsId                                 (u8 ledId);
extern  ErrorStatus                 LedMmy_GetParaFromMMY                       (uc8 ledId, u8 gParaBuff[]);
extern  ErrorStatus                 LedMmy_SetParaToMMY                         (uc8 ledId, uc8 sParaBuff[]);
extern  ErrorStatus                 LedMmy_GetParaFromRam                       (uc8 ledId, u8 gParaBuff[]);
extern  ErrorStatus                 LedMmy_SetParaToRam                         (uc8 ledId, uc8 sParaBuff[]);
extern  void                        LedMmy_RunParaInit                          (void);
extern  ErrorStatus                 LedMmy_GetAddrCatch                         (u8 ledId, AddrFromTypedef *addrFrom);
extern  ErrorStatus                 LedMmy_GetAddr                              (uc8 ledId, RunAddrTypedef *gAddr);
extern  ErrorStatus                 LedMmy_GetSwitchThresholdValue              (uc8 ledId, u8 *gSTV);
extern  ErrorStatus                 LedMmy_GetOpenDirection                     (uc8 ledId, LedOpenDirection *gDir);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __LED_MMY_H__ */
