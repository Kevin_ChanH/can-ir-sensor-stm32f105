/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : LED.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月1日
  最近修改   :
  功能描述   : LED操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static  LedAttrTypedef      LedAttr[LED_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
const PinNames LedGpioMap[LED_CFG_USER_NUM] =
{
    LED_Pin_1, LED_Pin_2, LED_Pin_3
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : LED_GpioInit
 功能描述  : 初始化LED的GPIO
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void    LED_GpioInit(void)
{
    u8 i = 0;

    for(i=0; i<LED_CFG_USER_NUM; i++)
    {
        Gpio_Init(LedGpioMap[i], GPIO_Mode_Out_PP, LED_INIT_OUT);
    }
}

/*****************************************************************************
 函 数 名  : LED_InitRunPara
 功能描述  : 初始化LED的运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void    LED_InitRunPara(void)
{
    u8 i = 0;
    LedStatTypedef ledInitStat = LED_STAT_OFF;

    if(LED_INIT_OUT == LED_OFF)
    {
        ledInitStat = LED_STAT_OFF;
    }
    else if(LED_INIT_OUT == LED_ON)
    {
        ledInitStat = LED_STAT_ON;
    }
    else
    {
        ledInitStat = LED_STAT_OFF;
    }

    for(i=0; i<LED_CFG_USER_NUM; i++)
    {
        LedAttr[i].LedFlashStat = LED_FLASH_STAT_OFF;
        LedAttr[i].LedStat      = ledInitStat;
        LedAttr[i].LedLevel     = 0;
    }
}

/*****************************************************************************
 函 数 名  : LED_IsId
 功能描述  : 判断是否为LED的ID
 输入参数  : uc8 ledId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  LED_IsId(uc8 ledId)
{
    /* ID 判断范围为 0~LED个数-1 */
    if(ledId < LED_CFG_USER_NUM)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : LED_Out
 功能描述  : 控制LED的输出
 输入参数  : uc8 ledId
             LedAttrTypedef outStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LED_Out(uc8 ledId, LedStatTypedef outStat)
{
    if(LED_IsId(ledId) == SET)
    {
        if(LedAttr[ledId].LedFlashStat == LED_FLASH_STAT_OFF)
        {
            if(outStat == LED_STAT_OFF)
            {
                LedAttr[ledId].LedStat = LED_STAT_OFF;

                Gpio_Write(LedGpioMap[ledId], LED_OFF);
            }
            else if(outStat == LED_STAT_ON)
            {
                LedAttr[ledId].LedStat = LED_STAT_ON;

                Gpio_Write(LedGpioMap[ledId], LED_ON);
            }
            else if(outStat == LED_STAT_FLIP)
            {
                if(LedAttr[ledId].LedStat == LED_STAT_OFF)
                {
                    LedAttr[ledId].LedStat = LED_STAT_ON;

                    Gpio_Write(LedGpioMap[ledId], LED_ON);
                }
                else if(LedAttr[ledId].LedStat == LED_STAT_ON)
                {
                    LedAttr[ledId].LedStat = LED_STAT_OFF;

                    Gpio_Write(LedGpioMap[ledId], LED_OFF);
                }
                else
                {
                    return ERROR;
                }
            }
            else
            {
                return ERROR;
            }
        }
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LED_FlashSet
 功能描述  : 设置LED的Flash状态
 输入参数  : uc8 ledId
             LedFlashStatTypedef flashStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LED_FlashSet(uc8 ledId, LedFlashStatTypedef flashStat)
{
    if(LED_IsId(ledId) == SET)
    {
        if(flashStat == LED_FLASH_STAT_FLIP)
        {
            if(LedAttr[ledId].LedFlashStat == LED_FLASH_STAT_OFF)
            {
                flashStat = LED_FLASH_STAT_ON;
            }
            else
            {
                flashStat = LED_FLASH_STAT_OFF;
            }
        }

        LedAttr[ledId].LedFlashStat = flashStat;

        if(LedAttr[ledId].LedFlashStat == LED_FLASH_STAT_OFF)
        {
            LED_Out(ledId, LedAttr[ledId].LedStat);
        }
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LED_GetFlashStat
 功能描述  : 获取LED的FLASH状态
 输入参数  : uc8 ledId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
LedFlashStatTypedef LED_GetFlashStat(uc8 ledId)
{
    if(LED_IsId(ledId) == SET)
    {
        return LedAttr[ledId].LedFlashStat;
    }
    else
    {
        return LED_FLASH_STAT_OFF;
    }
}

/*****************************************************************************
 函 数 名  : LED_Poll
 功能描述  : LED的轮巡函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void LED_Poll(void)
{
    u8 i = 0;

    if(Tmm_IsON(TMM_LED_FLASH) != SET)
    {
        Tmm_LoadMs(TMM_LED_FLASH, LED_CFG_FLASH_TIM_MS);

        for(i=0; i<LED_CFG_USER_NUM; i++)
        {
            if(LedAttr[i].LedFlashStat == LED_FLASH_STAT_ON)
            {
                Gpio_Toggle(LedGpioMap[i]);
            }
        }
    }
}

