/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : LED_Indicator.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月26日
  最近修改   :
  功能描述   : LED指示灯操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月26日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static LedIndicatorAttrTypedef LedIndicatorAttr[LED_ID_MAX];


/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

const PinNames LedIndicatorGpioMap[LED_ID_MAX] =
{
    LED_POWER, LED_CAN, LED_RF
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


void    LedIndicator_GpioInit(void)
{
    u8 i;

    // 初始化 各项指示灯的 GPIO
    for(i=0; i<LED_ID_MAX; i++)
    {
        Gpio_Init(LedIndicatorGpioMap[i], GPIO_Mode_Out_PP, LED_INDICATOR_INIT_OUT);
    }
}

void    LedIndicator_InitRunPara(void)
{
    u8 i;

    LedStatTypedef ledInitStat = LED_STAT_OFF;

    if(LED_INDICATOR_INIT_OUT == LED_IDC_OFF)
    {
        ledInitStat = LED_STAT_OFF;
    }
    else if(LED_INDICATOR_INIT_OUT == LED_IDC_ON)
    {
        ledInitStat = LED_STAT_ON;
    }
    else
    {
        ledInitStat = LED_STAT_OFF;
    }

    // 初始化 各项指示灯的 运行参数
    for(i=0; i<LED_ID_MAX; i++)
    {
        LedIndicatorAttr[i].LedFlashStat = LED_FLASH_STAT_OFF;
        LedIndicatorAttr[i].LedStat      = ledInitStat;
        LedIndicatorAttr[i].LedLevel     = 0;
        LedIndicatorAttr[i].BinkHold     = RESET;
        LedIndicatorAttr[i].TmmId        = (Tmm_ID)(TMM_INDICATOR_LED_START + i);
    }

    // 开启LED电源指示灯
    LedIndicator_Out(LED_ID_POWER, LED_STAT_ON);
}

FlagStatus  LedIndicator_IsId(LedIndicatorIdTypedef ledId)
{
    /* ID 判断范围 */
    if(ledId < LED_ID_MAX)
    {
        return SET;
    }

    return RESET;
}

ErrorStatus LedIndicator_Out(LedIndicatorIdTypedef ledId, LedStatTypedef outStat)
{
    if(LedIndicator_IsId(ledId) == SET)
    {
        if(LedIndicatorAttr[ledId].LedFlashStat == LED_FLASH_STAT_OFF)
        {
            if(outStat == LED_STAT_OFF)
            {
                LedIndicatorAttr[ledId].LedStat = LED_STAT_OFF;

                Gpio_Write(LedIndicatorGpioMap[ledId], LED_IDC_OFF);
            }
            else if(outStat == LED_STAT_ON)
            {
                LedIndicatorAttr[ledId].LedStat = LED_STAT_ON;

                Gpio_Write(LedIndicatorGpioMap[ledId], LED_IDC_ON);
            }
            else if(outStat == LED_STAT_FLIP)
            {
                if(LedIndicatorAttr[ledId].LedStat == LED_STAT_OFF)
                {
                    LedIndicatorAttr[ledId].LedStat = LED_STAT_ON;

                    Gpio_Write(LedIndicatorGpioMap[ledId], LED_IDC_ON);
                }
                else if(LedIndicatorAttr[ledId].LedStat == LED_STAT_ON)
                {
                    LedIndicatorAttr[ledId].LedStat = LED_STAT_OFF;

                    Gpio_Write(LedIndicatorGpioMap[ledId], LED_IDC_OFF);
                }
                else
                {
                    return ERROR;
                }
            }
            else
            {
                return ERROR;
            }
        }
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

ErrorStatus LedIndicator_FlashSet(LedIndicatorIdTypedef ledId, LedFlashStatTypedef flashStat)
{
    if(LedIndicator_IsId(ledId) == SET)
    {
        if(flashStat == LED_FLASH_STAT_FLIP)
        {
            if(LedIndicatorAttr[ledId].LedFlashStat == LED_FLASH_STAT_OFF)
            {
                flashStat = LED_FLASH_STAT_ON;
            }
            else
            {
                flashStat = LED_FLASH_STAT_OFF;
            }
        }

        LedIndicatorAttr[ledId].LedFlashStat = flashStat;

        if(LedIndicatorAttr[ledId].LedFlashStat == LED_FLASH_STAT_OFF)
        {
            LedIndicator_Out(ledId, LedIndicatorAttr[ledId].LedStat);
        }
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

LedFlashStatTypedef LedIndicator_GetFlashStat(LedIndicatorIdTypedef ledId)
{
    if(LedIndicator_IsId(ledId) == SET)
    {
        return LedIndicatorAttr[ledId].LedFlashStat;
    }
    else
    {
        return LED_FLASH_STAT_OFF;
    }
}

ErrorStatus LedIndicator_BlinkHoldSet(LedIndicatorIdTypedef ledId)
{
    if(LedIndicator_IsId(ledId) == SET)
    {
        if(LedIndicator_GetFlashStat(ledId) == LED_FLASH_STAT_OFF)
        {
            if(ledId == LED_ID_POWER)
            {
                LedIndicator_Out(ledId, LED_STAT_OFF);
            }
            else
            {
                LedIndicator_Out(ledId, LED_STAT_ON);
            }

            LedIndicatorAttr[ledId].BinkHold = SET;

            Tmm_LoadMs(LedIndicatorAttr[ledId].TmmId, LED_INDICATOR_HOLD_TIM_MS);
            return SUCCESS;
        }
    }

    return ERROR;
}



void LedIndicator_FlashPoll(void)
{
    u8 i = 0;

    if(Tmm_IsON(TMM_LED_INDICATOR_FLASH) != SET)
    {
        Tmm_LoadMs(TMM_LED_INDICATOR_FLASH, LED_INDICATOR_FLASH_TIM_MS);

        for(i=0; i<LED_ID_MAX; i++)
        {
            if(LedIndicatorAttr[i].LedFlashStat == LED_FLASH_STAT_ON)
            {
                Gpio_Toggle(LedIndicatorGpioMap[i]);
            }
        }
    }
}

void LedIndicator_BlinkHoldPoll(void)
{
    u8 i = 0;

    for(i=0; i<LED_ID_MAX; i++)
    {
        if(LedIndicatorAttr[i].BinkHold == SET)
        {
            if(Tmm_IsON(LedIndicatorAttr[i].TmmId) != SET)
            {
                LedIndicatorAttr[i].BinkHold = RESET;
                
                if(i == LED_ID_POWER)
                {
                    LedIndicator_Out((LedIndicatorIdTypedef)i, LED_STAT_ON);
                }
                else
                {
                    LedIndicator_Out((LedIndicatorIdTypedef)i, LED_STAT_OFF);
                }
            }
        }
    }
}

void LedIndicator_Poll(void)
{
    LedIndicator_FlashPoll();
    LedIndicator_BlinkHoldPoll();
}





