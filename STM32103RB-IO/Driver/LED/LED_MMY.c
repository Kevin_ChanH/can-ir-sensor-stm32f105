/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : LED_MMY.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月28日
  最近修改   :
  功能描述   : LED运行参数存储操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月28日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/* LED 存储的运行参数 */
static u8  LedMmyPara[LED_CFG_USER_NUM][LED_MMY_PARA_LEN] = {0};

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/* LED 存储的默认运行参数 */
static uc8 LedMmyParaDefault[LED_CFG_USER_NUM][LED_MMY_PARA_LEN] =
{
    {0x00, 100, 1, 0, 0},             // 指向自身存储地址
    {0x00, 100, 2, 0, 0},             // 指向自身存储地址
    {0x00, 100, 3, 0, 0},             // 指向自身存储地址
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : LedMmy_IsId
 功能描述  : 判断LED的ID是否在有效范围内
 输入参数  : u8 ledId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus LedMmy_IsId(u8 ledId)
{
    /* ID 判断范围为 0~LED个数-1 */
    if(ledId < LED_CFG_USER_NUM)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : LedMmy_GetParaFromMMY
 功能描述  : 从FLASH中获取参数
 输入参数  : uc8 ledId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_GetParaFromMMY(uc8 ledId, u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr = MCU_LED_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID号 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    gMmyDat.Type = LED_MMY_DATA_TYPE;

    /* 获取地址下标 */
    gMmyAddr += ((ledId) * LED_MMY_CFG_MSG_SIZE);

    /* 获取FLASH数据 */
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<LED_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedMmy_SetParaToMMY
 功能描述  : 写入参数到FLAHS中
 输入参数  : uc8 ledId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_SetParaToMMY(uc8 ledId, uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr = MCU_LED_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID号 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    sMmyDat.Type = LED_MMY_DATA_TYPE;

    /* 获取地址下标 */
    sMmyAddr += ((ledId) * LED_MMY_CFG_MSG_SIZE);

    /* 载入数据 */
    for(i = 0; i<LED_MMY_CFG_MSG_SIZE; i++)
    {
        if(i < LED_MMY_PARA_LEN)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    /* 将数据载入到FLASH内 */
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedMmy_GetParaFromRam
 功能描述  : 从RAM中获取数据
 输入参数  : uc8 ledId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_GetParaFromRam(uc8 ledId, u8 gParaBuff[])
{
    u8 i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID号 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<LED_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = LedMmyPara[ledId][i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedMmy_SetParaToRam
 功能描述  : 配置参数到RAM中
 输入参数  : uc8 ledId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_SetParaToRam(uc8 ledId, uc8 sParaBuff[])
{
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID号 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    /* 载入数据 */
    for(i = 0; i<LED_MMY_PARA_LEN; i++)
    {
        LedMmyPara[ledId][i] = sParaBuff[i];
    }

    return LedMmy_SetParaToMMY(ledId, sParaBuff);
}

/*****************************************************************************
 函 数 名  : LedMmy_RunParaInit
 功能描述  : 初始化LED_MMY的运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void    LedMmy_RunParaInit(void)
{
    u8  i = 0;
    u8  j = 0;

    for(i = 0; i<LED_CFG_USER_NUM; i++)
    {
        /* 获取数据 判断获取是否失败 */
        if(LedMmy_GetParaFromMMY(i, &LedMmyPara[i][0]) != SUCCESS)
        {
            /* 获取回路运行参数失败 载入默认数据 */
            for(j = 0; j < LED_MMY_PARA_LEN; j++)
            {
                LedMmyPara[i][j] = LedMmyParaDefault[i][j];
            }
        }
    }
}

/*****************************************************************************
 函 数 名  : LedMmy_GetAddrCatch
 功能描述  : 获取LED 指示的地址索引
 输入参数  : u8 ledId
             AddrFromTypedef *addrFrom
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_GetAddrCatch(u8 ledId, AddrFromTypedef *addrFrom)
{
    u8 addrCatch = 0;
    u8 addrIndex = 0;

    /* 判断指针是否为空 */
    if(addrFrom == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID是否合法 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    /* 获取地址跟随码 */
    addrCatch = (LedMmyPara[ledId][0] >> 5);
    /* 获取序号下标 */
    addrIndex = (LedMmyPara[ledId][0] & 0x1F);


    /* 跟随自身地址 自身存储的APP+GROUP */
    if(addrCatch == 0)
    {
        addrFrom->CatchFrom = ADDR_CATCH_SELF;
    }
    /* 跟随PWM值 - 实际值 */
    else if(addrCatch == 1)
    {
        addrFrom->CatchFrom = ADDR_CATCH_PWM;
    }
    /* 跟随继电器值 - 实际值 */
    else if(addrCatch == 2)
    {
        addrFrom->CatchFrom = ADDR_CATCH_RELAY;
    }
    /* 跟随按键值 - 按键的地址的实际值 */
    else if(addrCatch == 3)
    {
        addrFrom->CatchFrom = ADDR_CATCH_KEY;
    }
    else
    {
        return ERROR;
    }

    /* 载入下标 */
    addrFrom ->Index = addrIndex;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedMmy_GetAddr
 功能描述  : LED 获取运行地址
 输入参数  : uc8 ledId
             RunAddrTypedef *gAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_GetAddr(uc8 ledId, RunAddrTypedef *gAddr)
{
    /* 判断指针是否为空 */
    if(gAddr == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID是否合法 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    /* 载入APP */
    gAddr->App      = LedMmyPara[ledId][1];
    /* 载入GROUP */
    gAddr->Group    = LedMmyPara[ledId][2];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedMmy_GetSwitchThresholdValue
 功能描述  : LED 获取开关阀值
 输入参数  : uc8 ledId
             u8 *gSTV
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_GetSwitchThresholdValue(uc8 ledId, u8 *gSTV)
{
    /* 判断指针是否为空 */
    if(gSTV == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID是否合法 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    /* 载入阀值 */
    *gSTV = LedMmyPara[ledId][3];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedMmy_GetOpenDirection
 功能描述  : LED 获取开关方向
 输入参数  : uc8 ledId
             LedOpenDirection *gDir
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedMmy_GetOpenDirection(uc8 ledId, LedOpenDirection *gDir)
{
    u8 gValue = 0;

    /* 判断指针是否为空 */
    if(gDir == NULL)
    {
        return ERROR;
    }

    /* 判断Led ID是否合法 */
    if(LedMmy_IsId(ledId) != SET)
    {
        return ERROR;
    }

    gValue = LedMmyPara[ledId][4];

    /* 判断方向是否为向上方向 */
    if((gValue & 0x01) == 0x00)
    {
        *gDir = LED_OPEN_DIRECTION_ABOVE;
    }
    else
    {
        *gDir = LED_OPEN_DIRECTION_SMEQ;
    }

    /* 返回成功 */
    return SUCCESS;
}




