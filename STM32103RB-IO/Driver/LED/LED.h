/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : LED.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月1日
  最近修改   :
  功能描述   : LED.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __LED_H__
#define __LED_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    LED_FLASH_STAT_OFF = 0,
    LED_FLASH_STAT_ON,
    LED_FLASH_STAT_FLIP,
} LedFlashStatTypedef;

typedef enum
{
    LED_STAT_OFF = 0,
    LED_STAT_ON,
    LED_STAT_FLIP,
} LedStatTypedef;

typedef struct
{
    LedFlashStatTypedef LedFlashStat;
    LedStatTypedef      LedStat;
    u8                  LedLevel;
} LedAttrTypedef;





/* LED 引脚映射 */
//#define     LED_POWER               PC_12
#define     LED_COM                 PA_8
//#define     LED_CNL                 PA_8
#define     LED_Pin_1               PB_8
#define     LED_Pin_2               PA_1
#define     LED_Pin_3               PB_6


/* LED最大的使用 */
#define     LED_CFG_MAX_NUM         MCU_LED_CHANNEL_NUM
#define     LED_CFG_USER_NUM        MCU_LED_CHANNEL_NUM
#define     LED_ID_LIMIT_START      1
#define     LED_ID_LIMIT_END        LED_CFG_USER_NUM

/* LED ID */
#define     LED1                    1
#define     LED2                    2
#define     LED3                    3


/* LED ON Stat  */
#define     LED_ON                  Bit_SET
/* LED OFF Stat */
#define     LED_OFF                 Bit_RESET

/* LED 初始化输出状态 */
#define     LED_INIT_OUT            LED_OFF


/* LED闪烁时间 */
#define     LED_CFG_FLASH_TIM_MS    250



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  void                    LED_GpioInit                    (void);
extern  void                    LED_InitRunPara                 (void);
extern  FlagStatus              LED_IsId                        (uc8 ledId);
extern  ErrorStatus             LED_Out                         (uc8 ledId, LedStatTypedef outStat);
extern  ErrorStatus             LED_FlashSet                    (uc8 ledId, LedFlashStatTypedef flashStat);
extern  LedFlashStatTypedef     LED_GetFlashStat                (uc8 ledId);
extern  void                    LED_Poll                        (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __LED_H__ */
