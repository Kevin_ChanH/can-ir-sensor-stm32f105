/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : LED_Logic.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月8日
  最近修改   :
  功能描述   : LED 的 逻辑操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static LedRunAttrTypedef  LedRunAttr[LED_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : LedLogic_Init
 功能描述  : 初始化LED的逻辑参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void LedLogic_Init(void)
{
    u8 i;

    // 初始化存储参数
    LedMmy_RunParaInit();

    // 循环获取运行参数
    for(i=0; i<LED_CFG_USER_NUM; i++)
    {
        // 获取回路的运行参数
        if(LedLogic_GetMmyAttr(i) == SUCCESS)
        {
            LedLogic_AddrAnalyzeInit(i);
        }

        // 实际值初始化为0
        LedRunAttr[i].RealLevel = 0;
    }
}

/*****************************************************************************
 函 数 名  : LedLogic_AddrAnalyzeInit
 功能描述  : LED地址逻辑 加入地址（APP+GROUP）
 输入参数  : uc8 ledId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月13日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedLogic_AddrAnalyzeInit(uc8 ledId)
{
    // 判断ID是否为
    if(LedRunAttr[ledId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        VirtualAddr_Join(LedRunAttr[ledId].portAddr);
    }

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : LedLogic_GetMmyAttr
 功能描述  : 获取LED的逻辑运行参数
 输入参数  : uc8 ledId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedLogic_GetMmyAttr(uc8 ledId)
{
    // 判断ID是否符合范围
    if(LED_IsId(ledId) != SET)
    {
        return ERROR;
    }

    // 获取地址类型
    if(LedMmy_GetAddrCatch(ledId, &LedRunAttr[ledId].AddrFrom) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路存储的地址
    if(LedMmy_GetAddr(ledId, &LedRunAttr[ledId].portAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路的开关阀值
    if(LedMmy_GetSwitchThresholdValue(ledId, &LedRunAttr[ledId].SwitchTH) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路的开关方向
    if(LedMmy_GetOpenDirection(ledId, &LedRunAttr[ledId].OpenDir) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedLogic_UpdateRealLevel
 功能描述  : 更新LED的实际状态值
 输入参数  : uc8 ledId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedLogic_UpdateRealLevel(uc8 ledId)
{
    u8 tmpLevel;

    if(LED_IsId(ledId) != SET)
    {
        return ERROR;
    }

    // 判断地址类型是否为自身的
    if(LedRunAttr[ledId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        // 获取自身回路存储的地址 的状态
        if(VirtualAddr_GetVaRealLevel(LedRunAttr[ledId].portAddr, &tmpLevel) == SUCCESS)
        {
            LedRunAttr[ledId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(LedRunAttr[ledId].AddrFrom.CatchFrom == ADDR_CATCH_PWM)
    {
        // 获取PWM实际值
        /*
        if(PwmLogic_GetRealLevel(LedRunAttr[ledId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            LedRunAttr[ledId].RealLevel = tmpLevel;

            return SUCCESS;
        }
        */
    }
    else if(LedRunAttr[ledId].AddrFrom.CatchFrom == ADDR_CATCH_RELAY)
    {
        // 获取继电器的实际值
        /*
        if(RelayLogic_GetRealLevel(LedRunAttr[ledId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            LedRunAttr[ledId].RealLevel = tmpLevel;

            return SUCCESS;
        }
        */
    }
    else if(LedRunAttr[ledId].AddrFrom.CatchFrom == ADDR_CATCH_KEY)
    {
        // 获取按键回路的实际值
        if(KeyLogic_GetRealLevel(LedRunAttr[ledId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            LedRunAttr[ledId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedLogic_GetRealLevel
 功能描述  : 获取实际值
 输入参数  : uc8 ledId
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedLogic_GetRealLevel(uc8 ledId, u8 *gLevel)
{
    if(gLevel == NULL)
    {
        return ERROR;
    }

    if(LED_IsId(ledId) != SET)
    {
        return ERROR;
    }

    // 载入实际值
    *gLevel = LedRunAttr[ledId].RealLevel;

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : LedLogic_UpdateOutLogic
 功能描述  : 更新LED的输出逻辑
 输入参数  : uc8 ledId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus LedLogic_UpdateOutLogic(uc8 ledId)
{
    // 判断LED是否存在
    if(LED_IsId(ledId) != SET)
    {
        return ERROR;
    }

    // 判断LED的开启方向 是否为向上方向
    if(LedRunAttr[ledId].OpenDir == LED_OPEN_DIRECTION_ABOVE)
    {
        // 判断实际值是否大于阀值
        if(LedRunAttr[ledId].RealLevel > LedRunAttr[ledId].SwitchTH)
        {
            // 开启LED
            LED_Out(ledId, LED_STAT_ON);
        }
        else
        {
            // 关闭LED
            LED_Out(ledId, LED_STAT_OFF);
        }
    }
    // 判断继电器的开启方向 是否为向下方向
    else if(LedRunAttr[ledId].OpenDir == LED_OPEN_DIRECTION_SMEQ)
    {
        // 判断实际值是否小于等于阀值
        if(LedRunAttr[ledId].RealLevel <= LedRunAttr[ledId].SwitchTH)
        {
            // 开启LED
            LED_Out(ledId, LED_STAT_ON);
        }
        else
        {
            // 关闭LED
            LED_Out(ledId, LED_STAT_OFF);
        }
    }
    else
    {
        // 返回错误
        return ERROR;
    }

    // 返回成功
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : LedLogic_OutLogicPoll
 功能描述  : LED的逻辑输出轮询函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void  LedLogic_OutLogicPoll(void)
{
    u8 i = 0;

    // 遍历所有LED输出回路
    for(i=0; i<LED_CFG_USER_NUM; i++)
    {
        // 更新实际值
        if(LedLogic_UpdateRealLevel(i) == SUCCESS)
        {
            // 更新输出逻辑
            LedLogic_UpdateOutLogic(i);
        }
    }
}


