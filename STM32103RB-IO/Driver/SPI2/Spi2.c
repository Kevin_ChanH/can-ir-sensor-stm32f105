#include "includes.h"

void SPI2_GpioInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(SPI2_GPIO_PERIPH, ENABLE);

    GPIO_InitStructure.GPIO_Pin = SPI2_PIN_CS | SPI2_PIN_CLK | SPI2_PIN_MOSI;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(SPI2_IO_PORT, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = SPI2_PIN_MISO;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(SPI2_IO_PORT, &GPIO_InitStructure);
}

uc8 SPI2_SendByte(uc8 sData)
{
    u8 i = 0;
    u8 result = 0;
    u8 tmpData = sData;

    SPI2_CLK_L;
    for(i = 0; i<8; i++)
    {
        if(tmpData & 0x80)
        {
            SPI2_MOSI_H;
        }
        else
        {
            SPI2_MOSI_L;
        }

        tmpData <<= 1;
        SPI2_CLK_H;
        result <<=1;

        if((SPI2_MISO_GET) != RESET)
        {
            result++;
        }
        SPI2_CLK_L;
    }
    return result;
}



