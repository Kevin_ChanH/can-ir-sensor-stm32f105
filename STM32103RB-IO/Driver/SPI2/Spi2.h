#ifndef __SPI2_H__
#define __SPI2_H__

#define SPI2_PIN_CS        GPIO_Pin_12
#define SPI2_PIN_CLK       GPIO_Pin_13
#define SPI2_PIN_MISO      GPIO_Pin_14
#define SPI2_PIN_MOSI      GPIO_Pin_15
#define SPI2_IO_PORT       GPIOB
#define SPI2_GPIO_PERIPH   RCC_APB2Periph_GPIOB
#define SPI2_PERIPH        RCC_APB1Periph_SPI2

#define SPI2_CS_H          GPIO_SetBits(SPI2_IO_PORT, SPI2_PIN_CS)
#define SPI2_CLK_H         GPIO_SetBits(SPI2_IO_PORT, SPI2_PIN_CLK)
#define SPI2_MOSI_H        GPIO_SetBits(SPI2_IO_PORT, SPI2_PIN_MOSI)
#define SPI2_CS_L          GPIO_ResetBits(SPI2_IO_PORT, SPI2_PIN_CS)
#define SPI2_CLK_L         GPIO_ResetBits(SPI2_IO_PORT, SPI2_PIN_CLK)
#define SPI2_MOSI_L        GPIO_ResetBits(SPI2_IO_PORT, SPI2_PIN_MOSI)
#define SPI2_MISO_GET      GPIO_ReadInputDataBit(SPI2_IO_PORT, SPI2_PIN_MISO)

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

extern  void    SPI2_GpioInit           (void);
extern  uc8     SPI2_SendByte           (uc8 sData);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif /* __SPI2_H__ */

