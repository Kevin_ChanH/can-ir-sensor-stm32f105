/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : UART3_485.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月23日
  最近修改   :
  功能描述   : UART3_485 驱动文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月23日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
SQueue Uart3RxQ;
SQueue Uart3TxQ;


/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static u8 Uart3RxBuff[UART3_RX_QUEUE_SIZE][UART3_RX_BUFF_SIZE] = {0};
static u8 Uart3TxBuff[UART3_TX_QUEUE_SIZE][UART3_TX_BUFF_SIZE] = {0};
static SFrame Uart3RxSFrame[UART3_RX_QUEUE_SIZE];
static SFrame Uart3TxSFrame[UART3_TX_QUEUE_SIZE];


static u8 Uart3RxBase[UART3_RX_BUFF_SIZE];
static u8 Uart3TxBase[UART3_TX_BUFF_SIZE];

static SFrame Uart3RxFrame;
static SFrame Uart3TxFrame;

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


static void UART3_BaseFrameInit(void)
{
    Uart3RxFrame.buff  = Uart3RxBase;
    Uart3RxFrame.len   = 0;
    Uart3RxFrame.index = 0;
    Uart3RxFrame.size  = UART3_RX_BUFF_SIZE;


    Uart3TxFrame.buff  = Uart3TxBase;
    Uart3TxFrame.len   = 0;
    Uart3TxFrame.index = 0;
    Uart3TxFrame.size  = UART3_TX_BUFF_SIZE;
}


static void UART3_QueueInit(void)
{
    u32 i = 0;

    Uart3RxQ.QSize = UART3_RX_QUEUE_SIZE;
    Uart3RxQ.QHead = 0;
    Uart3RxQ.QTail = 0;
    Uart3RxQ.QStat = STAT_IDEL;

    Uart3RxQ.sFrame = Uart3RxSFrame;
    for(i = 0; i<UART3_RX_QUEUE_SIZE; i++)
    {
        Uart3RxQ.sFrame[i].buff  = &Uart3RxBuff[i][0];
        Uart3RxQ.sFrame[i].size  = UART3_RX_BUFF_SIZE;
        Uart3RxQ.sFrame[i].len   = 0;
        Uart3RxQ.sFrame[i].index = 0;
    }


    Uart3TxQ.QSize = UART3_TX_QUEUE_SIZE;
    Uart3TxQ.QHead = 0;
    Uart3TxQ.QTail = 0;
    Uart3TxQ.QStat = STAT_IDEL;

    Uart3TxQ.sFrame = Uart3TxSFrame;
    for(i = 0; i<UART3_TX_QUEUE_SIZE; i++)
    {
        Uart3TxQ.sFrame[i].buff  = &Uart3TxBuff[i][0];
        Uart3TxQ.sFrame[i].size  = UART3_TX_BUFF_SIZE;
        Uart3TxQ.sFrame[i].len   = 0;
        Uart3TxQ.sFrame[i].index = 0;
    }
}

static void UART3_GpioInit(void)
{
    // 配置UART3 RX GPIO
    Gpio_Init(UART3_Pin_RX, GPIO_Mode_IPU, Bit_SET);
    // 配置UART3 TX GPIO
    Gpio_Init(UART3_Pin_TX, GPIO_Mode_AF_PP, Bit_SET);

    // 配置UART3 RE CNL GPIO
    Gpio_Init(UART3_485_Pin_RE, GPIO_Mode_Out_PP, Bit_SET);
}

static void UART3_ModeInit(uc32 btr)
{
    USART_InitTypeDef       USART_InitStructure;
    USART_ClockInitTypeDef  USART_ClockInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);


    USART_InitStructure.USART_BaudRate = btr;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(USART3, &USART_InitStructure);

    USART_ClockInitStructure.USART_Clock    = USART_Clock_Disable;
    USART_ClockInitStructure.USART_CPOL     = USART_CPOL_Low;
    USART_ClockInitStructure.USART_CPHA     = USART_CPHA_1Edge;
    USART_ClockInitStructure.USART_LastBit  = USART_LastBit_Disable;
    USART_ClockInit(USART3, &USART_ClockInitStructure);

    USART_ITConfig(USART3, UART3_CFG_IT, ENABLE);

    USART_Cmd(USART3, ENABLE);
}

static void UART3_NvicInit(uc8 prePrio, uc8 subPrio, FunctionalState nvicCmd)
{
    NVIC_InitTypeDef NVIC_InitStruCture;

    NVIC_InitStruCture.NVIC_IRQChannel = USART3_IRQn;
    NVIC_InitStruCture.NVIC_IRQChannelPreemptionPriority = prePrio;
    NVIC_InitStruCture.NVIC_IRQChannelSubPriority = subPrio;
    NVIC_InitStruCture.NVIC_IRQChannelCmd = nvicCmd;
    NVIC_Init(&NVIC_InitStruCture);
}

void UART3_InitRunPara(void)
{
    UART3_BaseFrameInit();
    UART3_QueueInit();
}

void UART3_DriverInit(void)
{
    UART3_GpioInit();
    UART3_NvicInit(UART3_CFG_NVIC_PRE, UART3_CFG_NVIC_SUB, ENABLE);
    UART3_ModeInit(UART3_CFG_BAU);
}

void UART3_Init(void)
{
    UART3_InitRunPara();
    UART3_DriverInit();
}

void UART3_SendChar(uc8 ch)
{
    //if(Uart3TxQ.QStat == STAT_IDEL)
    //{
        USART_SendData(USART3, ch);
        while(USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET);
    //}
}

ErrorStatus UART3_SendFrame(const SFrame* frame)
{
    u32 i;

    if(frame == NULL)
    {
        return ERROR;
    }

    if(frame->buff == NULL)
    {
        return ERROR;
    }

    if((frame->len) > (frame->size))
    {
        return ERROR;
    }

    if((frame->len) == 0)
    {
        return ERROR;
    }

    for(i=0; i<frame->len; i++)
    {
        UART3_SendChar(frame->buff[i]);
    }

    return SUCCESS;
}

void UART3_RxIRQ(void)
{
    u32 rxIndex = Uart3RxFrame.index;

    Uart3RxFrame.buff[rxIndex++] = USART_ReceiveData(USART3);

    if(rxIndex >= Uart3RxFrame.size)
    {
        rxIndex = 0;
    }

    Uart3RxFrame.index = rxIndex;
    Uart3RxQ.QStat = STAT_NORMAL;

    // 载入成帧时间
    Tmm_LoadMs(TMM_UART3_RX_PACK, UART3_RX_PACK_MS);
}

void UART3_TxIRQ(void)
{
    Uart3TxQ.QStat = STAT_IDEL;

    // 载入间隔时间
    Tmm_LoadMs(TMM_UART3_TX_FC, UART3_TX_PACK_MS);
}

void UART3_Poll(void)
{
    if(Uart3RxQ.QStat == STAT_NORMAL)
    {
        if(Tmm_IsON(TMM_UART3_RX_PACK) == RESET)
        {
            SYSTEM_DISABLE_ALL_IRQ;

            Uart3RxFrame.len = Uart3RxFrame.index;
            Queue_Push(&Uart3RxQ, ((const SFrame*)&Uart3RxFrame));

            Uart3RxFrame.index = 0;
            Uart3RxFrame.len   = 0;
            Uart3RxQ.QStat = STAT_IDEL;

            SYSTEM_ENABLE_ALL_IRQ;
        }
    }
    else
    {
        if(Uart3TxQ.QStat == STAT_IDEL)
        {
            if(Tmm_IsON(TMM_UART3_TX_FC) == RESET)
            {
                if(Queue_Pull(&Uart3TxQ, &Uart3TxFrame) == SUCCESS)
                {
                    UART3_SendFrame(&Uart3TxFrame);
                }
            }
        }
    }
}

void USART3_IRQHandler(void)
{
    if(USART_GetITStatus(USART3, USART_IT_RXNE) != RESET)
    {
        UART3_RxIRQ();
    }

    USART_ClearITPendingBit(USART1, USART_IT_RXNE);
}


