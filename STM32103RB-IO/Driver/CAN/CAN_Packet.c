/******************************************************************************

                  版权所有 (C), 2013-2017, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_Packet.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年2月17日
  最近修改   :
  功能描述   : CAN数据包操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年2月17日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static FlagStatus IsErrorReceive = RESET;

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : CAN_ClrPacket
 功能描述  : 清除数据包
 输入参数  : CAN_DataPacket *cPacket
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_ClrPacket(CAN_DataPacket *cPacket)
{
    assert_app_param(IS_CAN_PACKET_POINTER(cPacket));

    // 进入临界
    SYSTEM_DISABLE_ALL_IRQ;

    cPacket->Lock = RESET;

    cPacket->FrameIndex = 0;
    cPacket->FrameLen   = 0;

    cPacket->PacketBase.PacketID  = 0;
    cPacket->PacketBase.PacketLen = 0;

    // 退出临界
    SYSTEM_ENABLE_ALL_IRQ;

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : CAN_SetPacketStat
 功能描述  : 设置数据包状态
 输入参数  : CAN_PacketRunStatTypedef sStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SetPacketStat(CAN_PacketRunStatTypedef sStat)
{
    u32 tmpRandomValue;

    assert_app_param(IS_PACKET_RUN_STAT(sStat));

    // 进入临界
    SYSTEM_DISABLE_ALL_IRQ;

    CanStatPara.PacketAttr.StatAttr.Stat = sStat;

    switch(sStat)
    {
        // 未知
        case CAN_PACKET_STAT_UNKONW:

            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // 清除时间
            Tmm_Clr(TMM_CAN_PACKET);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        // 未知等待
        case CAN_PACKET_STAT_UNKONW_WAIT:
            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // 未知等待时间 50ms
            Tmm_LoadBaseTick(TMM_CAN_PACKET, CAN_PACKET_GET_STAT_WAIT_TIME);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        // 空闲状态
        case CAN_PACKET_STAT_IDEL:

            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // 清除时间
            Tmm_Clr(TMM_CAN_PACKET);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        // 发送RTS状态
        case CAN_PACKET_STAT_RTS:

            // 获取带范围的随机数 1ms~20ms
            tmpRandomValue = Ramdom_GetRandom(CAN_PACKET_RTS_REQUEST_TIME_MAX , CAN_PACKET_RTS_REQUEST_TIME_MIN);

            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // RTS 等待时间 随机延迟
            Tmm_LoadBaseTick(TMM_CAN_PACKET, tmpRandomValue);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        // 发送等待状态
        case CAN_PACKET_STAT_SEND_WAIT:

            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // 发送等待延迟 - 25ms
            Tmm_LoadBaseTick(TMM_CAN_PACKET, CAN_PACKET_SEND_WAIT_TIME);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        // 发送状态
        case CAN_PACKET_STAT_SEND:

            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // 清除时间
            Tmm_Clr(TMM_CAN_PACKET);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        // 接收等待状态
        case CAN_PACKET_STAT_RECEIVE_WAIT:

            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // 接收等待时间 - 50ms
            Tmm_LoadBaseTick(TMM_CAN_PACKET, CAN_PACKET_RECEIVE_WAIT_TIME);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        // 接收状态
        case CAN_PACKET_STAT_RECEIVE:

            // 进入临界
            //SYSTEM_DISABLE_ALL_IRQ;

            // 数据包接收完成等待时间 100ms
            Tmm_LoadBaseTick(TMM_CAN_PACKET, CAN_PACKET_RECEIVE_TIMEOUT_TIME);

            // 退出临界
            //SYSTEM_ENABLE_ALL_IRQ;
            break;

        default:
            break;
    }

    // 退出临界
    SYSTEM_ENABLE_ALL_IRQ;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_ClrPacketAttr
 功能描述  : 清除所有的数据包参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_ClrPacketAttr(void)
{
    // 设置状态为空闲状态 - 刷新时间
    CAN_SetPacketStat(CAN_PACKET_STAT_UNKONW);

    // 清空 发送数据包
    CAN_ClrPacket(&(CanStatPara.PacketAttr.SendPacket));
    // 清空 接收数据包
    CAN_ClrPacket(&(CanStatPara.PacketAttr.RecPacket));

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_LoadPacketRandomNum
 功能描述  : 载入数据包 - 随机数
 输入参数  : u32 sNum
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_LoadPacketRandomNum(u32 sNum)
{
    CanStatPara.PacketAttr.StatAttr.RandomNum = sNum;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_SendTrslReadPacketStat
 功能描述  : 获取数据包的状态
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendTrslReadPacketStat(void)
{
    AddrAttrTypedef desAddr;
    u8              sBuff[FRAME_SINGLE_DATA_LEN];
    FrameTypedef    sFrame;
    CAN_DataBase    sDb;
    u8              sIndex = 0;

    // 发送TRSL必须处于 Online状态
    if(CAN_IsOnline() == SET)
    {
        // 发送 获取数据包状态 必须在状态未知的情况下
        if(CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_UNKONW)
        {
            // 目的地址 - 空 - 广播
            desAddr.Type    = CMD_ADDR_TYPE_NONE;

            // 载入 数据包寄存器
            sBuff[sIndex++] = CAN_TRSL_REG_PACKET;

            // 载入 数据包的状态寄存器
            sBuff[sIndex++] = CAN_TRSL_REG_PACKET_STAT;

            // 载入控制寄存器读取帧
            if(Frame_GetSingleReadCnlFrame(desAddr, sBuff, sIndex, &sFrame) == SUCCESS)
            {
                // 获取CAN基础帧
                if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                {
                    // 添加CAN传输层位
                    if(CAN_AddTrslBit(&sDb) == SUCCESS)
                    {
                        // 载入 - 发送CAN基础帧
                        if(CAN_SendDbMsg(&sDb) == SUCCESS)
                        {
                            // 进入数据包 未知等待状态
                            CAN_SetPacketStat(CAN_PACKET_STAT_UNKONW_WAIT);
                        }
                    }
                }
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_SendTrslPacketRts
 功能描述  : 发送数据包的RTS
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendTrslPacketRts(void)
{
    AddrAttrTypedef desAddr;
    u8              sBuff[FRAME_SINGLE_DATA_LEN];
    FrameTypedef    sFrame;
    CAN_DataBase    sDb;
    u8              sIndex = 0;
    u32             randomNum = 0;

    // 发送TRSL必须处于 Online状态
    if(CAN_IsOnline() == SET)
    {
        // 发送RTS 预约 数据包状态必须处于 RTS等待请求状态
        if(CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RTS)
        {
            // 获取随机数
            randomNum = Random_Rand();

            // 目的地址 - 空 - 广播
            desAddr.Type    = CMD_ADDR_TYPE_NONE;

            // 载入 数据包寄存器
            sBuff[sIndex++] = CAN_TRSL_REG_PACKET;

            // 载入 数据包的RTS 预约
            sBuff[sIndex++] = CAN_TRSL_REG_PACKET_RNG;

            // 载入 随机数码
            sBuff[sIndex++] = (u8)((randomNum >> 24) & 0xFF);
            sBuff[sIndex++] = (u8)((randomNum >> 16) & 0xFF);
            sBuff[sIndex++] = (u8)((randomNum >> 8)  & 0xFF);
            sBuff[sIndex++] = (u8)((randomNum) & 0xFF);

            // 载入控制帧
            if(Frame_GetSingleCnlFrame(desAddr, sBuff, sIndex, &sFrame) == SUCCESS)
            {
                // 获取CAN基础帧
                if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                {
                    // 添加CAN传输层位
                    if(CAN_AddTrslBit(&sDb) == SUCCESS)
                    {
                        // 载入 - 发送CAN基础帧
                        if(CAN_SendDbMsg(&sDb) == SUCCESS)
                        {
                            // 载入 - 数据包随机值
                            CAN_LoadPacketRandomNum(randomNum);

                            // 进入数据包的发送等待
                            CAN_SetPacketStat(CAN_PACKET_STAT_SEND_WAIT);

                            return SUCCESS;
                        }
                    }
                }
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_ReceivePacketStatPoll
 功能描述  : 接收数据包状态执行函数
 输入参数  : CmdAttrTypedef cmdAttr
             uc8 datBuff[]
             u8 datLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_ReceivePacketStatPoll(CmdAttrTypedef cmdAttr,uc8 datBuff[], u8 datLen)
{
    AddrAttrTypedef desAddr;
    u8              sBuff[FRAME_SINGLE_DATA_LEN];
    FrameTypedef    sFrame;
    CAN_DataBase    sDb;
    u8              sIndex = 0;
    u8              tmpIndex = 0;
    u8              tmpByte;

    // 判断操作是否为读
    if(cmdAttr.Opt ==  CMD_OPT_READ)
    {
        // 向下读取
        if(cmdAttr.Dir == CMD_DIR_DOWN)
        {
            // 判断 数据包状态是否为 发送状态
            if(CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_SEND)
            {
                // 目的地址 - 广播
                desAddr.Type = CMD_ADDR_TYPE_NONE;

                // 载入 数据包寄存器
                sBuff[sIndex++] = CAN_TRSL_REG_PACKET;

                // 载入 数据包的状态寄存器
                sBuff[sIndex++] = CAN_TRSL_REG_PACKET_STAT;

                // 载入 数据包的发送状态
                sBuff[sIndex++] = CAN_PACKET_STAT_SEND;

                // 载入控制寄存器读取反馈帧
                if(Frame_GetSingleFeedbackCnlFrame(desAddr, sBuff, sIndex, &sFrame) == SUCCESS)
                {
                    // 获取CAN基础帧
                    if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                    {
                        // 添加CAN传输层位
                        if(CAN_AddTrslBit(&sDb) == SUCCESS)
                        {
                            // 载入 - 发送CAN基础帧
                            return CAN_SendDbMsg(&sDb);
                        }
                    }
                }
            }
        }
        // 向上读反馈
        else if(cmdAttr.Dir == CMD_DIR_UP)
        {
            // 判断是否有接收到数据
            if(datLen > tmpIndex)
            {
                // 获取状态码
                tmpByte = datBuff[tmpIndex++];

                // 判断获取到的状态是否为 发送状态
                if((CAN_PacketRunStatTypedef)tmpByte == CAN_PACKET_STAT_SEND)
                {
                    // 判断数据包是否为 未知状态 或 未知等待状态
                    if((CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_UNKONW)
                       || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_UNKONW_WAIT))
                    {
                        // 设置为接收状态
                        return CAN_SetPacketStat(CAN_PACKET_STAT_RECEIVE);
                    }
                }
            }
        }
        else
        {
        }
    }
    // 操作为写
    else if(cmdAttr.Opt ==  CMD_OPT_WIRTE)
    {
        // 向下写状态 - 强制状态
        if(cmdAttr.Dir == CMD_DIR_DOWN)
        {
            // 判断是否有接收到数据
            if(datLen > tmpIndex)
            {
                // 获取状态码
                tmpByte = datBuff[tmpIndex++];

                // 判断获取到的状态是否为 接收状态
                if((CAN_PacketRunStatTypedef)tmpByte == CAN_PACKET_STAT_RECEIVE)
                {
                    // 判断数据包是否为 未知状态 或 未知等待状态 - 空闲状态 - RTS请求等待 - 发送等待 - 接收等待
                    if((CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_UNKONW)
                       || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_UNKONW_WAIT)
                       || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_IDEL)
                       || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RTS)
                       || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_SEND_WAIT)
                       || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RECEIVE_WAIT))
                    {
                        // 设置为接收状态
                        return CAN_SetPacketStat(CAN_PACKET_STAT_RECEIVE);
                    }
                    // 判断目前状态是否为发送状态
                    else if(CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_SEND)
                    {
                        // 清空发送数据包大包缓存
                        CAN_ClrPacket(&(CanStatPara.PacketAttr.SendPacket));

                        // 设置为接收状态
                        return CAN_SetPacketStat(CAN_PACKET_STAT_RECEIVE);
                    }
                    else
                    {
                        return SUCCESS;
                    }
                }
            }
        }
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_ReceivePacketRtsPoll
 功能描述  : 接收数据包RTS执行函数
 输入参数  : CmdAttrTypedef cmdAttr
             uc8 datBuff[]
             u8 datLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_ReceivePacketRtsPoll(CmdAttrTypedef cmdAttr,uc8 datBuff[], u8 datLen)
{
    AddrAttrTypedef desAddr;
    u8              sBuff[FRAME_SINGLE_DATA_LEN];
    FrameTypedef    sFrame;
    CAN_DataBase    sDb;
    u8              sIndex = 0;
    u8              tmpIndex = 0;
    u32             tmpRandomNum = 0;

    // 判断操作是否为写
    if(cmdAttr.Opt ==  CMD_OPT_WIRTE)
    {
        // 向下写入
        if(cmdAttr.Dir == CMD_DIR_DOWN)
        {
            // 判断 获取到的数据
            if(datLen >= (CAN_TRSL_PACKET_RNG_DATA_LEN + tmpIndex))
            {
                tmpRandomNum  = (u32)(datBuff[tmpIndex++] << 24);
                tmpRandomNum |= (u32)(datBuff[tmpIndex++] << 16);
                tmpRandomNum |= (u32)(datBuff[tmpIndex++] << 8);
                tmpRandomNum |= (u32)(datBuff[tmpIndex++] & 0xFF);

                // 如果状态为 未知 - 未知等待 - 空闲 - RTS的等待状态
                if((CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_UNKONW)
                   || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_UNKONW_WAIT)
                   || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_IDEL)
                   || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RTS))
                {
                    // 接收等待状态
                    CAN_SetPacketStat(CAN_PACKET_STAT_RECEIVE_WAIT);
                }
                // 如果状态为 发送等待状态
                else if(CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_SEND_WAIT)
                {
                    // 随机数的值 小于等于本随机数
                    if(tmpRandomNum <= (CanStatPara.PacketAttr.StatAttr.RandomNum))
                    {
                        // 接收等待状态
                        CAN_SetPacketStat(CAN_PACKET_STAT_RECEIVE_WAIT);

                        // 判断随机码是否相等
                        if(tmpRandomNum == (CanStatPara.PacketAttr.StatAttr.RandomNum))
                        {
                            // 重新生成随机数种子
                            Random_Init();
                        }
                    }
                }
                // 如果状态为 数据包发送状态
                else if(CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_SEND)
                {
                    // 发送强制进入 - 接收状态

                    // 目的地址 - 广播
                    desAddr.Type = CMD_ADDR_TYPE_NONE;

                    // 载入 数据包寄存器
                    sBuff[sIndex++] = CAN_TRSL_REG_PACKET;

                    // 载入 数据包的状态寄存器
                    sBuff[sIndex++] = CAN_TRSL_REG_PACKET_STAT;

                    // 载入 数据包的接收状态
                    sBuff[sIndex++] = CAN_PACKET_STAT_RECEIVE;

                    // 载入控制帧
                    if(Frame_GetSingleCnlFrame(desAddr, sBuff, sIndex, &sFrame) == SUCCESS)
                    {
                        // 获取CAN基础帧
                        if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                        {
                            // 添加CAN传输层位
                            if(CAN_AddTrslBit(&sDb) == SUCCESS)
                            {
                                // 载入 - 发送CAN基础帧
                                return (CAN_SendDbMsg(&sDb));
                            }
                        }
                    }
                }
                // 如果数据包的状态为 接收状态或接收等待状态
                else if((CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RECEIVE_WAIT)
                        || (CanStatPara.PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RECEIVE))
                {
                    return SUCCESS;
                }
                else
                {
                }
            }
        }
    }

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : CAN_IsPacketUnlock
 功能描述  : 判断数据包是否解锁
 输入参数  : CAN_DataPacket *sDBPack
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus CAN_IsPacketUnlock(CAN_DataPacket *sDBPack)
{
    assert_app_param(IS_CAN_PACKET_POINTER(sDBPack));

    // 判断数据包是否被锁定
    if(sDBPack->Lock == SET)
    {
        // 返回数据包被锁定
        return RESET;
    }

    // 返回数据包解锁
    return SET;
}


/*****************************************************************************
 函 数 名  : CAN_PacketSend
 功能描述  : CAN 大数据包发送
 输入参数  : CAN_StatParaTypedef *sCanPara
             const CAN_PacketBase *sPck
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_PacketSend(CAN_StatParaTypedef *sCanPara, const CAN_PacketBase *sPck)
{
    u16 i = 0;
    u8 tmpFrameLen = 0;

    assert_app_param(IS_CAN_PARA_POINTER(sCanPara));
    assert_app_param(IS_CAN_PACKET_BASE_POINTER(sPck));

    // 发送数据包必须处于在线状态
    if(CAN_IsOnline() == SET)
    {
        // 判断是否不被锁定
        if(sCanPara->PacketAttr.SendPacket.Lock != SET)
        {
            // 判断数据包长度
            if((sPck->PacketLen > 0) && (sPck->PacketLen <= CAN_BUF_MAX_LEN))
            {
                // 载入包ID
                sCanPara->PacketAttr.SendPacket.PacketBase.PacketID = sPck->PacketID;

                // 载入数据
                for(i=0; i<sPck->PacketLen; i++)
                {
                    sCanPara->PacketAttr.SendPacket.PacketBase.PacketBuff[i] = sPck->PacketBuff[i];
                }

                // 载入数据长度
                sCanPara->PacketAttr.SendPacket.PacketBase.PacketLen = sPck->PacketLen;

                // 帧号从1开始
                sCanPara->PacketAttr.SendPacket.FrameIndex = 1;

                // 获取数据包 分帧
                tmpFrameLen = ((sPck->PacketLen)/8);

                // 判断最后一帧是否需要存在
                if(((sPck->PacketLen)%8) > 0)
                {
                    // 最后一帧
                    tmpFrameLen++;
                }

                // 载入总帧数
                sCanPara->PacketAttr.SendPacket.FrameLen   = tmpFrameLen;

                // 锁定数据包
                sCanPara->PacketAttr.SendPacket.Lock = SET;

                // 返回成功
                return SUCCESS;
            }
        }
    }

    // 返回失败
    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_PacketRec
 功能描述  : 获取接收数据包
 输入参数  : CAN_StatParaTypedef *sCanPara
             CAN_PacketBase *sPck
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_PacketRec(CAN_StatParaTypedef *sCanPara, CAN_PacketBase *sPck)
{
    u16 i = 0;

    assert_app_param(IS_CAN_PARA_POINTER(sCanPara));
    assert_app_param(IS_CAN_PACKET_BASE_POINTER(sPck));

    // 在线的情况下才能接收完整的数据包
    if(CAN_IsOnline() == SET)
    {
        // 判断数据包是否成功组帧 判断帧序号与帧总长度是否匹配
        if((sCanPara->PacketAttr.RecPacket.Lock == SET)
           && (sCanPara->PacketAttr.RecPacket.FrameIndex == sCanPara->PacketAttr.RecPacket.FrameLen)
           && (sCanPara->PacketAttr.RecPacket.FrameLen > 0))
        {
            // 判断帧长度是否合法
            if((sCanPara->PacketAttr.RecPacket.PacketBase.PacketLen > 0)
               && (sCanPara->PacketAttr.RecPacket.PacketBase.PacketLen <= CAN_BUF_MAX_LEN))
            {
                // 循环复制数据
                for(i=0; i<sCanPara->PacketAttr.RecPacket.PacketBase.PacketLen; i++)
                {
                    sPck->PacketBuff[i] = sCanPara->PacketAttr.RecPacket.PacketBase.PacketBuff[i];
                }

                // 获取数据包ID
                sPck->PacketID = sCanPara->PacketAttr.RecPacket.PacketBase.PacketID;
                // 获取数据包长度
                sPck->PacketLen = sCanPara->PacketAttr.RecPacket.PacketBase.PacketLen;

                // 复位接收到的数据包
                CAN_ClrPacket(&(sCanPara->PacketAttr.RecPacket));

                // 返回数据获取成功
                return SUCCESS;
            }
        }

    }

    // 返回数据获取失败
    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_GetSendPacketFrame
 功能描述  : 获取发送数据包中的发送数据帧
 输入参数  : CAN_StatParaTypedef *sCanPara
             CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_GetSendPacketFrame(CAN_StatParaTypedef *sCanPara, CAN_DataBase *sDB)
{
    u32 tmpFrameID = 0;
    u16 sByteIndex = 0;
    u8  i = 0;

    assert_app_param(IS_CAN_PARA_POINTER(sCanPara));
    assert_app_param(IS_CAN_DB_POINTER(sDB));

    // 必须在在线的情况下 且 处于发送状态
    if((CAN_IsOnline() == SET)
       &&(sCanPara->PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_SEND))
    {
        // 判断是否有数据需要发送
        if(sCanPara->PacketAttr.SendPacket.Lock == SET)
        {
            // 判断帧序号是否合法
            if((sCanPara->PacketAttr.SendPacket.FrameLen > 0)
               && ((sCanPara->PacketAttr.SendPacket.FrameLen) >= (sCanPara->PacketAttr.SendPacket.FrameIndex))
               && (sCanPara->PacketAttr.SendPacket.FrameIndex > 0))
            {
                // 多数据包 标志位
                tmpFrameID |= CAN_FRAME_TYPE_PACKET;
                // 载入ID
                tmpFrameID |= sCanPara->PacketAttr.SendPacket.PacketBase.PacketID;
                // 清空ID号中的 帧总长度 和帧序号
                tmpFrameID &= 0xFFFF0000;
                // 载入帧总长度
                tmpFrameID |= ((sCanPara->PacketAttr.SendPacket.FrameLen) << 8);
                // 载入帧序号
                tmpFrameID |= sCanPara->PacketAttr.SendPacket.FrameIndex;
                // 载入ID
                sDB->FrameID = tmpFrameID;

                // 获取数据下标
                sByteIndex = (((sCanPara->PacketAttr.SendPacket.FrameIndex)-1) * CAN_FRAME_MAX_LEN);

                if(sCanPara->PacketAttr.SendPacket.FrameIndex < sCanPara->PacketAttr.SendPacket.FrameLen)
                {
                    // 循环复制数据
                    for(i=0; i<CAN_FRAME_MAX_LEN; i++)
                    {
                        sDB->DataBuff[i] = sCanPara->PacketAttr.SendPacket.PacketBase.PacketBuff[sByteIndex++];
                    }

                    // 载入帧长度
                    sDB->FrameLen = CAN_FRAME_MAX_LEN;

                    // 准备发送下一包数据
                    sCanPara->PacketAttr.SendPacket.FrameIndex += 1;

                    return SUCCESS;
                }
                else
                {
                    // 判断剩余数据是否小于等于 一帧的最大数据长度
                    if(((sCanPara->PacketAttr.SendPacket.PacketBase.PacketLen) - sByteIndex) <= CAN_FRAME_MAX_LEN)
                    {
                        // 循环复制剩余数据
                        for(i=0; sByteIndex<(sCanPara->PacketAttr.SendPacket.PacketBase.PacketLen); sByteIndex++)
                        {
                            sDB->DataBuff[i++] = sCanPara->PacketAttr.SendPacket.PacketBase.PacketBuff[sByteIndex];
                        }

                        // 载入帧长度
                        sDB->FrameLen = i;

                        // 复位数据包
                        CAN_ClrPacket(&(sCanPara->PacketAttr.SendPacket));

                        // 发送完成 - 设置为空闲状态
                        CAN_SetPacketStat(CAN_PACKET_STAT_IDEL);

                        return SUCCESS;
                    }
                    else
                    {
                        // 复位数据包
                        CAN_ClrPacket(&(sCanPara->PacketAttr.SendPacket));
                    }
                }
            }
            else
            {
                // 复位数据包
                CAN_ClrPacket(&(sCanPara->PacketAttr.SendPacket));
            }
        }
    }

    // 返回错误
    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_PutRecPacketFrame
 功能描述  : 将数据帧放入接收数据包内
 输入参数  : CAN_StatParaTypedef *sCanPara
             CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_PutRecPacketFrame(CAN_StatParaTypedef *sCanPara, CAN_DataBase *sDB)
{
    u8 tmpFrameLen = 0;
    u8 tmpFrameIndex = 0;
    u8 i = 0;
    u16 sByteIndex = 0;

    assert_app_param(IS_CAN_PARA_POINTER(sCanPara));
    assert_app_param(IS_CAN_DB_POINTER(sDB));

    // 判断是否还没有组成完整的数据帧
    if(sCanPara->PacketAttr.RecPacket.Lock != SET)
    {
        // 判断是否为大数据包帧 且 载入的数据长度无异常
        if(((sDB->FrameID & CAN_FRAME_TYPE_PACKET) == CAN_FRAME_TYPE_PACKET)
           && ((sDB->FrameLen > 0) && (sDB->FrameLen <= CAN_FRAME_MAX_LEN)))
        {
            tmpFrameLen     = (((sDB->FrameID) >> 8) & 0xFF);
            tmpFrameIndex   = ((sDB->FrameID) & 0xFF);

            // 判断状态是否为发送
            if(sCanPara->PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_SEND)
            {
                IsErrorReceive = SET;

                return ERROR;
            }

            // 判断数据帧 是否 合法
            if((tmpFrameLen > 0) && (tmpFrameIndex > 0) && (tmpFrameLen >= tmpFrameIndex))
            {
                // 判断是否不为第一帧数据
                if(tmpFrameIndex > 1)
                {
                    // 接收不为第一帧的数据 必须为接收状态
                    if(sCanPara->PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RECEIVE)
                    {
                        // 判断帧ID是否相同 且帧ID是链接的 且 帧总长度相等
                        if(((sCanPara->PacketAttr.RecPacket.PacketBase.PacketID) == (sDB->FrameID & 0x1FFF0000))
                           && (tmpFrameIndex == (sCanPara->PacketAttr.RecPacket.FrameIndex+1))
                           && (tmpFrameLen == sCanPara->PacketAttr.RecPacket.FrameLen))
                        {
                            // 载入数据包长度
                            sCanPara->PacketAttr.RecPacket.FrameLen = tmpFrameLen;
                            // 载入数据包帧下标
                            sCanPara->PacketAttr.RecPacket.FrameIndex = tmpFrameIndex;

                            // 获取数据下标
                            sByteIndex = sCanPara->PacketAttr.RecPacket.PacketBase.PacketLen;

                            // 复制数据
                            for(i=0; i<sDB->FrameLen; i++)
                            {
                                sCanPara->PacketAttr.RecPacket.PacketBase.PacketBuff[sByteIndex++] = sDB->DataBuff[i];
                            }

                            // 载入数据长度
                            sCanPara->PacketAttr.RecPacket.PacketBase.PacketLen = sByteIndex;

                            // 判断是否为最后一帧
                            if(tmpFrameLen == tmpFrameIndex)
                            {
                                // 组成完整的数据帧
                                sCanPara->PacketAttr.RecPacket.Lock = SET;

                                // 设置状态为空闲状态
                                CAN_SetPacketStat(CAN_PACKET_STAT_IDEL);
                            }
                            else
                            {
                                // 设置状态为接收状态 - 刷新时间
                                CAN_SetPacketStat(CAN_PACKET_STAT_RECEIVE);
                            }

                            return SUCCESS;
                        }
                        else
                        {
                            // 汇报执行错误
                            // LedIndicator_FlashSet(LED_ID_RS232, LED_FLASH_STAT_ON);
                        }
                    }
                    else
                    {
                        // 汇报执行错误
                        // LedIndicator_FlashSet(LED_ID_RS232, LED_FLASH_STAT_ON);
                    }
                }
                else
                {
                    if(tmpFrameIndex == 1)
                    {
                        if((sCanPara->PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RECEIVE)
                           || (sCanPara->PacketAttr.StatAttr.Stat == CAN_PACKET_STAT_RECEIVE_WAIT))
                        {
                            // 载入帧ID
                            sCanPara->PacketAttr.RecPacket.PacketBase.PacketID = (sDB->FrameID & 0x1FFF0000);

                            // 载入数据包长度
                            sCanPara->PacketAttr.RecPacket.FrameLen     = tmpFrameLen;
                            // 载入数据包帧下标
                            sCanPara->PacketAttr.RecPacket.FrameIndex   = tmpFrameIndex;

                            // 下标清零
                            sByteIndex = 0;

                            // 复制数据
                            for(i=0; i<sDB->FrameLen; i++)
                            {
                                sCanPara->PacketAttr.RecPacket.PacketBase.PacketBuff[sByteIndex++] = sDB->DataBuff[i];
                            }

                            // 载入当前保存的数据长度
                            sCanPara->PacketAttr.RecPacket.PacketBase.PacketLen = sByteIndex;

                            // 判断是否为最后一帧
                            if(tmpFrameLen == tmpFrameIndex)
                            {
                                // 组成完整的数据帧
                                sCanPara->PacketAttr.RecPacket.Lock = SET;

                                // 设置状态为空闲状态
                                CAN_SetPacketStat(CAN_PACKET_STAT_IDEL);
                            }
                            else
                            {
                                // 设置状态为接收状态 - 刷新时间
                                CAN_SetPacketStat(CAN_PACKET_STAT_RECEIVE);
                            }
                            return SUCCESS;
                        }
                    }
                }
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_PacketMsgPoll
 功能描述  : 执行数据包的消息轮询
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_PacketMsgPoll(void)
{
    static FlagStatus PacketIsOffLine = RESET;
    CAN_DataBase sDB;

    // 判断是否在线
    if(CAN_IsOnline() == SET)
    {
        // 设置 不为离线状态
        PacketIsOffLine = RESET;

        if(IsErrorReceive == SET)
        {
            // 进入临界
            SYSTEM_DISABLE_ALL_IRQ;

            IsErrorReceive = RESET;

            // 退出临界
            SYSTEM_ENABLE_ALL_IRQ;

            // 清除所有数据包的参数
            CAN_ClrPacketAttr();
        }
        else
        {
            // 进入临界
            SYSTEM_DISABLE_ALL_IRQ;

            // 检查所有的状态
            switch(CanStatPara.PacketAttr.StatAttr.Stat)
            {
                case CAN_PACKET_STAT_UNKONW:

                    // 发送查询状态
                    CAN_SendTrslReadPacketStat();
                    break;

                case CAN_PACKET_STAT_UNKONW_WAIT:

                    // 判断等待时间是否到达
                    if(Tmm_IsON(TMM_CAN_PACKET) != SET)
                    {
                        // 设置为空闲状态
                        CAN_SetPacketStat(CAN_PACKET_STAT_IDEL);
                    }
                    break;

                case CAN_PACKET_STAT_IDEL:

                    // 判断是否有数据需要发送
                    if(CanStatPara.PacketAttr.SendPacket.Lock == SET)
                    {
                        // 设置为RTS请求状态
                        CAN_SetPacketStat(CAN_PACKET_STAT_RTS);
                    }
                    break;

                case CAN_PACKET_STAT_RTS:

                    // 判断等待时间是否到达
                    if(Tmm_IsON(TMM_CAN_PACKET) != SET)
                    {
                        // 发送RTS预约包
                        CAN_SendTrslPacketRts();
                    }
                    break;

                case CAN_PACKET_STAT_SEND_WAIT:

                    // 判断等待时间是否到达
                    if(Tmm_IsON(TMM_CAN_PACKET) != SET)
                    {
                        // 设置为发送状态
                        CAN_SetPacketStat(CAN_PACKET_STAT_SEND);
                    }
                    break;

                case CAN_PACKET_STAT_SEND:

                    // 判断CAN1 是否可以发送
                    if(CAN_SendIsReady(CAN_1) == SET)
                    {
                        // 从发送队列中取出发送数据
                        if(CAN_GetSendPacketFrame(&(CanStatPara), &sDB) == SUCCESS)
                        {
                            // 发送一帧数据
                            CAN_SendCanDb(&sDB);
                        }
                        else
                        {
                            // 设置为空闲状态
                            CAN_SetPacketStat(CAN_PACKET_STAT_IDEL);

                            // 清空 发送数据包
                            CAN_ClrPacket(&(CanStatPara.PacketAttr.SendPacket));
                        }
                    }
                    break;

                case CAN_PACKET_STAT_RECEIVE_WAIT:

                    // 判断等待时间是否到达
                    if(Tmm_IsON(TMM_CAN_PACKET) != SET)
                    {
                        // 设置为空闲状态
                        CAN_SetPacketStat(CAN_PACKET_STAT_IDEL);

                        // 清空 发送数据包
                        CAN_ClrPacket(&(CanStatPara.PacketAttr.RecPacket));
                    }
                    break;

                case CAN_PACKET_STAT_RECEIVE:

                    // 判断等待时间是否到达
                    if(Tmm_IsON(TMM_CAN_PACKET) != SET)
                    {
                        // 设置为空闲状态
                        CAN_SetPacketStat(CAN_PACKET_STAT_IDEL);

                        // 清空 发送数据包
                        CAN_ClrPacket(&(CanStatPara.PacketAttr.RecPacket));
                    }
                    break;

                default:

                    // 清除所有数据包的参数
                    CAN_ClrPacketAttr();
                    break;
            }

            // 退出临界
            SYSTEM_ENABLE_ALL_IRQ;
        }

    }
    else
    {
        // 判断数据包是否不为 离线状态
        if(PacketIsOffLine == RESET)
        {
            // 设置为离线状态
            PacketIsOffLine = SET;

            // 清除所有数据包的参数
            CAN_ClrPacketAttr();
        }
    }

    return ERROR;
}

ErrorStatus CAN_PacketReceivePoll(void)
{
    CAN_PacketBase gPacket;
    u16 i;

    if(CAN_PacketRec(&(CanStatPara), &gPacket) == SUCCESS)
    {
        if((gPacket.PacketID & (0x00FF0000)) == RECEIVE_PACKET_ID_NUM)
        {
            if(gPacket.PacketLen == CAN_BUF_MAX_LEN)
            {
                for(i=0; i<CAN_BUF_MAX_LEN; i++)
                {
                    if(gPacket.PacketBuff[i] != (u8)i)
                    {
                        return ERROR;
                    }
                }

                // 取反LED
                // LedIndicator_Out(LED_ID_RS232, LED_STAT_FLIP);
            }
        }
    }

    return SUCCESS;
}
