/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_Trsl.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年2月24日
  最近修改   :
  功能描述   : CAN的传输层操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : CAN_TrslInit
 功能描述  : CAN传输层初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_TrslInit(void)
{
    return CAN_SendTrslAreaTestFrame();
}

/*****************************************************************************
 函 数 名  : CAN_AddTrslBit
 功能描述  : CAN基础帧 加入传输层标识位
 输入参数  : CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_AddTrslBit(CAN_DataBase *sDB)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sDB));

    // 加入传输层标识
    sDB->FrameID |= CAN_FRAME_TYPE_TRSL;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_SendTrslAreaTestFrame
 功能描述  : CAN 发送区域测试帧
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendTrslAreaTestFrame(void)
{
    AddrAttrTypedef desAddr;
    u8              sBuff[FRAME_SINGLE_DATA_LEN];
    FrameTypedef    sFrame;
    CAN_DataBase    sDb;
    u8              sIndex = 0;

    // 目的地址 - 空 - 广播
    desAddr.Type    = CMD_ADDR_TYPE_NONE;


    // 载入 传输层状态寄存器
    sBuff[sIndex++] = CAN_TRSL_REG_STAT;

    // 载入 区域状态寄存器
    sBuff[sIndex++] = CAN_TRSL_REG_STAT_AREA;

    // 载入 区域状态寄存器测试
    sBuff[sIndex++] = CAN_TRSL_STAT_AREA_TEST;

    // 载入控制帧
    if(Frame_GetSingleCnlFrame(desAddr, sBuff, sIndex, &sFrame) == SUCCESS)
    {
        // 获取CAN基础帧
        if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
        {
            // 添加CAN传输层位
            if(CAN_AddTrslBit(&sDb) == SUCCESS)
            {
                // 载入 - 发送CAN基础帧
                return CAN_LoadSendMsg(&sDb);
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_ReceiveTrslStatPoll
 功能描述  : CAN接收到传输层状态字节执行
 输入参数  : CmdAttrTypedef cmdAttr
             uc8 datBuff[]
             u8 datLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_ReceiveTrslStatPoll(CmdAttrTypedef cmdAttr,uc8 datBuff[], u8 datLen)
{
    u8 gIndex = 0;
    u8 tmpAreaStatCode = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(datBuff));

    // 判断是否有数据
    if(datLen > gIndex)
    {
        // 获取状态码
        tmpAreaStatCode = datBuff[gIndex++];

        // 区域保持码 - 上电发送 - 作为发送测试的
        if(tmpAreaStatCode == CAN_TRSL_STAT_AREA_TEST)
        {
            if((cmdAttr.Dir == CMD_DIR_DOWN)
               &&(cmdAttr.Opt == CMD_OPT_WIRTE))
            {
                return SUCCESS;
            }
        }
        // 区域广播码 - 定时广播 - 作为区域总线保持使用
        else if(tmpAreaStatCode == CAN_TRSL_STAT_AREA_BROADCAST)
        {
            return SUCCESS;
        }
        else
        {
        }
    }

    return ERROR;
}


