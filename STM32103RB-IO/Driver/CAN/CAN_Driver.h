/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_Driver.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : CAN_Driver.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __CAN_DRIVER_H__
#define __CAN_DRIVER_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
#ifndef     NULL
    #define     NULL     ((void *)0)
#endif

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

#define CAN_BTR_10K                             0
#define CAN_BTR_20K                             1
#define CAN_BTR_40K                             2
#define CAN_BTR_50K                             3
#define CAN_BTR_80K                             4
#define CAN_BTR_100K                            5
#define CAN_BTR_125K                            6
#define CAN_BTR_200K                            7
#define CAN_BTR_250K                            8
#define CAN_BTR_400K                            9
#define CAN_BTR_500K                            10
#define CAN_BTR_666K                            11
#define CAN_BTR_800K                            12
#define CAN_BTR_1000K                           13

typedef enum
{
    CanBtr_10K   = CAN_BTR_10K,
    CanBtr_20K   = CAN_BTR_20K,
    CanBtr_40K   = CAN_BTR_40K,
    CanBtr_50K   = CAN_BTR_50K,
    CanBtr_80K   = CAN_BTR_80K,
    CanBtr_100K  = CAN_BTR_100K,
    CanBtr_125K  = CAN_BTR_125K,
    CanBtr_200K  = CAN_BTR_200K,
    CanBtr_250K  = CAN_BTR_250K,
    CanBtr_400K  = CAN_BTR_400K,
    CanBtr_500K  = CAN_BTR_500K,
    CanBtr_666K  = CAN_BTR_666K,
    CanBtr_800K  = CAN_BTR_800K,
    CanBtr_1000K = CAN_BTR_1000K,
    CanBtr_MAX,
} CAN_BtrType;
#define     IS_CAN_BTR_TYPE(PARAM)      (((PARAM) == CanBtr_10K)    ||  \
                                         ((PARAM) == CanBtr_20K)    ||  \
                                         ((PARAM) == CanBtr_40K)    ||  \
                                         ((PARAM) == CanBtr_50K)    ||  \
                                         ((PARAM) == CanBtr_80K)    ||  \
                                         ((PARAM) == CanBtr_100K)   ||  \
                                         ((PARAM) == CanBtr_125K)   ||  \
                                         ((PARAM) == CanBtr_200K)   ||  \
                                         ((PARAM) == CanBtr_250K)   ||  \
                                         ((PARAM) == CanBtr_400K)   ||  \
                                         ((PARAM) == CanBtr_500K)   ||  \
                                         ((PARAM) == CanBtr_666K)   ||  \
                                         ((PARAM) == CanBtr_800K)   ||  \
                                         ((PARAM) == CanBtr_1000K))
#define     IS_CAN_BTR_PIONTER(PARAM)   (((PARAM) != NULL))





typedef enum
{
    CAN_1,
    CAN_2,
} CAN_Type;
#define     IS_CAN_TYPE(PARAM)              (((PARAM) == CAN_1)     ||  \
                                             ((PARAM) == CAN_2))

#define     IS_CAN_TYPE_LEGAL(PARAM)        (((PARAM) == CAN_1))



#define     IS_CAN_RX_MSG_POINTER(PARAM)    (((PARAM) != NULL))

// CAN1使用的波特率
#define     SYSTEM_CAN1_USER_BTR                CAN_BTR_20K



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  void            CAN_DriverInit              (void);

extern  ErrorStatus     CAN_DisRxIrn                (CAN_Type canId);
extern  ErrorStatus     CAN_DisTxIrn                (CAN_Type canId);
extern  ErrorStatus     CAN_EnbRxIrn                (CAN_Type canId);
extern  ErrorStatus     CAN_EnbTxIrn                (CAN_Type canId);

extern  ErrorStatus     CAN_InitModule              (CAN_Type canId, CAN_BtrType sBtr, u16 uFilterMaskId);

extern  FlagStatus      CAN_SendBuffNotFull         (CAN_Type canId);
extern  ErrorStatus     CAN_SendExtFrame            (CAN_Type canId, CAN_DataBase *sDB);

extern  ErrorStatus     CAN_RecISR                  (CAN_Type canId, CanRxMsg *sCanRxMsg);



#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __CAN_DRIVER_H__ */
