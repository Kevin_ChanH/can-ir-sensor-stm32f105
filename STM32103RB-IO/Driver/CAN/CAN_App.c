/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_App.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : CAN 应用层操作文件
  函数列表   :
              CAN_GetBtr
              CAN_InitRunPara
              CAN_MsgPoll
              CAN_RxMsgPoll
              CAN_SendIsReady
              CAN_SetBtr
              CAN_TxMsgPoll
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
// CAN 状态参数
CAN_StatParaTypedef     CanStatPara;

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
// 收发数据帧管理
static CAN_DataBase CanTxSFrame[CAN_TX_QUEUE_SIZE];
static CAN_DataBase CanRxSFrame[CAN_RX_QUEUE_SIZE];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/********************************************************************
* 名称 : ManageCANTrans_Init
* 功能 : 初始化变量
*
* 输入 :
* 输出 :
*
* 备注: (修改记录内容、时间)
* 应用层主调
********************************************************************/
void  CAN_InitRunPara(void)
{
    /* 波特率设置为 初始化波特率 */
    CanStatPara.CANBtr = CAN_INIT_BTR;

    /* 初始化CAN总线状态 */
    CanStatPara.StatAttr.BusStat    = CAN_BUS_STAT_UNKONW;
    CanStatPara.StatAttr.SendStat   = CAN_SEND_STAT_IDEL;
    CanStatPara.StatAttr.BrcActive  = RESET;
    CanStatPara.StatAttr.RandomNum  = CAN_INIT_BUS_STAT_RANDOM_NUM;

    /* 初始化数据包格式 */
    CanStatPara.PacketAttr.StatAttr.Stat        = CAN_PACKET_STAT_UNKONW;
    CanStatPara.PacketAttr.StatAttr.RandomNum   = CAN_INIT_PACKET_RANDOM_NUM;

    /* 初始化 接收数据包 */
    CanStatPara.PacketAttr.RecPacket.PacketBase.PacketID    = 0;
    CanStatPara.PacketAttr.RecPacket.PacketBase.PacketLen   = 0;
    CanStatPara.PacketAttr.RecPacket.FrameIndex             = 0;
    CanStatPara.PacketAttr.RecPacket.FrameLen               = 0;
    CanStatPara.PacketAttr.RecPacket.Lock                   = RESET;

    /* 初始化 发送数据包 */
    CanStatPara.PacketAttr.SendPacket.PacketBase.PacketID   = 0;
    CanStatPara.PacketAttr.SendPacket.PacketBase.PacketLen  = 0;
    CanStatPara.PacketAttr.SendPacket.FrameIndex            = 0;
    CanStatPara.PacketAttr.SendPacket.FrameLen              = 0;
    CanStatPara.PacketAttr.SendPacket.Lock                  = RESET;

    /* 接收控制帧队列 */
    CanStatPara.DbAtttr.RecDB.QSize     = CAN_RX_QUEUE_SIZE;
    CanStatPara.DbAtttr.RecDB.Surplus   = CAN_RX_QUEUE_SIZE;
    CanStatPara.DbAtttr.RecDB.QHead     = 0;
    CanStatPara.DbAtttr.RecDB.QTail     = 0;
    CanStatPara.DbAtttr.RecDB.sFrame    = CanRxSFrame;

    /* 发送控制帧队列 */
    CanStatPara.DbAtttr.SendDB.QSize    = CAN_TX_QUEUE_SIZE;
    CanStatPara.DbAtttr.SendDB.Surplus  = CAN_TX_QUEUE_SIZE;
    CanStatPara.DbAtttr.SendDB.QHead    = 0;
    CanStatPara.DbAtttr.SendDB.QTail    = 0;
    CanStatPara.DbAtttr.SendDB.sFrame   = CanTxSFrame;

    /* CAN 操作等待时间 - 随机范围2~3.5秒 */
    Tmm_LoadMs(TMM_CAN_OPT_WAIT, Ramdom_GetRandom(CAN_OPT_WAIT_RNG_MS_MAX, CAN_OPT_WAIT_RNG_MS_MIN));
}


/*****************************************************************************
 函 数 名  : CAN_GetCanAttr
 功能描述  : 获取CAN的属性
 输入参数  : CAN_Type canId
             CAN_StatParaTypedef *gCanAttr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月18日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_GetCanAttr(CAN_Type canId, CAN_StatParaTypedef *gCanAttr)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));
    assert_app_param(IS_CAN_PARA_POINTER(gCanAttr));

    gCanAttr = gCanAttr;
    gCanAttr = &CanStatPara;

    return SUCCESS;
}


/********************************************************************
* 名称 : CAN_SetBaudrate
* 功能 : 配置CAN总线的波特率
*
* 输入 : CAN总线端口 ，波特率
* 输出 : 成功失败标志
*
* 备注: (修改记录内容、时间)
* 应用层主调
********************************************************************/
ErrorStatus  CAN_SetBtr(CAN_Type  canId,   CAN_BtrType  sBtr)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));
    assert_app_param(IS_CAN_BTR_TYPE(sBtr));


    // 保存配置的波特率
    CanStatPara.CANBtr = sBtr;

    // 初始化CAN ，设置波特率
    CAN_InitModule(canId, sBtr, 0);

    // 打开中断
    CAN_EnbRxIrn(CAN_1);

    // 返回配置成功
    return(SUCCESS);
}

/********************************************************************
* 名称 : CAN_GetBaudrate
* 功能 : 获取CAN 总线的波特率
*
* 输入 :
* 输出 :
*
* 备注: (修改记录内容、时间)
* 应用层主调
********************************************************************/
ErrorStatus  CAN_GetBtr(CAN_Type  canId,   CAN_BtrType  *gBtr)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));
    assert_app_param(IS_CAN_BTR_PIONTER(gBtr));

    // 返回 当前配置的波特率
    *gBtr = CanStatPara.CANBtr;

    return SUCCESS;
}

/********************************************************************
* 名称 : CAN_SendIsReady
* 功能 : 查询CAN能否发送
*
* 输入 :
* 输出 :
*
* 备注: (修改记录内容、时间)
* 应用层主调
********************************************************************/
FlagStatus  CAN_SendIsReady(CAN_Type  canId)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));

    // 判断发送邮箱是否不为空 且 发送状态为空闲状态
    if((CAN_SendBuffNotFull(canId) == SET)
       && (CanStatPara.StatAttr.SendStat == CAN_SEND_STAT_IDEL))
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : CAN_IsOnline
 功能描述  : 判断CAN是否在线
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus CAN_IsOnline(void)
{
    // 判断是否在线
    if(CanStatPara.StatAttr.BusStat == CAN_BUS_STAT_ONLINE)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : CAN_LoadSendMsg
 功能描述  : 载入发送消息
 输入参数  : CAN_DataBase *sMsg
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_LoadSendMsg(CAN_DataBase *sMsg)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sMsg));

    // 载入数据
    return CAN_QueuePush(&(CanStatPara.DbAtttr.SendDB), sMsg);
}

/*****************************************************************************
 函 数 名  : CAN_SendDbMsg
 功能描述  : 发送基础消息帧
 输入参数  : CAN_DataBase *sMsg
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendDbMsg(CAN_DataBase *sMsg)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sMsg));

    // 判断是否在线
    if(CAN_IsOnline() == SET)
    {
        // 载入数据
        return CAN_QueuePush(&(CanStatPara.DbAtttr.SendDB), sMsg);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_SendCanDb
 功能描述  : 发送CAN数据
 输入参数  : CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendCanDb(CAN_DataBase *sDB)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sDB));

    // 发送状态设置 为 发送等待
    CAN_SetSendStat(CAN_SEND_STAT_WAIT);

    // 载入发送时间 10ms
    Tmm_LoadMs(TMM_CAN_SEND_STAT, CAN_BUS_SEND_WAIT_TIME_MS);

    // 发送一帧数据
    return CAN_SendExtFrame(CAN_1, sDB);
}

/*****************************************************************************
 函 数 名  : CAN_RxMsgPoll
 功能描述  : CAN执行接收
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_RxMsgPoll(void)
{
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_TxMsgPoll
 功能描述  : CAN执行发送
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_TxMsgPoll(void)
{
    CAN_DataBase sDB;

    // 判断发送队列是否不为空
    if(CAN_IsQueueNotEmpty(&(CanStatPara.DbAtttr.SendDB)) == SET)
    {
        // 判断CAN1 是否可以发送
        if(CAN_SendIsReady(CAN_1) == SET)
        {
            // 从发送队列中取出发送数据
            if(CAN_QueuePull(&(CanStatPara.DbAtttr.SendDB), &sDB) == SUCCESS)
            {
                // 发送一帧数据
                CAN_SendCanDb(&sDB);
            }
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_SetSendStat
 功能描述  : 设置发送状态
 输入参数  : CANBusSendStatTypedef gStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SetSendStat(CANBusSendStatTypedef gStat)
{
    assert_app_param(IS_CAN_SEND_STAT_TYPE(gStat));

    // 进入临界
    SYSTEM_DISABLE_ALL_IRQ;

    // 设置发送状态
    CanStatPara.StatAttr.SendStat = gStat;

    // 退出临界
    SYSTEM_ENABLE_ALL_IRQ;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_SetBusStat
 功能描述  : 设置总线状态
 输入参数  : CAN_BusRunStatTypedef gStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SetBusStat(CAN_BusRunStatTypedef gStat)
{
    assert_app_param(IS_CAN_BUS_STAT_TYPE(gStat));

    // 进入临界
    SYSTEM_DISABLE_ALL_IRQ;

    // 设置发送状态
    CanStatPara.StatAttr.BusStat = gStat;

    // 退出临界
    SYSTEM_ENABLE_ALL_IRQ;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_RefreshOfflineWaitTime
 功能描述  : 刷新离线等待时间
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
/*
ErrorStatus CAN_RefreshOfflineWaitTime(void)
{
    // 进入临界
    SYSTEM_DISABLE_ALL_IRQ;

    // 刷新 总线离线的等待时间 15秒
    TmmExp_Load(TMM_EXP_CAN_BUS_OFFLINE_WAIT, 0, 0, CAN_BUS_OFFLINE_WAIT_TIME_SEC);

    // 退出临界
    SYSTEM_ENABLE_ALL_IRQ;

    return SUCCESS;
}
*/

/*****************************************************************************
 函 数 名  : CAN_RefreshBroadCastWaitTime
 功能描述  : 刷新广播等待时间
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
/*
ErrorStatus CAN_RefreshBroadCastWaitTime(void)
{
    // 判断广播是否激活
    if(CanStatPara.StatAttr.BrcActive == SET)
    {
        // 进入临界
        SYSTEM_DISABLE_ALL_IRQ;

        // 刷新 广播的等待时间 10秒
        TmmExp_Load(TMM_EXP_CAN_BUS_BROADCAST_WAIT, 0, 0, CAN_BUS_BROADCAST_WAIT_TIME_SEC);


        // 退出临界
        SYSTEM_ENABLE_ALL_IRQ;
    }

    return SUCCESS;
}
*/

/*****************************************************************************
 函 数 名  : CAN_SetBusStatOnline
 功能描述  : 设置总线状态为在线
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SetBusStatOnline(void)
{
    // 判断状态是否不为在线
    if(CAN_IsOnline() != SET)
    {
        // 设置总线状态为 在线
        CAN_SetBusStat(CAN_BUS_STAT_ONLINE);
    }

    // 刷新离线等待时间
    // CAN_RefreshOfflineWaitTime();

    // 刷新广播等待时间
    // CAN_RefreshBroadCastWaitTime();

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_SendFinishSetStat
 功能描述  : 发送完成设置状态
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendFinishSetStat(void)
{
    // 设置发送状态为 - 空闲状态
    CAN_SetSendStat(CAN_SEND_STAT_IDEL);

    // 设置总线状态为 - 在线状态
    CAN_SetBusStatOnline();

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_ReceiveMsgSetStat
 功能描述  : 接收消息设置状态
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_ReceiveMsgSetStat(void)
{
    // 设置发送状态为 - 空闲状态
    // CAN_SetSendStat(CAN_SEND_STAT_IDEL);

    // 设置总线状态为 - 在线状态
    CAN_SetBusStatOnline();

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_SendStatCheckPoll
 功能描述  : CAN 检查发送状态
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月23日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendStatCheckPoll(void)
{
    // 判断是否为发送等待状态
    if(CanStatPara.StatAttr.SendStat == CAN_SEND_STAT_WAIT)
    {
        // 判断时间是否倒计时完成
        if(Tmm_IsON(TMM_CAN_SEND_STAT) != SET)
        {
            // 将发送状态设置为 超时状态
            CAN_SetSendStat(CAN_SEND_STAT_TIMEOUT);

            // 总线状态设置为 离线状态
            CAN_SetBusStat(CAN_BUS_STAT_OFFLINE);
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_MsgPoll
 功能描述  : CAN消息操作
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_MsgPoll(void)
{
    static FlagStatus CanTrslInitFlag = RESET;

    if(Tmm_IsON(TMM_CAN_OPT_WAIT) != SET)
    {
        // 判断是否未初始化 且 CAN总线处于未初始化状态
        if((CanTrslInitFlag != SET)
        && (CAN_IsOnline() != SET))
        {
            CanTrslInitFlag = SET;

            /* 初始化CAN传输层 */
            CAN_TrslInit();
        }

        // 执行CAN接收操作
        CAN_RxMsgPoll();
        // 执行CAN发送操作
        CAN_TxMsgPoll();

        // 执行CAN发送状态检查
        CAN_SendStatCheckPoll();

        // CAN数据包消息轮询
        CAN_PacketMsgPoll();
    }
    // CAN_PacketReceivePoll();

    // 返回成功
    return SUCCESS;
}

