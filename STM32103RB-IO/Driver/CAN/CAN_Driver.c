/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_Driver.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : CAN 驱动层文件
  函数列表   :
              CAN_DisRxIrn
              CAN_DisTxIrn
              CAN_DriveInit
              CAN_EnbRxIrn
              CAN_EnbTxIrn
              CAN_IdIsLegal
              CAN_InitModule
              CAN_RecISR
              CAN_SendBuffNotFull
              CAN_SendExtFrame
              USB_HP_CAN1_TX_IRQHandler
              USB_LP_CAN1_RX0_IRQHandler
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/* CAN 波特率  HCLK=72MHz APB1=CAN1_CLK=36MHz */
uc8 CanBaudRateTab[] =
{
    /* CAN_BS1,      CAN_BS2,       CAN_PRESCALER  */
    CAN_BS1_12tq,   CAN_BS2_5tq, 200,       // BaudRate_10k
    CAN_BS1_12tq,   CAN_BS2_5tq,   100,     // BaudRate_20k
    CAN_BS1_8tq,    CAN_BS2_1tq,   90,      // BaudRate_40k
    CAN_BS1_8tq,    CAN_BS2_1tq,   72,      // BaudRate_50k
    CAN_BS1_8tq,    CAN_BS2_1tq,   45,      // BaudRate_80k
    CAN_BS1_8tq,    CAN_BS2_1tq,   36,      // BaudRate_100k
    CAN_BS1_9tq,    CAN_BS2_2tq,   24,      // BaudRate_125k
    CAN_BS1_6tq,    CAN_BS2_2tq,   20,      // BaudRate_200k
    CAN_BS1_9tq,    CAN_BS2_2tq,   12,      // BaudRate_250k
    CAN_BS1_8tq,    CAN_BS2_1tq,   9,       // BaudRate_400k
    CAN_BS1_9tq,    CAN_BS2_2tq,   6,       // BaudRate_500k
    CAN_BS1_12tq,   CAN_BS2_5tq,   3,       // BaudRate_666k
    CAN_BS1_2tq,    CAN_BS2_5tq,   4,       // BaudRate_800k
    CAN_BS1_9tq,    CAN_BS2_2tq,   3        // BaudRate_1000k
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : CAN_DriverInit
 功能描述  : CAN 设备初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void CAN_DriverInit(void)
{
    NVIC_InitTypeDef    tmpNVIC_InitStructure;
    GPIO_InitTypeDef    tmpGPIO_InitStructure;


    // 使能GPIOA时钟
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    // 使能CAN1时钟
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);


    // 配置CAN管脚 RX -> PA11
    tmpGPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
    tmpGPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &tmpGPIO_InitStructure);
    // 配置CAN管脚 TX -> PA12
    tmpGPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
    tmpGPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    tmpGPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &tmpGPIO_InitStructure);


    // 使能CAN1的接收中断
    tmpNVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
    tmpNVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    tmpNVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    tmpNVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&tmpNVIC_InitStructure);

    // 使能CAN1的发送中断
    tmpNVIC_InitStructure.NVIC_IRQChannel = CAN1_TX_IRQn;
    tmpNVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    tmpNVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    tmpNVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&tmpNVIC_InitStructure);
}


/*****************************************************************************
 函 数 名  : CAN_DisRxIrn
 功能描述  : 关闭RX中断
 输入参数  : CAN_Type canId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_DisRxIrn(CAN_Type canId)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));

    // 关闭CAN的接收中断
    CAN_ITConfig(CAN1, CAN_IT_FMP0, DISABLE);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_EnbRxIrn
 功能描述  : 打开Rx中断
 输入参数  : CAN_Type canId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_EnbRxIrn(CAN_Type canId)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));

    // 打开CAN的接收中断
    CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_DisTxIrn
 功能描述  : 关闭Tx中断
 输入参数  : CAN_Type canId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_DisTxIrn(CAN_Type canId)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));

    // 关闭 邮箱空中断
    CAN_ITConfig(CAN1, CAN_IT_TME, DISABLE);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_EnbTxIrn
 功能描述  : 打开Tx中断
 输入参数  : CAN_Type canId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_EnbTxIrn(CAN_Type canId)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));

    // 打开 邮箱空中断
    CAN_ITConfig(CAN1, CAN_IT_TME, ENABLE);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_InitModule
 功能描述  : 初始化配置CAN
 输入参数  : CAN_Type canId
             CAN_BtrType sBtr
             u16 uFilterMaskId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

  2.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : STM32芯片中的CAN屏蔽标识符 与SJA100的定义相反
                 STM32的 屏蔽标识符:对应位为 0:不比较; 1:必须匹配;
*****************************************************************************/
ErrorStatus CAN_InitModule(CAN_Type canId, CAN_BtrType sBtr, u16 uFilterMaskId)
{
    CAN_FilterInitTypeDef  CAN_FilterInitStructure;
    CAN_InitTypeDef        CAN_InitStructure;
    u8  sBS1, sBS2, sPrescaler;

    assert_app_param(IS_CAN_TYPE_LEGAL(canId));
    assert_app_param(IS_CAN_BTR_TYPE(sBtr));


    /* 关闭接收中断 */
    CAN_DisRxIrn(canId);

    /* 关闭发送中断 */
    CAN_DisTxIrn(canId);


    /* CAN 寄存器初始化 */
    CAN_DeInit(CAN1);
    /* CAN 结构体初始化 */
    CAN_StructInit(&CAN_InitStructure);

    /* 获取配置波特率的参数 */
    sBS1         = CanBaudRateTab[3*sBtr];
    sBS2         = CanBaudRateTab[3*sBtr+1];
    sPrescaler   = CanBaudRateTab[3*sBtr+2];

    /* 时间触发通讯模式.0:时间触发通信模式关闭; */
    CAN_InitStructure.CAN_TTCM = DISABLE;
    /* 自动离线管理.    1:一旦监控到128次11个连续隐形位,自动退出离线状态; */
    CAN_InitStructure.CAN_ABOM = ENABLE;
    /* 自动唤醒模式.    1:硬件检测到CAN报文时自动离开休眠模式; */
    CAN_InitStructure.CAN_AWUM = ENABLE;
    /* 非自动重传输模式.0:CAN硬件发送失败后会一直重发直到发送成功; */
    CAN_InitStructure.CAN_NART = DISABLE;
    /* 接收FIFO锁定模式.0:接收FIFO满了,下一条传入的报文将覆盖前一条; */
    CAN_InitStructure.CAN_RFLM = DISABLE;
    /* 发送FIFO优先级.  1:由发送请求的顺序(时间先后顺序)来决定优先级. */
    CAN_InitStructure.CAN_TXFP = ENABLE;

    /* 工作模式为正常模式 */
    CAN_InitStructure.CAN_Mode  = CAN_Mode_Normal;
    /* 重新同步跳跃宽度 1个时间单位 */
    CAN_InitStructure.CAN_SJW   = CAN_SJW_1tq;
    /* 配置时间段1 */
    CAN_InitStructure.CAN_BS1   = sBS1;
    /* 配置时间段2 */
    CAN_InitStructure.CAN_BS2   = sBS2;
    /* 分频系数 */
    CAN_InitStructure.CAN_Prescaler = sPrescaler;

    /* 初始化CAN1 */
    CAN_Init(CAN1, &CAN_InitStructure);




    /* 这里低字节为FF，主要是表示CAN硬件不进行滤波 */
    uFilterMaskId = 0xFFFF;

    /* 指定了待初始化的过滤器，它的范围是1到13 */
    CAN_FilterInitStructure.CAN_FilterNumber = 1;
    /* 过滤器被初始化为标识符屏蔽位模式 */
    CAN_FilterInitStructure.CAN_FilterMode   = CAN_FilterMode_IdMask;
    /* 给出了过滤器位宽 */
    CAN_FilterInitStructure.CAN_FilterScale  = CAN_FilterScale_16bit;

    /* 过滤器 标识符(16位位宽) */
    CAN_FilterInitStructure.CAN_FilterIdHigh = uFilterMaskId & 0xFF00;
    /* 过滤器 屏蔽标识符 */
    CAN_FilterInitStructure.CAN_FilterMaskIdHigh = (~uFilterMaskId << 8) & 0xFF00;


    CAN_FilterInitStructure.CAN_FilterIdLow  = uFilterMaskId & 0xFF00;

    /* STM32芯片中CAN的 屏蔽标识符 与 SJA1000 的定义相反;
       对应位为 0:不比较; 1:必须匹配 */
    CAN_FilterInitStructure.CAN_FilterMaskIdLow   = 0xFFFF;

    /* 通过此滤波器信息包将被放在 FIFO0 里 */
    CAN_FilterInitStructure.CAN_FilterFIFOAssignment = CAN_FIFO0;
    /* 使能/失能 过滤器 */
    CAN_FilterInitStructure.CAN_FilterActivation     = ENABLE;
    CAN_FilterInit(&CAN_FilterInitStructure);


    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : CAN_SendBuffNotFull
 功能描述  : 发送Buff是否不为空
 输入参数  : CAN_Type canId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus CAN_SendBuffNotFull(CAN_Type canId)
{
    assert_app_param(IS_CAN_TYPE_LEGAL(canId));

    // 判断邮箱1是否为空
    if ((CAN1->TSR & CAN_TSR_TME0) == CAN_TSR_TME0)
    {
        return SET;
    }
    // 判断邮箱2是否为空
    else if ((CAN1->TSR & CAN_TSR_TME1) == CAN_TSR_TME1)
    {
        return SET;
    }
    // 判断邮箱3是否为空
    else if ((CAN1->TSR & CAN_TSR_TME2) == CAN_TSR_TME2)
    {
        return SET;
    }
    // 所有邮箱都满 返回邮箱满状态
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : CAN_SendExtFrame
 功能描述  : 发送扩展帧
 输入参数  : CAN_Type canId
             CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_SendExtFrame(CAN_Type canId, CAN_DataBase *sDB)
{
    // 发送消息结构
    CanTxMsg    txMsg;

    assert_app_param(IS_CAN_TYPE_LEGAL(canId));
    assert_app_param(IS_CAN_DB_POINTER(sDB));

    // 判断发送是否 准备完毕
    if(CAN_SendBuffNotFull(canId) != SET)
    {
        return ERROR;
    }

    // 判断数据长度是否合法
    if((sDB->FrameLen > 0)
       && (sDB->FrameLen <= CAN_FRAME_MAX_LEN))
    {
        // 唤醒CAN
        CAN_WakeUp(CAN1);

        // 填充帧ID
        txMsg.ExtId = sDB->FrameID;

        // 帧类型为扩展帧
        txMsg.IDE   =  CAN_ID_EXT;

        // 数据帧
        txMsg.RTR   =  CAN_RTR_DATA;

        // 载入发送数据长度
        txMsg.DLC   =  sDB->FrameLen;

        // 载入前4个Byte
        *((u32 *)(txMsg.Data))       = *((u32 *)(&sDB->DataBuff[0]));
        // 载入后4个Byte
        *((u32 *)(&(txMsg.Data[4]))) = *((u32 *)(&sDB->DataBuff[4]));

        // 允许发送中断
        CAN_EnbTxIrn(canId);

        // 发送数据
        if(CAN_Transmit(CAN1, &txMsg) != CAN_TxStatus_NoMailBox)
        {
            return SUCCESS;
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_RecISR
 功能描述  : CAN 接收中断
 输入参数  : CanRxMsg *sCanRxMsg
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_RecISR(CAN_Type canId, CanRxMsg *sCanRxMsg)
{
    u32 tmpMsgId = 0;
    u8 i = 0;
    CAN_DataBase        rDB;

    assert_app_param(IS_CAN_TYPE_LEGAL(canId));
    assert_app_param(IS_CAN_RX_MSG_POINTER(sCanRxMsg));

    // 判断消息是否合法
    if(sCanRxMsg->DLC > 0 && (sCanRxMsg->DLC <= CAN_FRAME_MAX_LEN))
    {
        // 获取ID号
        tmpMsgId = sCanRxMsg->ExtId;
        tmpMsgId &= 0x1FFFFFFF;

        // 载入帧ID
        rDB.FrameID = tmpMsgId;

        // 循环复制数据
        for(i=0; i<sCanRxMsg->DLC; i++)
        {
            rDB.DataBuff[i] = sCanRxMsg->Data[i];
        }

        // 载入数据长度
        rDB.FrameLen = sCanRxMsg->DLC;

        // 判断是否为数据包模式
        if((tmpMsgId & CAN_FRAME_TYPE_PACKET) == CAN_FRAME_TYPE_PACKET)
        {
            // 将数据帧 载入到数据包内
            CAN_PutRecPacketFrame(&CanStatPara, &rDB);
        }
        else
        {
            // 将数据帧 载入到数据帧队列内
            CAN_QueuePush(&(CanStatPara.DbAtttr.RecDB), &rDB);
        }

        return SUCCESS;
    }

    return ERROR;
}


/*****************************************************************************
 函 数 名  : CAN1_TX_IRQHandler
 功能描述  : CAN1 发送邮箱空闲 中断
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void CAN1_TX_IRQHandler(void)
{
    // 清除中断标志位
    CAN1->TSR |= 0x000F0F0F;

    // 发送完成设置状态
    CAN_SendFinishSetStat();
}

/*****************************************************************************
 函 数 名  : CAN1_RX0_IRQHandler
 功能描述  : CAN1 接收中断标志位
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void CAN1_RX0_IRQHandler(void)
{
    CanRxMsg  RxMsg;

    // 判断FIFO0中是否有数据
    if(CAN_MessagePending(CAN1, CAN_FIFO0) != 0)
    {
        // 释放了此消息所占用的邮箱
        CAN_Receive(CAN1, CAN_FIFO0, &RxMsg);

        // 将接收到的数据帧载入到对应的缓存中
        CAN_RecISR(CAN_1 , &RxMsg);

        // 接收消息设置状态
        CAN_ReceiveMsgSetStat();
    }

    // 清除中断标志位
    CAN1->RF0R |= 0x00000018;
}

