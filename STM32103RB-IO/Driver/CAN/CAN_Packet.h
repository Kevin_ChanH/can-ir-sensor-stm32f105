/******************************************************************************

                  版权所有 (C), 2013-2017, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_Packet.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年2月17日
  最近修改   :
  功能描述   : CAN_Packet.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年2月17日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __CAN_PACKET_H__
#define __CAN_PACKET_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


typedef struct
{
    u32     PacketID;                       // 数据包ID
    u8      PacketBuff[CAN_BUF_MAX_LEN];    // 数据包Buff
    u16     PacketLen;                      // 数据包长度
} CAN_PacketBase;
#define IS_CAN_PACKET_BASE_POINTER(PARAM)   (((PARAM) != NULL))

typedef struct
{
    u8 FrameIndex;                          // 数据包下标
    u8 FrameLen;                            // 数据包内容总长度
    FlagStatus  Lock;                       // 是否被锁定
    CAN_PacketBase PacketBase;              // 数据包
} CAN_DataPacket;
/* 判断是否为CAN的数据包指针 */
#define IS_CAN_PACKET_POINTER(PARAM)        (((PARAM) != NULL))


typedef enum
{
    CAN_PACKET_STAT_UNKONW = 0,             // 未知状态

    CAN_PACKET_STAT_UNKONW_WAIT,            // 未知等待时间

    CAN_PACKET_STAT_IDEL,                   // 空闲状态

    //CAN_PACKET_STAT_RANDOM_RTS,             // 随机延迟准备发送RTS
    CAN_PACKET_STAT_RTS,                    // 发送请求数据     CSMA 1msMin, 10msMAX

    CAN_PACKET_STAT_SEND_WAIT,              // 发送等待         20msMax
    CAN_PACKET_STAT_SEND,                   // 发送数据包

    CAN_PACKET_STAT_RECEIVE_WAIT,           // 接收等待         50msMax
    CAN_PACKET_STAT_RECEIVE,                // 接收数据包       100msMax
} CAN_PacketRunStatTypedef;
#define IS_PACKET_RUN_STAT(PARAM)           (((PARAM) == CAN_PACKET_STAT_UNKONW)        ||  \
                                             ((PARAM) == CAN_PACKET_STAT_IDEL)          ||  \
                                             ((PARAM) == CAN_PACKET_STAT_RTS)           ||  \
                                             ((PARAM) == CAN_PACKET_STAT_SEND_WAIT)     ||  \
                                             ((PARAM) == CAN_PACKET_STAT_SEND)          ||  \
                                             ((PARAM) == CAN_PACKET_STAT_RECEIVE_WAIT)  ||  \
                                             ((PARAM) == CAN_PACKET_STAT_RECEIVE))


typedef struct
{
    CAN_PacketRunStatTypedef    Stat;       // CAN数据包状态
    u32                         RandomNum;  // 随机生成的
} CAN_PacketStatTypedef;

typedef struct
{
    CAN_DataPacket          RecPacket;      // 接收数据包
    CAN_DataPacket          SendPacket;     // 发送数据包
    CAN_PacketStatTypedef   StatAttr;       // 数据包的状态内容
}CAN_PacketTypedef;

/* CAN Bus 状态 */
typedef enum
{
    CAN_BUS_STAT_UNKONW = 0,                // 总线状态 - 未知
    CAN_BUS_STAT_OFFLINE,                   // 总线状态 - 离线
    CAN_BUS_STAT_ONLINE,                    // 总线状态 - 在线
}CAN_BusRunStatTypedef;
#define     IS_CAN_BUS_STAT_TYPE(PARAM)     (((PARAM) == CAN_BUS_STAT_UNKONW)   ||  \
                                             ((PARAM) == CAN_BUS_STAT_OFFLINE)  ||  \
                                             ((PARAM) == CAN_BUS_STAT_ONLINE))


/* CAN Bus 发送状态 */
typedef enum
{
    CAN_SEND_STAT_IDEL = 0,                 // 总线发送状态 - 空闲
    CAN_SEND_STAT_WAIT,                     // 总线发送状态 - 等待
    CAN_SEND_STAT_TIMEOUT,                  // 总线发送状态 - 超时
} CANBusSendStatTypedef;
#define     IS_CAN_SEND_STAT_TYPE(PARAM)    (((PARAM) == CAN_SEND_STAT_IDEL)    || \
                                             ((PARAM) == CAN_SEND_STAT_WAIT)    || \
                                             ((PARAM) == CAN_SEND_STAT_TIMEOUT))


/* CAN 状态 属性 */
typedef struct
{
    CAN_BusRunStatTypedef   BusStat;                // 总线状态
    CANBusSendStatTypedef   SendStat;               // 总线发送状态
    FlagStatus              BrcActive;              // 广播发送是否激活
    u32                     RandomNum;              // 随机生成码
}CAN_BusStatTypedef;

/* CAN状态参数 */
typedef struct
{
    CAN_BtrType         CANBtr;                     // CAN 波特率
    CAN_BusStatTypedef  StatAttr;                   // CAN 总线状态
    CAN_PacketTypedef   PacketAttr;                 // CAN 数据包的参数
    CAN_DbQueueTypedef  DbAtttr;                    // CAN 基础数据包
} CAN_StatParaTypedef;
#define     IS_CAN_PARA_POINTER(PARAM)              (((PARAM) != NULL))




/*
    数据包获取状态等待时间 目前固定延时为 50ms 后期做随机延时
*/
#define     CAN_PACKET_GET_STAT_WAIT_TIME           5000

/* 
    单帧最大位数 - CAN 扩展帧 单帧最大发送位数
*/
#define     CAN_PACKET_FRAME_MAX_BITS               128

/* 
    1Bit发送时间 - 节拍数 - 根据不同的速率调整
*/
#if (SYSTEM_CAN1_USER_BTR == CAN_BTR_10K)
// 100us - 10Kbps
#define     CAN_SEND_BIT_TICKS                      (float)10.0

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_20K)
// 50us - 20Kbps
#define     CAN_SEND_BIT_TICKS                      (float)5.0

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_40K)
// 25us - 40Kbps
#define     CAN_SEND_BIT_TICKS                      (float)2.5

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_50K)
// 20us - 50Kbps
#define     CAN_SEND_BIT_TICKS                      (float)2.0

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_80K)
// 12.5us - 80Kbps
#define     CAN_SEND_BIT_TICKS                      (float)1.25

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_100K)
// 10us - 100Kbps
#define     CAN_SEND_BIT_TICKS                      (float)1.0

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_125K)
// 8us - 125Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.8

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_200K)
// 5us - 200Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.5

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_250K)
// 4us - 250Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.4

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_400K)
// 2.5us - 400Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.25

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_500K)
// 2us - 500Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.2

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_666K)
// 1.51us - 666Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.151

#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_800K)
// 1.25us - 800Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.125
#elif (SYSTEM_CAN1_USER_BTR == CAN_BTR_1000K)
// 1us - 1000Kbps
#define     CAN_SEND_BIT_TICKS                      (float)0.1
#else
// 100us - 10Kbps
#define     CAN_SEND_BIT_TICKS                      (float)10.2
#endif

/*
    运行等待时间 用于预防由于发送太快而导致程序跟不上
*/
#define     CAN_RUN_WAIT_TICKS_MAX                  100

/* 
    数据包RTS请求 最小倍数 * 单帧发送需要的时间
*/
#define     CAN_PACKET_RTS_REQUEST_MIN_MULTIPLE     2

/* 
    数据包RTS请求 最大倍数 * 单帧发送需要的时间
*/
#define     CAN_PACKET_RTS_REQUEST_MAX_MULTIPLE     20

/* 
    发送等待 倍数 * (最小RTS时间 + 最大RTS时间)    
*/
#define     CAN_PACKET_SEND_WAIT_MULTIPLE           1

/* 
    接收等待 倍数 * 发送等待时间
*/
#define     CAN_PACKET_RECEIVE_WAIT_MULTIPLE        2

/* 
    接收超时 倍数 * 接收等待时间
*/
#define     CAN_PACKET_RECEIVE_TIMEOUT_MULTIPLE     2

/*****************************************************************************
主题：数据包RTS请求最小时间
配方：根据RTS最小请求倍数 * \
      (发送的单帧最大位数 * 1Bit发送节拍数) + \
      程序最大的运行等待时间
*****************************************************************************/
#define     CAN_PACKET_RTS_REQUEST_TIME_MIN         ((CAN_PACKET_RTS_REQUEST_MIN_MULTIPLE * \
                                                (CAN_PACKET_FRAME_MAX_BITS * CAN_SEND_BIT_TICKS)) + \
                                                CAN_RUN_WAIT_TICKS_MAX)

/*****************************************************************************
主题：数据包RTS请求最大时间
配方：根据RTS最大请求倍数 * \
      (发送的单帧最大位数 * 1Bit发送节拍数) + \
      程序最大的运行等待时间
*****************************************************************************/
#define     CAN_PACKET_RTS_REQUEST_TIME_MAX         ((CAN_PACKET_RTS_REQUEST_MAX_MULTIPLE * \
                                                (CAN_PACKET_FRAME_MAX_BITS * CAN_SEND_BIT_TICKS)) + \
                                                CAN_RUN_WAIT_TICKS_MAX)


/*****************************************************************************
主题：数据包发送等待时间
配方：根据发送等待倍数 * \
      (数据包RTS请求最小时间 * 数据包RTS请求最大时间)
*****************************************************************************/
#define     CAN_PACKET_SEND_WAIT_TIME               (CAN_PACKET_SEND_WAIT_MULTIPLE * \
                                                  (CAN_PACKET_RTS_REQUEST_TIME_MIN + CAN_PACKET_RTS_REQUEST_TIME_MAX))


/*****************************************************************************
主题：数据包开始接收的等待时间
配方：接收等待倍数 * 数据包发送等待时间
*****************************************************************************/
#define     CAN_PACKET_RECEIVE_WAIT_TIME            (CAN_PACKET_RECEIVE_WAIT_MULTIPLE * \
                                                 CAN_PACKET_SEND_WAIT_TIME)


/*****************************************************************************
主题：数据包的接收超时时间
配方：接收超时倍数 * 数据包开始接收的等待时间
*****************************************************************************/
#define     CAN_PACKET_RECEIVE_TIMEOUT_TIME         (CAN_PACKET_RECEIVE_TIMEOUT_MULTIPLE * \
                                                 CAN_PACKET_RECEIVE_WAIT_TIME)


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus         CAN_ClrPacket               (CAN_DataPacket *cPacket);

// 设置数据包状态
extern  ErrorStatus         CAN_SetPacketStat           (CAN_PacketRunStatTypedef sStat);

extern  ErrorStatus         CAN_ClrPacketAttr           (void);

// 载入发送包的随机码
extern  ErrorStatus         CAN_LoadPacketRandomNum     (u32 sNum);

// 获取发送包的状态命令
extern  ErrorStatus         CAN_SendTrslReadPacketStat  (void);

// 发送数据包的RTS
extern  ErrorStatus         CAN_SendTrslPacketRts       (void);

// 接收数据包状态执行函数
extern  ErrorStatus         CAN_ReceivePacketStatPoll   (CmdAttrTypedef cmdAttr,uc8 datBuff[], u8 datLen);

// 接收数据包RTS执行函数
extern  ErrorStatus         CAN_ReceivePacketRtsPoll    (CmdAttrTypedef cmdAttr,uc8 datBuff[], u8 datLen);

// 判断数据包状态
extern  FlagStatus          CAN_IsPacketUnlock          (CAN_DataPacket *sDBPack);

// 数据包操作
extern  ErrorStatus         CAN_PacketSend              (CAN_StatParaTypedef *sCanPara, const CAN_PacketBase *sPck);
extern  ErrorStatus         CAN_PacketRec               (CAN_StatParaTypedef *sCanPara, CAN_PacketBase *sPck);

// 数据包收发
extern  ErrorStatus         CAN_GetSendPacketFrame      (CAN_StatParaTypedef *sCanPara, CAN_DataBase *sDB);
extern  ErrorStatus         CAN_PutRecPacketFrame       (CAN_StatParaTypedef *sCanPara, CAN_DataBase *sDB);

// 执行数据包的消息轮询
extern  ErrorStatus         CAN_PacketMsgPoll           (void);

extern  ErrorStatus         CAN_PacketReceivePoll       (void);
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __CAN_PACKET_H__ */
