/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_DataQueue.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月29日
  最近修改   :
  功能描述   : CAN_DataQueue.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __CAN_DATAQUEUE_H__
#define __CAN_DATAQUEUE_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/* 判断是否为 CAN的基础数据队列指针 */
#define IS_CAN_DB_QUEUE_POINTER(PARAM)      (((PARAM) != NULL))
/* 判断是否为 CAN的基础数据指针 */
#define IS_CAN_DB_POINTER(PARAM)            (((PARAM) != NULL))


/* CAN基础数据 */
typedef struct
{
    u32     FrameID;                        // 帧ID号
    u8      DataBuff[CAN_FRAME_MAX_LEN];    // 数据Buff
    u8      FrameLen;                       // 帧数据长度
} CAN_DataBase;

/* CAN基础数据队列 */
typedef struct
{
    u8 QHead;                               // 队列头
    u8 QTail;                               // 队列尾
    u8 QSize;                               // 队列长度
    u8 Surplus;                             // 队列剩余量
    CAN_DataBase *sFrame;                   // 队列内容指针
} CAN_DataBaseQueue;

/* CAN基础数据 收发队列 */
typedef struct
{
    CAN_DataBaseQueue   RecDB;              // 接收队列
    CAN_DataBaseQueue   SendDB;             // 发送队列
} CAN_DbQueueTypedef;

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

// 判断队列和数据包
extern  FlagStatus          CAN_IsQueueFull             (CAN_DataBaseQueue *sDBQueue);
extern  FlagStatus          CAN_IsQueueNotEmpty         (CAN_DataBaseQueue *sDBQueue);
extern  u8                  CAN_GetQueueSurplus         (CAN_DataBaseQueue *sDBQueue);

// 队列操作
extern  ErrorStatus         CAN_QueuePush               (CAN_DataBaseQueue *sDBQueue , const CAN_DataBase *sDB);
extern  ErrorStatus         CAN_QueuePull               (CAN_DataBaseQueue *sDBQueue, CAN_DataBase *sDB);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __CAN_DATAQUEUE_H__ */
