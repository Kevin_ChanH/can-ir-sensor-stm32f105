/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_DataQueue.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月29日
  最近修改   :
  功能描述   : CAN 数据队列操作操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/
static  ErrorStatus         CAN_QueueHeadInc            (CAN_DataBaseQueue *sDBQueue);
static  ErrorStatus         CAN_QueueTaillInc           (CAN_DataBaseQueue *sDBQueue);

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : CAN_IsQueueFull
 功能描述  : 判断队列是否满了
 输入参数  : CAN_DataBaseQueue *sDBQueue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus CAN_IsQueueFull(CAN_DataBaseQueue *sDBQueue)
{
    assert_app_param(IS_CAN_DB_QUEUE_POINTER(sDBQueue));

    // 判断队列是否还有剩余的空间
    if(sDBQueue->Surplus > 0)
    {
        // 返回队列还有空间
        return RESET;
    }

    // 返回队列已满
    return SET;
}

/*****************************************************************************
 函 数 名  : CAN_IsQueueNotEmpty
 功能描述  : 判断队列是否为非空
 输入参数  : CAN_DataBaseQueue *sDBQueue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus CAN_IsQueueNotEmpty(CAN_DataBaseQueue *sDBQueue)
{
    assert_app_param(IS_CAN_DB_QUEUE_POINTER(sDBQueue));

    // 判断队列内部是否还有数据
    if((sDBQueue->Surplus) < (sDBQueue->QSize))
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : CAN_GetQueueSurplus
 功能描述  : 获取队列剩余元素量
 输入参数  : CAN_DataBaseQueue *sDBQueue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u8 CAN_GetQueueSurplus(CAN_DataBaseQueue *sDBQueue)
{
    assert_app_param(IS_CAN_DB_QUEUE_POINTER(sDBQueue));

    // 返回队列剩余元素量
    return (sDBQueue->Surplus);
}

/*****************************************************************************
 函 数 名  : CAN_QueueHeadInc
 功能描述  : 队列头向下移动
 输入参数  : CAN_DataBaseQueue *sDBQueue
 输出参数  : 无
 返 回 值  : static
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus CAN_QueueHeadInc(CAN_DataBaseQueue *sDBQueue)
{
    u8 sIndex = 0;

    assert_app_param(IS_CAN_DB_QUEUE_POINTER(sDBQueue));

    SYSTEM_DISABLE_ALL_IRQ;

    // 队列头向下移动一个单位
    sIndex = sDBQueue->QHead;
    sIndex ++;
    sIndex %= sDBQueue->QSize;
    sDBQueue->QHead = sIndex;

    SYSTEM_ENABLE_ALL_IRQ;

    // 返回执行成功
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_QueueTaillInc
 功能描述  : 队列尾向下移动
 输入参数  : CAN_DataBaseQueue *sDBQueue
 输出参数  : 无
 返 回 值  : static
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus CAN_QueueTaillInc(CAN_DataBaseQueue *sDBQueue)
{
    u8 sIndex = 0;

    assert_app_param(IS_CAN_DB_QUEUE_POINTER(sDBQueue));

    SYSTEM_DISABLE_ALL_IRQ;

    // 队列尾向下移动一个单位
    sIndex = sDBQueue->QTail;
    sIndex ++;
    sIndex %= sDBQueue->QSize;
    sDBQueue->QTail = sIndex;

    SYSTEM_ENABLE_ALL_IRQ;

    // 返回执行成功
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CAN_QueuePush
 功能描述  : 将数据帧压入队列
 输入参数  : CAN_DataBaseQueue *sDBQueue
             const CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_QueuePush(CAN_DataBaseQueue *sDBQueue , const CAN_DataBase *sDB)
{
    u8 sQueueIndex = 0;
    u8 i = 0;

    assert_app_param(IS_CAN_DB_QUEUE_POINTER(sDBQueue));
    assert_app_param(IS_CAN_DB_POINTER(sDB));

    // 获取队列下标
    sQueueIndex = sDBQueue->QHead;

    // 判断数据长度是否在合法范围内
    if(((sDB->FrameLen) <= CAN_FRAME_MAX_LEN)
       && ((sDB->FrameLen) > 0) )
    {
        // 循环复制数据
        for(i=0; i<sDB->FrameLen; i++)
        {
            sDBQueue->sFrame[sQueueIndex].DataBuff[i] = sDB->DataBuff[i];
        }

        // 载入数据ID
        sDBQueue->sFrame[sQueueIndex].FrameID = sDB->FrameID;

        // 载入数据长度
        sDBQueue->sFrame[sQueueIndex].FrameLen = sDB->FrameLen;

        // 队列头向下移动一个单元
        CAN_QueueHeadInc(sDBQueue);

        // 判断队列是否还有剩余元素
        if(sDBQueue->Surplus > 0)
        {
            SYSTEM_DISABLE_ALL_IRQ;

            // 队列元素-1
            sDBQueue->Surplus -= 1;

            SYSTEM_ENABLE_ALL_IRQ;
        }
        else
        {
            // 覆盖了最旧的数据元素 所以队列尾向下移动一个单元
            CAN_QueueTaillInc(sDBQueue);
        }

        // 队列操作成功
        return SUCCESS;
    }

    // 返回队列操作失败
    return ERROR;
}

/*****************************************************************************
 函 数 名  : CAN_QueuePull
 功能描述  : 从队列获取数据帧
 输入参数  : CAN_DataBaseQueue *sDBQueue
             CAN_DataBase *sDB
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CAN_QueuePull(CAN_DataBaseQueue *sDBQueue, CAN_DataBase *sDB)
{
    u8 sQueueIndex = 0;
    u8 i = 0;
    u8 gLen = 0;

    assert_app_param(IS_CAN_DB_QUEUE_POINTER(sDBQueue));
    assert_app_param(IS_CAN_DB_POINTER(sDB));

    // 判断队列是否有数据
    if(CAN_IsQueueNotEmpty(sDBQueue) == SET)
    {
        // 获取队列下标
        sQueueIndex = sDBQueue->QTail;
        // 获取数据长度
        gLen = sDBQueue->sFrame[sQueueIndex].FrameLen;

        // 判断数据长度是否合法
        if((gLen > 0) && (gLen <= CAN_FRAME_MAX_LEN))
        {
            // 循环复制数据
            for(i=0; i<gLen; i++)
            {
                sDB->DataBuff[i] = sDBQueue->sFrame[sQueueIndex].DataBuff[i];
            }

            // 载入数据ID
            sDB->FrameID = sDBQueue->sFrame[sQueueIndex].FrameID;
            // 载入数据长度
            sDB->FrameLen = gLen;
        }

        // 队列尾向下移动一个单元
        CAN_QueueTaillInc(sDBQueue);

        // 判断剩余的数据单元是否小于最大的数据单元
        if(sDBQueue->Surplus < sDBQueue->QSize)
        {
            SYSTEM_DISABLE_ALL_IRQ;

            // 数据单元剩余量+1
            sDBQueue->Surplus += 1;

            SYSTEM_ENABLE_ALL_IRQ;
        }
        else
        {
            // 返回异常 剩余量大于等于最大数据单元 还可以取出数据
            return ERROR;
        }

        // 返回数据获取成功
        return SUCCESS;
    }

    // 返回数据获取失败
    return ERROR;
}



