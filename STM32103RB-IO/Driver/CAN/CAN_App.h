/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CAN_App.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : CAN_App.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __CAN_APP_H__
#define __CAN_APP_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
// CAN 数据包识别位
#define         CAN_FRAME_TYPE_PACKET               0x10000000
// CAN 传输层识别位
#define         CAN_FRAME_TYPE_TRSL                 0x08000000

// 初始化等待随机时间 - 最小值
#define         CAN_OPT_WAIT_RNG_MS_MIN             2000
// 初始化等待随机时间 - 最大值
#define         CAN_OPT_WAIT_RNG_MS_MAX             3500


// CAN 初始化波特率为 500K
#define         CAN_INIT_BTR                        CanBtr_500K

// CAN 数据包随机码为0xFFFFFFFF
#define         CAN_INIT_PACKET_RANDOM_NUM          0xFFFFFFFF
// CAN 总线状态随机码为0XFFFFFFFF
#define         CAN_INIT_BUS_STAT_RANDOM_NUM        0xFFFFFFFF

// CAN 总线发送等待时间 500ms
#define         CAN_BUS_SEND_WAIT_TIME_MS           500

// CAN 总线的离线等待时间 15秒
#define         CAN_BUS_OFFLINE_WAIT_TIME_SEC       15

// CAN 总线的广播等待时间 10秒
#define         CAN_BUS_BROADCAST_WAIT_TIME_SEC     10

typedef enum
{
    CAN_TRSL_REG_RESERVED = 0,                      /* 预留 */
    CAN_TRSL_REG_STAT,                              /* 状态   寄存器 */
    CAN_TRSL_REG_PACKET,                            /* 数据包 寄存器 */

    CAN_TRSL_REG_SEC = 0xFF,                        /* 安全组 寄存器 */
} CanTrslRegTypedef;


/* 状态寄存器 子寄存器 */
#define     CAN_TRSL_REG_STAT_RESERVED      0       /* 状态 - 预留 */
#define     CAN_TRSL_REG_STAT_AREA          1       /* 状态 - 总线区域状态 */

/* 数据包寄存器 - 子寄存器 */
#define     CAN_TRSL_REG_PACKET_RESERVED    0       /* 数据包 - 预留 */
#define     CAN_TRSL_REG_PACKET_STAT        1       /* 数据包 - 状态寄存器 */
#define     CAN_TRSL_REG_PACKET_RNG         2       /* 数据包 - RNG寄存器 */


/* 状态寄存器 - 区域状态码 */
#define     CAN_TRSL_STAT_AREA_TEST         1       /* 区域状态码 - 测试码 */
#define     CAN_TRSL_STAT_AREA_BROADCAST    2       /* 区域状态码 - 广播保持状态 */

/* 数据包寄存器 - RNG的随机码长度 */
#define     CAN_TRSL_PACKET_RNG_DATA_LEN    4       /* 数据包寄存器 - 随机数长度 */



// CAN的状态参数
extern  CAN_StatParaTypedef     CanStatPara;

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  void            CAN_InitRunPara                 (void);

extern  ErrorStatus     CAN_GetCanAttr                  (CAN_Type canId, CAN_StatParaTypedef *gCanAttr);

extern  ErrorStatus     CAN_SetBtr                      (CAN_Type  canId,   CAN_BtrType  sBtr);
extern  ErrorStatus     CAN_GetBtr                      (CAN_Type  canId,   CAN_BtrType  *gBtr);

extern  FlagStatus      CAN_SendIsReady                 (CAN_Type  canId);

extern  FlagStatus      CAN_IsOnline                    (void);

extern  ErrorStatus     CAN_LoadSendMsg                 (CAN_DataBase *sMsg);
extern  ErrorStatus     CAN_SendDbMsg                   (CAN_DataBase *sMsg);

extern  ErrorStatus     CAN_SendCanDb                   (CAN_DataBase *sDB);


extern  ErrorStatus     CAN_TxMsgPoll                   (void);
extern  ErrorStatus     CAN_RxMsgPoll                   (void);

extern  ErrorStatus     CAN_SetSendStat                 (CANBusSendStatTypedef gStat);
extern  ErrorStatus     CAN_SetBusStat                  (CAN_BusRunStatTypedef gStat);

extern  ErrorStatus     CAN_SetBusStatOnline            (void);
extern  ErrorStatus     CAN_SendFinishSetStat           (void);
extern  ErrorStatus     CAN_ReceiveMsgSetStat           (void);

extern  ErrorStatus     CAN_SendStatCheckPoll           (void);

//extern  ErrorStatus     CAN_RefreshOfflineWaitTime      (void);
//extern  ErrorStatus     CAN_RefreshBroadCastWaitTime    (void);

extern  ErrorStatus     CAN_MsgPoll                     (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __CAN_APP_H__ */

