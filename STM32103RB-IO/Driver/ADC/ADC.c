/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : ADC.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月26日
  最近修改   :
  功能描述   : AD转换驱动程序
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月26日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
u16 ADC1Buff[ADC1_CONVERT_SIZE];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

ErrorStatus ADC_GpioInit(PinNames sPin)
{
    if(Gpio_ChkPin(sPin) == RESET)
    {
        return ERROR;
    }

    // 模拟输入
    Gpio_Init(sPin, GPIO_Mode_AIN, Bit_RESET);

    return SUCCESS;
}

ErrorStatus ADC_RunModeInit(u8 sAdChannel)
{
    ADC_InitTypeDef ADC_InitStructure;
        
    /* 使能ADC1 时钟 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
    
    
    /* ADC时钟  72M/6 = 12M - MAX=14M */
    RCC_ADCCLKConfig(RCC_PCLK2_Div6); 
    
    // 判断通道是否失败
    if(IS_ADC_CHANNEL(sAdChannel) != SET)
    {
        return ERROR;
    }

    // 工作在独立模式
    ADC_InitStructure.ADC_Mode = ADC_Mode_Independent;
    // 不使用扫描模式 - 单通道转换
    ADC_InitStructure.ADC_ScanConvMode = DISABLE;
    // 连续转换模式
    ADC_InitStructure.ADC_ContinuousConvMode = ENABLE;
    // 触发模式 - 软件触发
    ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None;
    // 数据右对齐
    ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
    // 规则转换的ADC通道数目 1
    ADC_InitStructure.ADC_NbrOfChannel = 1;
    // 初始化ADC1
    ADC_Init(ADC1, &ADC_InitStructure);

    /* ADC1 regular channel14 configuration */ 
    ADC_RegularChannelConfig(ADC1, sAdChannel, 1, ADC_SampleTime_28Cycles5);

    /* Enable ADC1 DMA */
    ADC_DMACmd(ADC1, ENABLE);

    /* Enable ADC1 */
    ADC_Cmd(ADC1, ENABLE);

    return SUCCESS;
}

ErrorStatus ADC_DmaInit(void)
{    
    DMA_InitTypeDef DMA_InitStructure;

    /* 使能DMA时钟 */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    
    /* DMA1 channel1 configuration ----------------------------------------------*/
    DMA_DeInit(DMA1_Channel1);
    DMA_InitStructure.DMA_PeripheralBaseAddr = ADC1_DR_Address;
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&ADC1Buff;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;

    /* 一次转换长度 */
    DMA_InitStructure.DMA_BufferSize = ADC1_CONVERT_SIZE;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;

    /* 正常模式 */
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel1, &DMA_InitStructure);

    /* 清除DMA1_Channel1传输完成中断标志位 */
    DMA_ClearITPendingBit(DMA1_IT_GL1);

    /* Enable DMA1 channel1 */
    DMA_Cmd(DMA1_Channel1, ENABLE);

    return SUCCESS;
}


void ADC_StartConvert(void)
{
    /* Enable ADC1 reset calibration register */   
    ADC_ResetCalibration(ADC1);
    /* Check the end of ADC1 reset calibration register */
    while(ADC_GetResetCalibrationStatus(ADC1));
         
    /* Start ADC1 calibration */
    ADC_StartCalibration(ADC1);
    /* Check the end of ADC1 calibration */
    while(ADC_GetCalibrationStatus(ADC1));
 
    /* Start ADC1 Software Conversion */ 
    ADC_SoftwareStartConvCmd(ADC1, ENABLE);
}

void ADC_DriversInit(void)
{
    /* 初始化ADC GPIO */
    ADC_GpioInit(ADC_CONVERT_PIN);

    /* 初始化ADC1运行器 */
    ADC_RunModeInit(ADC_CONVERT_CHANNEL);

    /* 初始化DMA */
    ADC_DmaInit();
    
    /* 开始转换 */
    ADC_StartConvert();
}

