/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : PWM_Logic.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月9日
  最近修改   :
  功能描述   : PWM的逻辑操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static PwmRunAttrTypedef PwmRunAttr[PWM_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : PwmLogic_Init
 功能描述  : PWM逻辑运行数据初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void PwmLogic_Init(void)
{
    u8 i;

    // 初始化存储参数
    PwmMmy_RunParaInit();

    // 循环获取运行参数
    for(i=0; i<PWM_CFG_USER_NUM; i++)
    {
        // 获取回路的运行参数
        if(PwmLogic_GetMmyAttr(i) == SUCCESS)
        {
            // 加入地址
            PwmLogic_AddrAnalyzeInit(i);
        }

        // 实际值初始化为0
        PwmRunAttr[i].RealLevel = 0;
    }
}

/*****************************************************************************
 函 数 名  : PwmLogic_AddrAnalyzeInit
 功能描述  : PWM地址逻辑 加入地址（APP+GROUP）
 输入参数  : uc8 pwmId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月13日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmLogic_AddrAnalyzeInit(uc8 pwmId)
{
    // 判断ID是否为
    if(PwmRunAttr[pwmId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        VirtualAddr_Join(PwmRunAttr[pwmId].portAddr);
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmLogic_GetMmyAttr
 功能描述  : 获取PWM回路的逻辑运行数据
 输入参数  : uc8 pwmId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmLogic_GetMmyAttr(uc8 pwmId)
{
    // 判断ID是否符合范围
    if(PWM_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    // 获取地址类型
    if(PwmMmy_GetAddrCatch(pwmId, &PwmRunAttr[pwmId].AddrFrom) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路存储的地址
    if(PwmMmy_GetAddr(pwmId, &PwmRunAttr[pwmId].portAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路的向上调光阀值
    if(PwmMmy_GetRampUpTH(pwmId, &PwmRunAttr[pwmId].RampUpTH) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路的向下调光阀值
    if(PwmMmy_GetRampDownTH(pwmId, &PwmRunAttr[pwmId].RampDownTH) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路的取反输出状态
    if(PwmMmy_GetNotOutStat(pwmId, &PwmRunAttr[pwmId].NotOutStat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmLogic_UpdateRealLevel
 功能描述  : 更新PWM逻辑输出的实际值
 输入参数  : uc8 pwmId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmLogic_UpdateRealLevel(uc8 pwmId)
{
    u8 tmpLevel;

    if(PWM_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    // 判断地址类型是否为自身的
    if(PwmRunAttr[pwmId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        // 获取自身回路存储的地址 的状态
        if(ActualAddr_GetPwmRealLevel(pwmId, &tmpLevel) == SUCCESS)
        {
            PwmRunAttr[pwmId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(PwmRunAttr[pwmId].AddrFrom.CatchFrom == ADDR_CATCH_PWM)
    {
        // 获取PWM某个回路的实际值
        if(PwmLogic_GetRealLevel(PwmRunAttr[pwmId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            PwmRunAttr[pwmId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(PwmRunAttr[pwmId].AddrFrom.CatchFrom == ADDR_CATCH_RELAY)
    {
        // 获取继电器的实际值
        if(RelayLogic_GetRealLevel(PwmRunAttr[pwmId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            PwmRunAttr[pwmId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(PwmRunAttr[pwmId].AddrFrom.CatchFrom == ADDR_CATCH_KEY)
    {
        // 获取按键回路的实际值
        if(KeyLogic_GetRealLevel(PwmRunAttr[pwmId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            PwmRunAttr[pwmId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : PwmLogic_GetRealLevel
 功能描述  : PWM逻辑 获取实际值
 输入参数  : uc8 pwmId
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmLogic_GetRealLevel(uc8 pwmId, u8 *gLevel)
{
    if(gLevel == NULL)
    {
        return ERROR;
    }

    if(PWM_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    // 载入实际值
    *gLevel = PwmRunAttr[pwmId].RealLevel;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmLogic_UpdataOutLogic
 功能描述  : 更新PWM的逻辑输出值
 输入参数  : uc8 pwmId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmLogic_UpdataOutLogic(uc8 pwmId)
{
    u8 realLevel;

    // 判断LED是否存在
    if(PWM_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    // 载入实际值
    realLevel = PwmRunAttr[pwmId].RealLevel;

    if(realLevel > PwmRunAttr[pwmId].RampUpTH)
    {
        realLevel = PwmRunAttr[pwmId].RampUpTH;
    }
    else if(realLevel < PwmRunAttr[pwmId].RampDownTH)
    {
        realLevel = PwmRunAttr[pwmId].RampDownTH;
    }
    else
    {
    }

    if(PwmRunAttr[pwmId].NotOutStat == PWM_OUT_NOT_ENABLE)
    {
        realLevel = (255 - realLevel);
    }

    // 设置PWM输出值
    PWM_SetOutLevel(pwmId, realLevel);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmLogic_UpdataLogicPoll
 功能描述  : PWM逻辑 更新输出逻辑轮巡函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月12日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void PwmLogic_UpdataLogicPoll(void)
{
    u8 i = 0;

    for(i=0; i<PWM_CFG_USER_NUM; i++)
    {
        if(PwmLogic_UpdateRealLevel(i) == SUCCESS)
        {
            PwmLogic_UpdataOutLogic(i);
        }
    }
}




