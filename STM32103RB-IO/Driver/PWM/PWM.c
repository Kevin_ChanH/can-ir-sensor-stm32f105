/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : PWM.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月12日
  最近修改   :
  功能描述   : PWM驱动文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月12日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
u8 PwmOutLevel[PWM_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
const PinNames PwmGpioMap[PWM_CFG_USER_NUM] =
{
    PWM_Pin_1, PWM_Pin_2, PWM_Pin_3, PWM_Pin_4
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : PWM_GpioInit
 功能描述  : 初始化PWM的GPIO
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月12日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void PWM_GpioInit(void)
{
    u8 i = 0;

    for(i=0; i<PWM_CFG_USER_NUM; i++)
    {
        Gpio_Init(PwmGpioMap[i], GPIO_Mode_AF_PP, PWM_INIT_OUT);
    }
}

/*****************************************************************************
 函 数 名  : PWM_RunParaInit
 功能描述  : 初始化PWM的运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月12日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void PWM_RunParaInit(void)
{
    TIM_TimeBaseInitTypeDef     TIM_TimeBaseStructure;
    TIM_OCInitTypeDef           TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);


    /* 配置时钟基准  频率 */
    TIM_TimeBaseStructure.TIM_Period = 254;
    TIM_TimeBaseStructure.TIM_Prescaler = PWM_CFG_FERQ_PSC;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

    /* 配置运行模式 PWM1 */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC1Init(TIM2, &TIM_OCInitStructure);
    /* 使能PWM1重载 */
    TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Enable);

    /* 配置运行模式 PWM2 */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC2Init(TIM2, &TIM_OCInitStructure);
    /* 使能PWM2重载 */
    TIM_OC2PreloadConfig(TIM2, TIM_OCPreload_Enable);

    /* 配置运行模式 PWM3 */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC3Init(TIM2, &TIM_OCInitStructure);
    /* 使能PWM3重载 */
    TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);

    /* 配置运行模式 PWM4 */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OC4Init(TIM2, &TIM_OCInitStructure);
    /* 使能PWM4重载 */
    TIM_OC4PreloadConfig(TIM2, TIM_OCPreload_Enable);

    /* 使能基准重载 */
    TIM_ARRPreloadConfig(TIM2, ENABLE);

    /* 使能PWM1~PWM4 */
    TIM_Cmd(TIM2, ENABLE);

    PWM_OutLevelInit();
}

/*****************************************************************************
 函 数 名  : PWM_IsId
 功能描述  : 判断PWM ID 是否合法
 输入参数  : uc8  pwmId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus PWM_IsId(uc8  pwmId)
{
    if(pwmId < PWM_CFG_USER_NUM)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : PWM_OutLevelInit
 功能描述  : 初始化PWM的输出值
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月12日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void PWM_OutLevelInit(void)
{
    u8 i = 0;

    // 循环初始化PWM值
    for(i=0; i<PWM_CFG_USER_NUM; i++)
    {
        PwmOutLevel[i] = 0;
    }
}

/*****************************************************************************
 函 数 名  : PWM_SetOutLevel
 功能描述  : 设置输出值
 输入参数  : uc8 pwmId
             uc8 sLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月12日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PWM_SetOutLevel(uc8 pwmId, uc8 sLevel)
{
    if(PWM_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    switch (pwmId)
    {
        case 0:
            TIM_SetCompare1(TIM2, sLevel);
            break;

        case 1:
            TIM_SetCompare2(TIM2, sLevel);
            break;

        case 2:
            TIM_SetCompare3(TIM2, sLevel);
            break;

        case 3:
            TIM_SetCompare4(TIM2, sLevel);
            break;

        default:
            break;
    }

    return SUCCESS;
}


