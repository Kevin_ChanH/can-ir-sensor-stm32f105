/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : PWM_MMY.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月8日
  最近修改   :
  功能描述   : PWM_MMY.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __PWM_MMY_H__
#define __PWM_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
typedef enum
{
    PWM_OUT_NOT_DISABLE = 0,
    PWM_OUT_NOT_ENABLE,
} PwmOutNotTypedef;

typedef struct
{
    AddrFromTypedef     AddrFrom;       /* 地址跟随类型 */
    RunAddrTypedef      portAddr;       /* 端口存储地址 */
    u8                  RampUpTH;       /* PWM的向上调光阀值 */
    u8                  RampDownTH;     /* PWM的向下调光阀值 */
    u8                  RealLevel;      /* PWM端口的实际值 */
    PwmOutNotTypedef    NotOutStat;     /* PWM的输出是否取反 */
} PwmRunAttrTypedef;


/* PWM 存储参数 占6Byte */
#define     PWM_MMY_PARA_LEN            6                               /* 占用6Bytes配置字节 */
#define     PWM_MMY_DATA_TYPE           MMY_DATA_TYPE_MID               /* 存储数据类型为中等类型 */
#define     PWM_MMY_CFG_MSG_SIZE        MCU_MMY_CFG_MSG_MID_SIZE        /* 存储最大字节数为32Byte */




#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

extern  FlagStatus                  PwmMmy_IsId                                 (u8 pwmId);
extern  ErrorStatus                 PwmMmy_GetParaFromMMY                       (uc8 pwmId, u8 gParaBuff[]);
extern  ErrorStatus                 PwmMmy_SetParaToMMY                         (uc8 pwmId, uc8 sParaBuff[]);
extern  ErrorStatus                 PwmMmy_GetParaFromRam                       (uc8 pwmId, u8 gParaBuff[]);
extern  ErrorStatus                 PwmMmy_SetParaToRam                         (uc8 pwmId, uc8 sParaBuff[]);
extern  void                        PwmMmy_RunParaInit                          (void);
extern  ErrorStatus                 PwmMmy_GetAddrCatch                         (u8 pwmId, AddrFromTypedef *addrFrom);
extern  ErrorStatus                 PwmMmy_GetAddr                              (uc8 pwmId, RunAddrTypedef *gAddr);
extern  ErrorStatus                 PwmMmy_GetRampUpTH                          (uc8 pwmId, u8 *gTH);
extern  ErrorStatus                 PwmMmy_GetRampDownTH                        (uc8 pwmId, u8 *gTH);
extern  ErrorStatus                 PwmMmy_GetNotOutStat                        (uc8 pwmId, PwmOutNotTypedef *gStat);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __PWM_MMY_H__ */
