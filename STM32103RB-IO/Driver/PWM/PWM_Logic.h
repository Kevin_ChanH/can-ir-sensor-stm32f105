/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : PWM_Logic.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月9日
  最近修改   :
  功能描述   : PWM_Logic.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __PWM_LOGIC_H__
#define __PWM_LOGIC_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  void                    PwmLogic_Init                                   (void);
extern  ErrorStatus             PwmLogic_AddrAnalyzeInit                        (uc8 pwmId);
extern  ErrorStatus             PwmLogic_GetMmyAttr                             (uc8 pwmId);
extern  ErrorStatus             PwmLogic_UpdateRealLevel                        (uc8 pwmId);
extern  ErrorStatus             PwmLogic_GetRealLevel                           (uc8 pwmId, u8 *gLevel);
extern  ErrorStatus             PwmLogic_UpdataOutLogic                         (uc8 pwmId);
extern  void                    PwmLogic_UpdataLogicPoll                        (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __PWM_LOGIC_H__ */
