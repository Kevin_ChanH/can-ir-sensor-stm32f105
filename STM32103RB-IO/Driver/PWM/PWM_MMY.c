/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : PWM_MMY.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月8日
  最近修改   :
  功能描述   : PWM的存储操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
/* PWM 存储的运行参数 */
static u8  PwmMmyPara[PWM_CFG_USER_NUM][PWM_MMY_PARA_LEN] = {0};

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
/* PWM 存储的默认运行参数 */
static uc8 PwmMmyParaDefault[PWM_CFG_USER_NUM][PWM_MMY_PARA_LEN] =
{
    {0, 100, 1, 0xFF, 0, 0},
    {0, 100, 2, 0xFF, 0, 0},
    {0, 100, 3, 0xFF, 0, 0},
    {0, 100, 4, 0xFF, 0, 0},
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : PwmMmy_IsId
 功能描述  : 判断PWM的范围
 输入参数  : u8 pwmId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus PwmMmy_IsId(u8 pwmId)
{
    /* ID 判断范围为 0~PWM个数-1 */
    if(pwmId < PWM_CFG_USER_NUM)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : PwmMmy_GetParaFromMMY
 功能描述  : 从FLASH中获取PWM参数
 输入参数  : uc8 pwmId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_GetParaFromMMY(uc8 pwmId, u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr = MCU_PWM_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID号 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    gMmyDat.Type = PWM_MMY_DATA_TYPE;

    /* 获取地址下标 */
    gMmyAddr += ((pwmId) * PWM_MMY_CFG_MSG_SIZE);

    /* 获取FLASH数据 */
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<PWM_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmMmy_SetParaToMMY
 功能描述  : 设置PWM参数到FLASH中
 输入参数  : uc8 pwmId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_SetParaToMMY(uc8 pwmId, uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr = MCU_PWM_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID号 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    sMmyDat.Type = PWM_MMY_DATA_TYPE;

    /* 获取地址下标 */
    sMmyAddr += ((pwmId) * PWM_MMY_CFG_MSG_SIZE);

    /* 载入数据 */
    for(i = 0; i<PWM_MMY_CFG_MSG_SIZE; i++)
    {
        if(i < PWM_MMY_PARA_LEN)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    /* 将数据载入到FLASH内 */
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmMmy_GetParaFromRam
 功能描述  : 从RAM中获取PWM参数
 输入参数  : uc8 pwmId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_GetParaFromRam(uc8 pwmId, u8 gParaBuff[])
{
    u8 i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID号 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<PWM_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = PwmMmyPara[pwmId][i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmMmy_SetParaToRam
 功能描述  : 设置PWM参数到RAM中
 输入参数  : uc8 pwmId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_SetParaToRam(uc8 pwmId, uc8 sParaBuff[])
{
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID号 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 载入数据 */
    for(i = 0; i<PWM_MMY_PARA_LEN; i++)
    {
        PwmMmyPara[pwmId][i] = sParaBuff[i];
    }

    return PwmMmy_SetParaToMMY(pwmId, sParaBuff);
}

/*****************************************************************************
 函 数 名  : PwmMmy_RunParaInit
 功能描述  : 初始化PWM运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void    PwmMmy_RunParaInit(void)
{
    u8  i = 0;
    u8  j = 0;

    for(i = 0; i<PWM_CFG_USER_NUM; i++)
    {
        /* 获取数据 判断获取是否失败 */
        if(PwmMmy_GetParaFromMMY(i, &PwmMmyPara[i][0]) != SUCCESS)
        {
            /* 获取回路运行参数失败 载入默认数据 */
            for(j = 0; j < PWM_MMY_PARA_LEN; j++)
            {
                PwmMmyPara[i][j] = PwmMmyParaDefault[i][j];
            }
        }
    }
}

/*****************************************************************************
 函 数 名  : PwmMmy_GetAddrCatch
 功能描述  : 获取PWM的地址索引
 输入参数  : u8 pwmId
             AddrFromTypedef *addrFrom
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_GetAddrCatch(u8 pwmId, AddrFromTypedef *addrFrom)
{
    u8 addrCatch = 0;
    u8 addrIndex = 0;

    /* 判断指针是否为空 */
    if(addrFrom == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID是否合法 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 获取地址跟随码 */
    addrCatch = (PwmMmyPara[pwmId][0] >> 5);
    /* 获取序号下标 */
    addrIndex = (PwmMmyPara[pwmId][0] & 0x1F);


    /* 跟随自身地址 自身存储的APP+GROUP */
    if(addrCatch == 0)
    {
        addrFrom->CatchFrom = ADDR_CATCH_SELF;
    }
    /* 跟随PWM值 - 实际值 */
    else if(addrCatch == 1)
    {
        addrFrom->CatchFrom = ADDR_CATCH_PWM;
    }
    /* 跟随继电器值 - 实际值 */
    else if(addrCatch == 2)
    {
        addrFrom->CatchFrom = ADDR_CATCH_RELAY;
    }
    /* 跟随按键值 - 按键的地址的实际值 */
    else if(addrCatch == 3)
    {
        addrFrom->CatchFrom = ADDR_CATCH_KEY;
    }
    else
    {
        return ERROR;
    }

    /* 载入下标 */
    addrFrom ->Index = addrIndex;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmMmy_GetAddr
 功能描述  : 获取PWM的存储地址
 输入参数  : uc8 pwmId
             RunAddrTypedef *gAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_GetAddr(uc8 pwmId, RunAddrTypedef *gAddr)
{
    /* 判断指针是否为空 */
    if(gAddr == NULL)
    {
        return ERROR;
    }

    /* 判断LED ID是否合法 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 载入APP */
    gAddr->App      = PwmMmyPara[pwmId][1];
    /* 载入GROUP */
    gAddr->Group    = PwmMmyPara[pwmId][2];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmMmy_GetRampUpTH
 功能描述  : 获取PWM的向上调光阀值
 输入参数  : uc8 pwmId
             u8 *gTH
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_GetRampUpTH(uc8 pwmId, u8 *gTH)
{
    /* 判断指针是否为空 */
    if(gTH == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID是否合法 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 载入阀值 */
    *gTH = PwmMmyPara[pwmId][3];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmMmy_GetRampDownTH
 功能描述  : 获取PWM的向下调光阀值
 输入参数  : uc8 pwmId
             u8 *gTH
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_GetRampDownTH(uc8 pwmId, u8 *gTH)
{
    /* 判断指针是否为空 */
    if(gTH == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID是否合法 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    /* 载入阀值 */
    *gTH = PwmMmyPara[pwmId][4];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : PwmMmy_GetNotOutStat
 功能描述  : 获取PWM的取反输出状态
 输入参数  : uc8 pwmId
             PwmOutNotTypedef *gStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus PwmMmy_GetNotOutStat(uc8 pwmId, PwmOutNotTypedef *gStat)
{
    u8 gValue;

    /* 判断指针是否为空 */
    if(gStat == NULL)
    {
        return ERROR;
    }

    /* 判断PWM ID是否合法 */
    if(PwmMmy_IsId(pwmId) != SET)
    {
        return ERROR;
    }

    gValue = PwmMmyPara[pwmId][5];

    if((gValue & 0x01) == 0x01)
    {
        *gStat = PWM_OUT_NOT_ENABLE;
    }
    else
    {
        *gStat = PWM_OUT_NOT_DISABLE;
    }

    return SUCCESS;
}




