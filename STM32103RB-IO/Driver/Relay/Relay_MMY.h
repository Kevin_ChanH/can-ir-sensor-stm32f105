/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Relay_MMY.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月29日
  最近修改   :
  功能描述   : Relay_MMY.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月29日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __RELAY_MMY_H__
#define __RELAY_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    RELAY_OPEN_DIRECTION_ABOVE = 0,     /* 开启方向是大于阀值时开启 其余时间关闭 */
    RELAY_OPEN_DIRECTION_SMEQ,          /* 开启方向是小于等于阀值时开启 其余时间关闭 */
} RelayOpenDirection;

typedef struct
{
    AddrFromTypedef     AddrFrom;       /* 地址跟随类型 */
    RunAddrTypedef      portAddr;       /* 端口存储地址 */
    u8                  SwitchTH;       /* 继电器的开关阀值 */
    u8                  RealLevel;      /* 继电器端口的实际值 */
    RelayOpenDirection  OpenDir;        /* 继电器的开启方向 大于或小于阀值开启 */
} RelayRunAttrTypedef;


/* Relay 存储参数 占5Byte */
#define     RELAY_MMY_PARA_LEN              5                           /* 占用5Bytes配置字节 */
#define     RELAY_MMY_DATA_TYPE             MMY_DATA_TYPE_MID           /* 存储数据类型为中等类型 */
#define     RELAY_MMY_CFG_MSG_SIZE          MCU_MMY_CFG_MSG_MID_SIZE    /* 存储最大字节数为32Byte */



#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

extern  FlagStatus              RelayMmy_IsId                                   (uc8 relayId);
extern  ErrorStatus             RelayMmy_GetParaFromMMY                         (uc8 relayId, u8 gParaBuff[]);
extern  ErrorStatus             RelayMmy_SetParaToMMY                           (uc8 relayId, uc8 sParaBuff[]);
extern  ErrorStatus             RelayMmy_GetParaFromRam                         (uc8 relayId, u8 gParaBuff[]);
extern  ErrorStatus             RelayMmy_SetParaToRam                           (uc8 relayId, uc8 sParaBuff[]);
extern  void                    RelayMmy_RunParaInit                            (void);
extern  ErrorStatus             RelayMmy_GetAddrCatch                           (uc8 relayId, AddrFromTypedef *addrFrom);
extern  ErrorStatus             RelayMmy_GetAddr                                (uc8 relayId, RunAddrTypedef *gAddr);
extern  ErrorStatus             RelayMmy_GetSwitchThresholdValue                (uc8 relayId, u8 *gSTV);
extern  ErrorStatus             RelayMmy_GetOpenDirection                       (uc8 relayId, RelayOpenDirection *gDir);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RELAY_MMY_H__ */
