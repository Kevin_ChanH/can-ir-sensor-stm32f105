/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Relay_MMY.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月29日
  最近修改   :
  功能描述   : Relay的存储运行参数
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月29日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static u8  RelayMmyPara[RELAY_CFG_USER_NUM][RELAY_MMY_PARA_LEN] = {0};

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
static uc8 RelayMmyParaDefault[RELAY_CFG_USER_NUM][RELAY_MMY_PARA_LEN] =
{
    {0x20, 0, 0, 0, 0},         // 指向PWM第1个通道
    {0x21, 0, 0, 0, 0},         // 指向PWM第2个通道
    {0x22, 0, 0, 0, 0},         // 指向PWM第3个通道
    {0x23, 0, 0, 0, 0},         // 指向PWM第4个通道
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : RelayMmy_IsId
 功能描述  : 判断Relay的ID是否在正常范围内
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus RelayMmy_IsId(uc8 relayId)
{
    /* ID 判断范围为 0~Relay个数-1 */
    if(relayId < RELAY_CFG_USER_NUM)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : RelayMmy_GetParaFromMMY
 功能描述  : 从内部存储中获取运行参数
 输入参数  : uc8 relayId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_GetParaFromMMY(uc8 relayId, u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr = MCU_RELAY_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID号 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    gMmyDat.Type = RELAY_MMY_DATA_TYPE;

    /* 获取地址下标 */
    gMmyAddr += ((relayId) * RELAY_MMY_CFG_MSG_SIZE);

    /* 获取FLASH数据 */
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<RELAY_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : RelayMmy_SetParaToMMY
 功能描述  : 设置参数到内部存储
 输入参数  : uc8 relayId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_SetParaToMMY(uc8 relayId, uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr = MCU_RELAY_ADDR;
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID号 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    /* 载入存储类型 */
    sMmyDat.Type = RELAY_MMY_DATA_TYPE;

    /* 获取地址下标 */
    sMmyAddr += ((relayId) * RELAY_MMY_CFG_MSG_SIZE);

    /* 载入数据 */
    for(i = 0; i<RELAY_MMY_CFG_MSG_SIZE; i++)
    {
        if(i < RELAY_MMY_PARA_LEN)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    /* 将数据载入到FLASH内 */
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : RelayMmy_GetParaFromRam
 功能描述  : 从RAM中获取参数
 输入参数  : uc8 relayId
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_GetParaFromRam(uc8 relayId, u8 gParaBuff[])
{
    u8 i = 0;

    /* 判断地址类型 */
    if(gParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID号 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<RELAY_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = RelayMmyPara[relayId][i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : RelayMmy_SetParaToRam
 功能描述  : 设置参数到RAM中
 输入参数  : uc8 relayId
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_SetParaToRam(uc8 relayId, uc8 sParaBuff[])
{
    u8          i = 0;

    /* 判断地址类型 */
    if(sParaBuff == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID号 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    /* 载入数据 */
    for(i = 0; i<RELAY_MMY_PARA_LEN; i++)
    {
        RelayMmyPara[relayId][i] = sParaBuff[i];
    }

    return RelayMmy_SetParaToMMY(relayId, sParaBuff);
}

/*****************************************************************************
 函 数 名  : RelayMmy_RunParaInit
 功能描述  : RelayMmy运行参数初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void    RelayMmy_RunParaInit(void)
{
    u8  i = 0;
    u8  j = 0;


    for(i = 0; i<RELAY_CFG_USER_NUM; i++)
    {
        /* 获取数据 判断获取是否失败 */
        if(RelayMmy_GetParaFromMMY(i, &RelayMmyPara[i][0]) != SUCCESS)
        {
            /* 获取回路运行参数失败 载入默认数据 */
            for(j = 0; j < RELAY_MMY_PARA_LEN; j++)
            {
                RelayMmyPara[i][j] = RelayMmyParaDefault[i][j];
            }
        }
    }
}


/*****************************************************************************
 函 数 名  : RelayMmy_GetAddrCatch
 功能描述  : 获取地址索引号
 输入参数  : uc8 relayId
             AddrFromTypedef *addrFrom
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月29日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_GetAddrCatch(uc8 relayId, AddrFromTypedef *addrFrom)
{
    u8 addrCatch = 0;
    u8 addrIndex = 0;

    /* 判断指针是否为空 */
    if(addrFrom == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID是否合法 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    /* 获取地址跟随码 */
    addrCatch = (RelayMmyPara[relayId][0] >> 5);
    /* 获取序号下标 */
    addrIndex = (RelayMmyPara[relayId][0] & 0x1F);

    /* 跟随自身地址 自身存储的APP+GROUP */
    if(addrCatch == 0)
    {
        addrFrom->CatchFrom = ADDR_CATCH_SELF;
    }
    /* 跟随PWM值 - 实际值 */
    else if(addrCatch == 1)
    {
        addrFrom->CatchFrom = ADDR_CATCH_PWM;
    }
    /* 跟随继电器值 - 实际值 */
    else if(addrCatch == 2)
    {
        addrFrom->CatchFrom = ADDR_CATCH_RELAY;
    }
    /* 跟随按键值 - 按键的地址的实际值 */
    else if(addrCatch == 3)
    {
        addrFrom->CatchFrom = ADDR_CATCH_KEY;
    }
    else
    {
        return ERROR;
    }

    /* 载入下标 */
    addrFrom ->Index = addrIndex;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : RelayMmy_GetAddr
 功能描述  : 获取LED回路存储地址
 输入参数  : uc8 relayId
             RunAddrTypedef *gAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_GetAddr(uc8 relayId, RunAddrTypedef *gAddr)
{
    /* 判断指针是否为空 */
    if(gAddr == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID是否合法 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    /* 载入APP */
    gAddr->App      = RelayMmyPara[relayId][1];
    /* 载入GROUP */
    gAddr->Group    = RelayMmyPara[relayId][2];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : RelayMmy_GetSwitchThresholdValue
 功能描述  : 获取LED回路的阀值
 输入参数  : uc8 relayId
             u8 *gSTV
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_GetSwitchThresholdValue(uc8 relayId, u8 *gSTV)
{
    /* 判断指针是否为空 */
    if(gSTV == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID是否合法 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    /* 载入阀值 */
    *gSTV = RelayMmyPara[relayId][3];

    /* 返回成功 */
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : RelayMmy_GetOpenDirection
 功能描述  : 获取LED回路的开启方向
 输入参数  : uc8 relayId
             RelayOpenDirection *gDir
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayMmy_GetOpenDirection(uc8 relayId, RelayOpenDirection *gDir)
{
    u8 gValue = 0;

    /* 判断指针是否为空 */
    if(gDir == NULL)
    {
        return ERROR;
    }

    /* 判断继电器ID是否合法 */
    if(RelayMmy_IsId(relayId) != SET)
    {
        return ERROR;
    }

    gValue = RelayMmyPara[relayId][4];

    /* 判断方向是否为向上方向 */
    if((gValue & 0x01) == 0x00)
    {
        *gDir = RELAY_OPEN_DIRECTION_ABOVE;
    }
    else
    {
        *gDir = RELAY_OPEN_DIRECTION_SMEQ;
    }

    /* 返回成功 */
    return SUCCESS;
}


