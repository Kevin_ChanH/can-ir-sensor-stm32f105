/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Relay_Logic.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年10月8日
  最近修改   :
  功能描述   : 继电器 逻辑处理文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

static RelayRunAttrTypedef  RelayRunAttr[RELAY_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : RelayLogic_Init
 功能描述  : 继电器逻辑参数 初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void RelayLogic_Init(void)
{
    u8 i;

    // 初始化存储参数
    RelayMmy_RunParaInit();

    // 循环获取运行参数
    for(i=0; i<RELAY_CFG_USER_NUM; i++)
    {
        // 获取回路的运行参数
        RelayLogic_GetMmyAttr(i);

        // 实际值初始化为0
        RelayRunAttr[i].RealLevel = 0;
    }
}

/*****************************************************************************
 函 数 名  : RelayLogic_AddrAnalyzeInit
 功能描述  : Relay地址逻辑 加入地址（APP+GROUP）
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月13日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayLogic_AddrAnalyzeInit(uc8 relayId)
{
    if(RelayRunAttr[relayId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        VirtualAddr_Join(RelayRunAttr[relayId].portAddr);
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : RelayLogic_GetMmyAttr
 功能描述  : 获取继电器的存储参数
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayLogic_GetMmyAttr(uc8 relayId)
{
    // 判断ID是否符合范围
    if(Relay_IsId(relayId) != SET)
    {
        return ERROR;
    }

    // 获取地址类型
    if(RelayMmy_GetAddrCatch(relayId, &RelayRunAttr[relayId].AddrFrom) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路存储的地址
    if(RelayMmy_GetAddr(relayId, &RelayRunAttr[relayId].portAddr) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路的开关阀值
    if(RelayMmy_GetSwitchThresholdValue(relayId, &RelayRunAttr[relayId].SwitchTH) != SUCCESS)
    {
        return ERROR;
    }

    // 获取该回路的开关方向
    if(RelayMmy_GetOpenDirection(relayId, &RelayRunAttr[relayId].OpenDir) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : RelayLogic_UpdateRealLevel
 功能描述  : 更新继电器的实际值
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayLogic_UpdateRealLevel(uc8 relayId)
{
    u8 tmpLevel;

    if(Relay_IsId(relayId) != SET)
    {
        return ERROR;
    }

    // 判断地址类型是否为自身的
    if(RelayRunAttr[relayId].AddrFrom.CatchFrom == ADDR_CATCH_SELF)
    {
        // 获取自身回路存储的地址 的状态
        if(ActualAddr_GetRelayRealLevel(relayId, &tmpLevel) == SUCCESS)
        {
            RelayRunAttr[relayId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(RelayRunAttr[relayId].AddrFrom.CatchFrom == ADDR_CATCH_PWM)
    {
        // 获取PWM实际值
        if(PwmLogic_GetRealLevel(RelayRunAttr[relayId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            RelayRunAttr[relayId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(RelayRunAttr[relayId].AddrFrom.CatchFrom == ADDR_CATCH_RELAY)
    {
        // 获取继电器的实际值
        if(RelayLogic_GetRealLevel(RelayRunAttr[relayId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            RelayRunAttr[relayId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else if(RelayRunAttr[relayId].AddrFrom.CatchFrom == ADDR_CATCH_KEY)
    {
        // 获取按键回路的实际值
        if(KeyLogic_GetRealLevel(RelayRunAttr[relayId].AddrFrom.Index, &tmpLevel) == SUCCESS)
        {
            RelayRunAttr[relayId].RealLevel = tmpLevel;

            return SUCCESS;
        }
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : RelayLogic_GetRealLevel
 功能描述  : 继电器逻辑 获取实际值
 输入参数  : uc8 relayId
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayLogic_GetRealLevel(uc8 relayId, u8 *gLevel)
{
    if(gLevel == NULL)
    {
        return ERROR;
    }

    if(Relay_IsId(relayId) != SET)
    {
        return ERROR;
    }

    *gLevel = RelayRunAttr[relayId].RealLevel;

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : RelayLogic_UpdateOutLogic
 功能描述  : 更新输出逻辑
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RelayLogic_UpdateOutLogic(uc8 relayId)
{
    // 判断继电器是否存在
    if(Relay_IsId(relayId) != SET)
    {
        return ERROR;
    }

    // 判断继电器的开启方向 是否为向上方向
    if(RelayRunAttr[relayId].OpenDir == RELAY_OPEN_DIRECTION_ABOVE)
    {
        // 判断实际值是否大于阀值
        if(RelayRunAttr[relayId].RealLevel > RelayRunAttr[relayId].SwitchTH)
        {
            // 开启继电器
            Relay_CnlOut(relayId, Relay_Stat_ON);
        }
        else
        {
            // 关闭继电器
            Relay_CnlOut(relayId, Relay_Stat_OFF);
        }
    }
    // 判断继电器的开启方向 是否为向下方向
    else if(RelayRunAttr[relayId].OpenDir == RELAY_OPEN_DIRECTION_SMEQ)
    {
        // 判断实际值是否小于等于阀值
        if(RelayRunAttr[relayId].RealLevel <= RelayRunAttr[relayId].SwitchTH)
        {
            // 开启继电器
            Relay_CnlOut(relayId, Relay_Stat_ON);
        }
        else
        {
            // 关闭继电器
            Relay_CnlOut(relayId, Relay_Stat_OFF);
        }
    }
    else
    {
        // 返回错误
        return ERROR;
    }

    // 返回成功
    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : RelayLogic_OutLogicPoll
 功能描述  : 轮询输出逻辑
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void  RelayLogic_OutLogicPoll(void)
{
    u8 i = 0;

    // 遍历所有继电器输出回路
    for(i=0; i<RELAY_CFG_USER_NUM; i++)
    {
        // 更新实际值
        if(RelayLogic_UpdateRealLevel(i) == SUCCESS)
        {
            // 更新输出逻辑
            RelayLogic_UpdateOutLogic(i);
        }
    }
}



