/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Relay.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月1日
  最近修改   :
  功能描述   : Relay.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __RELAY_H__
#define __RELAY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
typedef struct
{
    PinNames OnPin;
    PinNames OffPin;
} RelayPinStruct;

typedef enum
{
    Relay_Stat_OFF = 0,
    Relay_Stat_ON,
    Relay_Stat_Unknown,
} RelayStatStruct;

typedef struct
{
    RelayStatStruct OutStat;    /* 输出状态 */
    RelayStatStruct CnlStat;    /* 控制状态 */
} RelayAttrTypedef;


/* 继电器最大的使用 */
// 继电器最大使用个数
#define     RELAY_CFG_MAX_NUM       MCU_RELAY_CHANNEL_NUM
// 继电器使用个数
#define     RELAY_CFG_USER_NUM      MCU_RELAY_CHANNEL_NUM
// 继电器起始ID号
#define     RELAY_ID_LIMIT_START    0
// 继电器结束ID号
#define     RELAY_ID_LIMIT_END      (RELAY_CFG_USER_NUM - 1)


/* 继电器ID */
#define     Relay1                  0       /* 继电器1 ID号 */
#define     Relay2                  1       /* 继电器2 ID号 */
#define     Relay3                  2       /* 继电器3 ID号 */
#define     Relay4                  3       /* 继电器4 ID号 */


/* 继电器 GPIO MAP */
#define RELAY_1_ON_PIN              PC_7    /* 继电器1的开管脚 */
#define RELAY_1_OFF_PIN             PC_3    /* 继电器1的关管脚 */
#define RELAY_2_ON_PIN              PC_6    /* 继电器2的开管脚 */
#define RELAY_2_OFF_PIN             PC_2    /* 继电器2的关管脚 */
#define RELAY_3_ON_PIN              PC_5    /* 继电器3的开管脚 */
#define RELAY_3_OFF_PIN             PC_1    /* 继电器3的关管脚 */
#define RELAY_4_ON_PIN              PC_4    /* 继电器4的开管脚 */
#define RELAY_4_OFF_PIN             PC_0    /* 继电器4的关管脚 */


/* 继电器的吸合 与 释放 状态 */
#define RELAY_OUT_ON                Bit_SET         /* 继电器线圈吸合动作 */
#define RELAY_OUT_OFF               Bit_RESET       /* 继电器线圈释放动作 */

/* 继电器的IO初始状态 */
#define RELAY_PIN_INIT_STAT         RELAY_OUT_OFF   /* 继电器引脚初始状态为线圈释放 */

/* 继电器的初始状态 */
#define RELAY_INIT_STAT             Relay_Stat_OFF  /* 继电器初始状态为关闭状态 */

/* 继电器的持续时间 */
#define RELAY_CONTINUE_TIM_MS       10              /* 继电器线圈吸合持续时间为10Ms */



#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

extern  void                Relay_GpioInit              (void);
extern  void                Relay_InitRunPara           (void);
extern  FlagStatus          Relay_IsId                  (uc8 relayId);
extern  ErrorStatus         Relay_GetMmyAttr            (uc8 relayId);
extern  ErrorStatus         Relay_CnlOut                (uc8 relayId, RelayStatStruct cnlStat);
extern  RelayStatStruct     Relay_GetCnlStat            (uc8 relayId);
extern  RelayStatStruct     Relay_GetOutStat            (uc8 relayId);
extern  void                Relay_ReleaseAllGpio        (void);
extern  ErrorStatus         Relay_Out                   (uc8 relayId, RelayStatStruct outStat);
extern  void                Relay_OutPoll               (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RELAY_H__ */
