/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Relay.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月1日
  最近修改   :
  功能描述   : 继电器操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月1日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

static RelayAttrTypedef     RelayAttr[RELAY_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
static const RelayPinStruct RelayPinMap[RELAY_CFG_USER_NUM] =
{
    {RELAY_1_ON_PIN, RELAY_1_OFF_PIN},
    {RELAY_2_ON_PIN, RELAY_2_OFF_PIN},
    {RELAY_3_ON_PIN, RELAY_3_OFF_PIN},
    {RELAY_4_ON_PIN, RELAY_4_OFF_PIN},
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : Relay_GpioInit
 功能描述  : 继电器初始化GPIO
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Relay_GpioInit(void)
{
    u8 i = 0;

    // 初始化GPIO
    for(i=0; i<RELAY_CFG_USER_NUM; i++)
    {
        Gpio_Init(RelayPinMap[i].OnPin, GPIO_Mode_Out_PP, RELAY_PIN_INIT_STAT);
        Gpio_Init(RelayPinMap[i].OffPin, GPIO_Mode_Out_PP, RELAY_PIN_INIT_STAT);
    }
}

/*****************************************************************************
 函 数 名  : Relay_InitRunPara
 功能描述  : 继电器初始化运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Relay_InitRunPara(void)
{
    u8 i = 0;

    // 循环初始化接口
    for(i=0; i<RELAY_CFG_USER_NUM; i++)
    {
        RelayAttr[i].OutStat = Relay_Stat_Unknown;
        RelayAttr[i].CnlStat = Relay_Stat_Unknown;
    }
}

/*****************************************************************************
 函 数 名  : Relay_IsId
 功能描述  : 判断继电器ID是否合法
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  Relay_IsId(uc8 relayId)
{
    // 判断继电器ID是否在本地范围内
    if(relayId < RELAY_CFG_USER_NUM)
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : Relay_CnlOut
 功能描述  : 继电器控制输出
 输入参数  : uc8 relayId
             RelayStatStruct cnlStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Relay_CnlOut(uc8 relayId, RelayStatStruct cnlStat)
{
    if(Relay_IsId(relayId) == SET)
    {
        // 判断控制状态是否为立即执行状态
        if((cnlStat == Relay_Stat_OFF)
           || (cnlStat == Relay_Stat_ON))
        {
            // 判断控制状态是否不等于输出状态
            if(RelayAttr[relayId].CnlStat != cnlStat)
            {
                // 载入输出状态
                RelayAttr[relayId].CnlStat = cnlStat;
            }

            return SUCCESS;
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : Relay_GetOutStat
 功能描述  : 获取继电器输出状态
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
RelayStatStruct Relay_GetOutStat(uc8 relayId)
{
    // 判断继电器回路是否合法
    if(Relay_IsId(relayId) == SET)
    {
        // 返回输出状态
        return RelayAttr[relayId].OutStat;
    }

    return Relay_Stat_Unknown;
}

/*****************************************************************************
 函 数 名  : Relay_GetCnlStat
 功能描述  : 获取继电器控制状态
 输入参数  : uc8 relayId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
RelayStatStruct Relay_GetCnlStat(uc8 relayId)
{
    // 判断继电器回路是否合法
    if(Relay_IsId(relayId) == SET)
    {
        // 返回控制状态
        return RelayAttr[relayId].CnlStat;
    }

    return Relay_Stat_Unknown;
}

/*****************************************************************************
 函 数 名  : Relay_ReleaseAllGpio
 功能描述  : 释放所有继电器管脚
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Relay_ReleaseAllGpio(void)
{
    u8 i = 0;

    // 关闭所有继电器IO 释放所有线圈
    for(i=0; i<RELAY_CFG_USER_NUM; i++)
    {
        Gpio_Write(RelayPinMap[i].OnPin, RELAY_OUT_OFF);
        Gpio_Write(RelayPinMap[i].OffPin, RELAY_OUT_OFF);
    }
}

/*****************************************************************************
 函 数 名  : Relay_Out
 功能描述  : 继电器输出
 输入参数  : uc8 relayId
             RelayStatStruct outStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Relay_Out(uc8 relayId, RelayStatStruct outStat)
{
    if(Relay_IsId(relayId) == SET)
    {
        // 判断控制状态是否为立即执行状态
        if((outStat == Relay_Stat_OFF)
           || (outStat == Relay_Stat_ON))
        {
            // 判断输出状态与之前的输出状态不相等
            if(RelayAttr[relayId].OutStat != outStat)
            {
                // 载入现在的输出状态
                RelayAttr[relayId].OutStat = outStat;

                // 判断控制状态是否为开
                if(outStat == Relay_Stat_ON)
                {
                    // 开启继电器
                    Gpio_Write(RelayPinMap[relayId].OffPin, RELAY_OUT_OFF);
                    Gpio_Write(RelayPinMap[relayId].OnPin, RELAY_OUT_ON);

                    return SUCCESS;
                }
                // 判断控制状态是否为关
                else if(outStat == Relay_Stat_OFF)
                {
                    // 关闭继电器
                    Gpio_Write(RelayPinMap[relayId].OnPin, RELAY_OUT_OFF);
                    Gpio_Write(RelayPinMap[relayId].OffPin, RELAY_OUT_ON);

                    return SUCCESS;
                }
                else
                {
                }
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : Relay_OutPoll
 功能描述  : 继电器输出轮巡函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月30日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Relay_OutPoll(void)
{
    u8 i = 0;
    static FlagStatus RelayLockFlag = RESET;

    // 判断时间是否到达
    if(Tmm_IsON(TMM_RELAY_CNL) != SET)
    {
        // 判断继电器是否被锁定
        if(RelayLockFlag == SET)
        {
            // 解锁继电器
            RelayLockFlag = RESET;
            // 释放所有继电器IO
            Relay_ReleaseAllGpio();
        }

        // 遍历所有继电器
        for(i=0; i<RELAY_CFG_USER_NUM; i++)
        {
            // 判断输出状态与控制状态是否不相同
            if(RelayAttr[i].OutStat != RelayAttr[i].CnlStat)
            {
                // 控制输出
                if(Relay_Out(i, RelayAttr[i].CnlStat) == SUCCESS)
                {
                    // 继电器状态设置为锁定状态
                    RelayLockFlag = SET;

                    // 载入继电器锁定时间
                    Tmm_LoadMs(TMM_RELAY_CNL, RELAY_CONTINUE_TIM_MS);

                    // 退出循环 一次只执行一个继电器动作
                    break;
                }
            }
        }
    }
}



