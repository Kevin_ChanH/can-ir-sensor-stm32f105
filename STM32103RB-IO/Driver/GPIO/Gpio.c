/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Gpio.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : GPIO的操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : Gpio_ChkPin
 功能描述  : 检查GPIO的合法性
 输入参数  : PinNames pin
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus Gpio_ChkPin(PinNames pin)
{
    if(pin == PIN_NC)
    {
        return RESET;
    }

    if((pin > MCU_MAX_GPIO) || (pin == PD_0) || (pin == PD_1))
    {
        return RESET;
    }

    return SET;
}

/*****************************************************************************
 函 数 名  : Gpio_GetIo
 功能描述  : 获取GPIO的详细参数
 输入参数  : PinNames pin
             PinPara *pinPara
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Gpio_GetIo(PinNames pin, PinPara *pinPara)
{
    if(Gpio_ChkPin(pin) != SET)
    {
        return ERROR;
    }

    // 获取具体的引脚下标
    pinPara->Pin = (1 << (pin & 0x0F));

    // 获取具体的端口号
    if((pin & 0xF0) == 0x00)
    {
        pinPara->Port = GPIOA;
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    }
    else if((pin & 0xF0) == 0x10)
    {
        pinPara->Port = GPIOB;
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    }
    else if((pin & 0xF0) == 0x20)
    {
        pinPara->Port = GPIOC;
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    }
    else if((pin & 0xF0) == 0x30)
    {
        pinPara->Port = GPIOD;
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Gpio_Init
 功能描述  : 初始化GPIO
 输入参数  : PinNames pin
             GPIOMode_TypeDef mode
             BitAction bitVal
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Gpio_Init(PinNames pin, GPIOMode_TypeDef mode, BitAction bitVal)
{
    GPIO_InitTypeDef    GPIO_InitStructure;
    PinPara             pinPara;

    if(IS_GPIO_MODE(mode) == 0)
    {
        return ERROR;
    }

    // 获取具体的GPIO
    if(Gpio_GetIo(pin, &pinPara) == ERROR)
    {
        return ERROR;
    }


    // 获取具体的端口号
    if(pinPara.Port == GPIOA)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    }
    else if(pinPara.Port == GPIOB)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
    }
    else if(pinPara.Port == GPIOC)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
    }
    else if(pinPara.Port == GPIOD)
    {
        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
    }
    else
    {
        return ERROR;
    }

    // 初始化GPIO
    GPIO_InitStructure.GPIO_Pin   = pinPara.Pin;
    GPIO_InitStructure.GPIO_Mode  = mode;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(pinPara.Port, &GPIO_InitStructure);

    // 判断是否为输出模式
    if((mode == GPIO_Mode_Out_PP) || (mode == GPIO_Mode_Out_OD))
    {
        // 填写入输出值
        GPIO_WriteBit(pinPara.Port, pinPara.Pin, bitVal);
    }

    // 返回操作成功
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Gpio_Write
 功能描述  : GPIO输出
 输入参数  : PinNames pin
             BitAction bitVal
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Gpio_Write(PinNames pin, BitAction bitVal)
{
    PinPara             pinPara;

    // 获取具体的GPIO
    if(Gpio_GetIo(pin, &pinPara) == ERROR)
    {
        return;
    }

    GPIO_WriteBit(pinPara.Port, pinPara.Pin, bitVal);
}

/*****************************************************************************
 函 数 名  : Gpio_Toggle
 功能描述  : GPIO取反状态
 输入参数  : PinNames pin
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Gpio_Toggle(PinNames pin)
{
    PinPara             pinPara;

    // 获取具体的GPIO
    if(Gpio_GetIo(pin, &pinPara) == ERROR)
    {
        return;
    }

    // 取反引脚
    pinPara.Port->ODR ^= pinPara.Pin;
}

/*****************************************************************************
 函 数 名  : Gpio_Read
 功能描述  : GPIO读取状态
 输入参数  : PinNames pin
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u8 Gpio_Read(PinNames pin)
{
    PinPara             pinPara;

    // 获取具体的GPIO
    if(Gpio_GetIo(pin, &pinPara) == ERROR)
    {
        return 0;
    }

    // 返回引脚输入状态
    return GPIO_ReadInputDataBit(pinPara.Port, pinPara.Pin);
}

/*****************************************************************************
 函 数 名  : Gpio_GetOutStat
 功能描述  : GPIO获取输出状态
 输入参数  : PinNames pin
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u8 Gpio_GetOutStat(PinNames pin)
{
    PinPara             pinPara;

    // 获取具体的GPIO
    if(Gpio_GetIo(pin, &pinPara) == ERROR)
    {
        return 0;
    }

    // 返回引脚输出状态
    return GPIO_ReadOutputDataBit(pinPara.Port, pinPara.Pin);
}

