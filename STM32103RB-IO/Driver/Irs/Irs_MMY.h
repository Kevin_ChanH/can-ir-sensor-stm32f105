/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Irs_MMY.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年11月4日
  最近修改   :
  功能描述   : Irs_MMY.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年11月4日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __IRS_MMY_H__
#define __IRS_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    IRS_MODE_IDLE = 0,                  /* 空闲模式 红外不检测 */
    IRS_MODE_INTERLOCK,                 /* 联动模式 红外检测 且根据命令执行 */
    IRS_MODE_NORMAL,                    /* 正常模式 检测到有人 OR 无人 就发送 */
}IrsRunModeTypedef;
#define IS_IRS_MODE(PARAM)              (((PARAM) == IRS_MODE_IDLE)         ||  \
                                         ((PARAM) == IRS_MODE_INTERLOCK)    ||  \
                                         ((PARAM) == IRS_MODE_NORMAL))

typedef enum
{
    IRS_SIGNAL_UNKOWN = 0,              /* 红外信号 - 未知信号 */
    IRS_SIGNAL_HAS,                     /* 红外信号 - 有人信号 */
    IRS_SIGNAL_NONE,                    /* 红外信号 - 无人信号 */
}IrsSignalTypedef;
#define IS_IRS_SIGNAL_TYPE(PARAM)       (((PARAM) == IRS_SIGNAL_UNKOWN)     ||  \
                                         ((PARAM) == IRS_SIGNAL_HAS)        ||  \
                                         ((PARAM) == IRS_SIGNAL_NONE))

typedef enum
{
    IRS_TIME_CNT_STAT_INIT = 0,         /* 计时状态为 - 初始化状态 */
    IRS_TIME_CNT_STAT_FEW_TIME_HAS,     /* 计时状态为 - 一个时间段有人状态 */
    IRS_TIME_CNT_STAT_MULT_TIME_HAS,    /* 计时状态为 - 多个时间段有人状态 */
    IRS_TIME_CNT_STAT_FEW_TIME_NONE,    /* 计时状态为 - 一个时间段无人状态 */
    IRS_TIME_CNT_STAT_MULT_TIME_NONE,   /* 计时状态为 - 多个时间段无人状态 */
}IrsTimeCntStatTypedef;
#define IS_ISR_TIME_CNT_STAT(PARAM)     (((PARAM) == IRS_TIME_CNT_STAT_INIT)            ||  \
                                         ((PARAM) == IRS_TIME_CNT_STAT_FEW_TIME_HAS)    ||  \
                                         ((PARAM) == IRS_TIME_CNT_STAT_MULT_TIME_HAS)   ||  \
                                         ((PARAM) == IRS_TIME_CNT_STAT_FEW_TIME_NONE)   ||  \
                                         ((PARAM) == IRS_TIME_CNT_STAT_MULT_TIME_NONE))

typedef enum
{
    IRS_TIME_CNT_OPT_ON,                /* 红外时间控制 - 打开 */
    IRS_TIME_CNT_OPT_OFF,               /* 红外时间控制 - 关闭 */
    IRS_TIME_CNT_OPT_PAUSE,             /* 红外时间控制 - 暂停 */
    IRS_TIME_CNT_OPT_CONTINUE,          /* 红外时间控制 - 继续 */
    IRS_TIME_CNT_OPT_RESTART,           /* 红外时间控制 - 重新开始 */
}IrsTimeCntOptTypedef;
#define IS_IRS_TIME_CNT_OPT(PARAM)      (((PARAM) == IRS_TIME_CNT_OPT_ON)       ||  \
                                         ((PARAM) == IRS_TIME_CNT_OPT_OFF)      ||  \
                                         ((PARAM) == IRS_TIME_CNT_OPT_PAUSE)    ||  \
                                         ((PARAM) == IRS_TIME_CNT_OPT_CONTINUE) ||  \
                                         ((PARAM) == IRS_TIME_CNT_OPT_RESTART))

typedef enum
{
    IRS_CATCH_STAT_IDEL = 0,            /* 空闲状态 */
    IRS_CATCH_STAT_INIT_WAIT,           /* 初始化等待状态 开始计时 等待第一个脉冲 */
    IRS_CATCH_STAT_ONE_PULSE,           /* 第一个脉冲出现 开始计时 等待进入其他状态 */
    IRS_CATCH_STAT_FEW_TIME_HAS,        /* 一个时间段内，出现几个脉冲 等待进入其他状态 */
    IRS_CATCH_STAT_MULT_TIME_HAS,       /* 多个时间段内，出现多个脉冲 等待进入其他状态 */
    IRS_CATCH_STAT_FEW_TIME_NONE,       /* 一个时间段内，检测不到脉冲 等待进入其他状态 */
    IRS_CATCH_STAT_MULT_TIME_NONE,      /* 多个时间段内，检测不到脉冲 等待进入其他状态 */
    IRS_CATCH_STAT_MAX,                 /* 最大数 */
}IrsCatchStatTypedef;
#define IS_IRS_CATCH_STAT(PARAM)        (((PARAM) == IRS_CATCH_STAT_IDEL)           ||  \
                                         ((PARAM) ==  IRS_CATCH_STAT_INIT_WAIT)     ||  \
                                         ((PARAM) ==  IRS_CATCH_STAT_ONE_PULSE)     ||  \
                                         ((PARAM) ==  IRS_CATCH_STAT_FEW_TIME_HAS)  ||  \
                                         ((PARAM) ==  IRS_CATCH_STAT_MULT_TIME_HAS) ||  \
                                         ((PARAM) ==  IRS_CATCH_STAT_FEW_TIME_NONE) ||  \
                                         ((PARAM) ==  IRS_CATCH_STAT_MULT_TIME_NONE))

typedef enum
{
    IRS_CATCH_EVENT_ONE_PULSE = 0,      /* 红外采集事件 - 第一个脉冲 */
    IRS_CATCH_EVENT_FEW_TIME_HAS,       /* 红外采集事件 - 一个时间段内检测到 */
    IRS_CATCH_EVENT_MULT_TIME_HAS,      /* 红外采集事件 - 多个时间段内检测到 */
    IRS_CATCH_EVENT_FEW_TIME_NONE,      /* 红外采集事件 - 一个时间段内没检测到 */
    IRS_CATCH_EVENT_MULT_TIME_NONE,     /* 红外采集事件 - 多个时间段内没检测到 */
    IRS_CATCH_EVENT_MAX,
}IrsCatchEvent;
#define IS_IRS_CATCH_EVENT(PARAM)       (((PARAM) == IRS_CATCH_EVENT_ONE_PULSE)         ||  \
                                         ((PARAM) ==  IRS_CATCH_EVENT_FEW_TIME_HAS)     ||  \
                                         ((PARAM) ==  IRS_CATCH_EVENT_MULT_TIME_HAS)    ||  \
                                         ((PARAM) ==  IRS_CATCH_EVENT_FEW_TIME_NONE)    ||  \
                                         ((PARAM) ==  IRS_CATCH_EVENT_MULT_TIME_NONE))

typedef struct
{
    TmmExp_ParaTypedef  HasFewTime;     /* 检测到有人的一个时间段 HMS */
    TmmExp_ParaTypedef  HasMultTime;    /* 检测到有人的多个时间段 HMS */
    TmmExp_ParaTypedef  NoneFewTime;    /* 检测到无人的一个时间段 HMS */
    TmmExp_ParaTypedef  NoneMultTime;   /* 检测到无人的多个时间段 HMS */

    u16 HasFewTimeMs;                   /* 检测到有人的一个时间段 该时段检测到有人的总和 Ms */
    u16 HasMultTimeMs;                  /* 检测到有人的多个时间段 该时段检测到有人的总和 Ms */

    IrsRunModeTypedef   IrsRunMode;     /* 红外的运行模式 */
    RunAddrTypedef      CatchIrAddr;    /* 红外检测的运行地址 */
    
    RunAddrTypedef      TriIrAddr;      /* 红外触发额外命令发送开关的AG地址 */
    
    u8                  ResetWaitSec;   /* 复位 - 等待时间 */
    FlagStatus          ResetWaitFinishi;   /* 复位等待完成标记 */
    IrsCatchStatTypedef CatchStat;      /* 红外的检测状态 */

    IrsTimeCntStatTypedef TimeStat;     /* 定时器状态 */

    TmmExp_ID           TexpNoneCnt;    /* 无人 倒计时定时器 */
    TmmExp_ID           TexpHasCnt;     /* 有人 倒计时定时器 */
    TmmCnt_ID           HasTimeCnt;     /* 有人信号 - 计时器 */
}IrsCatchRunAttr;


typedef enum
{
    IRS_LOGIC_STAT_IDEL = 0,            /* 逻辑状态 - 空闲状态      */
    IRS_LOGIC_STAT_AE,                  /* 逻辑状态 - 一直空闲      */
    IRS_LOGIC_STAT_E,                   /* 逻辑状态 - 空闲          */
    IRS_LOGIC_STAT_MO,                  /* 逻辑状态 - 可能检测到    */
    IRS_LOGIC_STAT_ME,                  /* 逻辑状态 - 可能空闲      */
    IRS_LOGIC_STAT_O,                   /* 逻辑状态 - 有人          */
    IRS_LOGIC_STAT_AO,                  /* 逻辑状态 - 一直有人      */
}IrsLogicStatTypedef;
#define IS_IRS_LOGIC_STAT(PARAM)                (((PARAM) == IRS_LOGIC_STAT_IDEL)   ||  \
                                                 ((PARAM) == IRS_LOGIC_STAT_AE)     ||  \
                                                 ((PARAM) == IRS_LOGIC_STAT_E)      ||  \
                                                 ((PARAM) == IRS_LOGIC_STAT_MO)     ||  \
                                                 ((PARAM) == IRS_LOGIC_STAT_ME)     ||  \
                                                 ((PARAM) == IRS_LOGIC_STAT_O)      ||  \
                                                 ((PARAM) == IRS_LOGIC_STAT_AO))

typedef enum
{
    IRS_LOGIC_EVENT_AE = 0,             /* AE的逻辑事件 */
    IRS_LOGIC_EVENT_E,                  /* E的逻辑事件 */
    IRS_LOGIC_EVENT_MO,                 /* MO的逻辑事件 */
    IRS_LOGIC_EVENT_ME,                 /* ME的逻辑事件 */
    IRS_LOGIC_EVENT_O,                  /* O的逻辑事件 */
    IRS_LOGIC_EVENT_AO,                 /* AO的逻辑事件 */
    IRS_LOGIC_EVENT_MAX,                /* 逻辑事件的MAX */
}IrsLogicEventType;
#define IS_IRS_LOGIC_EVENT(PARAM)               (((PARAM) == IRS_LOGIC_EVENT_AE)    ||  \
                                                 ((PARAM) == IRS_LOGIC_EVENT_E)     ||  \
                                                 ((PARAM) == IRS_LOGIC_EVENT_MO)    ||  \
                                                 ((PARAM) == IRS_LOGIC_EVENT_ME)    ||  \
                                                 ((PARAM) == IRS_LOGIC_EVENT_O)     ||  \
                                                 ((PARAM) == IRS_LOGIC_EVENT_AO))

typedef enum
{
    IRS_LOGIC_SEND_CMD_OFF = 0,         /* 逻辑发送 - 不发送额外控制命令 */
    IRS_LOGIC_SEND_CMD_ON,              /* 逻辑发送 - 发送额外控制命令   */
}IrsLogicSendCmdTypedef;
#define IS_IRS_LOGIC_SEND_CMD_TYPE(PARAM)       (((PARAM) == IRS_LOGIC_SEND_CMD_OFF)    ||  \
                                                 ((PARAM) == IRS_LOGIC_SEND_CMD_ON))

typedef enum
{
    IRS_MMY_TYPE_CATCH = 0,             /* 存储类型 - 红外捕获的参数 */
    IRS_MMY_TYPE_AE,                    /* 存储类型 - AE事件下的运行参数 */
    IRS_MMY_TYPE_E,                     /* 存储类型 - E事件下的运行参数 */
    IRS_MMY_TYPE_MO,                    /* 存储类型 - MO事件下的运行参数 */
    IRS_MMY_TYPE_ME,                    /* 存储类型 - ME事件下的运行参数 */
    IRS_MMY_TYPE_O,                     /* 存储类型 - O事件下的运行参数 */
    IRS_MMY_TYPE_AO,                    /* 存储类型 - AO事件下的运行参数 */
    ISR_MMY_TYPE_FORMULA_START,         /* 存储类型 - 复合公式起始 - 运行参数 */

    IRS_MMY_TYPE_MAX = (ISR_MMY_TYPE_FORMULA_START + MCU_IRS_FORMULA_NUM),  /* 存储类型 - 最大值 */
}IrsMmyType;
#define IS_IRS_MMY_TYPE(PARAM)                 ((((PARAM) == (IRS_MMY_TYPE_CATCH))       ||  \
                                                 ((PARAM) == (IRS_MMY_TYPE_AE))          ||  \
                                                 ((PARAM) == (IRS_MMY_TYPE_E))           ||  \
                                                 ((PARAM) == (IRS_MMY_TYPE_MO))          ||  \
                                                 ((PARAM) == (IRS_MMY_TYPE_ME))          ||  \
                                                 ((PARAM) == (IRS_MMY_TYPE_O))           ||  \
                                                 ((PARAM) == (IRS_MMY_TYPE_AO)))         ||  \
                                                (((PARAM) >= ISR_MMY_TYPE_FORMULA_START) &&  \
                                                 ((PARAM) <  (IRS_MMY_TYPE_MAX))))

#define IS_IRS_MMY_FORMULA_TYPE(PARAM)         (((PARAM) >= ISR_MMY_TYPE_FORMULA_START) &&  \
                                                ((PARAM) <  IRS_MMY_TYPE_MAX))

typedef struct
{
    u32         IrsMmyStartAddr;        /* Flash起始地址 */
    MmyDataType MmyDatType;             /* 存储的数据类型 */
    u8          IrsMmyOptParaLen;       /* 操作数据长度 */
    u8          IrsMmyOptParaSize;      /* 操作数据尺寸 */
    uc8         *DefaultRamAddr;        /* 默认数据的操作地址 */
}IrsOptTypeMmyTypedef;

typedef struct
{
    IrsOptTypeMmyTypedef OptMmyPara[IRS_MMY_TYPE_MAX];
}IrsOptMmyTypedef;

typedef struct
{
    IrsLogicStatTypedef     DesStat;        /* 需要执行到的目标状态 */
    IrsLogicSendCmdTypedef  SendFtn;        /* 是否需要执行发送功能 */
    RunAddrTypedef          SendDesAddr;    /* 发送的目的地址 */
    u8                      SendCmdL1;      /* 发送的命令中的L1 */
    u8                      SendCmdT1;      /* 发送的命令中的T1 */
}IrsLogicEventTypedef;

typedef enum
{
    IRS_NOW_EVENT_CLEAR = 0,                /* 红外所处的事件 - 明确化 */
    IRS_NOW_EVENT_O,                        /* 红外所处的事件 - 检测到人 */
    IRS_NOW_EVENT_E,                        /* 红外所处的事件 - 检查不到人 */
    IRS_NOW_EVENT_ANY,                      /* 红外所处的事件 - 任何事件都可以 */
}IrsNowEventType;
#define IS_IRS_NOW_EVENT_TYPE(PARAM)        (((PARAM) == IRS_NOW_EVENT_CLEAR)   || \
                                             ((PARAM) == IRS_NOW_EVENT_O)       || \
                                             ((PARAM) == IRS_NOW_EVENT_E)       || \
                                             ((PARAM) == IRS_NOW_EVENT_ANY))

typedef enum
{
    IRS_NOW_ADDR_GROUP_CLEAR = 0,           /* GROUP的地址 - 明确化 */
    IRS_NOW_ADDR_GROUP_ANY,                 /* GROUP的地址 - 任何值 */
}IrsNowAddrGroupTypedef;
#define IS_IRS_NOW_ADDR_GROUP_TYPE(PARAM)   (((PARAM) == IRS_NOW_ADDR_GROUP_CLEAR)  ||  \
                                             ((PARAM) == IRS_NOW_ADDR_GROUP_ANY))

typedef enum
{
    IRS_NOW_ADDR_LEVEL_CLEAR = 0,
    IRS_NOW_ADDR_LEVEL_ANY,
}IrsNowAddrLevelTypedef;
#define IS_IRS_NOW_ADDR_LEVEL_TYPE(PARAM)   (((PARAM) == IRS_NOW_ADDR_LEVEL_CLEAR)  ||  \
                                             ((PARAM) == IRS_NOW_ADDR_LEVEL_ANY))

typedef struct
{
    FlagStatus      IsActive;                   /* 是否激活 */
    RunAddrTypedef  CmdAddr;                    /* 命令的AG地址 */
    u8              CmdLevel1;                  /* 命令的状态值1 */
    FlagStatus      IsL2Active;                 /* 是否激活 */
    u8              CmdLevel2;                  /* 命令的状态值2 */
} IrMatchCmdTypedef;

typedef struct
{
    FlagStatus              IsActive;           /* 是否激活 */
    IrsNowEventType         NowEventType;       /* 现在所处的事件类型 */
    IrsNowAddrGroupTypedef  NowGroupType;       /* 现在所处的Group的类型 */
    IrsNowAddrLevelTypedef  NowLevelType;       /* 现在所处的Level值的类型 */

    IrsLogicStatTypedef     IrMatchStat;        /* 红外的匹配状态 */
    RunAddrTypedef          IrMatchAddr;        /* 红外的匹配AG地址 */
    u8                      IrMatchLevel;       /* 红外的匹配Level值 */

    FlagStatus              IsIrExeStatReset;   /* 红外的状态重设标志 */
    RunAddrTypedef          IrExecutionAddr;    /* 红外的执行地址 */
    IrsLogicStatTypedef     IrExecutionStat;    /* 红外的执行状态 */
} IrsFormulaTriggerTypedef;

#define     IRS_FORMULA_EXECUTION_CMD_SIZE      CNL_CMD_SHORT_BYTE_LEN
typedef struct
{
    FlagStatus              IsActive;           /* 是否被激活 */

    u8                      CmdLen;             /* 执行命令的长度 */
    RunAddrTypedef          ExecutionAddr;      /* 执行命令的地址 */

    // 执行命令的Buff缓存
    u8                      CmdBuff[IRS_FORMULA_EXECUTION_CMD_SIZE];
} IrsFormulaExecutionTypedef;

typedef struct
{
    IrsLogicStatTypedef         LogicStat;      /* 逻辑状态         */
    RunAddrTypedef              IrsAddr;        /* 红外的地址       */
    IrsRunModeTypedef           IrsRunMode;     /* 红外的运行模式   */
    IrsLogicStatTypedef         CmdSetStat;     /* 命令设置的状态 */

    // 红外的逻辑时间执行参数
    IrsLogicEventTypedef        LogicEvent[IRS_LOGIC_EVENT_MAX][IRS_CATCH_EVENT_MAX];

    // 红外的复合公式触发参数
    IrsFormulaTriggerTypedef    FormulaTirggerAttr[MCU_IRS_FORMULA_NUM];
    // 红外的匹配命令
    IrMatchCmdTypedef           IrMatchCmd;
}IrsLogicRunAttr;

typedef struct
{
    u8  CfgIrId;                            /* 红外ID号 */
    u8  CfgPara;                            /* 配置参数 */
}IrsCfgRegTypedef;

// 一个事件的执行参数 字节长度 6
#define     IRS_CATCH_EVENT_OPT_PARA_LEN    6


/* IRS 存储参数 */
// IRS BLOCK SIZE - 占用2048Byte
#define     IRS_MMY_BLOCK_SIZE          0x800                           /* 一个红外模块占用的空间 */


// IRS CATCH 存储参数
#define     IRS_CATCH_MMY_ADDR_START    MCU_IRS_ADDR                    /* 存储的起始地址 */
#define     IRS_CATCH_MMY_PARA_LEN      18                              /* 占用18Bytes配置字节 */
#define     IRS_CATCH_MMY_DATA_TYPE     MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_CATCH_MMY_CFG_MSG_SIZE  MCU_MMY_CFG_MSG_BIG_SIZE        /* 存储最大字节数为64Byte */

// 场景参数长度
#define     IRS_LOGIC_EVENT_PARA_LEN    30        
// IRS AE 场景下的 - 存储参数
// AE场景下的起始地址
#define     IRS_AE_MMY_ADDR_START       (IRS_CATCH_MMY_ADDR_START + IRS_CATCH_MMY_CFG_MSG_SIZE)
#define     IRS_AE_MMY_PARA_LEN         IRS_LOGIC_EVENT_PARA_LEN        /* 占用30Byte */
#define     IRS_AE_MMY_DATA_TYPE        MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_AE_MMY_CFG_MSG_SIZE     MCU_MMY_CFG_MSG_BIG_SIZE        /* 存储最大字节数为64Byte */

// IRS E  场景下的 - 存储参数
// AE场景下的起始地址
#define     IRS_E_MMY_ADDR_START        (IRS_AE_MMY_ADDR_START + IRS_AE_MMY_CFG_MSG_SIZE)
#define     IRS_E_MMY_PARA_LEN          IRS_LOGIC_EVENT_PARA_LEN        /* 占用30Byte */
#define     IRS_E_MMY_DATA_TYPE         MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_E_MMY_CFG_MSG_SIZE      MCU_MMY_CFG_MSG_BIG_SIZE        /* 存储最大字节数为64Byte */

// IRS MO 场景下的 - 存储参数
// AE场景下的起始地址
#define     IRS_MO_MMY_ADDR_START       (IRS_E_MMY_ADDR_START + IRS_E_MMY_CFG_MSG_SIZE)
#define     IRS_MO_MMY_PARA_LEN         IRS_LOGIC_EVENT_PARA_LEN        /* 占用30Byte */
#define     IRS_MO_MMY_DATA_TYPE        MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_MO_MMY_CFG_MSG_SIZE     MCU_MMY_CFG_MSG_BIG_SIZE        /* 存储最大字节数为64Byte */

// IRS ME 场景下的 - 存储参数
// AE场景下的起始地址
#define     IRS_ME_MMY_ADDR_START       (IRS_MO_MMY_ADDR_START + IRS_MO_MMY_CFG_MSG_SIZE)
#define     IRS_ME_MMY_PARA_LEN         IRS_LOGIC_EVENT_PARA_LEN        /* 占用30Byte */
#define     IRS_ME_MMY_DATA_TYPE        MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_ME_MMY_CFG_MSG_SIZE     MCU_MMY_CFG_MSG_BIG_SIZE        /* 存储最大字节数为64Byte */

// IRS O  场景下的 - 存储参数
// AE场景下的起始地址
#define     IRS_O_MMY_ADDR_START        (IRS_ME_MMY_ADDR_START + IRS_ME_MMY_CFG_MSG_SIZE)
#define     IRS_O_MMY_PARA_LEN          IRS_LOGIC_EVENT_PARA_LEN        /* 占用30Byte */
#define     IRS_O_MMY_DATA_TYPE         MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_O_MMY_CFG_MSG_SIZE      MCU_MMY_CFG_MSG_BIG_SIZE        /* 存储最大字节数为64Byte */

// IRS AO 场景下的 - 存储参数
// AE场景下的起始地址
#define     IRS_AO_MMY_ADDR_START       (IRS_O_MMY_ADDR_START + IRS_O_MMY_CFG_MSG_SIZE)
#define     IRS_AO_MMY_PARA_LEN         IRS_LOGIC_EVENT_PARA_LEN        /* 占用30Byte */
#define     IRS_AO_MMY_DATA_TYPE        MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_AO_MMY_CFG_MSG_SIZE     MCU_MMY_CFG_MSG_BIG_SIZE        /* 存储最大字节数为64Byte */

// IRS 复合场景 - 存储参数 - 起始
#define     IRS_FORMULA_MMY_ADDR_START  (MCU_IRS_ADDR + 0X200)          /* 存储的起始地址 */
#define     IRS_FORMULA_MMY_PARA_LEN    38                              /* 占用30Byte */
#define     IRS_FORMULA_MMY_DATA_TYPE   MMY_DATA_TYPE_BIG               /* 存储数据类型为大等类型 */
#define     IRS_FORMULA_MMY_CFG_MSG_SIZE     MCU_MMY_CFG_MSG_BIG_SIZE   /* 存储最大字节数为64Byte */

#define     IRS_FORMULA_MMY_EXECUTION_ADDR_NUM      6                   /* 红外复合公式的执行地址个数 6个 */
#define     IRS_FORMULA_MMY_EXECUTION_PARA_LEN      5                   /* 红外复合公式的执行参数长度 5Bytes */
#define     IRS_FORMULA_MMY_EXECUTION_START_INDEX   8                   /* 红外复合公式的执行起始下标 8 */


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  FlagStatus                  IrsMmy_IsId                                 (uc8 irsId);
extern  FlagStatus                  IrsMmy_IsMmyType                            (IrsMmyType mmyType);
extern  FlagStatus                  IrsMmy_IsCatchEvent                         (IrsCatchEvent chEvent);

extern  FlagStatus                  IrsMmy_IsMmyLogicEvent                      (u8 tEvent);
extern  FlagStatus                  IrsMmy_IsFormulaIndex                       (u8 tIndex);

extern  ErrorStatus                 IrsMmy_GetParaFromMMY                       (uc8 irsId, IrsMmyType mmyType, u8 gParaBuff[]);
extern  ErrorStatus                 IrsMmy_SetParaToMMY                         (uc8 irsId, IrsMmyType mmyType, uc8 sParaBuff[]);

extern  ErrorStatus                 IrsMmy_GetDefaultPara                       (uc8 irsId, IrsMmyType mmyType, u8 gParaBuff[]);
extern  ErrorStatus                 IrsMmy_SetDefaultToMMY                      (uc8 irsId, IrsMmyType mmyType);

extern  void                        IrsMmy_RunParaInit                          (void);

extern  ErrorStatus                 IrsMmy_GetCatchAttr                         (uc8 gParaBuff[], IrsCatchRunAttr *gRunAttr);
extern  ErrorStatus                 IrsMmy_GetCatchEventOptPara                 (uc8 gParaBuff[], IrsLogicEventTypedef *lgEvent);

extern  ErrorStatus                 IrsMmy_GetFormulaTriggerPara                (uc8 gParaBuff[], IrsFormulaTriggerTypedef *lgFtPara);
extern  ErrorStatus                 IrsMmy_GetFormulaExecutionPara              (uc8 gParaBuff[], IrsFormulaExecutionTypedef *lgFePara);

extern  FlagStatus                  IrsMmy_AddrIsLegal                          (RunAddrTypedef sAddr);
extern  ErrorStatus                 IrsMmy_WriteCatchDataToFlash                (uc8 irsId, uc8 sBuff[], u8 sLen);

extern  FlagStatus                  IrsMmy_IsIrsWriteEvent                      (uc8 sEvent);
extern  ErrorStatus                 IrsMmy_WriteLogicDataToFlash                (uc8 irsId, u8 lgEvent, uc8 sBuff[], u8 sLen);

extern  ErrorStatus                 IrsMmy_WriteFormulaDataToFlash              (uc8 irsId, u8 fmlIndex, uc8 sBuff[], u8 sLen);
    
#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __IRS_MMY_H__ */
