/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Irs_MMY.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年11月4日
  最近修改   :
  功能描述   : 红外传感器的存储操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年11月4日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/
static  ErrorStatus     IrsMmy_CatchOrganizeInit                (uc8 irsId);
static  ErrorStatus     IrsMmy_AeOrganizeInit                   (uc8 irsId);
static  ErrorStatus     IrsMmy_EOrganizeInit                    (uc8 irsId);
static  ErrorStatus     IrsMmy_MoOrganizeInit                   (uc8 irsId);
static  ErrorStatus     IrsMmy_MeOrganizeInit                   (uc8 irsId);
static  ErrorStatus     IrsMmy_OOrganizeInit                    (uc8 irsId);
static  ErrorStatus     IrsMmy_AOOrganizeInit                   (uc8 irsId);
static  ErrorStatus     IrsMmy_FormulaOrganizeInit              (uc8 irsId, uc8 formulaNum);
static  void            IrsMmy_OrganizeInit                     (void);

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

// 红外存储操作参数
static IrsOptMmyTypedef IrsOptMmyPara[IRS_CFG_USER_NUM];

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

// 红外捕获 默认参数
static uc8 IrsCatchMmyParaDefault[IRS_CATCH_MMY_PARA_LEN] __attribute__((at(MCU_IRS_CATCH_DEFAULT_MMY_ADDR))) =
{
    2,                  /* MODE:INTERLOCK */
    0x38, 0x01,          /* APP:202 GROUP:101 */
    0x3F, 0x80,         /* APP:63 GROUP:128 */
    0x05,               /* IRS_ResetWait:6Sec */
    0x00, 0x02,         /* FewNoneTime:2Sec */
    0x00, 0x02,         /* MultNoneTime:2Sec */
    0x00, 0x03,         /* FewHasTime:3Sec */
    0x03, 0xE8,         /* FewHasMs: 1000ms */
    0x00, 0x03,         /* MultHasTime:3Sec */
    0x04, 0xB0,         /* MultHasMs: 1200ms */
};

// 一直无人 默认参数 01
static uc8 IrsAEMmyParaDefault[IRS_AE_MMY_PARA_LEN] __attribute__((at(MCU_IRS_AE_DEFAULT_MMY_ADDR))) =
{
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00
};

// 无人 默认参数 02
static uc8 IrsEMmyParaDefault[IRS_E_MMY_PARA_LEN] __attribute__((at(MCU_IRS_E_DEFAULT_MMY_ADDR))) =
{
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00
};

// 可能有人 默认参数 03
static uc8 IrsMOMmyParaDefault[IRS_MO_MMY_PARA_LEN] __attribute__((at(MCU_IRS_MO_DEFAULT_MMY_ADDR))) =
{
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00
};

// 可能没人 默认参数 04
static uc8 IrsMEMmyParaDefault[IRS_ME_MMY_PARA_LEN] __attribute__((at(MCU_IRS_ME_DEFAULT_MMY_ADDR))) =
{
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00
};

// 有人 默认参数 05
static uc8 IrsOMmyParaDefault[IRS_O_MMY_PARA_LEN] __attribute__((at(MCU_IRS_O_DEFAULT_MMY_ADDR))) =
{
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00
};

// 一直有人 默认参数 06
static uc8 IrsAOMmyParaDefault[IRS_AO_MMY_PARA_LEN] __attribute__((at(MCU_IRS_AO_DEFAULT_MMY_ADDR))) =
{
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00,        \
    0x00, 0x02, 0xFF, 0xFF, 0x00, 0x00
};

// 复合公式 默认参数
static uc8 IrsFormulaMmyParaDefault[IRS_FORMULA_CFG_NUM][IRS_FORMULA_MMY_PARA_LEN] __attribute__((at(MCU_IRS_FORMULA_DEFAULT_MMY_ADDR))) =
{
    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,     \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },
};



/*****************************************************************************
 函 数 名  : IrsMmy_IsId
 功能描述  : 判断 传感器ID号是否合法
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
__INLINE FlagStatus IrsMmy_IsId(uc8 irsId)
{
    /* ID 判断范围为 0~IRS个数-1 */
    if(irsId < IRS_CFG_USER_NUM)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : IrsMmy_IsMmyType
 功能描述  : 判断存储类型是否合法
 输入参数  : IrsMmyType mmyType
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
__INLINE FlagStatus IrsMmy_IsMmyType(IrsMmyType mmyType)
{
    if(mmyType < IRS_MMY_TYPE_MAX)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : IrsMmy_IsCatchEvent
 功能描述  : 判断红外获取的事件类型是否合法
 输入参数  : IrsCatchEvent chEvent
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
__INLINE FlagStatus IrsMmy_IsCatchEvent(IrsCatchEvent chEvent)
{
    if(chEvent < IRS_CATCH_EVENT_MAX)
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : IrsMmy_IsMmyLogicEvent
 功能描述  : 判断红外的逻辑事件是否在范围内
 输入参数  : u8 tEvent  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月15日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  IrsMmy_IsMmyLogicEvent(u8 tEvent)
{
    if((tEvent >= IRS_MMY_TYPE_AE) &&
       (tEvent <= IRS_MMY_TYPE_AO))
    {
        return SET;
    }

    return RESET;       
}

/*****************************************************************************
 函 数 名  : IrsMmy_IsFormulaIndex
 功能描述  : 判断是否为复合公式下标
 输入参数  : u8 tIndex  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月15日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  IrsMmy_IsFormulaIndex(u8 tIndex)
{
    if((tIndex >= CFG_INPUT_REG_IR_SENSOR_FORMULA_CH_MIN) &&
       (tIndex <= CFG_INPUT_REG_IR_SENSOR_FORMULA_CH_MAX))
    {
        return SET;
    }

    return RESET;  
}



/*****************************************************************************
 函 数 名  : IrsMmy_CatchOrganizeInit
 功能描述  : 初始化 红外捕获架构
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_CatchOrganizeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_CATCH].IrsMmyStartAddr   = (IRS_CATCH_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_CATCH].MmyDatType        = IRS_CATCH_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_CATCH].IrsMmyOptParaLen  = IRS_CATCH_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_CATCH].IrsMmyOptParaSize = IRS_CATCH_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_CATCH].DefaultRamAddr    = IrsCatchMmyParaDefault;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_AeOrganizeInit
 功能描述  : 初始化AE架构
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_AeOrganizeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AE].IrsMmyStartAddr   = (IRS_AE_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AE].MmyDatType        = IRS_AE_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AE].IrsMmyOptParaLen  = IRS_AE_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AE].IrsMmyOptParaSize = IRS_AE_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AE].DefaultRamAddr    = IrsAEMmyParaDefault;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_EOrganizeInit
 功能描述  : 初始化E架构
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_EOrganizeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_E].IrsMmyStartAddr   = (IRS_E_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_E].MmyDatType        = IRS_E_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_E].IrsMmyOptParaLen  = IRS_E_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_E].IrsMmyOptParaSize = IRS_E_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_E].DefaultRamAddr    = IrsEMmyParaDefault;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_MoOrganizeInit
 功能描述  : 初始化MO架构
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_MoOrganizeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_MO].IrsMmyStartAddr   = (IRS_MO_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_MO].MmyDatType        = IRS_MO_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_MO].IrsMmyOptParaLen  = IRS_MO_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_MO].IrsMmyOptParaSize = IRS_MO_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_MO].DefaultRamAddr    = IrsMOMmyParaDefault;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_MeOrganizeInit
 功能描述  : 初始化ME架构
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_MeOrganizeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_ME].IrsMmyStartAddr   = (IRS_ME_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_ME].MmyDatType        = IRS_ME_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_ME].IrsMmyOptParaLen  = IRS_ME_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_ME].IrsMmyOptParaSize = IRS_ME_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_ME].DefaultRamAddr    = IrsMEMmyParaDefault;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_OOrganizeInit
 功能描述  : 初始化O架构
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_OOrganizeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_O].IrsMmyStartAddr   = (IRS_O_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_O].MmyDatType        = IRS_O_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_O].IrsMmyOptParaLen  = IRS_O_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_O].IrsMmyOptParaSize = IRS_O_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_O].DefaultRamAddr    = IrsOMmyParaDefault;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_AOOrganizeInit
 功能描述  : 初始化AO架构
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_AOOrganizeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AO].IrsMmyStartAddr   = (IRS_AO_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AO].MmyDatType        = IRS_AO_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AO].IrsMmyOptParaLen  = IRS_AO_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AO].IrsMmyOptParaSize = IRS_AO_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[IRS_MMY_TYPE_AO].DefaultRamAddr    = IrsAOMmyParaDefault;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_FormulaOrganizeInit
 功能描述  : 初始化Formula架构
 输入参数  : uc8 irsId
             uc8 formulaNum
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus IrsMmy_FormulaOrganizeInit(uc8 irsId, uc8 formulaNum)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_MMY_FORMULA_TYPE(formulaNum));

    // FLASH起始地址
    IrsOptMmyPara[irsId].OptMmyPara[formulaNum].IrsMmyStartAddr   = (IRS_FORMULA_MMY_ADDR_START + (irsId * IRS_MMY_BLOCK_SIZE) + ((formulaNum - ISR_MMY_TYPE_FORMULA_START) * IRS_FORMULA_MMY_CFG_MSG_SIZE));
    // 存储类型
    IrsOptMmyPara[irsId].OptMmyPara[formulaNum].MmyDatType        = IRS_FORMULA_MMY_DATA_TYPE;
    // 数据长度
    IrsOptMmyPara[irsId].OptMmyPara[formulaNum].IrsMmyOptParaLen  = IRS_FORMULA_MMY_PARA_LEN;
    // 存储大小
    IrsOptMmyPara[irsId].OptMmyPara[formulaNum].IrsMmyOptParaSize = IRS_FORMULA_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    IrsOptMmyPara[irsId].OptMmyPara[formulaNum].DefaultRamAddr    = IrsFormulaMmyParaDefault[formulaNum - ISR_MMY_TYPE_FORMULA_START];

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_OrganizeInit
 功能描述  : 初始化红外架构
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月21日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static void IrsMmy_OrganizeInit(void)
{
    u8 tmpIrsId;
    u8 tmpIrsFormulaId;

    // 轮询所有红外的 Id号
    for(tmpIrsId=0; tmpIrsId<IRS_CFG_USER_NUM; tmpIrsId++)
    {
        // 捕获架构
        IrsMmy_CatchOrganizeInit(tmpIrsId);
        // AE架构
        IrsMmy_AeOrganizeInit(tmpIrsId);
        // E架构
        IrsMmy_EOrganizeInit(tmpIrsId);
        // MO架构
        IrsMmy_MoOrganizeInit(tmpIrsId);
        // ME架构
        IrsMmy_MeOrganizeInit(tmpIrsId);
        // O架构
        IrsMmy_OOrganizeInit(tmpIrsId);
        // AO架构
        IrsMmy_AOOrganizeInit(tmpIrsId);

        // 轮询所有的Formula架构
        for(tmpIrsFormulaId=0; tmpIrsFormulaId<IRS_FORMULA_CFG_NUM; tmpIrsFormulaId++)
        {
            // Formula架构
            IrsMmy_FormulaOrganizeInit(tmpIrsId, (ISR_MMY_TYPE_FORMULA_START+tmpIrsFormulaId));
        }
    }
}


/*****************************************************************************
 函 数 名  : IrsMmy_GetParaFromMMY
 功能描述  : 红外传感器存储逻辑 - 从FLASH中获取参数
 输入参数  : uc8 irsId
             IrsMmyType mmyType
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_GetParaFromMMY(uc8 irsId, IrsMmyType mmyType, u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr;
    u8          gParaLen;
    u8          i;

    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));


    // 获取 存储类型
    gMmyDat.Type = IrsOptMmyPara[irsId].OptMmyPara[mmyType].MmyDatType;

    // 获取起始地址
    gMmyAddr = IrsOptMmyPara[irsId].OptMmyPara[mmyType].IrsMmyStartAddr;

    // 获取FLASH数据
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    // 获取数据长度
    gParaLen = IrsOptMmyPara[irsId].OptMmyPara[mmyType].IrsMmyOptParaLen;

    // 复制数据
    for(i = 0; i<gParaLen; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_SetParaToMMY
 功能描述  : 红外传感器存储逻辑 - 设置参数到FLASH中
 输入参数  : uc8 irsId
             IrsMmyType mmyType
             uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_SetParaToMMY(uc8 irsId, IrsMmyType mmyType, uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr;
    u8          sParaLen;
    u8          sParaSize;
    u8          i;

    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));


    // 获取 存储类型
    sMmyDat.Type = IrsOptMmyPara[irsId].OptMmyPara[mmyType].MmyDatType;

    // 获取起始地址
    sMmyAddr = IrsOptMmyPara[irsId].OptMmyPara[mmyType].IrsMmyStartAddr;


    // 获取数据容量大小
    sParaSize = IrsOptMmyPara[irsId].OptMmyPara[mmyType].IrsMmyOptParaSize;

    // 获取写入的数据长度
    sParaLen = IrsOptMmyPara[irsId].OptMmyPara[mmyType].IrsMmyOptParaLen;

    // 载入数据
    for(i = 0; i<sParaSize; i++)
    {
        if(i < sParaLen)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    // 将数据载入到FLASH内
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_GetDefaultPara
 功能描述  : 红外传感器存储逻辑 - 获取默认值
 输入参数  : uc8 irsId
             IrsMmyType mmyType
             u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_GetDefaultPara(uc8 irsId, IrsMmyType mmyType, u8 gParaBuff[])
{
    u8 i;
    u8 gLen;

    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));

    // 获取数据长度
    gLen = IrsOptMmyPara[irsId].OptMmyPara[mmyType].IrsMmyOptParaLen;

    // 载入数据
    for(i = 0; i<gLen; i++)
    {
        gParaBuff[i] = IrsOptMmyPara[irsId].OptMmyPara[mmyType].DefaultRamAddr[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_SetDefaultToMMY
 功能描述  : 设置默认参数到FLASH中
 输入参数  : uc8 irsId
             IrsMmyType mmyType
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_SetDefaultToMMY(uc8 irsId, IrsMmyType mmyType)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_MMY_TYPE(mmyType));

    // 设置默认数据到RAM中
    return IrsMmy_SetParaToMMY(irsId, mmyType, IrsOptMmyPara[irsId].OptMmyPara[mmyType].DefaultRamAddr);
}

/*****************************************************************************
 函 数 名  : IrsMmy_RunParaInit
 功能描述  : 红外传感器存储逻辑 - 初始化运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void    IrsMmy_RunParaInit(void)
{
    IrsMmy_OrganizeInit();
}

/*****************************************************************************
 函 数 名  : IrsMmy_GetCatchAttr
 功能描述  : 红外传感器存储逻辑 - 获取红外捕获参数
 输入参数  : uc8 irsId
             IrsCatchRunAttr *gRunAttr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_GetCatchAttr(uc8 gParaBuff[], IrsCatchRunAttr *gRunAttr)
{
    u16 tmpT2;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gRunAttr));

    // 判断红外的运行模式
    if(gParaBuff[0] == 0)
    {
        gRunAttr->IrsRunMode    = IRS_MODE_IDLE;
    }
    else if(gParaBuff[0] == 1)
    {
        gRunAttr->IrsRunMode    = IRS_MODE_INTERLOCK;
    }
    else if(gParaBuff[0] == 2)
    {
        gRunAttr->IrsRunMode    = IRS_MODE_NORMAL;
    }
    else
    {
        gRunAttr->IrsRunMode    = IRS_MODE_IDLE;
    }

    // 载入地址
    gRunAttr->CatchIrAddr.App   = gParaBuff[1];
    gRunAttr->CatchIrAddr.Group = gParaBuff[2];

    // 载入 触发开关的AG地址
    gRunAttr->TriIrAddr.App = gParaBuff[3];
    gRunAttr->TriIrAddr.Group = gParaBuff[4];
    
    // 载入初始化等待时间
    gRunAttr->ResetWaitSec = gParaBuff[5]; 

    // 获取一个时间段没人的时间段
    tmpT2 = gParaBuff[6];
    tmpT2 <<= 8;
    tmpT2 +=  gParaBuff[7];
    TmmExp_GetExpPara(tmpT2, &(gRunAttr->NoneFewTime));

    // 获取多个时间段没人的时间段
    tmpT2 = gParaBuff[8];
    tmpT2 <<= 8;
    tmpT2 +=  gParaBuff[9];
    TmmExp_GetExpPara(tmpT2, &(gRunAttr->NoneMultTime));


    // 获取一个时间段有人的时间段
    tmpT2 = gParaBuff[10];
    tmpT2 <<= 8;
    tmpT2 +=  gParaBuff[11];
    TmmExp_GetExpPara(tmpT2, &(gRunAttr->HasFewTime));

    // 获取一个时间段有人的ms数
    tmpT2 = gParaBuff[12];
    tmpT2 <<= 8;
    tmpT2 +=  gParaBuff[13];
    gRunAttr->HasFewTimeMs = tmpT2;

    // 获取多个时间段有人的时间段
    tmpT2 = gParaBuff[14];
    tmpT2 <<= 8;
    tmpT2 +=  gParaBuff[15];
    TmmExp_GetExpPara(tmpT2, &(gRunAttr->HasMultTime));

    // 获取一个时间段有人的ms数
    tmpT2 = gParaBuff[16];
    tmpT2 <<= 8;
    tmpT2 +=  gParaBuff[17];
    gRunAttr->HasMultTimeMs = tmpT2;


    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_GetCatchEventOptPara
 功能描述  : 红外传感器存储逻辑 - 获取事件操作参数
 输入参数  : uc8 irsId
             IrsMmyType mmyType
             IrsCatchEvent catchEvent
             IrsLogicEventTypedef *lgEvent
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_GetCatchEventOptPara(uc8 gParaBuff[], IrsLogicEventTypedef *lgEvent)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(lgEvent));

    // 获取事件的下一个逻辑状态
    if(gParaBuff[0] == 0)
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_IDEL;
    }
    else if(gParaBuff[0] == 1)
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_AE;
    }
    else if(gParaBuff[0] == 2)
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_E;
    }
    else if(gParaBuff[0] == 3)
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_MO;
    }
    else if(gParaBuff[0] == 4)
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_ME;
    }
    else if(gParaBuff[0] == 5)
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_O;
    }
    else if(gParaBuff[0] == 6)
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_AO;
    }
    else
    {
        lgEvent->DesStat = IRS_LOGIC_STAT_IDEL;
    }

    // 判断是否发送命令 - 执行命令是否激活
    if((gParaBuff[1] & 0x80) == 0x80)
    {
        lgEvent->SendFtn = IRS_LOGIC_SEND_CMD_ON;
    }
    else
    {
        lgEvent->SendFtn = IRS_LOGIC_SEND_CMD_OFF;
    }


    // 获取APP + Group
    lgEvent->SendDesAddr.App   = gParaBuff[2];
    lgEvent->SendDesAddr.Group = gParaBuff[3];

    // 获取命令的L1
    lgEvent->SendCmdL1 = gParaBuff[4];
    // 获取命令的T1
    lgEvent->SendCmdT1 = gParaBuff[5];


    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_GetFormulaTriggerPara
 功能描述  : 获取红外复合公式的触发参数
 输入参数  : uc8 gParaBuff[]
             IrsFormulaTriggerTypedef *lgFtPara
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_GetFormulaTriggerPara(uc8 gParaBuff[], IrsFormulaTriggerTypedef *lgFtPara)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(lgFtPara));

    // 获取公式是否激活
    if((gParaBuff[0] & 0x80) == 0x80)
    {
        lgFtPara->IsActive = SET;
    }
    else
    {
        lgFtPara->IsActive = RESET;
    }

    // 获取红外状态是否重设标志
    if((gParaBuff[0] & 0x40) == 0x40)
    {
        lgFtPara->IsIrExeStatReset = SET;
    }
    else
    {
        lgFtPara->IsIrExeStatReset = RESET;
    }

    // 获取当前事件的触发类型
    if((gParaBuff[0] & 0x0C) == 0x00)
    {
        lgFtPara->NowEventType = IRS_NOW_EVENT_CLEAR;
    }
    else if((gParaBuff[0] & 0x0C) == 0x04)
    {
        lgFtPara->NowEventType = IRS_NOW_EVENT_O;
    }
    else if((gParaBuff[0] & 0x0C) == 0x08)
    {
        lgFtPara->NowEventType = IRS_NOW_EVENT_E;
    }
    else if((gParaBuff[0] & 0x0C) == 0x0C)
    {
        lgFtPara->NowEventType = IRS_NOW_EVENT_ANY;
    }
    else
    {
        return ERROR;
    }

    // 获取当前触发地址Group的触发类型
    if((gParaBuff[0] & 0x02) == 0x00)
    {
        lgFtPara->NowGroupType = IRS_NOW_ADDR_GROUP_CLEAR;
    }
    else
    {
        lgFtPara->NowGroupType = IRS_NOW_ADDR_GROUP_ANY;
    }

    // 获取当前触发Level的触发类型
    if((gParaBuff[0] & 0x01) == 0x00)
    {
        lgFtPara->NowLevelType = IRS_NOW_ADDR_LEVEL_CLEAR;
    }
    else
    {
        lgFtPara->NowLevelType = IRS_NOW_ADDR_LEVEL_ANY;
    }

    // 获取当前触发事件
    if(gParaBuff[1] == 0)
    {
        lgFtPara->IrMatchStat = IRS_LOGIC_STAT_IDEL;
    }
    else if(gParaBuff[1] == 1)
    {
        lgFtPara->IrMatchStat = IRS_LOGIC_STAT_AE;
    }
    else if(gParaBuff[1] == 2)
    {
        lgFtPara->IrMatchStat = IRS_LOGIC_STAT_E;
    }
    else if(gParaBuff[1] == 3)
    {
        lgFtPara->IrMatchStat = IRS_LOGIC_STAT_MO;
    }
    else if(gParaBuff[1] == 4)
    {
        lgFtPara->IrMatchStat = IRS_LOGIC_STAT_ME;
    }
    else if(gParaBuff[1] == 5)
    {
        lgFtPara->IrMatchStat = IRS_LOGIC_STAT_O;
    }
    else if(gParaBuff[1] == 6)
    {
        lgFtPara->IrMatchStat = IRS_LOGIC_STAT_AO;
    }
    else
    {
        return ERROR;
    }

    // 获取当前触发的AG地址
    lgFtPara->IrMatchAddr.App   = gParaBuff[2];
    lgFtPara->IrMatchAddr.Group = gParaBuff[3];

    // 获取当前触发Level值
    lgFtPara->IrMatchLevel      = gParaBuff[4];


    // 获取红外的执行地址
    lgFtPara->IrExecutionAddr.App   = gParaBuff[5];
    lgFtPara->IrExecutionAddr.Group = gParaBuff[6];

    // 获取红外的执行状态
    if(gParaBuff[7] == 0)
    {
        lgFtPara->IrExecutionStat = IRS_LOGIC_STAT_IDEL;
    }
    else if(gParaBuff[7] == 1)
    {
        lgFtPara->IrExecutionStat = IRS_LOGIC_STAT_AE;
    }
    else if(gParaBuff[7] == 2)
    {
        lgFtPara->IrExecutionStat = IRS_LOGIC_STAT_E;
    }
    else if(gParaBuff[7] == 3)
    {
        lgFtPara->IrExecutionStat = IRS_LOGIC_STAT_MO;
    }
    else if(gParaBuff[7] == 4)
    {
        lgFtPara->IrExecutionStat = IRS_LOGIC_STAT_ME;
    }
    else if(gParaBuff[7] == 5)
    {
        lgFtPara->IrExecutionStat = IRS_LOGIC_STAT_O;
    }
    else if(gParaBuff[7] == 6)
    {
        lgFtPara->IrExecutionStat = IRS_LOGIC_STAT_AO;
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_GetFormulaExecutionPara
 功能描述  : 获取复合公式的执行参数
 输入参数  : uc8 gParaBuff[]
             IrsFormulaExecutionTypedef *lgFePara
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_GetFormulaExecutionPara(uc8 gParaBuff[], IrsFormulaExecutionTypedef *lgFePara)
{
    u8 i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(lgFePara));

    // 获取公式是否激活
    if((gParaBuff[0] & 0x80) == 0x80)
    {
        lgFePara->IsActive = SET;
    }
    else
    {
        lgFePara->IsActive = RESET;
    }

    // 判断命令长度是否匹配
    if((gParaBuff[0] & 0x07) == IRS_FORMULA_EXECUTION_CMD_SIZE)
    {
        lgFePara->CmdLen = IRS_FORMULA_EXECUTION_CMD_SIZE;
    }
    else
    {
        return ERROR;
    }

    // 获取复合公式的执行AG地址
    lgFePara->ExecutionAddr.App     = gParaBuff[1];
    lgFePara->ExecutionAddr.Group   = gParaBuff[2];

    // 获取复合公式的执行命令内容
    for(i=0; i<IRS_FORMULA_EXECUTION_CMD_SIZE; i++)
    {
        lgFePara->CmdBuff[i] = gParaBuff[3+i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsMmy_AddrIsLegal
 功能描述  : 判断AG地址是否合法
 输入参数  : RunAddrTypedef sAddr  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus IrsMmy_AddrIsLegal(RunAddrTypedef sAddr)
{
    if((sAddr.App != 0) &&
       (sAddr.Group != 0))
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : IrsMmy_WriteCatchDataToFlash
 功能描述  : 写入捕获数据到FLASH中
 输入参数  : uc8 irsId    
             uc8 sBuff[]  
             u8 sLen      
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_WriteCatchDataToFlash(uc8 irsId, uc8 sBuff[], u8 sLen)
{
    u8 gIndex = 0;
    u16 tmpTmmValue = 0;
    TmmExp_ParaTypedef tmpTmmExp;
    RunAddrTypedef  newCatchIrAddr;
    RunAddrTypedef  oldCatchIrAddr;
    RunAddrTypedef  newTriIrAddr;
    RunAddrTypedef  oldTriIrAddr;
    FlagStatus oldCatchAddrIsActive = RESET;
    FlagStatus oldTriAddrIsActive = RESET;
    ErrorStatus reStat = SUCCESS;
    AddrAttrTypedef gAddr;
    IrsRunModeTypedef oldRunMode;
        
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sBuff));

    // 长度字节是否 大于等于 内容字节+1 且 内容字节等于捕获内容长度
    if(((sLen - 1) >= sBuff[0])     &&
       ((sBuff[0]) == IRS_CATCH_MMY_PARA_LEN))
    {
        // 越过数据长度字节
        gIndex++;

        // 判断模式是否符合规范
        if(IS_IRS_MODE(sBuff[gIndex]) != SET)
        {
            return ERROR;
        }
        // 越过模式字节
        gIndex++;

        // 获取红外捕获的AG地址
        newCatchIrAddr.App = sBuff[gIndex++];
        newCatchIrAddr.Group = sBuff[gIndex++];
        // 检查是否合法
        if(IrsMmy_AddrIsLegal(newCatchIrAddr) != SET)
        {
            return ERROR;
        }

        // 获取红外触发额外命令的AG地址
        newTriIrAddr.App = sBuff[gIndex++];
        newTriIrAddr.Group = sBuff[gIndex++];
        // 检查是否合法
        if(IrsMmy_AddrIsLegal(newTriIrAddr) != SET)
        {
            return ERROR;
        }

        // 越过初始化等待时间
        gIndex++;

        // 获取时间 - 一段时间无人配置参数
        tmpTmmValue = sBuff[gIndex++];
        tmpTmmValue <<= 8;
        tmpTmmValue += sBuff[gIndex++];
        // 判断时间是否合法
        if(TmmExp_GetExpPara(tmpTmmValue, &tmpTmmExp) != SUCCESS)
        {
            return ERROR;
        }

        // 获取时间 - 长时间无人配置参数
        tmpTmmValue = sBuff[gIndex++];
        tmpTmmValue <<= 8;
        tmpTmmValue += sBuff[gIndex++];
        // 判断时间是否合法
        if(TmmExp_GetExpPara(tmpTmmValue, &tmpTmmExp) != SUCCESS)
        {
            return ERROR;
        }

        // 获取时间 - 一段时间有人配置参数
        tmpTmmValue = sBuff[gIndex++];
        tmpTmmValue <<= 8;
        tmpTmmValue += sBuff[gIndex++];
        // 判断时间是否合法
        if(TmmExp_GetExpPara(tmpTmmValue, &tmpTmmExp) != SUCCESS)
        {
            return ERROR;
        }

        // 越过一段时间内，红外探头检测到的毫秒数
        gIndex += 2;

        // 获取时间 - 长时间有人配置参数
        tmpTmmValue = sBuff[gIndex++];
        tmpTmmValue <<= 8;
        tmpTmmValue += sBuff[gIndex++];
        // 判断时间是否合法
        if(TmmExp_GetExpPara(tmpTmmValue, &tmpTmmExp) != SUCCESS)
        {
            return ERROR;
        }

        // 写入数据到RAM中
        if(IrsMmy_SetParaToMMY(irsId, IRS_MMY_TYPE_CATCH, &(sBuff[1])) == SUCCESS)
        {
            // 判断红外运行模式是否不处于空闲模式
            if(IrsCatchAttr[irsId].IrsRunMode != IRS_MODE_IDLE)
            {
                // 设置旧的捕获地址是激活的
                oldCatchAddrIsActive = SET;

                // 保存旧的捕获地址
                oldCatchIrAddr.App = IrsCatchAttr[irsId].CatchIrAddr.App;
                oldCatchIrAddr.Group = IrsCatchAttr[irsId].CatchIrAddr.Group;
            }

            // 判断红外的工作模式是否为联动模式
            if(IrsCatchAttr[irsId].IrsRunMode == IRS_MODE_INTERLOCK)
            {
                // 设置旧的触发地址是激活的
                oldTriAddrIsActive = SET;

                // 保存旧地址
                oldTriIrAddr.App = IrsCatchAttr[irsId].TriIrAddr.App;
                oldTriIrAddr.Group = IrsCatchAttr[irsId].TriIrAddr.Group;
            }

            // 获取旧的运行模式
            oldRunMode = IrsLogicAttr[irsId].IrsRunMode;
            
            // 重新获取红外的捕获数据
            if(IrsMmy_GetCatchAttr(&(sBuff[1]), &IrsCatchAttr[irsId]) == SUCCESS)
            {
                // 重新获取运行模式
                Irs_GetRunMode(irsId, &(IrsLogicAttr[irsId].IrsRunMode));

                // 重新红外的AG地址
                Irs_GetAddr(irsId, &(IrsLogicAttr[irsId].IrsAddr));

                if(IrsLogicAttr[irsId].IrsRunMode == IRS_MODE_INTERLOCK)
                {
                    // 如果之前的状态不是联动状态的话 - 就需要初始化逻辑状态为一直无人状态
                    if(oldRunMode != IRS_MODE_INTERLOCK)
                    {
                        // 逻辑状态 - 一直无人状态
                        IrsLogicAttr[irsId].LogicStat = IRS_LOGIC_INIT_STAT;
                    }
                }
                else
                {
                    // 逻辑状态 - 空闲状态
                    IrsLogicAttr[irsId].LogicStat = IRS_LOGIC_STAT_IDEL;
                }
                
                // 载入重新反馈状态的地址类型为AG地址
                gAddr.Type = CMD_ADDR_TYPE_APP;
                
                // 判断红外运行模式是否不处于空闲模式
                if(IrsCatchAttr[irsId].IrsRunMode != IRS_MODE_IDLE)
                {
                    // 修改新旧的AG地址
                    if(oldCatchAddrIsActive == SET)
                    {
                        // 修改新/旧的AG地址
                        if(VirtualAddr_ReviseAddr(oldCatchIrAddr, IrsCatchAttr[irsId].CatchIrAddr) != SUCCESS)
                        {
                            reStat = ERROR;
                        }
                    }
                    else
                    {
                        // 加入新的AG地址
                        if(VirtualAddr_Join(IrsCatchAttr[irsId].CatchIrAddr) != SUCCESS)
                        {
                            reStat = ERROR;
                        }
                        else
                        {
                            // 载入上报状态的AG地址
                            gAddr.AddrBuff[0] = IrsCatchAttr[irsId].CatchIrAddr.App;
                            gAddr.AddrBuff[1] = IrsCatchAttr[irsId].CatchIrAddr.Group;
                            StatLogic_ResponseOneAgStat(gAddr);
                        }
                    }
                }
                else
                {
                    // 退出旧的红外捕获AG地址
                    if(oldCatchAddrIsActive == SET)
                    {
                        // 退出旧的AG地址
                        if(VirtualAddr_Abort(oldCatchIrAddr) != SUCCESS)
                        {
                            reStat = ERROR;
                        }   
                    }
                }

                // 判断红外的工作模式是否为联动模式
                if(IrsCatchAttr[irsId].IrsRunMode == IRS_MODE_INTERLOCK)
                {
                    // 修改新旧的AG地址
                    if(oldTriAddrIsActive == SET)
                    {
                        // 修改新/旧的AG地址
                        if(VirtualAddr_ReviseAddr(oldTriIrAddr, IrsCatchAttr[irsId].TriIrAddr) != SUCCESS)
                        {
                            reStat = ERROR;
                        }
                    }
                    else
                    {
                        // 加入新的AG地址
                        if(VirtualAddr_Join(IrsCatchAttr[irsId].TriIrAddr) != SUCCESS)
                        {
                            reStat = ERROR;
                        }                    
                        else
                        {
                            // 载入上报状态的AG地址
                            gAddr.AddrBuff[0] = IrsCatchAttr[irsId].TriIrAddr.App;
                            gAddr.AddrBuff[1] = IrsCatchAttr[irsId].TriIrAddr.Group;
                            StatLogic_ResponseOneAgStat(gAddr);
                        }
                    }
                }
                else
                {
                    // 退出旧的AG地址
                    if(oldTriAddrIsActive == SET)
                    {
                        // 退出旧的AG地址
                        if(VirtualAddr_Abort(oldTriIrAddr) != SUCCESS)
                        {
                            reStat = ERROR;
                        }
                    }
                }
            }
            else
            {
                reStat = ERROR;
            }
        }
        else
        {
            reStat = ERROR;
        }
    }
    else
    {
        reStat = ERROR;
    }
    
    return reStat;
}

FlagStatus  IrsMmy_IsIrsWriteEvent(uc8 sEvent)
{
    if(sEvent <= IRS_LOGIC_STAT_AO)
    {
        return SET;
    }

    return RESET;
}

ErrorStatus IrsMmy_WriteLogicDataToFlash(uc8 irsId, u8 lgEvent, uc8 sBuff[], u8 sLen)
{
    u8 i;
    u8 gIndex = 0;
    u8 tmpByte;
    RunAddrTypedef  tmpCmdAddr;
    ErrorStatus reStat = SUCCESS;
        
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sBuff));

    // 长度字节是否 大于等于 内容字节+1 且 内容字节等于逻辑内容长度
    if(((sLen - 1) >= sBuff[0])     &&
       ((sBuff[0]) == IRS_LOGIC_EVENT_PARA_LEN))
    {
        // 越过数据长度字节
        gIndex++;

        // 遍历所有事件
        for(i=0; i<IRS_CATCH_EVENT_MAX; i++)
        {
            // 获取事件
            tmpByte = sBuff[gIndex++];
            // 判断事件是否非法
            if(IrsMmy_IsIrsWriteEvent(tmpByte) != SET)
            {
                return ERROR;
            }

            // 获取命令标识
            tmpByte = sBuff[gIndex++];
            // 判断命令长度是非法
            if((tmpByte & 0x0F) != 0x02)
            {
                return ERROR;
            }

            // 获取命令的AG地址
            tmpCmdAddr.App = sBuff[gIndex++];
            tmpCmdAddr.Group = sBuff[gIndex++];
            // 判断命令AG地址是否非法
            if(IrsMmy_AddrIsLegal(tmpCmdAddr) != SET)
            {
                return ERROR;
            }

            // 跳过命令内容判断
            gIndex += 2;
        }

        // 写入数据到RAM中
        if(IrsMmy_SetParaToMMY(irsId, (IrsMmyType)lgEvent, &(sBuff[1])) == SUCCESS)
        {
            // 事件逻辑从0开始
            lgEvent -= 1;
            
            // 循环解析逻辑事件操作数据
            for(i=IRS_CATCH_EVENT_ONE_PULSE; i<IRS_CATCH_EVENT_MAX; i++)
            {
                // 解析逻辑事件读操作数据
                IrsMmy_GetCatchEventOptPara(&(sBuff[1+(i*IRS_CATCH_EVENT_OPT_PARA_LEN)]), &(IrsLogicAttr[irsId].LogicEvent[lgEvent][i]));
            }

            // 返回成功
            reStat = SUCCESS;
        }
        else
        {
            reStat = ERROR;
        }
    }
    else
    {
        reStat = ERROR;
    }

    return reStat;
}

/*****************************************************************************
 函 数 名  : IrsMmy_WriteFormulaDataToFlash
 功能描述  : 写入复合公式
 输入参数  : uc8 irsId    
             u8 fmlIndex  
             uc8 sBuff[]  
             u8 sLen      
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月16日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsMmy_WriteFormulaDataToFlash(uc8 irsId, u8 fmlIndex, uc8 sBuff[], u8 sLen)
{
    u8 i;
    u8 gIndex = 0;
    u8 tmpByte;
    RunAddrTypedef  tmpCmdAddr;
    u8 startIndex;
    ErrorStatus reStat = SUCCESS;
        
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sBuff));

    // 长度字节是否 大于等于 内容字节+1 且 内容字节等于复合公式内容长度
    if(((sLen - 1) >= sBuff[0])     &&
       ((sBuff[0]) == IRS_FORMULA_MMY_PARA_LEN))
    {
        // 越过数据长度字节
        gIndex++;

        // 越过复合公式命令标识字节
        gIndex++;

        // 获取事件
        tmpByte = sBuff[gIndex++];
        // 判断事件是否合法
        if(IrsMmy_IsIrsWriteEvent(tmpByte) != SET)
        {
            return ERROR;
        }

        // 获取匹配的AG地址
        tmpCmdAddr.App = sBuff[gIndex++];
        tmpCmdAddr.Group = sBuff[gIndex++];
        // 判断匹配AG地址是否非法
        if(IrsMmy_AddrIsLegal(tmpCmdAddr) != SET)
        {
            return ERROR;
        }
        // 越过匹配Level值字节
        gIndex++;

        // 获取更改状态的AG地址
        tmpCmdAddr.App = sBuff[gIndex++];
        tmpCmdAddr.Group = sBuff[gIndex++];
        // 判断匹配AG地址是否非法
        if(IrsMmy_AddrIsLegal(tmpCmdAddr) != SET)
        {
            return ERROR;
        }

        // 获取事件
        tmpByte = sBuff[gIndex++];
        // 判断事件是否合法
        if(IrsMmy_IsIrsWriteEvent(tmpByte) != SET)
        {
            return ERROR;
        }

        // 获取复合公式的执行命令起始下标
        startIndex = (IRS_FORMULA_MMY_EXECUTION_START_INDEX + 1);

        // 遍历所有执行命令
        for(i=0; i<IRS_FORMULA_MMY_EXECUTION_ADDR_NUM; i++)
        {
            // 获取命令标识
            tmpByte = sBuff[startIndex++];
            // 判断命令长度是非法
            if((tmpByte & 0x0F) != 0x02)
            {
                return ERROR;
            }

            // 获取命令的AG地址
            tmpCmdAddr.App = sBuff[startIndex++];
            tmpCmdAddr.Group = sBuff[startIndex++];
            // 判断匹配AG地址是否非法
            if(IrsMmy_AddrIsLegal(tmpCmdAddr) != SET)
            {
                return ERROR;
            }

            startIndex += 2;
        }

        // 获取复合公式存储标识
        fmlIndex -= 1;
        fmlIndex += ISR_MMY_TYPE_FORMULA_START;

        // 写入数据到RAM中
        if(IrsMmy_SetParaToMMY(irsId, (IrsMmyType)fmlIndex, &(sBuff[1])) == SUCCESS)
        {
            // 归复复合公式下标
            fmlIndex -= ISR_MMY_TYPE_FORMULA_START;

            // 获取复合公式的触发参数
            IrsMmy_GetFormulaTriggerPara(&(sBuff[1]), &(IrsLogicAttr[irsId].FormulaTirggerAttr[fmlIndex]));

            // 返回成功
            reStat = SUCCESS;
        }
        else
        {
            reStat = ERROR;
        }
    }
    else
    {
        reStat = ERROR;
    }

    return reStat;
}



