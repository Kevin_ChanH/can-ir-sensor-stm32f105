/******************************************************************************

                  版权所有 (C), 2013-2014, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Irs.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年11月1日
  最近修改   :
  功能描述   : 红外感应器 驱动文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年11月1日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
IrsCatchRunAttr      IrsCatchAttr[IRS_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

// 定义红外的IO_MAP
const PinNames IrsGpioMap[IRS_CFG_USER_NUM] =
{
    IRS_PIN_1,
};


/*****************************************************************************
 函 数 名  : Irs_GpioInit
 功能描述  : 初始化红外的GPIO
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Irs_GpioInit(void)
{
    u8 i = 0;

    for(i=0; i<IRS_CFG_USER_NUM; i++)
    {
        Gpio_Init(IrsGpioMap[i], IRS_GPIO_MODE, IRS_GPIO_INIT_VALUE);
    }
}

/*****************************************************************************
 函 数 名  : Irs_InitRunPara
 功能描述  : 初始化红外的运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Irs_InitRunPara(void)
{
    u8 i = 0;
    u8 irsCatchBuff[IRS_CATCH_MMY_PARA_LEN];
    TmmExp_ParaTypedef irResetWaitTime = {0, 0, 0};

    // 初始化各类运行参数
    IrsMmy_RunParaInit();

    for(i=0; i<IRS_CFG_USER_NUM; i++)
    {
        // 判断存储数据是否获取成功
        if(IrsMmy_GetParaFromMMY(i, IRS_MMY_TYPE_CATCH, irsCatchBuff) != SUCCESS)
        {
            // 获取默认数据
            IrsMmy_GetDefaultPara(i, IRS_MMY_TYPE_CATCH, irsCatchBuff);

            // 设置默认数据到FLASH
            IrsMmy_SetDefaultToMMY(i, IRS_MMY_TYPE_CATCH);
        }

        // 获取红外的捕获数据
        IrsMmy_GetCatchAttr(irsCatchBuff, &IrsCatchAttr[i]);

        // 红外采集状态 - 空闲状态
        IrsCatchAttr[i].CatchStat   = IRS_CATCH_STAT_IDEL;

        // 定时器状态  - 初始化状态
        IrsCatchAttr[i].TimeStat    = IRS_TIME_CNT_STAT_INIT;

        // 无人 - 倒计时定时器
        IrsCatchAttr[i].TexpNoneCnt = (TmmExp_ID)(TMM_EXP_IRS_NONE_START + i);
        // 有人 - 倒计时定时器
        IrsCatchAttr[i].TexpHasCnt  = (TmmExp_ID)(TMM_EXP_IRS_HAS_START + i);

        // 有人信号 - 计时器
        IrsCatchAttr[i].HasTimeCnt  = (TmmCnt_ID)(TMM_CNT_IRS_HAS_START + i);

        // 初始等待完成标记 - 设置为 - 未完成
        IrsCatchAttr[i].ResetWaitFinishi = RESET;

        // 载入初始化等待秒数
        irResetWaitTime.TmmSecond = IrsCatchAttr[i].ResetWaitSec;
        // 载入初始化等待时间
        TmmExp_Load(IrsCatchAttr[i].TexpNoneCnt, irResetWaitTime);

        // 红外的地址解析初始化
        Irs_AddrAnalyzeInit(i);
    }
}

/*****************************************************************************
 函 数 名  : Irs_AddrAnalyzeInit
 功能描述  : 红外的地址解析初始化
 输入参数  : uc8 irsId  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月13日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_AddrAnalyzeInit(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // 判断红外的工作模式是否不为空闲模式
    if(IrsCatchAttr[irsId].IrsRunMode != IRS_MODE_IDLE)
    {
        // 加入红外状态的运行地址 - 进入状态监控
        VirtualAddr_Join(IrsCatchAttr[irsId].CatchIrAddr);
    }

    // 判断红外的工作模式是否为联动模式
    if(IrsCatchAttr[irsId].IrsRunMode == IRS_MODE_INTERLOCK)
    {
        // 加入红外状态的运行地址 - 进入状态监控
        VirtualAddr_Join(IrsCatchAttr[irsId].TriIrAddr);
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_Reset
 功能描述  : 复位红外状态获取
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_Reset(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // 设置红外捕获状态为 - 空闲状态
    return Irs_SetCatchStat(irsId, IRS_CATCH_STAT_IDEL);
}

/*****************************************************************************
 函 数 名  : Irs_SetCatchStat
 功能描述  : 红外 - 设置探测状态
 输入参数  : uc8 irsId
             IrsCatchStatTypedef sCatchStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_SetCatchStat(uc8 irsId, IrsCatchStatTypedef sCatchStat)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_CATCH_STAT(sCatchStat));

    // 载入状态
    IrsCatchAttr[irsId].CatchStat = sCatchStat;

    // 初始化各类定时器
    Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_INIT, IRS_TIME_CNT_OPT_OFF);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_GetCatchStat
 功能描述  : 获取 红外的探测状态
 输入参数  : uc8 irsId
             IrsCatchStatTypedef *gCatchStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月11日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_GetCatchStat(uc8 irsId, IrsCatchStatTypedef *gCatchStat)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gCatchStat));

    // 载入捕获状态
    *gCatchStat = IrsCatchAttr[irsId].CatchStat;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_GetRunMode
 功能描述  : 获取红外传感器的运行模式
 输入参数  : uc8 irsId
             IrsRunModeTypedef *gRunMode
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月12日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_GetRunMode(uc8 irsId, IrsRunModeTypedef *gRunMode)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gRunMode));

    // 载入运行模式
    *gRunMode = IrsCatchAttr[irsId].IrsRunMode;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_GetAddr
 功能描述  : 获取红外传感器的运行地址
 输入参数  : uc8 irsId
             RunAddrTypedef *gAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月12日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_GetAddr(uc8 irsId, RunAddrTypedef *gAddr)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gAddr));

    gAddr->App      = IrsCatchAttr[irsId].CatchIrAddr.App;
    gAddr->Group    = IrsCatchAttr[irsId].CatchIrAddr.Group;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_GetTriAddr
 功能描述  : 获取红外传感器的触发地址
 输入参数  : uc8 irsId              
             RunAddrTypedef *gAddr  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月11日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_GetTriAddr(uc8 irsId, RunAddrTypedef *gAddr)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gAddr));

    gAddr->App      = IrsCatchAttr[irsId].TriIrAddr.App;
    gAddr->Group    = IrsCatchAttr[irsId].TriIrAddr.Group;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_GetTriLevel
 功能描述  : 获取红外传感器的触发值
 输入参数  : uc8 irsId   
             u8 *gLevel  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月11日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_GetTriLevel(uc8 irsId, u8 *gLevel)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gLevel));

    return VirtualAddr_GetVaRealLevel(IrsCatchAttr[irsId].TriIrAddr, gLevel);
}


/*****************************************************************************
 函 数 名  : Irs_GetResetWaitFinishStat
 功能描述  : 获取复位等待完成状态标志位
 输入参数  : uc8 irsId
             FlagStatus *gStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月26日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_GetResetWaitFinishStat(uc8 irsId, FlagStatus *gStat)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gStat));

    *gStat = IrsCatchAttr[irsId].ResetWaitFinishi;

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : Irs_SetTimeCntStat
 功能描述  : 设置计时状态
 输入参数  : uc8 irsId
             IrsTimeCntStatTypedef sStat
             IrsTimeCntOptTypedef timOpt
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_SetTimeCntStat(uc8 irsId, IrsTimeCntStatTypedef sStat, IrsTimeCntOptTypedef timOpt)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_ISR_TIME_CNT_STAT(sStat));
    assert_app_param(IS_IRS_TIME_CNT_OPT(timOpt));

    // 设置计时状态为 - 初始化状态
    if(sStat == IRS_TIME_CNT_STAT_INIT)
    {
        // 定时器状态 设置为初始化状态
        IrsCatchAttr[irsId].TimeStat    = IRS_TIME_CNT_STAT_INIT;

        // 清空有人计时的所有参数
        TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_OFF);

        // 清空 无人 -  倒计时定时器
        TmmExp_Clr(IrsCatchAttr[irsId].TexpNoneCnt);
        // 清空 有人 - 倒计时定时器
        TmmExp_Clr(IrsCatchAttr[irsId].TexpHasCnt);
    }
    // 设置计时状态为 - 一段时间没人状态
    else if(sStat == IRS_TIME_CNT_STAT_FEW_TIME_NONE)
    {
        // 定时器状态 倒计时 - 一个时间段计时
        IrsCatchAttr[irsId].TimeStat = IRS_TIME_CNT_STAT_FEW_TIME_NONE;

        // 判断定时器操作状态 是否为 ON 或者 RESTART
        if((timOpt == IRS_TIME_CNT_OPT_ON) || (timOpt == IRS_TIME_CNT_OPT_RESTART))
        {
            // 载入倒计时时间
            TmmExp_Load(IrsCatchAttr[irsId].TexpNoneCnt, IrsCatchAttr[irsId].NoneFewTime);
        }
        else if(timOpt == IRS_TIME_CNT_OPT_OFF)
        {
            // 清空倒计时定时器
            TmmExp_Clr(IrsCatchAttr[irsId].TexpNoneCnt);
        }
        else
        {
            return ERROR;
        }
    }
    // 设置计时状态为 - 多个时间段没人状态
    else if(sStat == IRS_TIME_CNT_STAT_MULT_TIME_NONE)
    {
        // 定时器状态 倒计时 - 多个时间段计时
        IrsCatchAttr[irsId].TimeStat = IRS_TIME_CNT_STAT_MULT_TIME_NONE;

        // 判断定时器操作状态 是否为 ON 或者 RESTART
        if((timOpt == IRS_TIME_CNT_OPT_ON) || (timOpt == IRS_TIME_CNT_OPT_RESTART))
        {
            // 载入倒计时时间
            TmmExp_Load(IrsCatchAttr[irsId].TexpNoneCnt, IrsCatchAttr[irsId].NoneMultTime);
        }
        else if(timOpt == IRS_TIME_CNT_OPT_OFF)
        {
            // 清空倒计时定时器
            TmmExp_Clr(IrsCatchAttr[irsId].TexpNoneCnt);
        }
        else
        {
            return ERROR;
        }
    }
    // 设置计时状态为 - 一段时间有人状态
    else if(sStat == IRS_TIME_CNT_STAT_FEW_TIME_HAS)
    {
        IrsCatchAttr[irsId].TimeStat = IRS_TIME_CNT_STAT_FEW_TIME_HAS;

        // 计时操作状态是否为 ON - RESTART
        if((timOpt == IRS_TIME_CNT_OPT_ON) || (timOpt == IRS_TIME_CNT_OPT_RESTART))
        {
            // 载入倒计时时间
            TmmExp_Load(IrsCatchAttr[irsId].TexpHasCnt, IrsCatchAttr[irsId].HasFewTime);

            // 设置计时状态为开
            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_ON);
        }
        // 计时操作状态是否为 OFF
        else if(timOpt == IRS_TIME_CNT_OPT_OFF)
        {
            // 清空有人计时的所有参数
            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_OFF);

            // 清空倒计时定时器
            TmmExp_Clr(IrsCatchAttr[irsId].TexpHasCnt);
        }
        // 计时操作状态是否为 PAUSE
        else if(timOpt == IRS_TIME_CNT_OPT_PAUSE)
        {
            // 暂停计时
            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_PAUSE);
        }
        // 计时操作状态是否为 CONTINUE
        else if(timOpt == IRS_TIME_CNT_OPT_CONTINUE)
        {
            // 继续计时
            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_CONTINUE);
        }
        else
        {
            return ERROR;
        }
    }
    // 设置计时状态为 - 多个时间段有人状态
    else if(sStat == IRS_TIME_CNT_STAT_MULT_TIME_HAS)
    {
        IrsCatchAttr[irsId].TimeStat = IRS_TIME_CNT_STAT_MULT_TIME_HAS;

        // 计时操作状态是否为 ON - RESTART
        if((timOpt == IRS_TIME_CNT_OPT_ON) || (timOpt == IRS_TIME_CNT_OPT_RESTART))
        {
            // 载入倒计时时间
            TmmExp_Load(IrsCatchAttr[irsId].TexpHasCnt, IrsCatchAttr[irsId].HasMultTime);

            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_ON);
        }
        // 计时操作状态是否为 OFF
        else if(timOpt == IRS_TIME_CNT_OPT_OFF)
        {
            // 清空有人计时的所有参数
            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_OFF);

            // 清空倒计时定时器
            TmmExp_Clr(IrsCatchAttr[irsId].TexpHasCnt);
        }
        // 计时操作状态是否为 PAUSE
        else if(timOpt == IRS_TIME_CNT_OPT_PAUSE)
        {
            // 暂停计时
            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_PAUSE);
        }
        // 计时操作状态是否为 CONTINUE
        else if(timOpt == IRS_TIME_CNT_OPT_CONTINUE)
        {
            // 继续计时
            TmmCnt_SetStat(IrsCatchAttr[irsId].HasTimeCnt, TMM_CNT_STAT_CONTINUE);
        }
        else
        {
            return ERROR;
        }
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_IdelStatPoll
 功能描述  : 红外捕获 空闲状态-执行
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_IdelStatPoll(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));

    // 进入 初始化等待状态
    return Irs_SetCatchStat(irsId, IRS_CATCH_STAT_INIT_WAIT);
}

/*****************************************************************************
 函 数 名  : Irs_InitWaitStatPoll
 功能描述  : 红外捕获 初始化等待状态-执行
 输入参数  : uc8 irsId
             FlagStatus gIrStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_InitWaitStatPoll(uc8 irsId, FlagStatus gIrStat)
{
    assert_app_param(IS_IRS_ID(irsId));

    // 判断信号是否为 捕获到有人的信号
    if(gIrStat == ISR_HAS_SIGNAL)
    {
        // 进入 捕获到第一个脉冲状态
        Irs_SetCatchStat(irsId, IRS_CATCH_STAT_ONE_PULSE);
    }
    else
    {
        // 判断定时器状态是否不为 一段时间无人倒计时状态
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_FEW_TIME_NONE)
        {
            // 重新 倒计时
            Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_NONE, IRS_TIME_CNT_OPT_RESTART);
        }
        else
        {
            // 判断定时器状态 是否 已经倒计时完毕
            if(TmmExp_IsON(IrsCatchAttr[irsId].TexpNoneCnt) != SET)
            {
                // 进入 一个时间段无人状态
                Irs_SetCatchStat(irsId, IRS_CATCH_STAT_FEW_TIME_NONE);
            }
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_OnePulseStatPoll
 功能描述  : 红外捕获 第一个脉冲状态-执行
 输入参数  : uc8 irsId
             FlagStatus gIrStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_OnePulseStatPoll(uc8 irsId, FlagStatus gIrStat)
{
    u32 gHasMs = 0;

    assert_app_param(IS_IRS_ID(irsId));

    // 判断红外是否感应到有人
    if(gIrStat == ISR_HAS_SIGNAL)
    {
        // 判断之前是否不为 - 存在有人的计时中
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_FEW_TIME_HAS)
        {
            // 判断是否处于计时状态
            if(TmmCnt_GetStat(IrsCatchAttr[irsId].HasTimeCnt) == TMM_CNT_STAT_OFF)
            {
                // 打开 计时
                Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_HAS, IRS_TIME_CNT_OPT_ON);
            }
            else
            {
                // 继续 计时
                Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_HAS, IRS_TIME_CNT_OPT_CONTINUE);
            }
        }
        else
        {
            // 获取计时时间
            TmmCnt_GetCntMs(IrsCatchAttr[irsId].HasTimeCnt, &gHasMs);

            // 判断 计时时间 是否已经 达到配置的时间
            if(gHasMs >= IrsCatchAttr[irsId].HasFewTimeMs)
            {
                // 进入 一个时间段有人状态
                Irs_SetCatchStat(irsId, IRS_CATCH_STAT_FEW_TIME_HAS);
            }
            else
            {
                // 判断有人倒计时时间是否到达
                if(TmmExp_IsON(IrsCatchAttr[irsId].TexpHasCnt) != SET)
                {
                    // 重新计时有人状态
                    Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_HAS, IRS_TIME_CNT_OPT_RESTART);
                }
            }
        }
    }
    else
    {
        // 判断定时器状态是否不为 一段时间无人倒计时状态
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_FEW_TIME_NONE)
        {
            // 判断 计时器 是否 正在计数
            if(TmmCnt_GetStat(IrsCatchAttr[irsId].HasTimeCnt) == TMM_CNT_STAT_ON)
            {
                // 暂停 计时器 计数
                Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_HAS, IRS_TIME_CNT_OPT_PAUSE);
            }

            // 重新 倒计时
            Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_NONE, IRS_TIME_CNT_OPT_RESTART);
        }
        else
        {
            // 判断定时器状态 是否 已经倒计时完毕
            if(TmmExp_IsON(IrsCatchAttr[irsId].TexpNoneCnt) != SET)
            {
                // 进入 一个时间段无人状态
                Irs_SetCatchStat(irsId, IRS_CATCH_STAT_FEW_TIME_NONE);
            }
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_FewTimeHasStatPoll
 功能描述  : 红外捕获 一个时间段有人-执行
 输入参数  : uc8 irsId
             FlagStatus gIrStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_FewTimeHasStatPoll(uc8 irsId, FlagStatus gIrStat)
{
    u32 gHasMs = 0;

    assert_app_param(IS_IRS_ID(irsId));

    // 判断红外是否感应到有人
    if(gIrStat == ISR_HAS_SIGNAL)
    {
        // 判断之前是否不为 存在有人的计时中
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_MULT_TIME_HAS)
        {
            // 判断是否处于计时状态
            if(TmmCnt_GetStat(IrsCatchAttr[irsId].HasTimeCnt) == TMM_CNT_STAT_OFF)
            {
                // 打开 计时
                Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_MULT_TIME_HAS, IRS_TIME_CNT_OPT_ON);
            }
            else
            {
                // 继续 计时
                Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_MULT_TIME_HAS, IRS_TIME_CNT_OPT_CONTINUE);
            }
        }
        else
        {
            // 获取计时时间
            TmmCnt_GetCntMs(IrsCatchAttr[irsId].HasTimeCnt, &gHasMs);

            // 判断 计时时间 是否已经 达到配置的时间
            if(gHasMs >= IrsCatchAttr[irsId].HasMultTimeMs)
            {
                // 进入 多个时间段有人状态
                Irs_SetCatchStat(irsId, IRS_CATCH_STAT_MULT_TIME_HAS);
            }
            else
            {
                // 判断有人倒计时时间是否到达
                if(TmmExp_IsON(IrsCatchAttr[irsId].TexpHasCnt) != SET)
                {
                    // 重新计时有人状态
                    Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_MULT_TIME_HAS, IRS_TIME_CNT_OPT_RESTART);
                }
            }
        }
    }
    else
    {
        // 判断定时器状态是否不为 一段时间无人倒计时状态
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_FEW_TIME_NONE)
        {
            // 判断 计时器 是否 正在计数
            if(TmmCnt_GetStat(IrsCatchAttr[irsId].HasTimeCnt) == TMM_CNT_STAT_ON)
            {
                // 暂停 计时器 计数
                Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_MULT_TIME_HAS, IRS_TIME_CNT_OPT_PAUSE);
            }

            // 重新 倒计时
            Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_NONE, IRS_TIME_CNT_OPT_RESTART);
        }
        else
        {
            // 判断定时器状态 是否 已经倒计时完毕
            if(TmmExp_IsON(IrsCatchAttr[irsId].TexpNoneCnt) != SET)
            {
                // 进入 一个时间段无人状态
                Irs_SetCatchStat(irsId, IRS_CATCH_STAT_FEW_TIME_NONE);
            }
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_MultTiemHasStatPoll
 功能描述  : 红外捕获 多个时间段有人状态-执行
 输入参数  : uc8 irsId
             FlagStatus gIrStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_MultTiemHasStatPoll(uc8 irsId, FlagStatus gIrStat)
{
    assert_app_param(IS_IRS_ID(irsId));

    // 判断信号是否为 捕获到有人的信号
    if(gIrStat == ISR_HAS_SIGNAL)
    {
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_INIT)
        {
            // 初始化各类定时器
            Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_INIT, IRS_TIME_CNT_OPT_OFF);
        }
    }
    else
    {
        // 判断定时器状态是否不为 一段时间无人倒计时状态
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_FEW_TIME_NONE)
        {
            // 重新 倒计时
            Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_FEW_TIME_NONE, IRS_TIME_CNT_OPT_RESTART);
        }
        else
        {
            // 判断定时器状态 是否 已经倒计时完毕
            if(TmmExp_IsON(IrsCatchAttr[irsId].TexpNoneCnt) != SET)
            {
                // 进入 一个时间段无人状态
                Irs_SetCatchStat(irsId, IRS_CATCH_STAT_FEW_TIME_NONE);
            }
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_FewTimeNoneStatPoll
 功能描述  : 红外捕获 一个时间段没人状态-执行
 输入参数  : uc8 irsId
             FlagStatus gIrStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_FewTimeNoneStatPoll(uc8 irsId, FlagStatus gIrStat)
{
    assert_app_param(IS_IRS_ID(irsId));

    // 判断信号是否为 捕获到有人的信号
    if(gIrStat == ISR_HAS_SIGNAL)
    {
        // 进入 捕获到第一个脉冲状态
        Irs_SetCatchStat(irsId, IRS_CATCH_STAT_ONE_PULSE);
    }
    else
    {
        // 判断定时器状态是否不为 多段时间无人倒计时状态
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_MULT_TIME_NONE)
        {
            // 重新 倒计时
            Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_MULT_TIME_NONE, IRS_TIME_CNT_OPT_RESTART);
        }
        else
        {
            // 判断定时器状态 是否 已经倒计时完毕
            if(TmmExp_IsON(IrsCatchAttr[irsId].TexpNoneCnt) != SET)
            {
                // 进入 多个时间段无人状态
                Irs_SetCatchStat(irsId, IRS_CATCH_STAT_MULT_TIME_NONE);
            }
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_MultTiemNoneStatPoll
 功能描述  : 红外捕获 多个时间段没人状态-执行
 输入参数  : uc8 irsId
             FlagStatus gIrStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_MultTiemNoneStatPoll(uc8 irsId, FlagStatus gIrStat)
{
    assert_app_param(IS_IRS_ID(irsId));

    // 判断信号是否为 捕获到有人的信号
    if(gIrStat == ISR_HAS_SIGNAL)
    {
        // 进入 捕获到第一个脉冲状态
        Irs_SetCatchStat(irsId, IRS_CATCH_STAT_ONE_PULSE);
    }
    else
    {
        if(IrsCatchAttr[irsId].TimeStat != IRS_TIME_CNT_STAT_INIT)
        {
            // 初始化各类定时器
            Irs_SetTimeCntStat(irsId, IRS_TIME_CNT_STAT_INIT, IRS_TIME_CNT_OPT_OFF);
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_StatPoll
 功能描述  : 红外状态执行
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Irs_StatPoll(uc8 irsId)
{
    volatile FlagStatus gIrStat;

    // 获取红外状态
    gIrStat = (FlagStatus)(Gpio_Read(IrsGpioMap[irsId]));

    // 判断红外捕获状态
    switch(IrsCatchAttr[irsId].CatchStat)
    {
        // 空闲状态
        case IRS_CATCH_STAT_IDEL:

            Irs_IdelStatPoll(irsId);
            break;

        // 初始化等待状态
        case IRS_CATCH_STAT_INIT_WAIT:

            Irs_InitWaitStatPoll(irsId, gIrStat);
            break;

        // 捕获到第一个脉冲的状态
        case IRS_CATCH_STAT_ONE_PULSE:

            Irs_OnePulseStatPoll(irsId, gIrStat);
            break;

        // 捕获到一个时间段有人状态
        case IRS_CATCH_STAT_FEW_TIME_HAS:

            Irs_FewTimeHasStatPoll(irsId, gIrStat);
            break;

        // 捕获到多个时间段有人状态
        case IRS_CATCH_STAT_MULT_TIME_HAS:

            Irs_MultTiemHasStatPoll(irsId, gIrStat);
            break;

        // 捕获到一个时间段无人状态
        case IRS_CATCH_STAT_FEW_TIME_NONE:

            Irs_FewTimeNoneStatPoll(irsId, gIrStat);
            break;

        // 捕获到多个时间段无人状态
        case IRS_CATCH_STAT_MULT_TIME_NONE:

            Irs_MultTiemNoneStatPoll(irsId, gIrStat);
            break;

        default:

            // 超出范围 重新设置状态
            Irs_SetCatchStat(irsId, IRS_CATCH_STAT_IDEL);
            break;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : Irs_CatchPoll
 功能描述  : 红外捕获 轮巡
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月10日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Irs_CatchPoll(void)
{
    u8 i;

    for(i=0; i<IRS_CFG_USER_NUM; i++)
    {
        // 判断初始等待是否完成
        if(IrsCatchAttr[i].ResetWaitFinishi == SET)
        {
            // 判断是否为联动状态 或 正常状态
            if((IrsCatchAttr[i].IrsRunMode == IRS_MODE_INTERLOCK)
            || (IrsCatchAttr[i].IrsRunMode == IRS_MODE_NORMAL))
            {
                // 执行红外状态轮询
                Irs_StatPoll(i);
            }
        }
        else
        {
            // 判断初始等待时间是否倒计时完成
            if(TmmExp_IsON(IrsCatchAttr[i].TexpNoneCnt) == RESET)
            {
                // 设置为初始化等待完成
                IrsCatchAttr[i].ResetWaitFinishi = SET;
            }
        }
    }
}


