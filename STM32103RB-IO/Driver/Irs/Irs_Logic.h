/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Irs_Logic.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年11月11日
  最近修改   :
  功能描述   : Irs_Logic.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年11月11日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __IRS_LOGIC_H__
#define __IRS_LOGIC_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


#define         IRS_LOGIC_INIT_STAT             IRS_LOGIC_STAT_AE
#define         IRS_LOGIC_INIT_CMD_SET_STAT     IRS_LOGIC_STAT_IDEL

#define         IRS_LOGIC_TRIGGER_LEVEL_ENABLE      0
#define         IRS_LOGIC_TRIGGER_LEVEL_DISABLE     1



extern  IrsLogicRunAttr IrsLogicAttr[IRS_CFG_USER_NUM];

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  void                    IrsLogic_Init                                   (void);
    
extern  FlagStatus              IrsLogic_IsTriggerExtCmd                        (uc8 irsId);
extern  FlagStatus              IrsLogic_IsLogicTriggerExtCmd                   (uc8 irsId);
extern  FlagStatus              IrsLogic_IsFormulaTirggerExtCmd                 (uc8 irsId);

extern  ErrorStatus             IrsLogic_GetEventOptAttr                        (uc8 irsId,IrsLogicStatTypedef lgStat, IrsLogicEventTypedef gLogicEventBuff[]);

extern  ErrorStatus             IrsLogic_GetLogicStat                           (uc8 irsId, IrsLogicStatTypedef *gStat);
extern  ErrorStatus             IrsLogic_ChangeLogicStat                        (uc8 irsId, IrsLogicStatTypedef sStat);
extern  ErrorStatus             IrsLogic_SetLogicStat                           (uc8 irsId, IrsLogicStatTypedef sStat);
extern  ErrorStatus             IrsLogic_LoadCmd                                (RunAddrTypedef irsAddr, u8 sLevel);
extern  ErrorStatus             IrsLogic_LoadFormulaTriggetCmd                  (RunAddrTypedef irsAddr, u8 cmdBuff[], uc8 cmdLen);

extern  ErrorStatus             IrsLogic_CatchStatLogicPoll                     (uc8 irsId, IrsLogicEventTypedef gLogicEventBuff[], IrsCatchEvent catchEvent);
extern  void                    IrsLogic_StatLogicPoll                          (uc8 irsId, IrsCatchEvent catchEvent);
extern  ErrorStatus             IrsLogic_GetCatchEvent                          (uc8 irsId, IrsCatchEvent *catchEvent);
extern  ErrorStatus             IrsLogic_InterlockPoll                          (uc8 irsId);

extern  void                    IrsLogic_Poll                                   (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __IRS_LOGIC_H__ */
