/******************************************************************************

                  版权所有 (C), 2013-2014, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Irs_Logic.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年11月11日
  最近修改   :
  功能描述   : 红外感应器 - 逻辑操作 文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年11月11日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/
// CAN 状态参数
extern  CAN_StatParaTypedef     CanStatPara;

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
IrsLogicRunAttr     IrsLogicAttr[IRS_CFG_USER_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : IrsLogic_ParaInit
 功能描述  : 初始化参数
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月28日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_ParaInit(uc8 irsId)
{
    u8 i, j;
    u8 irsEventBuff[IRS_AE_MMY_PARA_LEN];

    assert_app_param(IS_IRS_ID(irsId));

    // 循环获取该红外的所有逻辑事件数据
    for(i=IRS_LOGIC_EVENT_AE; i<IRS_LOGIC_EVENT_MAX; i++)
    {
        // 从FLASH处获取逻辑事件数据
        if(IrsMmy_GetParaFromMMY(irsId, (IrsMmyType)(IRS_MMY_TYPE_AE+i), irsEventBuff) != SUCCESS)
        {
            // 从默认数据处获取逻辑事件数据
            IrsMmy_GetDefaultPara(irsId, (IrsMmyType)(IRS_MMY_TYPE_AE+i), irsEventBuff);

            // 设置默认数据到FLASH
            IrsMmy_SetDefaultToMMY(irsId, (IrsMmyType)(IRS_MMY_TYPE_AE+i));
        }
        // 循环解析逻辑事件操作数据
        for(j=IRS_CATCH_EVENT_ONE_PULSE; j<IRS_CATCH_EVENT_MAX; j++)
        {
            // 解析逻辑事件读操作数据
            IrsMmy_GetCatchEventOptPara(&(irsEventBuff[j*IRS_CATCH_EVENT_OPT_PARA_LEN]), &(IrsLogicAttr[irsId].LogicEvent[i][j]));
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_FormulaMatchCmdInit
 功能描述  : 初始化复合公式参数
 输入参数  : IrMatchCmdTypedef *irMatchCmdAttr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月28日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_FormulaMatchCmdInit(IrMatchCmdTypedef *irMatchCmdAttr)
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(irMatchCmdAttr));

    // 不激活命令
    irMatchCmdAttr->IsActive    = RESET;
    irMatchCmdAttr->IsL2Active  = RESET;

    // 初始化AG地址
    irMatchCmdAttr->CmdAddr.App     = 0xFF;
    irMatchCmdAttr->CmdAddr.Group   = 0xFF;

    // 初始化命令值
    irMatchCmdAttr->CmdLevel1 = 0;
    irMatchCmdAttr->CmdLevel2 = 0;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_FormulaTirggerAttrInit
 功能描述  : 初始化复合公式触发数据
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月28日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_FormulaTirggerAttrInit(uc8 irsId)
{
    u8 i;
    u8 irsFormulaBuff[IRS_FORMULA_MMY_PARA_LEN];

    assert_app_param(IS_IRS_ID(irsId));

    // 循环获取复合公式的运行参数
    for(i=0; i<IRS_FORMULA_CFG_NUM; i++)
    {
        // 从FLASH处获取复合公式数据
        if(IrsMmy_GetParaFromMMY(irsId, (IrsMmyType)(ISR_MMY_TYPE_FORMULA_START+i), irsFormulaBuff) != SUCCESS)
        {
            // 从默认数据处获取复合公式数据
            IrsMmy_GetDefaultPara(irsId, (IrsMmyType)(ISR_MMY_TYPE_FORMULA_START+i), irsFormulaBuff);

            // 设置默认参数到FLASH
            IrsMmy_SetDefaultToMMY(irsId, (IrsMmyType)(ISR_MMY_TYPE_FORMULA_START+i));
        }

        // 解析复合公式数据
        IrsMmy_GetFormulaTriggerPara(irsFormulaBuff, &(IrsLogicAttr[irsId].FormulaTirggerAttr[i]));
    }

    // 初始化复合公式的匹配命令
    IrsLogic_FormulaMatchCmdInit(&(IrsLogicAttr[irsId].IrMatchCmd));

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_Init
 功能描述  : 红外逻辑 初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void IrsLogic_Init(void)
{
    u8 i;

    for(i=0; i<IRS_CFG_USER_NUM; i++)
    {
        // 获取运行模式
        Irs_GetRunMode(i, &(IrsLogicAttr[i].IrsRunMode));

        // 获取红外的AG地址
        Irs_GetAddr(i, &(IrsLogicAttr[i].IrsAddr));

        if(IrsLogicAttr[i].IrsRunMode == IRS_MODE_INTERLOCK)
        {
            // 逻辑状态 - 一直无人状态
            IrsLogicAttr[i].LogicStat = IRS_LOGIC_INIT_STAT;
        }
        else
        {
            // 逻辑状态 - 空闲状态
            IrsLogicAttr[i].LogicStat = IRS_LOGIC_STAT_IDEL;
        }

        // 命令设置状态 - 空闲状态
        IrsLogicAttr[i].CmdSetStat  = IRS_LOGIC_INIT_CMD_SET_STAT;

        // 初始化逻辑运行数据
        IrsLogic_ParaInit(i);

        // 初始化复合公式运行数据
        IrsLogic_FormulaTirggerAttrInit(i);
    }
}

/*****************************************************************************
 函 数 名  : IrsLogic_IsTriggerExtCmd
 功能描述  : 判断红外是否触发额外的发送命令
 输入参数  : uc8 irsId  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月11日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus IrsLogic_IsTriggerExtCmd(uc8 irsId)
{
    u8 gTmpLevel;
    
    assert_app_param(IS_IRS_ID(irsId));

    // 判断获取触发Level值是否成功
    if(Irs_GetTriLevel(irsId, &gTmpLevel) == SUCCESS)
    {
        // 判断Level值是否允许触发发送额外的发送命令
        if(gTmpLevel == IRS_LOGIC_TRIGGER_LEVEL_ENABLE)
        {
            // 发送额外的触发命令
            return SET;
        }
    }

    // 不发送额外的触发命令
    return  RESET;
}

/*****************************************************************************
 函 数 名  : IrsLogic_IsLogicTriggerExtCmd
 功能描述  : 判断红外逻辑部分是否触发额外的发送命令
 输入参数  : uc8 irsId  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月11日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  IrsLogic_IsLogicTriggerExtCmd(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));
    
    // 判断是否允许触发额外的发送命令
    return IrsLogic_IsTriggerExtCmd(irsId);
}

/*****************************************************************************
 函 数 名  : IrsLogic_IsFormulaTirggerExtCmd
 功能描述  : 判断红外复合公式部分是否触发额外的发送命令
 输入参数  : uc8 irsId  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月11日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  IrsLogic_IsFormulaTirggerExtCmd(uc8 irsId)
{
    assert_app_param(IS_IRS_ID(irsId));
    
    // 判断是否允许触发额外的发送命令
    return IrsLogic_IsTriggerExtCmd(irsId);
}

/*****************************************************************************
 函 数 名  : IrsLogic_GetLogicStat
 功能描述  : 红外逻辑 获取逻辑状态
 输入参数  : uc8 irsId
             IrsLogicStatTypedef *gStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_GetLogicStat(uc8 irsId, IrsLogicStatTypedef *gStat)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gStat));

    *gStat = IrsLogicAttr[irsId].LogicStat;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_ChangeLogicStat
 功能描述  : 红外逻辑 修改逻辑状态
 输入参数  : uc8 irsId
             IrsLogicStatTypedef sStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_ChangeLogicStat(uc8 irsId, IrsLogicStatTypedef sStat)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_LOGIC_STAT(sStat));

    // 复位红外状态
    Irs_Reset(irsId);

    IrsLogicAttr[irsId].LogicStat = sStat;

    // 让POWER灯闪烁一下
    LedIndicator_BlinkHoldSet(LED_ID_POWER);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_SetLogicStat
 功能描述  : 红外逻辑 设置逻辑状态 - 外部调用
 输入参数  : uc8 irsId
             IrsLogicStatTypedef sStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_SetLogicStat(uc8 irsId, IrsLogicStatTypedef sStat)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_LOGIC_STAT(sStat));

    // 外部命令 - 不支持IDEL状态
    //if(sStat == IRS_LOGIC_STAT_IDEL)
    //{
    //    return ERROR;
    //}

    // 判断设置逻辑状态的合法性
    if(IS_IRS_LOGIC_STAT(sStat) != SET)
    {
        return ERROR;
    }

    IrsLogicAttr[irsId].CmdSetStat = sStat;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_LoadCmd
 功能描述  : 载入AG地址-以及其最终状态值
 输入参数  : RunAddrTypedef irsAddr  
             u8 sLevel               
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2017年12月14日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_LoadCmd(RunAddrTypedef irsAddr, u8 sLevel)
{
    u8 i;

    // 判断是否不在范围内
    if(sLevel > IRS_LOGIC_STAT_AO)
    {
        // 不在范围内 - 设为空闲状态 进入静默
        sLevel = IRS_LOGIC_STAT_IDEL;
    }
    
    // 遍历整个红外地址
    for(i=0; i<IRS_CFG_USER_NUM; i++)
    {
        // 判断地址是否匹配
        if((irsAddr.App) == (IrsLogicAttr[i].IrsAddr.App)
           &&(irsAddr.Group) == (IrsLogicAttr[i].IrsAddr.Group))
        {
            // 载入状态
            IrsLogic_SetLogicStat(i, (IrsLogicStatTypedef)sLevel);
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_LoadFormulaTriggetCmd
 功能描述  : 载入复合公式触发命令
 输入参数  : RunAddrTypedef irsAddr
             u8 cmdBuff[]
             uc8 cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月28日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_LoadFormulaTriggetCmd(RunAddrTypedef irsAddr, u8 cmdBuff[], uc8 cmdLen)
{
    u8 i;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(cmdBuff));

    // 判断AG地址的合法性
    if((irsAddr.App == 0x00)
    || (irsAddr.App == 0xFF)
    || (irsAddr.Group == 0x00)
    || (irsAddr.Group == 0xFF))
    {
        return ERROR;
    }

    // 判断命令长度是否不为 2Byte OR 6Byte
    if((cmdLen != CNL_CMD_SHORT_BYTE_LEN)
    && (cmdLen != CNL_CMD_LONG_BYTE_LEN))
    {
        return ERROR;
    }

    // 轮询整个红外
    for(i=0; i<IRS_CFG_USER_NUM; i++)
    {
        // 匹配命令激活
        IrsLogicAttr[i].IrMatchCmd.IsActive = SET;

        IrsLogicAttr[i].IrMatchCmd.CmdAddr.App = irsAddr.App;
        IrsLogicAttr[i].IrMatchCmd.CmdAddr.Group = irsAddr.Group;

        // 2Byte命令
        if(cmdLen == CNL_CMD_SHORT_BYTE_LEN)
        {
            // 载入L1
            IrsLogicAttr[i].IrMatchCmd.CmdLevel1 = cmdBuff[0];

            // L2不激活
            IrsLogicAttr[i].IrMatchCmd.IsL2Active = RESET;
            // 复位L2数据
            IrsLogicAttr[i].IrMatchCmd.CmdLevel2 = 0;
        }
        // 6Byte命令
        else
        {
            // 载入L1
            IrsLogicAttr[i].IrMatchCmd.CmdLevel1 = cmdBuff[0];
            // L2激活
            IrsLogicAttr[i].IrMatchCmd.IsL2Active = SET;
            // 载入L2数据
            IrsLogicAttr[i].IrMatchCmd.CmdLevel2 = cmdBuff[1];
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_CatchStatLogicPoll
 功能描述  : 红外逻辑 获取状态逻辑执行
 输入参数  : uc8 irsId
             IrsLogicEventTypedef gLogicEventBuff[]
             IrsCatchEvent catchEvent
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_CatchStatLogicPoll(uc8 irsId, IrsLogicEventTypedef gLogicEventBuff[], IrsCatchEvent catchEvent)
{
    FrameTypedef sFrame;
    CAN_DataBase    sDb;
    AddrAttrTypedef desAddr;

    u8 cnlCmdBuff[CNL_CMD_LONG_BYTE_LEN] = {0};
    u8 cnlCmdLen = 0;

    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gLogicEventBuff));
    assert_app_param(IS_IRS_CATCH_EVENT(catchEvent));

    // 判断该事件下的目标状态是否为 - 空闲状态
    if(gLogicEventBuff[catchEvent].DesStat == IRS_LOGIC_STAT_IDEL)
    {
        return SUCCESS;
    }

    // 载入参数获取命令
    CnlCmd_GetAbsCnlBuff((u8)gLogicEventBuff[catchEvent].DesStat,   \
                         cnlCmdBuff, &cnlCmdLen);


    // 地址类型设为 APP+GROUP
    desAddr.Type = CMD_ADDR_TYPE_APP;
    // 载入目的地址 APP
    desAddr.AddrBuff[0] = IrsLogicAttr[irsId].IrsAddr.App;
    // 载入目的地址 GROUP
    desAddr.AddrBuff[1] = IrsLogicAttr[irsId].IrsAddr.Group;

    // 载入控制命令到本模块的虚拟地址命令内
    VirtualAddr_VaLoadCmd(IrsLogicAttr[irsId].IrsAddr, cnlCmdBuff, cnlCmdLen);
    
    // 载入控制命令 获取控制帧
    if(Frame_GetSingleCnlFrame(desAddr, cnlCmdBuff, cnlCmdLen, &sFrame) == SUCCESS)
    {
        // 获取CAN控制帧
        if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
        {
            // 载入控制帧到发送队列
            CAN_SendDbMsg(&sDb);

            // 载入控制命令
            ItfLogic_LoadCanCmd(&sDb);
        }
    }

    // 判断红外逻辑部分是否触发额外的发送命令 且 判断该事件状态是否需要发送命令
    if((IrsLogic_IsLogicTriggerExtCmd(irsId) == SET)    && \
       (gLogicEventBuff[catchEvent].SendFtn == IRS_LOGIC_SEND_CMD_ON))
    {
        // 载入发送命令
        CnlCmd_GetRampBuff(gLogicEventBuff[catchEvent].SendCmdL1,   \
                           gLogicEventBuff[catchEvent].SendCmdT1,   \
                           cnlCmdBuff, &cnlCmdLen);

        // 地址类型设为 APP+GROUP
        desAddr.Type = CMD_ADDR_TYPE_APP;
        // 载入目的地址 APP
        desAddr.AddrBuff[0] = gLogicEventBuff[catchEvent].SendDesAddr.App;
        // 载入目的地址 GROUP
        desAddr.AddrBuff[1] = gLogicEventBuff[catchEvent].SendDesAddr.Group;

        // 载入控制命令到本模块的虚拟地址命令内
        VirtualAddr_VaLoadCmd(gLogicEventBuff[catchEvent].SendDesAddr, cnlCmdBuff, cnlCmdLen);
        
        // 载入控制命令 获取控制帧
        if(Frame_GetSingleCnlFrame(desAddr, cnlCmdBuff, cnlCmdLen, &sFrame) == SUCCESS)
        {
            // 获取CAN控制帧
            if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
            {
                // 载入控制帧到发送队列
                CAN_SendDbMsg(&sDb);

                // 载入控制命令
                ItfLogic_LoadCanCmd(&sDb);
            }
        }
    }

    // 载入修改的状态
    IrsLogic_ChangeLogicStat(irsId, gLogicEventBuff[catchEvent].DesStat);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_StatLogicPoll
 功能描述  : 红外逻辑 状态逻辑执行函数
 输入参数  : uc8 irsId
             IrsCatchEvent catchEvent
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void IrsLogic_StatLogicPoll(uc8 irsId, IrsCatchEvent catchEvent)
{
    assert_app_param(IS_IRS_ID(irsId));

    switch(IrsLogicAttr[irsId].LogicStat)
    {
        case IRS_LOGIC_STAT_IDEL:

            break;

        case IRS_LOGIC_STAT_AE:
            IrsLogic_CatchStatLogicPoll(irsId, IrsLogicAttr[irsId].LogicEvent[IRS_LOGIC_EVENT_AE], catchEvent);
            break;

        case IRS_LOGIC_STAT_E:
            IrsLogic_CatchStatLogicPoll(irsId, IrsLogicAttr[irsId].LogicEvent[IRS_LOGIC_EVENT_E], catchEvent);
            break;

        case IRS_LOGIC_STAT_MO:
            IrsLogic_CatchStatLogicPoll(irsId, IrsLogicAttr[irsId].LogicEvent[IRS_LOGIC_EVENT_MO], catchEvent);
            break;

        case IRS_LOGIC_STAT_ME:
            IrsLogic_CatchStatLogicPoll(irsId, IrsLogicAttr[irsId].LogicEvent[IRS_LOGIC_EVENT_ME], catchEvent);
            break;

        case IRS_LOGIC_STAT_O:
            IrsLogic_CatchStatLogicPoll(irsId, IrsLogicAttr[irsId].LogicEvent[IRS_LOGIC_EVENT_O], catchEvent);
            break;

        case IRS_LOGIC_STAT_AO:
            IrsLogic_CatchStatLogicPoll(irsId, IrsLogicAttr[irsId].LogicEvent[IRS_LOGIC_EVENT_AO], catchEvent);
            break;

        default:
            IrsLogic_ChangeLogicStat(irsId, IRS_LOGIC_STAT_IDEL);
            break;
    }
}

/*****************************************************************************
 函 数 名  : IrsLogic_GetCatchEvent
 功能描述  : 红外逻辑 获取红外捕获事件
 输入参数  : uc8 irsId
             IrsCatchEvent *catchEvent
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_GetCatchEvent(uc8 irsId, IrsCatchEvent *catchEvent)
{
    IrsCatchStatTypedef gCatchStat;

    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(catchEvent));

    // 获取红外捕获到的状态
    if(Irs_GetCatchStat(irsId, &gCatchStat) != SUCCESS)
    {
        return ERROR;
    }

    // 判断状态的合法性
    if(gCatchStat == IRS_CATCH_STAT_ONE_PULSE)
    {
        *catchEvent = IRS_CATCH_EVENT_ONE_PULSE;
    }
    else if(gCatchStat == IRS_CATCH_STAT_FEW_TIME_HAS)
    {
        *catchEvent = IRS_CATCH_EVENT_FEW_TIME_HAS;
    }
    else if(gCatchStat == IRS_CATCH_STAT_MULT_TIME_HAS)
    {
        *catchEvent = IRS_CATCH_EVENT_MULT_TIME_HAS;
    }
    else if(gCatchStat == IRS_CATCH_STAT_FEW_TIME_NONE)
    {
        *catchEvent = IRS_CATCH_EVENT_FEW_TIME_NONE;
    }
    else if(gCatchStat == IRS_CATCH_STAT_MULT_TIME_NONE)
    {
        *catchEvent = IRS_CATCH_EVENT_MULT_TIME_NONE;
    }
    else
    {
        return ERROR;
    }

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : IrsLogic_InterlockPoll
 功能描述  : 红外逻辑 联动模式执行函数
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_InterlockPoll(uc8 irsId)
{
    IrsCatchEvent gCatchEvent;

    assert_app_param(IS_IRS_ID(irsId));

    // 判断外部修改状态是否需要载入
    if(IrsLogicAttr[irsId].CmdSetStat != IRS_LOGIC_STAT_IDEL)
    {
        // 载入外部修改状态
        IrsLogic_ChangeLogicStat(irsId, IrsLogicAttr[irsId].CmdSetStat);
        // 设置外部修改状态为空闲
        IrsLogicAttr[irsId].CmdSetStat = IRS_LOGIC_STAT_IDEL;
    }

    // 获取 - 红外捕获状态
    if(IrsLogic_GetCatchEvent(irsId, &gCatchEvent) != SUCCESS)
    {
        return ERROR;
    }

    // 执行红外的状态逻辑轮询
    IrsLogic_StatLogicPoll(irsId, gCatchEvent);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_NormalPoll
 功能描述  : 红外的正常模式执行函数
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_NormalPoll(uc8 irsId)
{
    IrsCatchEvent gCatchEvent;
    FrameTypedef sFrame;
    CAN_DataBase    sDb;
    AddrAttrTypedef desAddr;
    u8 cnlCmdBuff[CNL_CMD_LONG_BYTE_LEN] = {0};
    u8 cnlCmdLen = 0;
    static FlagStatus  AoStat = RESET;

    assert_app_param(IS_IRS_ID(irsId));

    // 获取 - 红外捕获状态
    if(IrsLogic_GetCatchEvent(irsId, &gCatchEvent) != SUCCESS)
    {
        return ERROR;
    }

    // 检测到有人
    if(gCatchEvent == IRS_CATCH_EVENT_ONE_PULSE)
    {
        // 判断前一个状态是否不为一直有人
        if(AoStat != SET)
        {
            // 修改状态为 - 一直有人状态
            AoStat = SET;

            // 获取ON的命令
            CnlCmd_GetCnlBuff(CNL_CMD_TYPE_ON, cnlCmdBuff, &cnlCmdLen);

            // 地址类型设为 APP+GROUP
            desAddr.Type = CMD_ADDR_TYPE_APP;
            // 载入目的地址 APP
            desAddr.AddrBuff[0] = IrsLogicAttr[irsId].IrsAddr.App;
            // 载入目的地址 GROUP
            desAddr.AddrBuff[1] = IrsLogicAttr[irsId].IrsAddr.Group;

            // 载入控制命令到本模块的虚拟地址命令内
            VirtualAddr_VaLoadCmd(IrsLogicAttr[irsId].IrsAddr, cnlCmdBuff, cnlCmdLen);
            
            // 载入控制命令 获取控制帧
            if(Frame_GetSingleCnlFrame(desAddr, cnlCmdBuff, cnlCmdLen, &sFrame) == SUCCESS)
            {
                // 获取CAN控制帧
                if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                {
                    // 载入控制帧到发送队列
                    CAN_SendDbMsg(&sDb);

                    // 载入控制命令
                    ItfLogic_LoadCanCmd(&sDb);
                }
            }
        }
    }
    // 检测到一段时间没人
    else if(gCatchEvent == IRS_CATCH_EVENT_FEW_TIME_NONE)
    {
        // 判断前一个状态是否不为非一直有人
        if(AoStat != RESET)
        {
            // 修改状态为 - 非一直没人状态
            AoStat = RESET;

            // 获取OFF的命令
            CnlCmd_GetCnlBuff(CNL_CMD_TYPE_OFF, cnlCmdBuff, &cnlCmdLen);

            // 地址类型设为 APP+GROUP
            desAddr.Type = CMD_ADDR_TYPE_APP;
            // 载入目的地址 APP
            desAddr.AddrBuff[0] = IrsLogicAttr[irsId].IrsAddr.App;
            // 载入目的地址 GROUP
            desAddr.AddrBuff[1] = IrsLogicAttr[irsId].IrsAddr.Group;

            // 载入控制命令到本模块的虚拟地址命令内
            VirtualAddr_VaLoadCmd(IrsLogicAttr[irsId].IrsAddr, cnlCmdBuff, cnlCmdLen);
            
            // 载入控制命令 获取控制帧
            if(Frame_GetSingleCnlFrame(desAddr, cnlCmdBuff, cnlCmdLen, &sFrame) == SUCCESS)
            {
                // 获取CAN控制帧
                if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                {
                    // 载入控制帧到发送队列
                    CAN_SendDbMsg(&sDb);

                    // 载入控制命令
                    ItfLogic_LoadCanCmd(&sDb);
                }
            }
        }
    }
    else
    {
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_IsFormulaIrsStatMatch
 功能描述  : 红外逻辑 - 复合公式的红外状态是否匹配
 输入参数  : uc8 irsId
             uc8 formulaId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年5月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus IrsLogic_IsFormulaIrsStatMatch(uc8 irsId, uc8 formulaId)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_FORMULA_ID(formulaId));

    // 判断触发事件的类型 - 精确匹配
    if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowEventType == IRS_NOW_EVENT_CLEAR)
    {
        // 判断现在的状态是否与 触发状态匹配
        if(IrsLogicAttr[irsId].LogicStat == IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrMatchStat)
        {
            return SET;
        }
    }
    // 判断触发事件的类型 - 有人触发
    else if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowEventType == IRS_NOW_EVENT_O)
    {
        // 判断是否为 - 检测到人的状态
        if((IrsLogicAttr[irsId].LogicStat == IRS_LOGIC_STAT_MO)
        || (IrsLogicAttr[irsId].LogicStat == IRS_LOGIC_STAT_O)
        || (IrsLogicAttr[irsId].LogicStat == IRS_LOGIC_STAT_AO))
        {
            return SET;
        }

    }
    // 判断触发事件的类型 - 无人触发
    else if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowEventType == IRS_NOW_EVENT_E)
    {
        // 判断是否为 - 检测不到人的状态
        if((IrsLogicAttr[irsId].LogicStat == IRS_LOGIC_STAT_AE)
        || (IrsLogicAttr[irsId].LogicStat == IRS_LOGIC_STAT_E)
        || (IrsLogicAttr[irsId].LogicStat == IRS_LOGIC_STAT_ME))
        {
            return SET;
        }
    }
    // 判断触发事件的类型 - 任何状态
    else if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowEventType == IRS_NOW_EVENT_ANY)
    {
        return SET;
    }
    else
    {
        return RESET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : IrsLogic_FormulaIrsCmdMatch
 功能描述  : 复合公式命令匹配
 输入参数  : uc8 irsId
             uc8 formulaId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年5月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus IrsLogic_FormulaIrsCmdMatch(uc8 irsId, uc8 formulaId)
{
    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_FORMULA_ID(formulaId));

    // 判断App是否匹配
    if(IrsLogicAttr[irsId].IrMatchCmd.CmdAddr.App != IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrMatchAddr.App)
    {
        return RESET;
    }

    // 判断地址的Group是否为 - 精确匹配
    if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowGroupType == IRS_NOW_ADDR_LEVEL_CLEAR)
    {
        // 判断Group是否匹配
        if(IrsLogicAttr[irsId].IrMatchCmd.CmdAddr.Group != IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrMatchAddr.Group)
        {
            return RESET;
        }
    }
    // 判断地址的Group是否为 - 任意Group
    else if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowGroupType == IRS_NOW_ADDR_LEVEL_ANY)
    {
    }
    else
    {
        return RESET;
    }

    // 判断Level值是否为 - 精确匹配
    if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowLevelType == IRS_NOW_ADDR_LEVEL_CLEAR)
    {
        // 判断L1是否不匹配
        if(IrsLogicAttr[irsId].IrMatchCmd.CmdLevel1 != IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrMatchLevel)
        {
            // 判断L2是否激活
            if(IrsLogicAttr[irsId].IrMatchCmd.IsL2Active == SET)
            {
                // 判断L2是否不匹配
                if(IrsLogicAttr[irsId].IrMatchCmd.CmdLevel2 != IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrMatchLevel)
                {
                    return RESET;
                }
            }
            else
            {
                return RESET;
            }
        }
    }
    // 判断Level值是否为 - 任意Level值匹配
    else if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].NowLevelType == IRS_NOW_ADDR_LEVEL_ANY)
    {
    }
    else
    {
        return RESET;
    }


    return SET;
}

/*****************************************************************************
 函 数 名  : IrsLogic_FormulaExecutionPoll
 功能描述  : 红外逻辑 - 复合公式执行轮巡
 输入参数  : uc8 irsId
             uc8 formulaId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年5月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_FormulaExecutionPoll(uc8 irsId, uc8 formulaId)
{
    FrameTypedef sFrame;
    CAN_DataBase    sDb;
    AddrAttrTypedef desAddr;
    u8 cnlCmdBuff[CNL_CMD_LONG_BYTE_LEN] = {0};
    u8 irsFormulaBuff[IRS_FORMULA_MMY_PARA_LEN];
    IrsFormulaExecutionTypedef tmpFormulaExecutionPara;
    u8 cnlCmdLen = 0;
    u8 i = 0;
    u8 tmpIndex = 0;

    assert_app_param(IS_IRS_ID(irsId));
    assert_app_param(IS_IRS_FORMULA_ID(formulaId));

    // 判断是否需要 重新设置 红外状态
    if(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IsIrExeStatReset == SET)
    {        
        // 载入参数获取命令
        CnlCmd_GetAbsCnlBuff((u8)IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrExecutionStat,   \
                             cnlCmdBuff, &cnlCmdLen);


        // 地址类型设为 APP+GROUP
        desAddr.Type = CMD_ADDR_TYPE_APP;
        // 载入目的地址 APP
        desAddr.AddrBuff[0] = IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrExecutionAddr.App;
        // 载入目的地址 GROUP
        desAddr.AddrBuff[1] = IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrExecutionAddr.Group;

        // 载入红外状态命令
        IrsLogic_LoadCmd(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrExecutionAddr, (u8)IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrExecutionStat);

        // 载入控制命令到本模块的虚拟地址命令内
        VirtualAddr_VaLoadCmd(IrsLogicAttr[irsId].FormulaTirggerAttr[formulaId].IrExecutionAddr, cnlCmdBuff, cnlCmdLen);
        
        // 载入控制命令 获取控制帧
        if(Frame_GetSingleCnlFrame(desAddr, cnlCmdBuff, cnlCmdLen, &sFrame) == SUCCESS)
        {
            // 获取CAN控制帧
            if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
            {
                // 载入控制帧到发送队列
                CAN_SendDbMsg(&sDb);

                // 载入控制命令
                ItfLogic_LoadCanCmd(&sDb);
            }
            else
            {
                return ERROR;
            }
        }
        else
        {
            return ERROR;
        }
    }

    // 判断红外复合公式部分是否触发额外的发送命令
    if(IrsLogic_IsFormulaTirggerExtCmd(irsId) == SET)
    {
        // 从FLASH处获取复合公式数据
        if(IrsMmy_GetParaFromMMY(irsId, (IrsMmyType)(ISR_MMY_TYPE_FORMULA_START+formulaId), irsFormulaBuff) != SUCCESS)
        {
            // 从默认数据处获取复合公式数据
            IrsMmy_GetDefaultPara(irsId, (IrsMmyType)(ISR_MMY_TYPE_FORMULA_START+formulaId), irsFormulaBuff);
        }

        // 数据下标
        tmpIndex = IRS_FORMULA_MMY_EXECUTION_START_INDEX;

        // 轮询整个复合公式执行数据区
        for(i=0; i<IRS_FORMULA_MMY_EXECUTION_ADDR_NUM; i++)
        {
            // 获取运行参数
            if(IrsMmy_GetFormulaExecutionPara(&(irsFormulaBuff[tmpIndex]), &tmpFormulaExecutionPara) != SUCCESS)
            {
                return ERROR;
            }

            // 判断是否激活
            if(tmpFormulaExecutionPara.IsActive == SET)
            {
                // 地址类型设为 APP+GROUP
                desAddr.Type = CMD_ADDR_TYPE_APP;
                // 载入目的地址 APP
                desAddr.AddrBuff[0] = tmpFormulaExecutionPara.ExecutionAddr.App;
                // 载入目的地址 GROUP
                desAddr.AddrBuff[1] = tmpFormulaExecutionPara.ExecutionAddr.Group;

                // 载入控制命令到本模块的虚拟地址命令内
                VirtualAddr_VaLoadCmd(tmpFormulaExecutionPara.ExecutionAddr, tmpFormulaExecutionPara.CmdBuff, tmpFormulaExecutionPara.CmdLen);
                
                // 载入控制命令 获取控制帧
                if(Frame_GetSingleCnlFrame(desAddr, tmpFormulaExecutionPara.CmdBuff, tmpFormulaExecutionPara.CmdLen, &sFrame) == SUCCESS)
                {
                    // 获取CAN控制帧
                    if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                    {
                        // 载入控制帧到发送队列
                        CAN_SendDbMsg(&sDb);

                        // 载入控制命令
                        ItfLogic_LoadCanCmd(&sDb);
                    }
                    else
                    {
                        return ERROR;
                    }
                }
                else
                {
                    return ERROR;
                }
            }

            // 获取数据下标
            tmpIndex += IRS_FORMULA_MMY_EXECUTION_PARA_LEN;
        }
    }
    
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : IrsLogic_FormulaTirggerPoll
 功能描述  : 复合公式触发轮询
 输入参数  : uc8 irsId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年5月2日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus IrsLogic_FormulaTirggerPoll(uc8 irsId)
{
    u8 i;
    assert_app_param(IS_IRS_ID(irsId));

    // 判断是否激活
    if(IrsLogicAttr[irsId].IrMatchCmd.IsActive != SET)
    {
        return ERROR;
    }

    // 轮询所有公式
    for(i=0; i<IRS_FORMULA_CFG_NUM; i++)
    {
        // 判断复合公式是否未激活
        if(IrsLogicAttr[irsId].FormulaTirggerAttr[i].IsActive != SET)
        {
            continue;
        }

        // 判断红外状态是否匹配
        if(IrsLogic_IsFormulaIrsStatMatch(irsId, i) != SET)
        {
            continue;
        }

        // 判断复合公式命令是否匹配
        if(IrsLogic_FormulaIrsCmdMatch(irsId, i) != SET)
        {
            continue;
        }

        // 执行复合公式
        IrsLogic_FormulaExecutionPoll(irsId, i);
    }

    // 初始化复合公式
    return IrsLogic_FormulaMatchCmdInit(&(IrsLogicAttr[irsId].IrMatchCmd));
}

/*****************************************************************************
 函 数 名  : IrsLogic_Poll
 功能描述  : 红外逻辑 执行函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void IrsLogic_Poll(void)
{
    u8 i;
    FlagStatus irResetWaitFinish;

    // 遍历所有红外
    for(i=0; i<IRS_CFG_USER_NUM; i++)
    {
        // 获取复位等待完成标识
        Irs_GetResetWaitFinishStat(i, &irResetWaitFinish);

        // 等待红外初始化完成
        if(irResetWaitFinish == SET)
        {
            // 判断红外是否处于联动模式
            if(IrsLogicAttr[i].IrsRunMode == IRS_MODE_INTERLOCK)
            {
                // 执行联动模式轮询
                IrsLogic_InterlockPoll(i);

                // 执行复合公式轮询
                IrsLogic_FormulaTirggerPoll(i);
            }
            // 判断红外是否处于正常模式
            else if(IrsLogicAttr[i].IrsRunMode == IRS_MODE_NORMAL)
            {
                // 执行正常模式轮询
                IrsLogic_NormalPoll(i);
            }
            else
            {
            }
        }
    }
}

