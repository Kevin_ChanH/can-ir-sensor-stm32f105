/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Irs.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年11月1日
  最近修改   :
  功能描述   : Irs.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年11月1日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __IRS_H__
#define __IRS_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/





/* 定义捕获到有人 的 信号类型 */
#define         ISR_HAS_SIGNAL          SET
/* 定义捕获到无人 的 信号类型 */
#define         ISR_NONE_SIGNAL         RESET



/* 人体红外最大的使用 */
#define     IRS_CFG_MAX_NUM             MCU_IRS_CHANNEL_NUM
#define     IRS_CFG_USER_NUM            MCU_IRS_CHANNEL_NUM
#define     IRS_ID_LIMIT_START          1
#define     IRS_ID_LIMIT_END            IRS_CFG_USER_NUM
#define     IS_IRS_ID(PARAM)            ((PARAM) < IRS_CFG_USER_NUM)


#define     IRS_FORMULA_MAX_NUM         MCU_IRS_FORMULA_NUM
#define     IRS_FORMULA_CFG_NUM         MCU_IRS_FORMULA_NUM
#define     IRS_FORMULA_LIMIT_START     1
#define     IRS_FORMULA_LIMIT_END       IRS_FORMULA_CFG_NUM
#define     IS_IRS_FORMULA_ID(PARAM)    ((PARAM) < IRS_FORMULA_CFG_NUM)


#define     IRS_PIN_1                   PA_0

#define     IRS_GPIO_MODE               GPIO_Mode_IPU
#define     IRS_GPIO_INIT_VALUE         Bit_SET

extern  IrsCatchRunAttr      IrsCatchAttr[IRS_CFG_USER_NUM];


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  void                    Irs_GpioInit                                    (void);

extern  void                    Irs_InitRunPara                                 (void);

extern  ErrorStatus             Irs_AddrAnalyzeInit                             (uc8 irsId);

extern  ErrorStatus             Irs_Reset                                       (uc8 irsId);
extern  ErrorStatus             Irs_SetCatchStat                                (uc8 irsId, IrsCatchStatTypedef sCatchStat);
extern  ErrorStatus             Irs_GetCatchStat                                (uc8 irsId, IrsCatchStatTypedef *gCatchStat);
extern  ErrorStatus             Irs_GetRunMode                                  (uc8 irsId, IrsRunModeTypedef *gRunMode);
extern  ErrorStatus             Irs_GetAddr                                     (uc8 irsId, RunAddrTypedef *gAddr);
extern  ErrorStatus             Irs_GetTriAddr                                  (uc8 irsId, RunAddrTypedef *gAddr);
extern  ErrorStatus             Irs_GetTriLevel                                 (uc8 irsId, u8 *gLevel);
extern  ErrorStatus             Irs_GetResetWaitFinishStat                      (uc8 irsId, FlagStatus *gStat);
extern  ErrorStatus             Irs_SetTimeCntStat                              (uc8 irsId, IrsTimeCntStatTypedef sStat, IrsTimeCntOptTypedef timOpt);

extern  ErrorStatus             Irs_IdelStatPoll                                (uc8 irsId);
extern  ErrorStatus             Irs_InitWaitStatPoll                            (uc8 irsId, FlagStatus gIrStat);
extern  ErrorStatus             Irs_OnePulseStatPoll                            (uc8 irsId, FlagStatus gIrStat);
extern  ErrorStatus             Irs_FewTimeHasStatPoll                          (uc8 irsId, FlagStatus gIrStat);
extern  ErrorStatus             Irs_MultTiemHasStatPoll                         (uc8 irsId, FlagStatus gIrStat);
extern  ErrorStatus             Irs_FewTimeNoneStatPoll                         (uc8 irsId, FlagStatus gIrStat);
extern  ErrorStatus             Irs_MultTiemNoneStatPoll                        (uc8 irsId, FlagStatus gIrStat);
extern  ErrorStatus             Irs_StatPoll                                    (uc8 irsId);

extern  void                    Irs_CatchPoll                                   (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __IRS_H__ */
