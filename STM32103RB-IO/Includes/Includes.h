/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Includes.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : 设备配置文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __INCLUDES_H__
#define __INCLUDES_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

#include "stm32f10x.h"
#include "stdio.h"
#include "string.h"
#include "main.h"
#include "CRC_CCITT.h"


#include "DataStructBase.h"         // 数据结构



#include "McuMemory.h"

#include "Random.h"
#include "Systick.h"
#include "DrivesConfig.h"
#include "Gpio.h"
#include "Queue.h"
#include "CAN_DataQueue.h"

#include "Tmm.h"
#include "TmmCnt.h"
#include "TmmExp.h"
#include "CnlCmd.h"

#include "Address_MMY.h"
#include "Address.h"

#include "Frame.h"

//#include "UART3_485.h"
#include "RFQueue.h"
#include "CSMA.h"
#include "SPI2.h"
#include "CC1101.h"

#include "CAN_Driver.h"
#include "CAN_Trsl.h"
#include "CAN_Packet.h"
#include "CAN_App.h"

#include "ADC.h"
#include "BH1621.h"
#include "BH1621_MMY.h"
#include "BH1621_App.h"

//#include "RunPara.h"
#include "VirtualAddr.h"
#include "Receive.h"

#include "Firmware.h"

#include "RF_Interface_Mmy.h"
#include "RF_Interface_Queue.h"
#include "RF_Interface_Logic.h"
#include "RF_App.h"


#include "Irs_MMY.h"
#include "Irs.h"
#include "Irs_Logic.h"

//#include "Addr.h"
//#include "Security.h"
//#include "Key.h"


//#include "CSMA.h"
//#include "SPI2.h"
//#include "Uart1.h"
//#include "CC1101.h"


#include "StatMtc_Logic.h"          // 状态维护逻辑操作文件

//#include "Relay.h"                  // 继电器操作头文件
//#include "Relay_MMY.h"              // 继电器的存储操作头文件
//#include "Relay_Logic.h"            // 继电器逻辑头文件

//#include "PWM.h"                    // PWM操作头文件
//#include "PWM_MMY.h"                // PWM的存储操作头文件
//#include "PWM_Logic.h"              // PWM逻辑头文件

//#include "Key.h"                    // Key操作头文件
//#include "Key_MMY.h"                // Key的存储操作头文件
//#include "Key_Logic.h"              // Key逻辑头文件

//#include "LED.h"                    // LED操作头文件
//#include "LED_MMY.h"                // LED的存储操作头文件
//#include "LED_Logic.h"              // LED逻辑头文件
#include "LED_Indicator.h"          // LED指示操作头文件

//#include "OutPut.h"
//#include "Input.h"


//#include "Scene.h"
//#include "Transmit.h"
//#include "App.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
extern  uc16 SystemUnitDefaultValue;

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/
/* MCU的SN基地址 */
#define     MCU_SN_ADDR_BASE                            0x1FFFF7E8


/* 判断指针是否为空 */
#define     IS_PARAM_POINTER_NOT_NULL(PARAM)            (((PARAM) != NULL))


#ifdef  USE_APP_FULL_ASSERT
#define assert_app_param(expr) ((expr) ? (void)0 : assert_app_failed((uint8_t *)__FILE__, __LINE__))
/* Exported functions ------------------------------------------------------- */
void assert_app_failed(uint8_t* file, uint32_t line);
#else
#define assert_app_param(expr) ((void)0)
#endif /* USE_APP_FULL_ASSERT */


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */



#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __INCLUDES_H__ */
