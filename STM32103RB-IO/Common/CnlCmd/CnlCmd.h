/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CnlCmd.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月7日
  最近修改   :
  功能描述   : CnlCmd.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __CNLCMD_H__
#define __CNLCMD_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    CNL_CMD_TYPE_OFF = 0,                       /* 关命令 */
    CNL_CMD_TYPE_ON,                            /* 开命令 */
    CNL_CMD_TYPE_RAMP_UP,                       /* 向上调光 */
    CNL_CMD_TYPE_RAMP_DOWN,                     /* 向下调光 */
    CNL_CMD_TYPE_RAMP_STOP,                     /* 停止调光 */
    CNL_CMD_TYPE_ENTER_AUTO,                    /* 进入自动模式 */
} CnlCmdTypedef;

#define     CNL_CMD_LONG_BYTE_LEN               6
#define     CNL_CMD_SHORT_BYTE_LEN              2

#define     CNL_STOP_CMD_CODE                   0x3F

#define     CNL_ENTER_AUTO_CMD_CODE             0x7F

// 控制命令 默认调光时间
#define     CNL_CMD_DEFAULT_RAMP_TIM            5



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus         CnlCmd_GetCnlBuff           (CnlCmdTypedef cnlType, u8 gBuff[], u8 *gLen);
extern  ErrorStatus         CnlCmd_GetAbsCnlBuff        (uc8 cnlValue, u8 gBuff[], u8 *gLen);
extern  ErrorStatus         CnlCmd_GetRampBuff          (uc8 cnlValue, uc8 tim, u8 gBuff[], u8 *gLen);
extern  FlagStatus          CnlCmd_IsAbsCnlCmd          (u8 cmdBuff[], uc8 cmdlen);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __CNLCMD_H__ */
