/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : CnlCmd.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月7日
  最近修改   :
  功能描述   : 控制命令 操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : CnlCmd_GetCnlBuff
 功能描述  : 获取控制命令
 输入参数  : CnlCmdTypedef cnlType
             u8 gBuff[]
             u8 *gLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CnlCmd_GetCnlBuff(CnlCmdTypedef cnlType, u8 gBuff[], u8 *gLen)
{
    u8 index = 0;

    if((gBuff == NULL)
       || (gLen == NULL))
    {
        return ERROR;
    }

    // 关命令
    if(cnlType == CNL_CMD_TYPE_OFF)
    {
        gBuff[index++] = 0;
        gBuff[index++] = 0;

        *gLen = index;

        return SUCCESS;
    }
    // 开命令
    else if(cnlType == CNL_CMD_TYPE_ON)
    {
        gBuff[index++] = 0xFF;
        gBuff[index++] = 0;

        *gLen = index;

        return SUCCESS;
    }
    // 向上调光
    else if(cnlType == CNL_CMD_TYPE_RAMP_UP)
    {
        gBuff[index++] = 0xFF;
        gBuff[index++] = 5;

        *gLen = index;

        return SUCCESS;
    }
    // 向下调光
    else if(cnlType == CNL_CMD_TYPE_RAMP_DOWN)
    {
        gBuff[index++] = 0;
        gBuff[index++] = 5;

        *gLen = index;

        return SUCCESS;
    }
    // 停止调光
    else if(cnlType == CNL_CMD_TYPE_RAMP_STOP)
    {
        gBuff[index++] = 0;
        gBuff[index++] = CNL_STOP_CMD_CODE;

        *gLen = index;

        return SUCCESS;
    }
    // 进入自动模式
    else if(cnlType == CNL_CMD_TYPE_ENTER_AUTO)
    {
        gBuff[index++] = 0;
        gBuff[index++] = CNL_ENTER_AUTO_CMD_CODE;

        *gLen = index;

        return SUCCESS;
    }
    else
    {
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : CnlCmd_GetAbsCnlBuff
 功能描述  : 获取绝对控制命令
 输入参数  : uc8 cnlValue
             u8 gBuff[]
             u8 *gLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CnlCmd_GetAbsCnlBuff(uc8 cnlValue, u8 gBuff[], u8 *gLen)
{
    u8 index = 0;

    if((gBuff == NULL)
       || (gLen == NULL))
    {
        return ERROR;
    }

    gBuff[index++] = cnlValue;
    gBuff[index++] = 0;

    *gLen = index;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CnlCmd_GetRampBuff
 功能描述  : 获取调光命令
 输入参数  : uc8 cnlValue
             uc8 tim
             u8 gBuff[]
             u8 *gLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus CnlCmd_GetRampBuff(uc8 cnlValue, uc8 tim, u8 gBuff[], u8 *gLen)
{
    u8 index = 0;

    if((gBuff == NULL)
       || (gLen == NULL))
    {
        return ERROR;
    }

    gBuff[index++] = cnlValue;
    gBuff[index++] = tim;

    *gLen = index;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CnlCmd_IsAbsCnlCmd
 功能描述  : 判断控制命令是否为 绝对控制命令
 输入参数  : u8 cmdBuff[]
             uc8 cmdlen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月14日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus CnlCmd_IsAbsCnlCmd(u8 cmdBuff[], uc8 cmdlen)
{
    if(cmdBuff == NULL)
    {
        return RESET;
    }

    if(cmdlen >= CNL_CMD_LONG_BYTE_LEN)
    {
        if(cmdBuff[2] != 0x00)
        {
            return RESET;
        }

        if(cmdBuff[3] != 0xFF)
        {
            return RESET;
        }

        if(cmdBuff[4] != 0xFF)
        {
            return RESET;
        }
    }
    else if(cmdlen >= CNL_CMD_SHORT_BYTE_LEN)
    {
        if(cmdBuff[1] != 0x00)
        {
            return RESET;
        }
    }
    else
    {
        return RESET;
    }

    return SET;
}


