#include "includes.h"

static  ErrorStatus     RFQueue_HeadInc       (RFSQueue *sQueue);
static  ErrorStatus     RFQueue_TailInc       (RFSQueue *sQueue);

/*****************************************************************************
 函 数 名  : IsRFQueueEmpty
 功能描述  : 判断队列是否为空
 输入参数  : RFSQueue *sQueue  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus IsRFQueueEmpty(RFSQueue *sQueue)
{
    if(sQueue != ((RFSQueue*)0))
    {
        if((sQueue->QHead) == (sQueue->QTail))
        {
            return  SET;
        }
        else
        {
            return RESET;
        }
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : RFQueue_IsNotEmpty
 功能描述  : 判断队列是否非空
 输入参数  : RFSQueue *sQueue  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus RFQueue_IsNotEmpty(RFSQueue *sQueue)
{
    if(sQueue != ((RFSQueue*)0))
    {
        if((sQueue->QHead) != (sQueue->QTail))
        {
            return  SET;
        }
        else
        {
            return RESET;
        }
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : RFQueue_Clr
 功能描述  : 清除队列
 输入参数  : RFSQueue *sQueue  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void RFQueue_Clr(RFSQueue *sQueue)
{
    u32 i = 0;

    if(sQueue != ((RFSQueue*)0))
    {
        for(i = 0; i<sQueue->QSize; i++)
        {
            sQueue->sFrame[i].len   = 0;
            sQueue->sFrame[i].index = 0;
        }
        sQueue->QHead = 0;
        sQueue->QTail = 0;
    }
}

/*****************************************************************************
 函 数 名  : RFQueue_HeadInc
 功能描述  : 队列头+1
 输入参数  : RFSQueue *sQueue  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus RFQueue_HeadInc(RFSQueue *sQueue)
{
    u32 headIndex = 0;

    if(sQueue != ((RFSQueue*)0))
    {
        headIndex = sQueue->QHead;

        headIndex++;
        headIndex %= (sQueue->QSize);
        sQueue->QHead = headIndex;

        if(sQueue->QHead == sQueue->QTail)
        {
            RFQueue_TailInc(sQueue);
        }
        return SUCCESS;
    }
    return ERROR;
}

/*****************************************************************************
 函 数 名  : RFQueue_TailInc
 功能描述  : 队列尾+1
 输入参数  : RFSQueue *sQueue  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus RFQueue_TailInc(RFSQueue *sQueue)
{
    u32 tailIndex = 0;

    if(sQueue != ((RFSQueue*)0))
    {
        tailIndex = sQueue->QTail;

        tailIndex++;
        tailIndex %= (sQueue->QSize);
        sQueue->QTail = tailIndex;
        return SUCCESS;
    }
    return ERROR;
}

/*****************************************************************************
 函 数 名  : RFQueue_Push
 功能描述  : 将消息压入队列
 输入参数  : RFSQueue *sQueue       
             const RFSFrame* frame  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RFQueue_Push(RFSQueue *sQueue, const RFSFrame* frame)
{
    u32 sFrameIndex = 0;
    u32 i = 0;

    if((sQueue != ((RFSQueue*)0)) && (frame != ((RFSFrame*)0)))
    {
        sFrameIndex = sQueue->QHead;

        if((sQueue->sFrame[sFrameIndex].size) >= (frame->len))
        {
            for(i = 0; i<frame->len; i++)
            {
                sQueue->sFrame[sFrameIndex].buff[i] = frame->buff[i];
            }

            sQueue->sFrame[sFrameIndex].len = frame->len;
            sQueue->sFrame[sFrameIndex].trmitPower = frame->trmitPower;
            RFQueue_HeadInc(sQueue);

            return SUCCESS;
        }
        else
        {
            return ERROR;
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : RFQueue_Pull
 功能描述  : 从队列中获取消息
 输入参数  : RFSQueue *sQueue  
             RFSFrame* frame   
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RFQueue_Pull(RFSQueue *sQueue, RFSFrame* frame)
{
    u32 getFramelen = 0;
    u32 tailIndex = 0;
    u32 i = 0;

    if((sQueue != ((RFSQueue*)0)) && (frame != ((RFSFrame*)0)))
    {
        if(RFQueue_IsNotEmpty(sQueue) == SET)
        {
            tailIndex = sQueue->QTail;

            getFramelen = sQueue->sFrame[tailIndex].len;

            if((getFramelen > 0) && (getFramelen <= frame->size))
            {
                for(i = 0; i<getFramelen; i++)
                {
                    frame->buff[i] = sQueue->sFrame[tailIndex].buff[i];
                }
                frame->len = getFramelen;
                frame->trmitPower = sQueue->sFrame[tailIndex].trmitPower;
            }

            RFQueue_TailInc(sQueue);

            if((frame->len == getFramelen) && (getFramelen != 0))
            {
                return SUCCESS;
            }
        }
    }
    return ERROR;
}



