#ifndef __RFQUEUE_H__
#define __RFQUEUE_H__

typedef enum
{
    CC1101_TRANSMIT_POWER_LOW = 0,          // -20dB
    CC1101_TRANSMIT_POWER_MINOR,            // -9.9dB
    CC1101_TRANSMIT_POWER_FLAT,             // -5.5dB
    CC1101_TRANSMIT_POWER_AVERAGE,          // +0.1dB
    CC1101_TRANSMIT_POWER_MID,              // +5.0dB 
    CC1101_TRANSMIT_POWER_NORMAL,           // +8.5dB
    CC1101_TRANSMIT_POWER_HIGHT,            // +10.6dB
} CC1101_TransmitPowerStruct;

typedef struct
{
    vu32 len;
    vu32 index;
    u32  size;
    u8   *buff;
    CC1101_TransmitPowerStruct trmitPower;
} RFSFrame;

typedef enum
{
    RF_STAT_NORMAL,
    RF_STAT_IDEL
} RFQueueStat;

typedef struct
{
    vu32 QHead;
    vu32 QTail;
    vu32 QSize;
    RFQueueStat QStat;
    RFSFrame *sFrame;
} RFSQueue;

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

extern  FlagStatus  IsRFQueueEmpty        (RFSQueue *sQueue);
extern  FlagStatus  RFQueue_IsNotEmpty    (RFSQueue *sQueue);
extern  void        RFQueue_Clr         (RFSQueue *sQueue);
extern  ErrorStatus RFQueue_HeadInc       (RFSQueue *sQueue);
extern  ErrorStatus RFQueue_TailInc       (RFSQueue *sQueue);
extern  ErrorStatus RFQueue_Push          (RFSQueue *sQueue, const RFSFrame *frame);
extern  ErrorStatus RFQueue_Pull          (RFSQueue *sQueue, RFSFrame *frame);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif /* __RFQUEUE_H__ */

