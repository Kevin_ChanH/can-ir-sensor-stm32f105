/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Queue.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : 队列操作文件
  函数列表   :
              Queue_Clr
              Queue_HeadInc
              Queue_IsEmpty
              Queue_IsNotEmpty
              Queue_Pull
              Queue_Push
              Queue_TailInc
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/
static  ErrorStatus     Queue_HeadInc       (SQueue *sQueue);
static  ErrorStatus     Queue_TailInc       (SQueue *sQueue);

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : Queue_IsEmpty
 功能描述  : 判断队列是否为空
 输入参数  : SQueue *sQueue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus Queue_IsEmpty(SQueue *sQueue)
{
    if(sQueue != ((SQueue*)0))
    {
        if((sQueue->QHead) == (sQueue->QTail))
        {
            return  SET;
        }
        else
        {
            return RESET;
        }
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : Queue_IsNotEmpty
 功能描述  : 判断队列是否为非空
 输入参数  : SQueue *sQueue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus Queue_IsNotEmpty(SQueue *sQueue)
{
    if(sQueue != ((SQueue*)0))
    {
        if((sQueue->QHead) != (sQueue->QTail))
        {
            return  SET;
        }
        else
        {
            return RESET;
        }
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : Queue_Clr
 功能描述  : 队列清空
 输入参数  : SQueue *sQueue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void Queue_Clr(SQueue *sQueue)
{
    u32 i = 0;

    if(sQueue != ((SQueue*)0))
    {
        for(i = 0; i<sQueue->QSize; i++)
        {
            sQueue->sFrame[i].len   = 0;
            sQueue->sFrame[i].index = 0;
        }
        sQueue->QHead = 0;
        sQueue->QTail = 0;
    }
}

/*****************************************************************************
 函 数 名  : Queue_HeadInc
 功能描述  : 队列头指向下一个单元
 输入参数  : SQueue *sQueue
 输出参数  : 无
 返 回 值  : ErrorStatus
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus Queue_HeadInc(SQueue *sQueue)
{
    u32 headIndex = 0;

    if(sQueue != ((SQueue*)0))
    {
        headIndex = sQueue->QHead;

        headIndex++;
        headIndex %= (sQueue->QSize);
        sQueue->QHead = headIndex;

        if(sQueue->QHead == sQueue->QTail)
        {
            Queue_TailInc(sQueue);
        }
        return SUCCESS;
    }
    return ERROR;
}

/*****************************************************************************
 函 数 名  : Queue_TailInc
 功能描述  : 队列尾指向下一个单元
 输入参数  : SQueue *sQueue
 输出参数  : 无
 返 回 值  : ErrorStatus
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus Queue_TailInc(SQueue *sQueue)
{
    u32 tailIndex = 0;

    if(sQueue != ((SQueue*)0))
    {
        tailIndex = sQueue->QTail;

        tailIndex++;
        tailIndex %= (sQueue->QSize);
        sQueue->QTail = tailIndex;
        return SUCCESS;
    }
    return ERROR;
}

/*****************************************************************************
 函 数 名  : Queue_Push
 功能描述  : 队列压入单元
 输入参数  : SQueue *sQueue
             const SFrame* frame
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Queue_Push(SQueue *sQueue, const SFrame* frame)
{
    u32 sFrameIndex = 0;
    u32 i = 0;

    if((sQueue != ((SQueue*)0)) && (frame != ((SFrame*)0)))
    {
        sFrameIndex = sQueue->QHead;

        if((sQueue->sFrame[sFrameIndex].size) >= (frame->len))
        {
            for(i = 0; i<frame->len; i++)
            {
                sQueue->sFrame[sFrameIndex].buff[i] = frame->buff[i];
            }

            sQueue->sFrame[sFrameIndex].len = frame->len;
            Queue_HeadInc(sQueue);

            return SUCCESS;
        }
        else
        {
            return ERROR;
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : Queue_Pull
 功能描述  : 从队列中取出单元数据
 输入参数  : SQueue *sQueue
             SFrame* frame
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Queue_Pull(SQueue *sQueue, SFrame* frame)
{
    u32 getFramelen = 0;
    u32 tailIndex = 0;
    u32 i = 0;

    if((sQueue != ((SQueue*)0)) && (frame != ((SFrame*)0)))
    {
        if(Queue_IsNotEmpty(sQueue) == SET)
        {
            tailIndex = sQueue->QTail;

            getFramelen = sQueue->sFrame[tailIndex].len;

            if((getFramelen > 0) && (getFramelen <= frame->size))
            {
                for(i = 0; i<getFramelen; i++)
                {
                    frame->buff[i] = sQueue->sFrame[tailIndex].buff[i];
                }
                frame->len = getFramelen;
            }

            Queue_TailInc(sQueue);

            if((frame->len == getFramelen) && (getFramelen != 0))
            {
                return SUCCESS;
            }
        }
    }
    return ERROR;
}



