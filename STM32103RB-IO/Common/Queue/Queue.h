/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Queue.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : Queue.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __QUEUE_H__
#define __QUEUE_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef struct
{
    vu32 len;
    vu32 index;
    u32  size;
    u8   *buff;
} SFrame;

typedef enum
{
    STAT_NORMAL,
    STAT_IDEL
} QueueStat;

typedef struct
{
    vu32 QHead;
    vu32 QTail;
    vu32 QSize;
    QueueStat QStat;
    SFrame *sFrame;
} SQueue;

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

extern  FlagStatus      Queue_IsEmpty                   (SQueue *sQueue);
extern  FlagStatus      Queue_IsNotEmpty                (SQueue *sQueue);
extern  void            Queue_Clr                       (SQueue *sQueue);
extern  ErrorStatus     Queue_HeadInc                   (SQueue *sQueue);
extern  ErrorStatus     Queue_TailInc                   (SQueue *sQueue);
extern  ErrorStatus     Queue_Push                      (SQueue *sQueue, const SFrame *frame);
extern  ErrorStatus     Queue_Pull                      (SQueue *sQueue, SFrame *frame);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif /* __QUEUE_H__ */

