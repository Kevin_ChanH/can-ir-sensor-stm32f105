/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : TmmExp.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : TmmExp.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __TMMEXP_H__
#define __TMMEXP_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


typedef enum
{
    TMMEXP_STAT_OFF = 0,
    TMMEXP_STAT_ON = 1
} TmmExp_Stat;

typedef struct
{
    TmmExp_Stat TmmExpStat;
    u8 TmmHour;
    u8 TmmMinute;
    u8 TmmSecond;
} TmmExp_Typedef;

typedef struct
{
    u8 TmmHour;
    u8 TmmMintue;
    u8 TmmSecond;
} TmmExp_ParaTypedef;

typedef enum
{
    TMM_EXP_TIME1 = 0,
    TMM_EXP_APP_LEDTEST,

    TMM_EXP_VIRTUAL_START,

    TMM_EXP_IRS_NONE_START = (TMM_EXP_VIRTUAL_START + VIRTUAL_ADDR_LEN),

    TMM_EXP_IRS_HAS_START = (TMM_EXP_IRS_NONE_START + MCU_IRS_CHANNEL_NUM),

    TMM_EXP_MAX_NUM = (TMM_EXP_IRS_HAS_START + MCU_IRS_CHANNEL_NUM),

} TmmExp_ID;

#define TMM_EXP_ONE_SEC_FOR_MS    1000

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

extern  ErrorStatus TmmExp_GetExpPara           (uc16 sPara, TmmExp_ParaTypedef *gPara);
extern  void        TmmExp_Load                 (TmmExp_ID tmmExpId, TmmExp_ParaTypedef sTmmExp);
extern  FlagStatus  TmmExp_IsON                 (TmmExp_ID tmmExpId);
extern  ErrorStatus TmmExp_Clr                  (TmmExp_ID tmmExpId);
extern  ErrorStatus TmmExp_Copy                 (TmmExp_ID bTid, TmmExp_ID nTid);
extern  void        TmmExp_Init                 (void);
extern  void        TmmExp_Poll                 (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif /* __TMMEXP_H__ */

