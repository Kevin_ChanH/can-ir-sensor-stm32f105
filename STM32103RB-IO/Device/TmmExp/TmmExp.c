/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : TmmExp.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : 扩展定时器 操作文件
  函数列表   :
              TmmExp_Impl
              TmmExp_Init
              TmmExp_IsON
              TmmExp_Load
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static TmmExp_Typedef sTmmExp[TMM_EXP_MAX_NUM];

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : TmmExp_GetExpPara
 功能描述  : 扩展定时器 根据u16的值 获取各项参数
 输入参数  : uc16 sPara
             TmmExp_ParaTypedef *gPara
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus TmmExp_GetExpPara(uc16 sPara, TmmExp_ParaTypedef *gPara)
{
    u16 tmpValue;

    if(gPara == NULL)
    {
        return ERROR;
    }

    tmpValue = (sPara & 0x003F);
    if(tmpValue < 60)
    {
        gPara->TmmSecond = tmpValue;
    }
    else
    {
        return ERROR;
    }

    tmpValue = (sPara & 0x0FC0);
    tmpValue >>= 6;
    if(tmpValue < 60)
    {
        gPara->TmmMintue = tmpValue;
    }
    else
    {
        return ERROR;
    }


    tmpValue = sPara;
    tmpValue >>= 12;
    gPara->TmmHour = tmpValue;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : TmmExp_Load
 功能描述  : 载入Tmm扩展时间
 输入参数  : TmmExp_ID tmmExpId
             TmmExp_ParaTypedef sTmmExp
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年4月25日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void TmmExp_Load(TmmExp_ID tmmExpId, TmmExp_ParaTypedef tmmExp)
{
    if((tmmExp.TmmHour < 60) && (tmmExp.TmmMintue < 60) && (tmmExp.TmmSecond < 60))
    {
        sTmmExp[tmmExpId].TmmHour = tmmExp.TmmHour;
        sTmmExp[tmmExpId].TmmMinute = tmmExp.TmmMintue;
        sTmmExp[tmmExpId].TmmSecond = tmmExp.TmmSecond;

        if((tmmExp.TmmHour == 0)
        && (tmmExp.TmmMintue == 0)
        && (tmmExp.TmmSecond == 0))
        {
            sTmmExp[tmmExpId].TmmSecond = 1;
        }

        sTmmExp[tmmExpId].TmmExpStat = TMMEXP_STAT_ON;
    }
}

/*****************************************************************************
 函 数 名  : TmmExp_IsON
 功能描述  : 扩展定时器 判断是否运行
 输入参数  : TmmExp_ID tmmExpId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus TmmExp_IsON(TmmExp_ID tmmExpId)
{
    if(tmmExpId >= TMM_EXP_MAX_NUM)
    {
        return RESET;
    }

    if(sTmmExp[tmmExpId].TmmExpStat == TMMEXP_STAT_ON)
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : TmmExp_Clr
 功能描述  : 扩展定时器 清除定时器参数
 输入参数  : TmmExp_ID tmmExpId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年11月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus TmmExp_Clr(TmmExp_ID tmmExpId)
{
    if(tmmExpId >= TMM_EXP_MAX_NUM)
    {
        return ERROR;
    }

    sTmmExp[tmmExpId].TmmExpStat = TMMEXP_STAT_OFF;
    sTmmExp[tmmExpId].TmmHour    = 0;
    sTmmExp[tmmExpId].TmmMinute  = 0;
    sTmmExp[tmmExpId].TmmSecond  = 0;

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : TmmExp_Init
 功能描述  : 初始化 扩展定时器
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void TmmExp_Init(void)
{
    u8 i;

    for(i = 0; i<TMM_EXP_MAX_NUM; i++)
    {
        sTmmExp[i].TmmExpStat = TMMEXP_STAT_OFF;
        sTmmExp[i].TmmHour = 0;
        sTmmExp[i].TmmMinute = 0;
        sTmmExp[i].TmmSecond = 0;
    }
    Tmm_LoadMs(TMM_TIMER_EXP, TMM_EXP_ONE_SEC_FOR_MS);
}

/*****************************************************************************
 函 数 名  : TmmExp_Copy
 功能描述  : 复制扩展定时器
 输入参数  : TmmExp_ID bTid
             TmmExp_ID nTid
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus TmmExp_Copy(TmmExp_ID bTid, TmmExp_ID nTid)
{
    if((bTid != nTid)
        &&(bTid < TMM_EXP_MAX_NUM)
        &&(nTid < TMM_EXP_MAX_NUM))
    {
        sTmmExp[nTid].TmmExpStat = sTmmExp[bTid].TmmExpStat;
        sTmmExp[nTid].TmmHour    = sTmmExp[bTid].TmmHour;
        sTmmExp[nTid].TmmMinute  = sTmmExp[bTid].TmmMinute;
        sTmmExp[nTid].TmmSecond  = sTmmExp[bTid].TmmSecond;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : TmmExp_Poll
 功能描述  : 扩展定时器 执行函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void TmmExp_Poll(void)
{
    u8 i;

    if(Tmm_IsON(TMM_TIMER_EXP) != SET)
    {
        Tmm_LoadMs(TMM_TIMER_EXP, TMM_EXP_ONE_SEC_FOR_MS);

        for(i = 0; i<TMM_EXP_MAX_NUM; i++)
        {
            if(sTmmExp[i].TmmExpStat == TMMEXP_STAT_ON)
            {
                if(sTmmExp[i].TmmSecond == 0)
                {
                    if(sTmmExp[i].TmmMinute == 0)
                    {
                        if(sTmmExp[i].TmmHour == 0)
                        {
                            sTmmExp[i].TmmExpStat = TMMEXP_STAT_OFF;
                        }
                        else
                        {
                            sTmmExp[i].TmmHour--;
                            sTmmExp[i].TmmMinute = 59;
                            sTmmExp[i].TmmSecond = 59;
                        }
                    }
                    else
                    {
                        sTmmExp[i].TmmMinute--;
                        sTmmExp[i].TmmSecond = 59;
                    }
                }
                else
                {
                    sTmmExp[i].TmmSecond--;
                }
            }
        }
    }
}

