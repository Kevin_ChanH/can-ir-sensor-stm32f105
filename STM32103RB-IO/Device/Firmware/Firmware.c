/******************************************************************************

                  版权所有 (C), 2013-2017, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Firmware.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年6月17日
  最近修改   :
  功能描述   : 固件信息操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年6月17日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
// 固件名称
sc8 FirmwareNameStr[] __attribute__((at(FIRMWARE_NAME_ADDR))) = FIRMWARE_NAME;
// 固件版本号
uc8 FirmwareVersion[] __attribute__((at(FIRMWARE_VERSION_ADDR))) = {SYSTEM_FIRMWARE_MAIN, SYSTEM_FIRMWARE_SUB};

// 固件生成日期
sc8 FirmwareReleaseDateStr[] __attribute__((at(FIRMWARE_DATE_ADDR))) = __DATE__;
// 固件生成时间
sc8 FirmwareReleaseTimeStr[] __attribute__((at(FIRMWARE_TIME_ADDR))) = __TIME__;

// 固件作者名称
sc8 FirmwareAuthorStr[] __attribute__((at(FIRMWARE_AUTHOR_ADDR))) = FIRMWARE_AUTHOR;

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : FirmwareResponse
 功能描述  : 反馈固件信息
 输入参数  : FrameCfgTypedef *gCfgFrame
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年6月17日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus FirmwareResponse(FrameCfgTypedef *gCfgFrame)
{
    CAN_PacketBase rAddrMsg;
    u8 sIndex = 0;
    u32 sId = 0;
    u8 tmpByte = 0;
    u8 i = 0;

    // 获取CMD字节
    tmpByte = (FRAME_CMD_BIT_FRAME | FRAME_CMD_BIT_DIR | FRAME_CMD_BIT_REG | FRAME_CMD_BIT_OPT | FRAME_CMD_SRC_MAC);

    // 载入SN地址
    Addr_GetSnAddr(&(rAddrMsg.PacketBuff[sIndex]));
    // 偏移数据下标
    sIndex += SN_MMY_PARA_LEN;

    if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_MAC)
    {
        tmpByte |= FRAME_CMD_DES_MAC;
        for(i=0; i<SYSTEM_SN_APP_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else if(gCfgFrame->CmdAttr.SrcAddr.Type == CMD_ADDR_TYPE_UNIT)
    {
        tmpByte |= FRAME_CMD_DES_UNIT;
        for(i=0; i<SYSTEM_UNIT_ADDR_LEN; i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = gCfgFrame->CmdAttr.SrcAddr.AddrBuff[i];
        }
    }
    else
    {
        return ERROR;
    }

    sId |= (u32)(tmpByte << 16);
    // 载入数据包ID
    rAddrMsg.PacketID = sId;

    // 载入对应的寄存器
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.TypeReg);
    // 对应的场景通道号
    rAddrMsg.PacketBuff[sIndex++] = (u8)(gCfgFrame->RegAttr.DetailedReg);

    // 判断是否获取所有关键的固件信息
    if(gCfgFrame->RegAttr.DetailedReg == CFG_FIRMWARE_REG_DETAILED_ALL)
    {
        // 反馈固件的参与寄存器
        rAddrMsg.PacketBuff[sIndex++] = 0x00;
        rAddrMsg.PacketBuff[sIndex++] = 0x00;
        rAddrMsg.PacketBuff[sIndex++] = 0x00;
        rAddrMsg.PacketBuff[sIndex++] = 0x03;

        // 获取固件名称长度
        rAddrMsg.PacketBuff[sIndex++] = sizeof(FirmwareNameStr);
        // 循环获取固件名称元素
        for(i=0; i<(sizeof(FirmwareNameStr)); i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = FirmwareNameStr[i];
        }

        // 获取固件版本号长度
        rAddrMsg.PacketBuff[sIndex++] = sizeof(FirmwareVersion);
        // 循环获取固件名称元素
        for(i=0; i<(sizeof(FirmwareVersion)); i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = FirmwareVersion[i];
        }
    }
    // 判断是否获取固件名称
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_FIRMWARE_REG_DETAILED_NAME)
    {
        // 获取固件名称长度
        rAddrMsg.PacketBuff[sIndex++] = sizeof(FirmwareNameStr);
        // 循环获取固件名称元素
        for(i=0; i<(sizeof(FirmwareNameStr)); i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = FirmwareNameStr[i];
        }
    }
    // 判断是否获取固件版本号
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_FIRMWARE_REG_DETAILED_VERSION)
    {
        // 获取固件版本号长度
        rAddrMsg.PacketBuff[sIndex++] = sizeof(FirmwareVersion);
        // 循环获取固件名称元素
        for(i=0; i<(sizeof(FirmwareVersion)); i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = FirmwareVersion[i];
        }
    }
    // 判断是否获取固件生成日期
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_FIRMWARE_REG_DETAILED_TIME)
    {
        // 获取固件生成日期
        rAddrMsg.PacketBuff[sIndex++] = sizeof(FirmwareReleaseDateStr);
        // 循环获取固件生成日期
        for(i=0; i<(sizeof(FirmwareReleaseDateStr)); i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = FirmwareReleaseDateStr[i];
        }
    }
    // 判断是否获取固件作者名称
    else if(gCfgFrame->RegAttr.DetailedReg == CFG_FIRMWARE_REG_DETAILED_AUTHOR)
    {
        // 获取固件作者名称
        rAddrMsg.PacketBuff[sIndex++] = sizeof(FirmwareAuthorStr);
        // 循环获取固件作者名称
        for(i=0; i<(sizeof(FirmwareAuthorStr)); i++)
        {
            rAddrMsg.PacketBuff[sIndex++] = FirmwareAuthorStr[i];
        }
    }
    else
    {
        return ERROR;
    }

    // 载入数据长度
    rAddrMsg.PacketLen = sIndex;

    // 反馈读取信息
    return CAN_PacketSend(&CanStatPara, &rAddrMsg);
}

/*****************************************************************************
 函 数 名  : Firmware_GetVersion
 功能描述  : 获取固件版本号
 输入参数  : u8 gVersionBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年7月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Firmware_GetVersion(u8 gVersionBuff[])
{
    u8 i = 0;

    // 循环获取固件名称元素
    for(i=0; i<(sizeof(FirmwareVersion)); i++)
    {
        gVersionBuff[i] = FirmwareVersion[i];
    }

    return SUCCESS;
}

