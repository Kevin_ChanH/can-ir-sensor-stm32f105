/******************************************************************************

                  版权所有 (C), 2013-2014, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Firmware.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年6月17日
  最近修改   :
  功能描述   : Firmware.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年6月17日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __FIRMWARE_H__
#define __FIRMWARE_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

#define FIRMWARE_MMY_DATA_SIZE  0x40

#define FIRMWARE_NAME_ADDR          (MCU_FIRMWARE_DEFAULT_MMY_START_ADDR + (FIRMWARE_MMY_DATA_SIZE * 0))
#define FIRMWARE_VERSION_ADDR       (MCU_FIRMWARE_DEFAULT_MMY_START_ADDR + (FIRMWARE_MMY_DATA_SIZE * 1))
#define FIRMWARE_DATE_ADDR          (MCU_FIRMWARE_DEFAULT_MMY_START_ADDR + (FIRMWARE_MMY_DATA_SIZE * 2))
#define FIRMWARE_TIME_ADDR          (MCU_FIRMWARE_DEFAULT_MMY_START_ADDR + (FIRMWARE_MMY_DATA_SIZE * 3))
#define FIRMWARE_AUTHOR_ADDR        (MCU_FIRMWARE_DEFAULT_MMY_START_ADDR + (FIRMWARE_MMY_DATA_SIZE * 4))

#if (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_DIMMER4_16)
#define FIRMWARE_NAME   "IDP-RGDM-410"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_RELAY4_16A)
#define FIRMWARE_NAME   "IDP-RGOE-416"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_RELAY4_10A)
#define FIRMWARE_NAME   "IDP-RGOE-410"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_CURTAIN4)
#define FIRMWARE_NAME   "IDP-RGCC-410"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_INTERFACE)
#define FIRMWARE_NAME   "IDP-RGIPG"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_IO6)
#define FIRMWARE_NAME   "IDP-RGIO-6"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_IO16)
#define FIRMWARE_NAME   "IDP-RGIO-15"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_IR_SENSOR)
#define FIRMWARE_NAME   "IDP-RGIR-SENSOR"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_IR_REMOTE)
#define FIRMWARE_NAME   "IDP-RGIR-REMOTE"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_RELAY32_10A)
#define FIRMWARE_NAME   "IDP-RGOE-3210"
#elif (SYSTEM_DEVICE_TYPE == SYSTEM_DEVICE_KEY3)
#define FIRMWARE_NAME   "IDP-RGKB-3"
#else
#define FIRMWARE_NAME   "IDP-RGDM-410"
#endif

#define FIRMWARE_AUTHOR     "KelvinChan"


extern  uc8 FirmwareVersion[2];

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus     FirmwareResponse                        (FrameCfgTypedef *gCfgFrame);

extern  ErrorStatus     Firmware_GetVersion                     (u8 gVersionBuff[]);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __FIRMWARE_H__ */
