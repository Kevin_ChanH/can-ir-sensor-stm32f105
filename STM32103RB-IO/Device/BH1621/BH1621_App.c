/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : BH1621_App.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月27日
  最近修改   :
  功能描述   : BH1621的应用操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月27日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
BeamAttrTypedef BeamFormulaAttr[BEAM_FORMULA_CFG_NUM];

u16 Illuminance;

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

ErrorStatus BeamApp_Init(void)
{
    // 初始化运行参数
    BeamMmy_RunParaInit();

    // 初始化复合公式触发参数
    BeamApp_FormulaTirggerAttrInit();   

    return SUCCESS;
}

ErrorStatus BeamApp_FormulaTirggerAttrInit(void)
{
    u8 i = 0;
    u8 BeamFormulaBuff[BEAM_FORMULA_MMY_PARA_LEN];

    // 循环获取复合公式的运行参数
    for(i=0; i<BEAM_FORMULA_CFG_NUM; i++)
    {
        // 从FLASH处获取复合公式数据
        if(BeamMmy_GetParaFromMMY((BeamMmyType)(BEAM_MMY_TYPE_FORMULA_START+i), BeamFormulaBuff) != SUCCESS)
        {
            // 从默认数据处获取复合公式数据
            BeamMmy_GetDefaultPara((BeamMmyType)(BEAM_MMY_TYPE_FORMULA_START+i), BeamFormulaBuff);

            // 设置默认参数到FLASH
            BeamMmy_SetDefaultToMMY((BeamMmyType)(BEAM_MMY_TYPE_FORMULA_START+i));
        }

        // 解析复合公式数据
        BeamMmy_GetFormulaTriggerPara(BeamFormulaBuff, &(BeamFormulaAttr[i].TriAttr));

        // 设置照度趋势为空闲模式
        BeamFormulaAttr[i].Trend = BEAM_TREND_EXCEED_IDLE;
    }

    return SUCCESS;
}

ErrorStatus BeamApp_SetFormulaTread(uc8 sFormulaCh, BeamTrendTypedef sTrend)
{
    // 判断复合公式通道是否正常
    if(IS_BEAM_FORMULA_ID(sFormulaCh) != SET)
    {
        return ERROR;
    }
    
    // 判断趋势是否正常
    if(IS_BEAM_TREND_TYPE(sTrend) != SET)
    {
        return ERROR;
    }

    // 设置趋势
    BeamFormulaAttr[sFormulaCh].Trend = sTrend;

    return SUCCESS;
}

BeamTrendTypedef BeamApp_GetFormulaTread(uc8 sFormulaCh)
{
    // 判断复合公式通道是否正常
    if(IS_BEAM_FORMULA_ID(sFormulaCh) != SET)
    {
        // 返回空闲趋势
        return BEAM_TREND_EXCEED_IDLE;
    }

    // 返回趋势
    return BeamFormulaAttr[sFormulaCh].Trend;
}

ErrorStatus BeamApp_FormulaExcuted(uc8 sFormulaCh, BeamFormulaExEventTypedef pEven)
{
    u8 BeamFormulaBuff[BEAM_FORMULA_MMY_PARA_LEN];
    u8 gIndex = BEAM_FORMULA_MMY_EXECUTION_START_INDEX;
    BeamMmyType gMmyType;
    BeamFormulaExecutionTypedef tmpExAttr;

    FrameTypedef sFrame;
    CAN_DataBase    sDb;
    AddrAttrTypedef desAddr;
    
    // 判断复合公式通道是否正常
    if(IS_BEAM_FORMULA_ID(sFormulaCh) != SET)
    {
        // 返回空闲趋势
        return ERROR;
    }

    // 判断执行事件是否正常
    if(IS_BEAM_EX_EVENT_TYPE(pEven) != SET)
    {
        return ERROR;
    }

    // 获取存储类型
    gMmyType = (BeamMmyType)(BEAM_MMY_TYPE_FORMULA_START + sFormulaCh);

    // 获取参数
    if(BeamMmy_GetParaFromMMY(gMmyType, BeamFormulaBuff) != SUCCESS)
    {
        if(BeamMmy_GetDefaultPara(gMmyType, BeamFormulaBuff) != SUCCESS)
        {
            return ERROR;
        }
    }

    // 判断是否为最小事件
    if(pEven == BEAM_FORMULA_EXECUTION_MIN)
    {
        gIndex += BEAM_FORMULA_MMY_EXECUTION_PARA_LEN;
    }

    // 判断获取执行参数是否成功
    if(BeamMmy_GetFormulaExecutionPara(&(BeamFormulaBuff[gIndex]), &tmpExAttr) == SUCCESS)
    {
        if(tmpExAttr.IsActive == SET)
        {
            // 地址类型设为 APP+GROUP
            desAddr.Type = CMD_ADDR_TYPE_APP;
            // 载入目的地址 APP
            desAddr.AddrBuff[0] = tmpExAttr.ExecutionAddr.App;
            // 载入目的地址 GROUP
            desAddr.AddrBuff[1] = tmpExAttr.ExecutionAddr.Group;

            // 载入控制命令到本模块的虚拟地址命令内
            VirtualAddr_VaLoadCmd(tmpExAttr.ExecutionAddr, tmpExAttr.CmdBuff, tmpExAttr.CmdLen);
                
            // 载入控制命令 获取控制帧
            if(Frame_GetSingleCnlFrame(desAddr, tmpExAttr.CmdBuff, tmpExAttr.CmdLen, &sFrame) == SUCCESS)
            {
                // 获取CAN控制帧
                if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
                {
                    // 载入控制帧到发送队列
                    CAN_SendDbMsg(&sDb);

                    // 载入控制命令
                    ItfLogic_LoadCanCmd(&sDb);
                }
                else
                {
                    return ERROR;
                }
            }
            else
            {
                return ERROR;
            }
        }
    }

    return SUCCESS;
}

ErrorStatus BeamApp_FormulaChannelPoll(uc8 sFormulaCh, u16 illuminance)
{
    u16 illMax = 0;
    u16 illMin = 0;
    
    // 判断复合公式通道是否正常
    if(IS_BEAM_FORMULA_ID(sFormulaCh) != SET)
    {
        return ERROR;
    }

    // 判断复合公式是否激活
    if(BeamFormulaAttr[sFormulaCh].TriAttr.IsActive == SET)
    {
        // 最大照度
        illMax = BeamFormulaAttr[sFormulaCh].TriAttr.TargetValue + BeamFormulaAttr[sFormulaCh].TriAttr.RangeValue;

        // 判断照度是否复合范围
        if(BeamFormulaAttr[sFormulaCh].TriAttr.TargetValue >= BeamFormulaAttr[sFormulaCh].TriAttr.RangeValue)
        {
            illMin = BeamFormulaAttr[sFormulaCh].TriAttr.TargetValue - BeamFormulaAttr[sFormulaCh].TriAttr.RangeValue;
        }
        else
        {
            illMin = 0;
        }

        // 判断之前的趋势是IDLE 或者 最小趋势
        if((BeamFormulaAttr[sFormulaCh].Trend == BEAM_TREND_EXCEED_IDLE) || 
           (BeamFormulaAttr[sFormulaCh].Trend == BEAM_TREND_EXCEED_MIN))
        {
            // 判断是否触发最大值
            if(illuminance > illMax)
            {
                // 执行最大触发事件
                BeamApp_FormulaExcuted(sFormulaCh, BEAM_FORMULA_EXECUTION_MAX);

                // 设置为最大值趋势
                BeamApp_SetFormulaTread(sFormulaCh, BEAM_TREND_EXCEED_MAX);
            }
        }
        
        // 判断之前的趋势是IDLE 或者最大趋势
        if((BeamFormulaAttr[sFormulaCh].Trend == BEAM_TREND_EXCEED_IDLE) || 
           (BeamFormulaAttr[sFormulaCh].Trend == BEAM_TREND_EXCEED_MAX))
        {
            // 判断是否触发最大值
            if(illuminance < illMin)
            {
                // 执行最小触发事件
                BeamApp_FormulaExcuted(sFormulaCh, BEAM_FORMULA_EXECUTION_MIN);

                // 设置为最小值趋势
                BeamApp_SetFormulaTread(sFormulaCh, BEAM_TREND_EXCEED_MIN);
            }
        }
    }

    return SUCCESS;
}

#define H_GAIN_VALUE        (float)(0.6 * 51)
#define L_GAIN_VALUE        (float)(0.063 * 51)

ErrorStatus BeamApp_GetIlluminance(uc16 iValue, u16 *gIllum)
{
    float Tmpviout;
    u16   tIllum;
    
    Tmpviout = (iValue * (float)(3.3));
    Tmpviout = (Tmpviout/4095);

    // 获取到上面的数据
    Tmpviout = (Tmpviout * 10000);
    
    if(BH1621Attr.Mode == BH1621_MODE_H_GAIN)
    {
        // 获取数据
        Tmpviout = (Tmpviout / H_GAIN_VALUE);
    }
    else if(BH1621Attr.Mode == BH1621_MODE_L_GAIN)
    {
        // 获取数据
        Tmpviout = (Tmpviout / L_GAIN_VALUE);
    }
    else 
    {
        return ERROR;
    }

    // 获取数据
    tIllum = (u16)Tmpviout;
    
    // 载入数据
    *gIllum = tIllum;

    return SUCCESS;
}

ErrorStatus BeamApp_UpdateIlluminance(u16 *gIllum)
{
    u16 tmpBh1621Value;
        
    if(BH1621Attr.HasNew == SET)
    {
        BH1621Attr.HasNew = RESET;

        tmpBh1621Value = BH1621Attr.BH1621Value;

        // 获取照度
        if(BeamApp_GetIlluminance(tmpBh1621Value, gIllum) == SUCCESS)
        {
            return SUCCESS;
        }       
    }

    return ERROR;
}

u16 BeamApp_GetIlluminanceVaule(void)
{
    return Illuminance;
}

ErrorStatus BeamApp_FormulaPoll(uc16 tIllum)
{
    u8 i;

    // 轮询所有复合公式
    for(i=0; i<BEAM_FORMULA_CFG_NUM; i++)
    {
        BeamApp_FormulaChannelPoll(i, tIllum);
    }

    return SUCCESS;
}

ErrorStatus BeamApp_Poll(void)
{
    
    u16 tIllum;

    // 获取新的照度值
    if(BeamApp_UpdateIlluminance(&tIllum) == SUCCESS)
    {
        // 执行复合公式
        BeamApp_FormulaPoll(tIllum);

        // 保存照度值
        Illuminance = tIllum;
    }

    return SUCCESS;
}



