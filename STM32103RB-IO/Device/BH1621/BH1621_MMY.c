/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : BH1621_MMY.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月27日
  最近修改   :
  功能描述   : BH1621的存储操作命令
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月27日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static BeamOptMmyTypedef BeamOptMmyPara;

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
// 复合公式 默认参数
static uc8  BeamFormulaMmyParaDefault[BEAM_FORMULA_CFG_NUM][BEAM_FORMULA_MMY_PARA_LEN] __attribute__((at(MCU_BEAM_FORMULA_DEFAULT_MMY_ADDR))) =
{
    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },

    {
        0x00, 0x00, 0x00, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
        0x02, 0xFF, 0xFF, 0x00, 0x00,                       \
    },
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

FlagStatus BeamMmy_IsMmyType(BeamMmyType mmyType)
{
    if(mmyType < BEAM_MMY_TYPE_MAX)
    {
        return SET;
    }

    return RESET;
}

FlagStatus  BeamMmy_IsFormulaIndex(u8 tIndex)
{
    if(IS_BEAM_FORMULA_ID(tIndex) == SET)
    {
        return SET;
    }
    
    return RESET;  
}

ErrorStatus BeamMmy_FormulaOrganizeInit(uc8 formulaNum)
{
    assert_app_param(IS_BEAM_MMY_FORMULA_TYPE(formulaNum));

    // FLASH起始地址
    BeamOptMmyPara.OptMmyPara[formulaNum].BeamMmyStartAddr   = (BEAM_FORMULA_MMY_ADDR_START + ((formulaNum - BEAM_MMY_TYPE_FORMULA_START) * BEAM_FORMULA_MMY_CFG_MSG_SIZE));
    // 存储类型
    BeamOptMmyPara.OptMmyPara[formulaNum].MmyDatType        = BEAM_FORMULA_MMY_DATA_TYPE;
    // 数据长度
    BeamOptMmyPara.OptMmyPara[formulaNum].BeamMmyOptParaLen  = BEAM_FORMULA_MMY_PARA_LEN;
    // 存储大小
    BeamOptMmyPara.OptMmyPara[formulaNum].BeamMmyOptParaSize = BEAM_FORMULA_MMY_CFG_MSG_SIZE;
    // 默认参数的数据指针
    BeamOptMmyPara.OptMmyPara[formulaNum].DefaultRamAddr    = BeamFormulaMmyParaDefault[formulaNum - BEAM_MMY_TYPE_FORMULA_START];

    return SUCCESS;
}

void BeamMmy_OrganizeInit(void)
{
    u8 tmpBeamFormulaId;

    // 轮询所有的Formula架构
    for(tmpBeamFormulaId=0; tmpBeamFormulaId<BEAM_FORMULA_CFG_NUM; tmpBeamFormulaId++)
    {
        // Formula架构
        BeamMmy_FormulaOrganizeInit(BEAM_MMY_TYPE_FORMULA_START + tmpBeamFormulaId);
    }
}

ErrorStatus BeamMmy_GetParaFromMMY(BeamMmyType mmyType, u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr;
    u8          gParaLen;
    u8          i;
    
    assert_app_param(IS_BEAM_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));


    // 获取 存储类型
    gMmyDat.Type = BeamOptMmyPara.OptMmyPara[mmyType].MmyDatType;

    // 获取起始地址
    gMmyAddr = BeamOptMmyPara.OptMmyPara[mmyType].BeamMmyStartAddr;

    // 获取FLASH数据
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    // 获取数据长度
    gParaLen = BeamOptMmyPara.OptMmyPara[mmyType].BeamMmyOptParaLen;

    // 复制数据
    for(i = 0; i<gParaLen; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

ErrorStatus BeamMmy_SetParaToMMY(BeamMmyType mmyType, uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr;
    u8          sParaLen;
    u8          sParaSize;
    u8          i;

    assert_app_param(IS_BEAM_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));


    // 获取 存储类型
    sMmyDat.Type = BeamOptMmyPara.OptMmyPara[mmyType].MmyDatType;

    // 获取起始地址
    sMmyAddr = BeamOptMmyPara.OptMmyPara[mmyType].BeamMmyStartAddr;


    // 获取数据容量大小
    sParaSize = BeamOptMmyPara.OptMmyPara[mmyType].BeamMmyOptParaSize;

    // 获取写入的数据长度
    sParaLen = BeamOptMmyPara.OptMmyPara[mmyType].BeamMmyOptParaLen;

    // 载入数据
    for(i = 0; i<sParaSize; i++)
    {
        if(i < sParaLen)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    // 将数据载入到FLASH内
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

ErrorStatus BeamMmy_GetDefaultPara(BeamMmyType mmyType, u8 gParaBuff[])
{
    u8 i;
    u8 gLen;

    assert_app_param(IS_BEAM_MMY_TYPE(mmyType));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));

    // 获取数据长度
    gLen = BeamOptMmyPara.OptMmyPara[mmyType].BeamMmyOptParaLen;

    // 载入数据
    for(i = 0; i<gLen; i++)
    {
        gParaBuff[i] = BeamOptMmyPara.OptMmyPara[mmyType].DefaultRamAddr[i];
    }

    return SUCCESS;
}

ErrorStatus BeamMmy_SetDefaultToMMY(BeamMmyType mmyType)
{
    assert_app_param(IS_BEAM_MMY_TYPE(mmyType));

    // 设置默认数据到RAM中
    return BeamMmy_SetParaToMMY(mmyType, BeamOptMmyPara.OptMmyPara[mmyType].DefaultRamAddr);
}

void    BeamMmy_RunParaInit(void)
{
    BeamMmy_OrganizeInit();
}

// 写入触发参数到存储内
ErrorStatus BeamMmy_WriteFormulaParaToMmy(BeamMmyType mmyType, u8 sBuff[], u8 sLen)
{
    u8 gIndex = 0;
    
    // 长度字节是否 大于等于 内容字节+1 且 内容字节等于触发公式内容长度
    if(((sLen - 1) >= sBuff[0])     &&
       ((sBuff[0]) == BEAM_FORMULA_MMY_PARA_LEN))
    {
        // 越过数据长度
        gIndex++;

        // 设置参数到存储里面
        if(BeamMmy_SetParaToMMY(mmyType, &(sBuff[gIndex])) == SUCCESS)
        {
            // 解析触发公式数据
            return BeamMmy_GetFormulaTriggerPara(&(sBuff[gIndex]), &(BeamFormulaAttr[mmyType].TriAttr));
        }
    }

    // 返回错误
    return ERROR;
}


ErrorStatus BeamMmy_GetFormulaTriggerPara(uc8 gParaBuff[], BeamFormulaTriggerTypedef *lgFtPara)
{
    u16 tmpWord;
    
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(lgFtPara));

    // 获取公式是否激活
    if((gParaBuff[0] & 0x80) == 0x80)
    {
        lgFtPara->IsActive = SET;
    }
    else
    {
        lgFtPara->IsActive = RESET;
    }

    // 获取目标照度值
    tmpWord = gParaBuff[1];
    tmpWord <<= 8;
    tmpWord += gParaBuff[2];
    // 获取目标照度值
    lgFtPara->TargetValue = tmpWord;

    // 获取范围值
    tmpWord = gParaBuff[3];
    tmpWord <<= 8;
    tmpWord += gParaBuff[4];
    // 获取范围值
    lgFtPara->RangeValue = tmpWord;
    
    return SUCCESS;
}

ErrorStatus BeamMmy_GetFormulaExecutionPara(uc8 gParaBuff[], BeamFormulaExecutionTypedef *lgFePara)
{
    u8 i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(lgFePara));

    // 获取公式是否激活
    if((gParaBuff[0] & 0x80) == 0x80)
    {
        lgFePara->IsActive = SET;
    }
    else
    {
        lgFePara->IsActive = RESET;
    }

    // 判断命令长度是否匹配
    if((gParaBuff[0] & 0x07) == BEAM_FORMULA_EXECUTION_CMD_SIZE)
    {
        lgFePara->CmdLen = BEAM_FORMULA_EXECUTION_CMD_SIZE;
    }
    else
    {
        return ERROR;
    }

    // 获取复合公式的执行AG地址
    lgFePara->ExecutionAddr.App     = gParaBuff[1];
    lgFePara->ExecutionAddr.Group   = gParaBuff[2];

    // 获取复合公式的执行命令内容
    for(i=0; i<BEAM_FORMULA_EXECUTION_CMD_SIZE; i++)
    {
        lgFePara->CmdBuff[i] = gParaBuff[3+i];
    }

    return SUCCESS;
}


