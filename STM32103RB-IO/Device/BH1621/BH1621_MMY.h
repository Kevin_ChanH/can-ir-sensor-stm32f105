/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : BH1621_MMY.h
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月27日
  最近修改   :
  功能描述   : BH1621_MMY.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月27日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/
#ifndef __BH1621_MMY_H__
#define __BH1621_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    BEAM_TREND_EXCEED_IDLE,             /* 照度趋势 空闲 */
    BEAM_TREND_EXCEED_MAX,              /* 照度趋势 超过最大值 */
    BEAM_TREND_EXCEED_MIN,              /* 照度趋势 小于最小值 */
}BeamTrendTypedef;

#define IS_BEAM_TREND_TYPE(PARAM)           (((PARAM) == BEAM_TREND_EXCEED_IDLE)    || \
                                          ((PARAM) == BEAM_TREND_EXCEED_MAX)    || \
                                          ((PARAM) == BEAM_TREND_EXCEED_MIN))

typedef enum
{
    BEAM_FORMULA_EXECUTION_MAX,         /* 执行最大触发事件 */
    BEAM_FORMULA_EXECUTION_MIN,         /* 执行最小触发事件 */
}BeamFormulaExEventTypedef;
#define IS_BEAM_EX_EVENT_TYPE(PARAM)        (((PARAM) == BEAM_FORMULA_EXECUTION_MAX)    || \
                                          ((PARAM) == BEAM_FORMULA_EXECUTION_MIN))


typedef struct
{
    FlagStatus           IsActive;          /* 命令是否激活 */
    u16                 TargetValue;        /* 目标值 */
    u16                 RangeValue;         /* ±值 */
}BeamFormulaTriggerTypedef;

typedef struct
{
    BeamFormulaTriggerTypedef   TriAttr;            /* 触发参数 */
    BeamTrendTypedef            Trend;             /* 运行趋势 */
}BeamAttrTypedef;


#define     BEAM_FORMULA_EXECUTION_CMD_SIZE      CNL_CMD_SHORT_BYTE_LEN
typedef struct
{
    FlagStatus              IsActive;           /* 是否被激活 */

    u8                      CmdLen;             /* 执行命令的长度 */
    RunAddrTypedef          ExecutionAddr;      /* 执行命令的地址 */

    // 执行命令的Buff缓存
    u8                      CmdBuff[BEAM_FORMULA_EXECUTION_CMD_SIZE];
} BeamFormulaExecutionTypedef;


typedef enum
{
    BEAM_MMY_TYPE_FORMULA_START,         /* 存储类型 - 复合公式起始 - 运行参数 */

    BEAM_MMY_TYPE_MAX = (BEAM_MMY_TYPE_FORMULA_START + MCU_BEAM_FORMULA_NUM),  /* 存储类型 - 最大值 */
}BeamMmyType;
    
#define IS_BEAM_MMY_TYPE(PARAM)                 ((((PARAM) >= BEAM_MMY_TYPE_FORMULA_START) &&  \
                                             ((PARAM) <  (BEAM_MMY_TYPE_MAX))))

#define IS_BEAM_MMY_FORMULA_TYPE(PARAM)         (((PARAM) >= BEAM_MMY_TYPE_FORMULA_START) &&  \
                                                ((PARAM) <  BEAM_MMY_TYPE_MAX))


typedef struct
{
    u32         BeamMmyStartAddr;        /* Flash起始地址 */
    MmyDataType  MmyDatType;             /* 存储的数据类型 */
    u8          BeamMmyOptParaLen;       /* 操作数据长度 */
    u8          BeamMmyOptParaSize;      /* 操作数据尺寸 */
    uc8         *DefaultRamAddr;         /* 默认数据的操作地址 */
}BeamOptTypeMmyTypedef;

typedef struct
{
    u8  CfgIllId;                           /* 照度传感器ID号 */
    u8  CfgPara;                            /* 配置参数 */
}IllCfgRegTypedef;


typedef struct
{
    BeamOptTypeMmyTypedef OptMmyPara[BEAM_MMY_TYPE_MAX];
}BeamOptMmyTypedef;


#define     BEAM_FORMULA_MAX_NUM         MCU_BEAM_FORMULA_NUM
#define     BEAM_FORMULA_CFG_NUM         MCU_BEAM_FORMULA_NUM
#define     BEAM_FORMULA_LIMIT_START     1
#define     BEAM_FORMULA_LIMIT_END       BEAM_FORMULA_CFG_NUM
#define     IS_BEAM_FORMULA_ID(PARAM)    ((PARAM) < BEAM_FORMULA_CFG_NUM)

#define     IS_BEAM_FORMULA_TRIGGET_ID(PARAM)    (((PARAM) >= BEAM_FORMULA_LIMIT_START)     &&  \
                                              ((PARAM) <= BEAM_FORMULA_LIMIT_END))
/* BEAM 存储参数 */
// BEAM BLOCK SIZE - 占用2048Byte
#define     BEAM_MMY_BLOCK_SIZE          0x800                           /* 一个照度传感器占用的空间 */


// BEAM 复合公式 - 存储参数 - 起始
#define     BEAM_FORMULA_MMY_ADDR_START         (MCU_BEAM_ADDR + 0X200)     /* 存储的起始地址 */
#define     BEAM_FORMULA_MMY_PARA_LEN           15                        /* 占用15Byte */
#define     BEAM_FORMULA_MMY_DATA_TYPE          MMY_DATA_TYPE_MID           /* 存储数据类型为中等类型 */
#define     BEAM_FORMULA_MMY_CFG_MSG_SIZE       MCU_MMY_CFG_MSG_MID_SIZE     /* 存储最大字节数为32Byte */

/* 照度复合公式的执行地址个数 2个 */
#define     BEAM_FORMULA_MMY_EXECUTION_ADDR_NUM         2
/* 照度复合公式的执行参数长度 5Bytes */
#define     BEAM_FORMULA_MMY_EXECUTION_PARA_LEN         5
/* 照度复合公式的执行起始下标 5 */
#define     BEAM_FORMULA_MMY_EXECUTION_START_INDEX      5


/* 照度值的数据长度 */
#define     BEAM_ILLUMINANCE_VALUE_SIZE                 2

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  FlagStatus      BeamMmy_IsMmyType                   (BeamMmyType mmyType);
extern  FlagStatus      BeamMmy_IsFormulaIndex              (u8 tIndex);
extern  ErrorStatus     BeamMmy_FormulaOrganizeInit         (uc8 formulaNum);

extern  void            BeamMmy_OrganizeInit                (void);

extern  ErrorStatus     BeamMmy_GetParaFromMMY              (BeamMmyType mmyType, u8 gParaBuff[]);
extern  ErrorStatus     BeamMmy_SetParaToMMY                (BeamMmyType mmyType, uc8 sParaBuff[]);

extern  ErrorStatus     BeamMmy_GetDefaultPara              (BeamMmyType mmyType, u8 gParaBuff[]);
extern  ErrorStatus     BeamMmy_SetDefaultToMMY             (BeamMmyType mmyType);

extern  void            BeamMmy_RunParaInit                 (void);

extern  ErrorStatus     BeamMmy_WriteFormulaParaToMmy       (BeamMmyType mmyType, u8 sBuff[], u8 sLen);

extern  ErrorStatus     BeamMmy_GetFormulaTriggerPara       (uc8 gParaBuff[], BeamFormulaTriggerTypedef *lgFtPara);
extern  ErrorStatus     BeamMmy_GetFormulaExecutionPara     (uc8 gParaBuff[], BeamFormulaExecutionTypedef *lgFePara);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __BH1621_MMY_H__ */
