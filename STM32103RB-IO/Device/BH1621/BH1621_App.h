/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : BH1621_App.h
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月27日
  最近修改   :
  功能描述   : BH1621_App.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月27日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/
#ifndef __BH1621_APP_H__
#define __BH1621_APP_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

extern  BeamAttrTypedef BeamFormulaAttr[BEAM_FORMULA_CFG_NUM];



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus                 BeamApp_Init                            (void);
extern  ErrorStatus                 BeamApp_FormulaTirggerAttrInit          (void);

extern  ErrorStatus                 BeamApp_SetFormulaTread                 (uc8 sFormulaCh, BeamTrendTypedef sTrend);
extern  BeamTrendTypedef             BeamApp_GetFormulaTread                (uc8 sFormulaCh);

extern  ErrorStatus                 BeamApp_FormulaExcuted                  (uc8 sFormulaCh, BeamFormulaExEventTypedef pEven);
extern  ErrorStatus                 BeamApp_FormulaChannelPoll              (uc8 sFormulaCh, u16 illuminance);

extern  ErrorStatus                 BeamApp_GetIlluminance                  (uc16 iValue, u16 *gIllum);
extern  ErrorStatus                 BeamApp_UpdateIlluminance               (u16 *gIllum);

extern  u16                         BeamApp_GetIlluminanceVaule             (void);

extern  ErrorStatus                 BeamApp_FormulaPoll                     (uc16 tIllum);
extern  ErrorStatus                 BeamApp_Poll                            (void);

#ifdef __cplusplus
#if __cplusplus
}

#endif
#endif /* __cplusplus */


#endif /* __BH1621_APP_H__ */
