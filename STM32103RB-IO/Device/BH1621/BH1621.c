/******************************************************************************

                  版权所有 (C), 20XX-2017, XXXX_GONGSHI

 ******************************************************************************
  文 件 名   : BH1621.c
  版 本 号   : 初稿
  作    者   : KelvinChan
  生成日期   : 2017年12月26日
  最近修改   :
  功能描述   : 照度传感器操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年12月26日
    作    者   : KelvinChan
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/
BH1621AttrTypedef   BH1621Attr;

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

ErrorStatus BH1621_GpioInit(void)
{
    // 初始化GC1
    Gpio_Init(BH1621_GC1, GPIO_Mode_Out_PP, BH1621_GC1_INIT_STAT);
    // 初始化GC2
    Gpio_Init(BH1621_GC2, GPIO_Mode_Out_PP, BH1621_GC2_INIT_STAT);

    return SUCCESS;
}

ErrorStatus BH1621_SetMode(BH1621ModeTypedef sMode)
{
    if(sMode == BH1621_MODE_SHUTDOWN)
    {
        /* 设置GC1&GC2 为0 */
        Gpio_Write(BH1621_GC1, Bit_RESET);
        Gpio_Write(BH1621_GC2, Bit_RESET);
    }
    else if(sMode == BH1621_MODE_H_GAIN)
    {
        /* 设置GC1=1 GC2=0 */
        Gpio_Write(BH1621_GC1, Bit_SET);
        Gpio_Write(BH1621_GC2, Bit_RESET);
    }
    else if(sMode == BH1621_MODE_L_GAIN)
    {
        /* 设置GC1=0 GC2=1 */
        Gpio_Write(BH1621_GC1, Bit_RESET);
        Gpio_Write(BH1621_GC2, Bit_SET);
    }
    else
    {
        return ERROR;
    }

    // 载入运行模式
    BH1621Attr.Mode = sMode;
    
    return SUCCESS;
}

ErrorStatus BH1621_AttrInit(void)
{
    // 运行模式 - 高分辨率模式
    BH1621Attr.Mode = BH1621_MODE_H_GAIN;
    
    // 平均值计算
    BH1621Attr.MeanCnt = 0;
    
    // 是否有新数据
    BH1621Attr.HasNew = RESET;
    
    // 照度值
    BH1621Attr.BH1621Value = 0;

    return SUCCESS;
}

ErrorStatus BH1621_Init(void)
{
    /* 初始化BH1621 GPIO */
    BH1621_GpioInit();

    /* 初始化运行 */
    BH1621_AttrInit();

    /* 设置BH1621运行模式 为 高精度模式 */
    BH1621_SetMode(BH1621_MODE_H_GAIN);

    /* 初始化ADC */
    ADC_DriversInit();

    // 载入采样周期
    Tmm_LoadBaseTick (TMM_BH1621_SAMPLING, BH1621_SAMPLING_INTERVAL_TICKS);
    
    return SUCCESS;
}

u16 BH1621_Mean(void)
{
    u8 i;
    u16 meanValue = 0;

    meanValue = ADC1Buff[0];
    for(i=0; i<ADC1_CONVERT_SIZE; i++)
    {
        // meanValue + AD采样值
        meanValue += ADC1Buff[i];

        // meanValue/2
        meanValue >>= 1;
    }

    return meanValue;
}


ErrorStatus BH1621_SampingPoll(void)
{
    u8 i;
    u16 meanValue = 0;
    
    // 判断是否到达采样时间
    if(Tmm_IsON(TMM_BH1621_SAMPLING) != SET)
    {
        if(BH1621Attr.MeanCnt < BH1621_MEAM_SIZE)
        {
            // 获取 AD采样平均值
            BH1621Attr.MeanBuff[BH1621Attr.MeanCnt] = BH1621_Mean();

            // 采样样本+1
            BH1621Attr.MeanCnt++;

            // 判断是否已经采样完成
            if(BH1621Attr.MeanCnt == BH1621_MEAM_SIZE)
            {
                meanValue = BH1621Attr.MeanBuff[0];
                
                for(i=0; i<BH1621_MEAM_SIZE; i++)
                {
                    // meanValue + AD采样值
                    meanValue += BH1621Attr.MeanBuff[i];

                    // meanValue/2
                    meanValue >>= 1;
                }

                // 载入采样平均值
                BH1621Attr.BH1621Value = meanValue;

                // 重新开始采样
                BH1621Attr.MeanCnt = 0;
                
                // 设置状态 变成有新的采样值
                BH1621Attr.HasNew = SET;
            }
        }
        else
        {
            // 重新开始采样
            BH1621Attr.MeanCnt = 0;
            // 之前数据作废
            BH1621Attr.HasNew = RESET;
        }

        // 载入采样周期
        Tmm_LoadBaseTick (TMM_BH1621_SAMPLING, BH1621_SAMPLING_INTERVAL_TICKS);
    }

    return SUCCESS;
}


ErrorStatus BH1621_Poll(void)
{
    BH1621_SampingPoll();

    return SUCCESS;
}




