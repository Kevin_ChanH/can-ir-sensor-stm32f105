/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : VirtualAddr.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月6日
  最近修改   :
  功能描述   : VirtualAddr.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __VIRTUALADDR_H__
#define __VIRTUALADDR_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef struct
{
    u8  RunTim1;                /* 运行时间T1 */
    u8  RunTim3;                /* 运行时间T3 */
    u16 RunTim2;                /* 运行时间T2 */
    u16 WorkTimTick;            /* 运行参数 节拍数 */
    Tmm_ID RunTmmId;            /* 运行参数 时间ID */
    TmmExp_ID RunTmmExpId;      /* 运行参数 拓展时间参数 */
} RunTimeTypedef;

typedef enum
{
    DIMMER_CMD_STAT_IDEL = 0,   /* 命令状态 - 空闲 */
    DIMMER_CMD_STAT_ACTIVE,     /* 命令状态 - 活动 */
} DimmerCmdStat;

typedef struct
{
    u8  SetLevel1;              /* 设置值L1 */
    u8  SetLevel2;              /* 设置值L2 */
} SetLevelTypedef;

typedef struct
{
    u8 StableLevel;             /* 状态 稳定值 */
    u8 RealLevel;               /* 状态 实际值 */
    s8 Version;                 /* 反馈 版本号 */
    FlagStatus VersionUpdate;   /* 版本 更新标志 */
} RunLevelStat;

typedef enum
{
    DIMMER_TREDN_EQUAL = 0,     /* 调光趋势 相等 */
    DIMMER_TREND_UP,            /* 调光趋势 向上 */
    DIMMER_TREND_DOWN,          /* 调光趋势 向下 */
} RunDimmerTrend;

typedef enum
{
    DIMMER_STAT_IDLE=0,         /* 调光状态 空闲 */
    DIMMER_STAT_LOAD,           /* 调光状态 载入 */
    DIMMER_STAT_START,          /* 调光状态 开始 */
    DIMMER_STAT_S1ING,          /* 调光状态 T1 ING */
    DIMMER_STAT_S1END,          /* 调光状态 T1 结束 */
    DIMMER_STAT_S2ING,          /* 调光状态 T2 ING */
    DIMMER_STAT_S2END,          /* 调光状态 T2 结束 */
    DIMMER_STAT_S3ING,          /* 调光状态 T3 ING */
    DIMMER_STAT_S3END,          /* 调光状态 T3 结束 */
} DimmerStatTypedef;

typedef enum
{
    DIMMER_CT_STAT_STOP = 0,    /* 调光的窗帘状态 - STOP */
    DIMMER_CT_STAT_OPEN,        /* 调光的窗帘状态 - OPEN */
    DIMMER_CT_STAT_CLOSE,       /* 调光的窗帘状态 - CLOSE */
} DimmerCtStatTypedef;

typedef struct
{
    FlagStatus          IsCt;
    DimmerCtStatTypedef CtStat;
    u8                  OpenTimes;
    u8                  CloseTimes;
} DimmerCtAttrTypedef;

typedef struct
{
    DimmerStatTypedef   Stat;       /* 调光状态 */
    RunTimeTypedef      WorkTim;    /* 运行时间参数 */
    SetLevelTypedef     SetLevel;   /* 设置值 */
    RunLevelStat        LevelStat;  /* 状态值 */
    RunDimmerTrend      Trend;      /* 运行趋势 */
    DimmerCmdStat       CmdStat;    /* 命令状态 */
    DimmerCtAttrTypedef CtAttr;     /* 窗帘状态 */
} DimmerAttrTypedef;

typedef struct
{
    DimmerAttrTypedef   Dimmer;     /* 调光参数 */
    RunAddrTypedef      WorkAddr;   /* 运行地址 */
    FunctionalState     Activation; /* 是否执行 */
    u8                  SemCnt;     /* 信号量 */
} VirtualAtrrTypedef;

typedef struct
{
    DimmerAttrTypedef   Dimmer;     /* 调光参数 */
    FunctionalState     Activation; /* 是否执行 */
} ActualPortTypedef;

#define     DIMMER_T1T3_SCALE_0         1
#define     DIMMER_T1T3_SCALE_1         5
#define     DIMMER_T1T3_SCALE_2         10

#define     DIMMER_T1T3_SCALE_BITS      0xC0
#define     DIMMER_T1T3_SCALE_0_BIT     0x00
#define     DIMMER_T1T3_SCALE_1_BIT     0x40
#define     DIMMER_T1T3_SCALE_2_BIT     0x80


#define     DIMMER_T2_PARA_SECOND       0
#define     DIMMER_T2_PARA_MINUTE       1
#define     DIMMER_T2_PARA_HOUR         2


#define     ACTUAL_PWM_PORT_ALL_ID      0xFF
#define     ACTUAL_RELAY_PORT_ALL_ID    0xFF




#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus         VirtualAddr_DimmerChkT1                 (uc8 para);
extern  FlagStatus          VirtualAddr_DimmerT1IsStop              (uc8 para);
extern  FlagStatus          VirtualAddr_DimmerT1IsEnterStopMode     (uc8 para);

extern  void                VirtualAddr_Init                        (void);
extern  ErrorStatus         VirtualAddr_DimmerAttrInit              (DimmerAttrTypedef *iDimmer, Tmm_ID timID, TmmExp_ID timExpID);
extern  ErrorStatus         VirtualAddr_DimmerAttrCopy              (DimmerAttrTypedef *bDimmer, DimmerAttrTypedef *nDimmer);
extern  ErrorStatus         VirtualAddr_GetIndex                    (RunAddrTypedef jAddr, u8 *index);
extern  ErrorStatus         VirtualAddr_GetEmptyIndex               (u8 *index);
extern  FlagStatus          VirtualAddr_IsRun                       (RunAddrTypedef jAddr);
extern  ErrorStatus         VirtualAddr_Join                        (RunAddrTypedef jAddr);
extern  ErrorStatus         VirtualAddr_Abort                       (RunAddrTypedef jAddr);
extern  ErrorStatus         VirtualAddr_ReviseAddr                  (RunAddrTypedef oldAddr, RunAddrTypedef newAddr);

extern  ErrorStatus         VirtualAddr_GetStableLevel              (DimmerAttrTypedef *bDimmer, u8 *gLevel);
extern  ErrorStatus         VirtualAddr_GetStableVersion            (DimmerAttrTypedef *bDimmer, s8 *gVersion);
extern  ErrorStatus         VirtualAddr_GetRealLevel                (DimmerAttrTypedef *bDimmer, u8 *gLevel);
extern  ErrorStatus         VirtualAddr_GetCmdStat                  (DimmerAttrTypedef *bDimmer, DimmerCmdStat *gCmdStat);
extern  ErrorStatus         VirtualAddr_SetCtAttr                   (DimmerAttrTypedef *bDimmer, DimmerCtAttrTypedef ctAttr);
extern  ErrorStatus         VirtualAddr_GetIsCt                     (DimmerAttrTypedef *bDimmer, FlagStatus *isCt);
extern  ErrorStatus         VirtualAddr_GetCtStat                   (DimmerAttrTypedef *bDimmer, DimmerCtStatTypedef *ctStat);
extern  ErrorStatus         VirtualAddr_GetCtOpenTimes              (DimmerAttrTypedef *bDimmer, u8 *ctOpenTimes);
extern  ErrorStatus         VirtualAddr_GetCtCloseTimes             (DimmerAttrTypedef *bDimmer, u8 *ctCloseTimes);

extern  ErrorStatus         VirtualAddr_GetVaStableLevel            (RunAddrTypedef gAddr, u8 *gLevel);
extern  ErrorStatus         VirtualAddr_GetVaRealLevel              (RunAddrTypedef gAddr, u8 *gLevel);
extern  ErrorStatus         VirtualAddr_GetVaSemCnt                 (RunAddrTypedef gAddr, u8 *gSemCnt);
extern  ErrorStatus         VirtualAddr_GetVaCmdStat                (RunAddrTypedef gAddr, DimmerCmdStat *gCmdStat);
extern  ErrorStatus         VirtualAddr_SetVaCtAttr                 (RunAddrTypedef sAddr, DimmerCtAttrTypedef ctAttr);
extern  ErrorStatus         VirtualAddr_GetVaIsCt                   (RunAddrTypedef gAddr, FlagStatus *isCt);
extern  ErrorStatus         VirtualAddr_GetVaCtStat                 (RunAddrTypedef gAddr, DimmerCtStatTypedef *ctStat);
extern  ErrorStatus         VirtualAddr_GetVaCtOpenTimes            (RunAddrTypedef gAddr, u8 *ctOpenTimes);
extern  ErrorStatus         VirtualAddr_GetVaCtCloseTimes           (RunAddrTypedef gAddr, u8 *ctCloseTimes);

extern  ErrorStatus         ActualAddr_GetStableLevel               (ActualPortTypedef *actPort, u8 *gLevel);
extern  ErrorStatus         ActualAddr_GetStableVersion             (ActualPortTypedef *actPort, s8 *gVersion);
extern  ErrorStatus         ActualAddr_GetRealLevel                 (ActualPortTypedef *actPort, u8 *gLevel);
extern  ErrorStatus         ActualAddr_GetCmdStat                   (ActualPortTypedef *actPort, DimmerCmdStat *gCmdStat);
extern  FunctionalState     ActualAddr_GetActiveStat                (ActualPortTypedef *actPort);
extern  ErrorStatus         ActualAddr_SetActiveStat                (ActualPortTypedef *actPort, FunctionalState cmdStat);

extern  ErrorStatus         ActualAddr_GetPwmStableLevel            (uc8 gPwm, u8 *gLevel);
extern  ErrorStatus         ActualAddr_GetPwmStableVersion          (uc8 gPwm, s8 *gVersion);
extern  ErrorStatus         ActualAddr_GetPwmRealLevel              (uc8 gPwm, u8 *gLevel);
extern  ErrorStatus         ActualAddr_GetPwmCmdStat                (uc8 gPwm, DimmerCmdStat *gCmdStat);
extern  FunctionalState     ActualAddr_GetPwmActiveStat             (uc8 gPwm);
extern  ErrorStatus         ActualAddr_SetPwmActiveStat             (uc8 sPwm, FunctionalState cmdStat);

extern  ErrorStatus         ActualAddr_GetRelayStableLevel          (uc8 gRelay, u8 *gLevel);
extern  ErrorStatus         ActualAddr_GetRelayStableVersion        (uc8 gRelay, s8 *gVersion);
extern  ErrorStatus         ActualAddr_GetRelayRealLevel            (uc8 gRelay, u8 *gLevel);
extern  ErrorStatus         ActualAddr_GetRelayCmdStat              (uc8 gRelay, DimmerCmdStat *gCmdStat);
extern  FunctionalState     ActualAddr_GetRelayActiveStat           (uc8 gRelay);
extern  ErrorStatus         ActualAddr_SetRelayActiveStat           (uc8 sRelay, FunctionalState cmdStat);

extern  ErrorStatus         ActualAddr_PwmLoadCmd                   (uc8 gPwm, uc8 *cmdBuff, uc8 cmdLen);
extern  ErrorStatus         ActualAddr_RelayLoadCmd                 (uc8 gRelay, uc8 *cmdBuff, uc8 cmdLen);

extern  ErrorStatus         VirtualAddr_LoadCmd                     (DimmerAttrTypedef *lDimmer, uc8 *cmdBuff, uc8 cmdLen);
extern  ErrorStatus         VirtualAddr_VaLoadCmd                   (RunAddrTypedef gAddr, uc8 *cmdBuff, uc8 cmdLen);
extern  void                VirtualAddr_Poll                        (void);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __VIRTUALADDR_H__ */
