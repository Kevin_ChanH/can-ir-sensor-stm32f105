/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : VirtualAddr.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月5日
  最近修改   :
  功能描述   : 虚拟地址操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

static  u16         VirtualAddr_DimmerGetSecFromT1T3                (uc8 para);
static  u8          VirtualAddr_DimmerGetParaFromT2                 (uc8 hms, uc16 para);
//static  ErrorStatus VirtualAddr_DimmerChkT1                         (uc8 para);
//static  FlagStatus  VirtualAddr_DimmerT1IsStop                      (uc8 para);
//static  FlagStatus  VirtualAddr_DimmerT1IsEnterStopMode             (uc8 para);
static  ErrorStatus VirtualAddr_DimmerStart                         (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerS1ING                         (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerS1END                         (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerS2ING                         (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerS2END                         (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerS3ING                         (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerS3END                         (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_VersionIteration                    (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerSetIdle                       (DimmerAttrTypedef *pDimmer);
static  ErrorStatus VirtualAddr_DimmerPoll                          (DimmerAttrTypedef *pDimmer);


/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
// 虚拟地址个数
VirtualAtrrTypedef  VirtualAttr[VIRTUAL_ADDR_LEN];


/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/




/*****************************************************************************
 函 数 名  : VirtualAddr_Init
 功能描述  : 初始化地址
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void VirtualAddr_Init(void)
{
    u8 i = 0;

    // 虚拟运行数据
    for(i=0; i<VIRTUAL_ADDR_LEN; i++)
    {
        VirtualAttr[i].Activation = DISABLE;

        VirtualAttr[i].WorkAddr.App   = 0;
        VirtualAttr[i].WorkAddr.Group = 0;

        VirtualAttr[i].SemCnt = 0;

        // 初始化该通道参数 同时载入 定时器ID 以及 扩展定时器ID
        VirtualAddr_DimmerAttrInit(&(VirtualAttr[i].Dimmer), (Tmm_ID)(TMM_VIRTUAL_START+i), (TmmExp_ID)(TMM_EXP_VIRTUAL_START+i));
    }

    //Tmm_LoadMs(TMM_AG_STAT_REGET, AG_MTC_INIT_REGET_TMM_MS_DEFAULT * SystemUnitDefaultValue);
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerAttrInit
 功能描述  : 初始化调光参数
 输入参数  : DimmerAttrTypedef *iDimmer
             Tmm_ID timID
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_DimmerAttrInit(DimmerAttrTypedef *iDimmer, Tmm_ID timID, TmmExp_ID timExpID)
{
    if(iDimmer != NULL)
    {
        iDimmer->Stat = DIMMER_STAT_IDLE;

        iDimmer->LevelStat.RealLevel        = 0;
        iDimmer->LevelStat.StableLevel      = AG_MTC_INIT_LEVEL_VALUE;
        iDimmer->LevelStat.Version          = AG_MTC_INIT_VERSION_STAT;
        iDimmer->LevelStat.VersionUpdate    = RESET;

        iDimmer->SetLevel.SetLevel1 = 0;
        iDimmer->SetLevel.SetLevel2 = 0;

        iDimmer->WorkTim.RunTim1 = 0;
        iDimmer->WorkTim.RunTim2 = 0;
        iDimmer->WorkTim.RunTim3 = 0;
        iDimmer->WorkTim.RunTmmId = timID;
        iDimmer->WorkTim.RunTmmExpId = timExpID;
        iDimmer->WorkTim.WorkTimTick = 0;

        iDimmer->Trend = DIMMER_TREDN_EQUAL;

        iDimmer->CmdStat = DIMMER_CMD_STAT_IDEL;

        iDimmer->CtAttr.IsCt        = RESET;
        iDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_STOP;
        iDimmer->CtAttr.OpenTimes   = 0;
        iDimmer->CtAttr.CloseTimes  = 0;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerAttrCopy
 功能描述  : 复制运行参数
 输入参数  : DimmerAttrTypedef *bDimmer
             DimmerAttrTypedef *nDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_DimmerAttrCopy(DimmerAttrTypedef *bDimmer, DimmerAttrTypedef *nDimmer)
{
    if((bDimmer != NULL)
            && (nDimmer != NULL))
    {
        nDimmer->Stat = bDimmer->Stat;

        nDimmer->CmdStat = bDimmer->CmdStat;

        nDimmer->CtAttr.IsCt        = bDimmer->CtAttr.IsCt;
        nDimmer->CtAttr.CtStat      = bDimmer->CtAttr.CtStat;
        nDimmer->CtAttr.OpenTimes   = bDimmer->CtAttr.OpenTimes;
        nDimmer->CtAttr.CloseTimes  = bDimmer->CtAttr.CloseTimes;

        nDimmer->LevelStat.RealLevel = bDimmer->LevelStat.RealLevel;
        nDimmer->LevelStat.StableLevel = bDimmer->LevelStat.StableLevel;
        nDimmer->LevelStat.Version = bDimmer->LevelStat.Version;
        nDimmer->LevelStat.VersionUpdate = bDimmer->LevelStat.VersionUpdate;

        nDimmer->SetLevel.SetLevel1 = bDimmer->SetLevel.SetLevel1;
        nDimmer->SetLevel.SetLevel2 = bDimmer->SetLevel.SetLevel2;

        nDimmer->WorkTim.RunTim1 = bDimmer->WorkTim.RunTim1;
        nDimmer->WorkTim.RunTim2 = bDimmer->WorkTim.RunTim1;
        nDimmer->WorkTim.RunTim3 = bDimmer->WorkTim.RunTim1;
        nDimmer->WorkTim.WorkTimTick = bDimmer->WorkTim.WorkTimTick;

        Tmm_Copy(bDimmer->WorkTim.RunTmmId, nDimmer->WorkTim.RunTmmId);
        TmmExp_Copy(bDimmer->WorkTim.RunTmmExpId, nDimmer->WorkTim.RunTmmExpId);

        nDimmer->Trend = bDimmer->Trend;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetIndex
 功能描述  : 获取地址下标
 输入参数  : RunAddrTypedef jAddr
             u8 *index
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetIndex(RunAddrTypedef jAddr, u8 *index)
{
    u8 i = 0;

    for(i=0; i<VIRTUAL_ADDR_LEN; i++)
    {
        if(VirtualAttr[i].Activation == ENABLE)
        {
            if(((VirtualAttr[i].WorkAddr.App) == (jAddr.App))
                    && ((VirtualAttr[i].WorkAddr.Group) == (jAddr.Group)))
            {
                *index = i;
                return SUCCESS;
            }
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetEmptyIndex
 功能描述  : 获取一个空闲元素下标
 输入参数  : u8 *index
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetEmptyIndex(u8 *index)
{
    u8 i = 0;

    for(i=0; i<VIRTUAL_ADDR_LEN; i++)
    {
        if(VirtualAttr[i].Activation == DISABLE)
        {
            *index = i;
            return SUCCESS;
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_IsRun
 功能描述  : 判断该虚拟地址是否正在运行
 输入参数  : RunAddrTypedef jAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus VirtualAddr_IsRun(RunAddrTypedef jAddr)
{
    u8 tmpIndex = 0;

    if(VirtualAddr_GetIndex(jAddr, &tmpIndex) == SUCCESS)
    {
        return SET;
    }

    return RESET;
}


/*****************************************************************************
 函 数 名  : VirtualAddr_Join
 功能描述  : 进入新地址
 输入参数  : RunAddrTypedef jAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_Join(RunAddrTypedef jAddr)
{
    u8 i = 0;

    if((jAddr.App == 0xFF)
            || (jAddr.App == 0x00)
            || (jAddr.Group == 0xFF)
            || (jAddr.Group == 0x00))
    {
        return ERROR;
    }

    // 该地址已经存在
    if(VirtualAddr_GetIndex(jAddr, &i) == SUCCESS)
    {
        VirtualAttr[i].SemCnt += 1;

        return SUCCESS;
    }

    // 获取一个空闲元素
    if(VirtualAddr_GetEmptyIndex(&i) == SUCCESS)
    {
        // 载入地址
        VirtualAttr[i].WorkAddr.App = jAddr.App;
        VirtualAttr[i].WorkAddr.Group = jAddr.Group;

        // 设置 活跃度为 活跃模式
        VirtualAttr[i].Activation = ENABLE;

        // 设置 信号量为1
        VirtualAttr[i].SemCnt = 1;

        // 初始化调光数据 但TmmId不变
        VirtualAddr_DimmerAttrInit(&(VirtualAttr[i].Dimmer), VirtualAttr[i].Dimmer.WorkTim.RunTmmId, VirtualAttr[i].Dimmer.WorkTim.RunTmmExpId);

        // 返回成功
        return SUCCESS;
    }

    // 返回失败
    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_Abort
 功能描述  : 退出虚拟地址
 输入参数  : RunAddrTypedef jAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_Abort(RunAddrTypedef jAddr)
{
    u8 i = 0;

    if(VirtualAddr_GetIndex(jAddr, &i) == SUCCESS)
    {
        // 信号量-1
        if(VirtualAttr[i].SemCnt > 0)
        {
            VirtualAttr[i].SemCnt -= 1;
        }
        else
        {
            VirtualAttr[i].SemCnt = 0;
        }

        if(VirtualAttr[i].SemCnt == 0)
        {
            VirtualAttr[i].Activation = DISABLE;
            VirtualAttr[i].WorkAddr.App   = 0;
            VirtualAttr[i].WorkAddr.Group = 0;
            VirtualAttr[i].SemCnt = 0;

            // 初始化调光数据 但TmmId不变
            VirtualAddr_DimmerAttrInit(&(VirtualAttr[i].Dimmer), VirtualAttr[i].Dimmer.WorkTim.RunTmmId, VirtualAttr[i].Dimmer.WorkTim.RunTmmExpId);
        }

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_ReviseAddr
 功能描述  : 修改地址
 输入参数  : RunAddrTypedef oldAddr
             RunAddrTypedef newAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_ReviseAddr(RunAddrTypedef oldAddr, RunAddrTypedef newAddr)
{

    AddrAttrTypedef responseAddr;
    u8 oldaddrIndex = 0;
    u8 newAddrIndex = 0;

    // 判断新地址是否已经存在
    if(VirtualAddr_IsRun(newAddr) == SET)
    {
        if((newAddr.App != oldAddr.App)
                || (newAddr.Group != oldAddr.Group))
        {
            /* 退出旧的地址 */
            VirtualAddr_Abort(oldAddr);
            /* 引入新的地址 */
            VirtualAddr_Join(newAddr);
        }

        return SUCCESS;
    }
    else
    {
        // 主动上报的地址类型 - AG地址
        responseAddr.Type = CMD_ADDR_TYPE_APP;
        responseAddr.AddrBuff[0] = newAddr.App;
        responseAddr.AddrBuff[1] = newAddr.Group;

        // 获取旧地址的下标
        if(VirtualAddr_GetIndex(oldAddr, &oldaddrIndex) == SUCCESS)
        {
            // 判断旧地址的信号量是否只有1 即只有唯一一个
            if(VirtualAttr[oldaddrIndex].SemCnt <= 1)
            {
                // 过滤新地址为 0x00 或 0xFF的情况
                if((newAddr.App != 0x00)
                        && (newAddr.App != 0xFF)
                        && (newAddr.Group != 0x00)
                        && (newAddr.Group != 0xFF))
                {
                    // 直接修改地址
                    VirtualAttr[oldaddrIndex].WorkAddr.App = newAddr.App;
                    VirtualAttr[oldaddrIndex].WorkAddr.Group= newAddr.Group;

                    // 主动上报新地址的状态
                    StatLogic_ResponseOneAgStat(responseAddr);
                }
                else
                {
                    /* 退出旧的地址 */
                    VirtualAddr_Abort(oldAddr);
                }

                return SUCCESS;
            }
            else
            {
                // 过滤新地址为 0x00 或 0xFF的情况
                if((newAddr.App != 0x00)
                        && (newAddr.App != 0xFF)
                        && (newAddr.Group != 0x00)
                        && (newAddr.Group != 0xFF))
                {
                    // 获取一个新的 地址元素
                    if(VirtualAddr_GetEmptyIndex(&newAddrIndex) == SUCCESS)
                    {
                        // 载入新元素的数据
                        VirtualAttr[newAddrIndex].Activation = ENABLE;

                        // 载入新地址
                        VirtualAttr[newAddrIndex].WorkAddr.App   = newAddr.App;
                        VirtualAttr[newAddrIndex].WorkAddr.Group = newAddr.Group;

                        // 信号量设置为1
                        VirtualAttr[newAddrIndex].SemCnt = 1;

                        // 复制调光运行参数
                        VirtualAddr_DimmerAttrCopy(&(VirtualAttr[oldaddrIndex].Dimmer), &(VirtualAttr[newAddrIndex].Dimmer));

                        // 退出旧的地址
                        VirtualAddr_Abort(oldAddr);

                        // 主动上报新地址的状态
                        StatLogic_ResponseOneAgStat(responseAddr);

                        return SUCCESS;
                    }
                }
                else
                {
                    // 退出旧的地址
                    return VirtualAddr_Abort(oldAddr);
                }
            }
        }
        else
        {
            /* 引入新的地址 */
            VirtualAddr_Join(newAddr);

            // 主动上报新地址的状态
            return StatLogic_ResponseOneAgStat(responseAddr);
        }
    }


    return ERROR;
}


/*****************************************************************************
 函 数 名  : VirtualAddr_GetStableLevel
 功能描述  : 获取稳定值
 输入参数  : DimmerAttrTypedef *bDimmer
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetStableLevel(DimmerAttrTypedef *bDimmer, u8 *gLevel)
{
    if((bDimmer != NULL)
            && (gLevel != NULL))
    {
        *gLevel = bDimmer->LevelStat.StableLevel;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetStableVersion
 功能描述  : 获取稳定版本号
 输入参数  : DimmerAttrTypedef *bDimmer
             s8 *gVersion
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年12月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetStableVersion(DimmerAttrTypedef *bDimmer, s8 *gVersion)
{
    if((bDimmer != NULL)
            && (gVersion != NULL))
    {
        *gVersion = bDimmer->LevelStat.Version;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetRealLevel
 功能描述  : 获取实际值
 输入参数  : DimmerAttrTypedef *bDimmer
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetRealLevel(DimmerAttrTypedef *bDimmer, u8 *gLevel)
{
    if((bDimmer != NULL)
            && (gLevel != NULL))
    {
        *gLevel = bDimmer->LevelStat.RealLevel;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetCmdStat
 功能描述  : 获取命令执行状态
 输入参数  : DimmerAttrTypedef *bDimmer
             DimmerCmdStat *gCmdStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年12月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetCmdStat(DimmerAttrTypedef *bDimmer, DimmerCmdStat *gCmdStat)
{
    if((bDimmer != NULL)
            && (gCmdStat != NULL))
    {
        *gCmdStat = bDimmer->CmdStat;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_SetCtAttr
 功能描述  : 设置窗帘部分的参数
 输入参数  : DimmerAttrTypedef *iDimmer
             DimmerCtAttrTypedef ctAttr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_SetCtAttr(DimmerAttrTypedef *bDimmer, DimmerCtAttrTypedef ctAttr)
{
    if(bDimmer != NULL)
    {
        bDimmer->CtAttr.IsCt        = ctAttr.IsCt;
        bDimmer->CtAttr.CtStat      = ctAttr.CtStat;
        bDimmer->CtAttr.OpenTimes   = ctAttr.OpenTimes;
        bDimmer->CtAttr.CloseTimes  = ctAttr.CloseTimes;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetIsCt
 功能描述  : 获取是否为窗帘回路
 输入参数  : DimmerAttrTypedef *bDimmer
             FlagStatus *isCt
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetIsCt(DimmerAttrTypedef *bDimmer, FlagStatus *isCt)
{
    if((bDimmer != NULL)
            && (isCt != NULL))
    {
        *isCt = bDimmer->CtAttr.IsCt;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetCtStat
 功能描述  : 获取窗帘的状态
 输入参数  : DimmerAttrTypedef *bDimmer
             DimmerCtStatTypedef *ctStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetCtStat(DimmerAttrTypedef *bDimmer, DimmerCtStatTypedef *ctStat)
{
    if((bDimmer != NULL)
            && (ctStat != NULL))
    {
        *ctStat = bDimmer->CtAttr.CtStat;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetCtOpenTimes
 功能描述  : 获取窗帘的打开总时间
 输入参数  : DimmerAttrTypedef *bDimmer
             u8 *ctOpenTimes
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetCtOpenTimes(DimmerAttrTypedef *bDimmer, u8 *ctOpenTimes)
{
    if((bDimmer != NULL)
            && (ctOpenTimes != NULL))
    {
        *ctOpenTimes = bDimmer->CtAttr.OpenTimes;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetCtCloseTimes
 功能描述  : 获取窗帘的关闭总时间
 输入参数  : DimmerAttrTypedef *bDimmer
             u8 *ctCloseTimes
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetCtCloseTimes(DimmerAttrTypedef *bDimmer, u8 *ctCloseTimes)
{
    if((bDimmer != NULL)
            && (ctCloseTimes != NULL))
    {
        *ctCloseTimes = bDimmer->CtAttr.CloseTimes;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaStableLevel
 功能描述  : 获取虚拟地址稳定值
 输入参数  : RunAddrTypedef gAddr
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaStableLevel(RunAddrTypedef gAddr, u8 *gLevel)
{
    u8 gIndex = 0;

    if((gLevel != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        return VirtualAddr_GetStableLevel(&(VirtualAttr[gIndex].Dimmer), gLevel);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaRealLevel
 功能描述  : 获取虚拟地址实际值
 输入参数  : RunAddrTypedef gAddr
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaRealLevel(RunAddrTypedef gAddr, u8 *gLevel)
{
    u8 gIndex = 0;

    if((gLevel != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        return VirtualAddr_GetRealLevel(&(VirtualAttr[gIndex].Dimmer), gLevel);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaSemCnt
 功能描述  : 获取虚拟地址信号量
 输入参数  : RunAddrTypedef gAddr
             u8 *gSemCnt
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaSemCnt(RunAddrTypedef gAddr, u8 *gSemCnt)
{
    u8 gIndex = 0;

    if((gSemCnt != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        *gSemCnt = VirtualAttr[gIndex].SemCnt;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaCmdStat
 功能描述  : 获取虚拟地址 的 命令执行状态
 输入参数  : RunAddrTypedef gAddr
             DimmerCmdStat *gCmdStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年12月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaCmdStat(RunAddrTypedef gAddr, DimmerCmdStat *gCmdStat)
{
    u8 gIndex = 0;

    if((gCmdStat != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        return VirtualAddr_GetCmdStat(&(VirtualAttr[gIndex].Dimmer), gCmdStat);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_SetVaCtAttr
 功能描述  : 设置虚拟地址得到窗帘参数
 输入参数  : RunAddrTypedef sAddr
             DimmerCtAttrTypedef ctAttr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_SetVaCtAttr(RunAddrTypedef sAddr, DimmerCtAttrTypedef ctAttr)
{
    u8 gIndex = 0;

    if(VirtualAddr_GetIndex(sAddr, &gIndex) == SUCCESS)
    {
        return VirtualAddr_SetCtAttr(&(VirtualAttr[gIndex].Dimmer), ctAttr);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaIsCt
 功能描述  : 获取虚拟地址的 窗帘标志
 输入参数  : RunAddrTypedef gAddr
             FlagStatus *isCt
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaIsCt(RunAddrTypedef gAddr, FlagStatus *isCt)
{
    u8 gIndex = 0;

    if((isCt != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        return VirtualAddr_GetIsCt(&(VirtualAttr[gIndex].Dimmer), isCt);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaCtStat
 功能描述  : 获取虚拟地址的 窗帘状态
 输入参数  : RunAddrTypedef gAddr
             DimmerCtStatTypedef *ctStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaCtStat(RunAddrTypedef gAddr, DimmerCtStatTypedef *ctStat)
{
    u8 gIndex = 0;

    if((ctStat != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        return VirtualAddr_GetCtStat(&(VirtualAttr[gIndex].Dimmer), ctStat);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaCtOpenTimes
 功能描述  : 获取虚拟地址的开窗帘时间
 输入参数  : RunAddrTypedef gAddr
             u8 *ctOpenTimes
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaCtOpenTimes(RunAddrTypedef gAddr, u8 *ctOpenTimes)
{
    u8 gIndex = 0;

    if((ctOpenTimes != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        return VirtualAddr_GetCtOpenTimes(&(VirtualAttr[gIndex].Dimmer), ctOpenTimes);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_GetVaCtCloseTimes
 功能描述  : 获取虚拟地址的关窗帘时间
 输入参数  : RunAddrTypedef gAddr
             u8 *ctCloseTimes
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年1月3日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_GetVaCtCloseTimes(RunAddrTypedef gAddr, u8 *ctCloseTimes)
{
    u8 gIndex = 0;

    if((ctCloseTimes != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &gIndex) == SUCCESS))
    {
        return VirtualAddr_GetCtCloseTimes(&(VirtualAttr[gIndex].Dimmer), ctCloseTimes);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : ActualAddr_GetStableLevel
 功能描述  : 获取实际端口的稳定值
 输入参数  : ActualPortTypedef *actPort
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月11日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ActualAddr_GetStableLevel(ActualPortTypedef *actPort, u8 *gLevel)
{
    if((actPort == NULL)
            || (gLevel == NULL))
    {
        return ERROR;
    }

    return VirtualAddr_GetStableLevel(&(actPort->Dimmer), gLevel);
}

/*****************************************************************************
 函 数 名  : ActualAddr_GetStableVersion
 功能描述  : 获取实际端口的版本号
 输入参数  : ActualPortTypedef *actPort
             s8 *gVersion
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年12月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ActualAddr_GetStableVersion(ActualPortTypedef *actPort, s8 *gVersion)
{
    if((actPort == NULL)
            || (gVersion == NULL))
    {
        return ERROR;
    }

    return VirtualAddr_GetStableVersion(&(actPort->Dimmer), gVersion);
}

/*****************************************************************************
 函 数 名  : ActualAddr_GetRealLevel
 功能描述  : 获取实际端口的实际值
 输入参数  : ActualPortTypedef *actPort
             u8 *gLevel
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月11日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ActualAddr_GetRealLevel(ActualPortTypedef *actPort, u8 *gLevel)
{
    if((actPort == NULL)
            || (gLevel == NULL))
    {
        return ERROR;
    }

    return VirtualAddr_GetRealLevel(&(actPort->Dimmer), gLevel);
}

/*****************************************************************************
 函 数 名  : ActualAddr_GetCmdStat
 功能描述  : 获取实际端口的命令状态
 输入参数  : ActualPortTypedef *actPort
             DimmerCmdStat *gCmdStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年12月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ActualAddr_GetCmdStat(ActualPortTypedef *actPort, DimmerCmdStat *gCmdStat)
{
    if((actPort == NULL)
            ||(gCmdStat == NULL))
    {
        return ERROR;
    }

    return VirtualAddr_GetCmdStat(&(actPort->Dimmer), gCmdStat);
}

/*****************************************************************************
 函 数 名  : ActualAddr_GetActiveStat
 功能描述  : 获取实际端口的运行状态
 输入参数  : ActualPortTypedef *actPort
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月11日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FunctionalState ActualAddr_GetActiveStat(ActualPortTypedef *actPort)
{
    if(actPort == NULL)
    {
        return DISABLE;
    }

    return actPort->Activation;
}

/*****************************************************************************
 函 数 名  : ActualAddr_SetActiveStat
 功能描述  : 设置运行状态
 输入参数  : ActualPortTypedef *actPort
             FunctionalState cmdStat
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus ActualAddr_SetActiveStat(ActualPortTypedef *actPort, FunctionalState cmdStat)
{
    if(actPort == NULL)
    {
        return ERROR;
    }

    actPort->Activation = cmdStat;

    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : VirtualAddr_LoadCmd
 功能描述  : 载入命令
 输入参数  : DimmerAttrTypedef *lDimmer
             uc8 *cmdBuff
             uc8 cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_LoadCmd(DimmerAttrTypedef *lDimmer, uc8 *cmdBuff, uc8 cmdLen)
{
    u16 tmpT2 = 0;

    if((lDimmer != NULL)
            &&(cmdBuff != NULL))
    {
        if(cmdLen >= CNL_CMD_LONG_BYTE_LEN)
        {
            if(VirtualAddr_DimmerChkT1(cmdBuff[1]) != SUCCESS)
            {
                return ERROR;
            }

            // 检查T1是否为STOP命令
            if(VirtualAddr_DimmerT1IsStop(cmdBuff[1]) == SET)
            {
                // 指示灯闪一下
                // LedIndicator_BlinkHoldSet(LED_ID_CNL);

                // 执行STOP
                return VirtualAddr_DimmerSetIdle(lDimmer);
            }
            // 检查T1是否为进入自动模式
            else if(VirtualAddr_DimmerT1IsEnterStopMode(cmdBuff[1]) == SET)
            {
                return ERROR;
            }
            else
            {
                // 配置L1  & L2
                lDimmer->SetLevel.SetLevel1 = cmdBuff[0];
                lDimmer->SetLevel.SetLevel2 = cmdBuff[1];

                // 载入T1
                lDimmer->WorkTim.RunTim1 = cmdBuff[2];

                tmpT2 = cmdBuff[3];
                tmpT2 <<= 8;
                tmpT2 += cmdBuff[4];

                // 载入T2
                lDimmer->WorkTim.RunTim2 = tmpT2;

                // 载入T3
                lDimmer->WorkTim.RunTim3 = cmdBuff[5];

                // 状态配置为载入状态
                lDimmer->Stat = DIMMER_STAT_LOAD;

                // 命令状态设置为激活状态
                lDimmer->CmdStat = DIMMER_CMD_STAT_ACTIVE;

                // 指示灯闪一下
                // LedIndicator_BlinkHoldSet(LED_ID_CNL);

                return SUCCESS;
            }
        }
        else if(cmdLen >= CNL_CMD_SHORT_BYTE_LEN)
        {
            if(VirtualAddr_DimmerChkT1(cmdBuff[1]) != SUCCESS)
            {
                return ERROR;
            }

            // 检查T1是否为STOP命令
            if(VirtualAddr_DimmerT1IsStop(cmdBuff[1]) == SET)
            {
                // 指示灯闪一下
                // LedIndicator_BlinkHoldSet(LED_ID_CNL);

                // 执行STOP
                return VirtualAddr_DimmerSetIdle(lDimmer);
            }
            // 检查T1是否为进入自动模式
            else if(VirtualAddr_DimmerT1IsEnterStopMode(cmdBuff[1]) == SET)
            {
                return ERROR;
            }
            else
            {
                // 载入L1
                lDimmer->SetLevel.SetLevel1 = cmdBuff[0];
                // 载入L2
                lDimmer->SetLevel.SetLevel2 = 0;

                // 载入T1
                lDimmer->WorkTim.RunTim1 = cmdBuff[1];

                // 载入T2
                lDimmer->WorkTim.RunTim2 = 0xFFFF;

                // 载入T3
                lDimmer->WorkTim.RunTim3 = 0;

                // 状态配置为载入状态
                lDimmer->Stat = DIMMER_STAT_LOAD;

                // 命令状态设置为激活状态
                lDimmer->CmdStat = DIMMER_CMD_STAT_ACTIVE;

                // 指示灯闪一下
                // LedIndicator_BlinkHoldSet(LED_ID_CNL);

                return SUCCESS;
            }
        }
        else
        {
            return ERROR;
        }
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_VaLoadCmd
 功能描述  : 虚拟地址载入命令
 输入参数  : RunAddrTypedef gAddr
             uc8 *cmdBuff
             uc8 cmdLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月6日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_VaLoadCmd(RunAddrTypedef gAddr, uc8 *cmdBuff, uc8 cmdLen)
{
    u8  loadIndex = 0;

    // 判断该地址是否与 虚拟地址相关
    if((cmdBuff != NULL)
            &&(VirtualAddr_GetIndex(gAddr, &loadIndex) == SUCCESS))
    {
        // 载入虚拟地址控制命令
        return VirtualAddr_LoadCmd(&(VirtualAttr[loadIndex].Dimmer), cmdBuff, cmdLen);
    }

    return ERROR;
}


/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerGetSecFromT1T3
 功能描述  : 从T1T3中获取调光的秒数
 输入参数  : uc8 para
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static u16 VirtualAddr_DimmerGetSecFromT1T3(uc8 para)
{
    u16 res;

    res = (u16)(para & 0x3F);

    if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_0_BIT)
    {
        res *= DIMMER_T1T3_SCALE_0;
    }
    else if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_1_BIT)
    {
        res *= DIMMER_T1T3_SCALE_1;
    }
    else if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_2_BIT)
    {
        res *= DIMMER_T1T3_SCALE_2;
    }
    else
    {
        res = 0;
    }

    return res;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerGetParaFromT2
 功能描述  : 从T2中获取时分秒
 输入参数  : uc8 hms
             uc16 para
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static u8 VirtualAddr_DimmerGetParaFromT2(uc8 hms, uc16 para)
{
    u16 res;

    if(hms == DIMMER_T2_PARA_SECOND)
    {
        res = (para & 0x003F);
    }
    else if(hms == DIMMER_T2_PARA_MINUTE)
    {
        res   = (para & 0x0FC0);
        res >>= 6;
    }
    else if(hms == DIMMER_T2_PARA_HOUR)
    {
        res = para;
        res >>= 12;
    }
    else
    {
        res = 0;
    }

    return (u8)res;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerChkT1
 功能描述  : 检查T1调光值
 输入参数  : uc8 para
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_DimmerChkT1(uc8 para)
{
    if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_0_BIT)
    {
        return SUCCESS;
    }
    else if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_1_BIT)
    {
        return SUCCESS;
    }
    else if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_2_BIT)
    {
        return SUCCESS;
    }
    else
    {
        return ERROR;
    }
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerT1IsStop
 功能描述  : 检查T1参数是否表达为STOP命令
 输入参数  : uc8 para
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus VirtualAddr_DimmerT1IsStop(uc8 para)
{
    if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_0_BIT)
    {
        if((para & (~DIMMER_T1T3_SCALE_BITS)) == CNL_STOP_CMD_CODE)
        {
            return  SET;
        }
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerT1IsEnterStopMode
 功能描述  : 进入自动模式命令
 输入参数  : uc8 para
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年10月24日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus VirtualAddr_DimmerT1IsEnterStopMode(uc8 para)
{
    if((para & DIMMER_T1T3_SCALE_BITS) == DIMMER_T1T3_SCALE_1_BIT)
    {
        if((para & (~DIMMER_T1T3_SCALE_BITS)) == (CNL_ENTER_AUTO_CMD_CODE & (~DIMMER_T1T3_SCALE_BITS)))
        {
            return  SET;
        }
    }

    return RESET;
}



/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerStart
 功能描述  : 开始执行调光
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerStart(DimmerAttrTypedef *pDimmer)
{
    u32 tmpMs;

    if(pDimmer == NULL)
    {
        return ERROR;
    }

    if((pDimmer->LevelStat.RealLevel) > (pDimmer->SetLevel.SetLevel1))
    {
        pDimmer->Trend = DIMMER_TREND_DOWN;
    }
    else if((pDimmer->LevelStat.RealLevel) < (pDimmer->SetLevel.SetLevel1))
    {
        pDimmer->Trend = DIMMER_TREND_UP;
    }
    else
    {
        pDimmer->Trend = DIMMER_TREDN_EQUAL;
    }

    if(pDimmer->Trend == DIMMER_TREDN_EQUAL)
    {
        // 判断是否不为窗帘回路
        if(pDimmer->CtAttr.IsCt != SET)
        {
            Tmm_Clr(pDimmer->WorkTim.RunTmmId);

            pDimmer->Stat = DIMMER_STAT_S1ING;

            return SUCCESS;
        }
        else
        {
            // 判断是否为绝对开窗帘命令
            if(pDimmer->SetLevel.SetLevel1 == 0xFF)
            {
                pDimmer->WorkTim.RunTim1    = pDimmer->CtAttr.OpenTimes;

                // 设置窗帘状态为开窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_OPEN;
            }
            // 判断是否为绝对关窗帘命令
            else if(pDimmer->SetLevel.SetLevel1 == 0x00)
            {
                pDimmer->WorkTim.RunTim1    = pDimmer->CtAttr.CloseTimes;

                // 设置窗帘状态为关窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_CLOSE;
            }
            else
            {
                Tmm_Clr(pDimmer->WorkTim.RunTmmId);

                pDimmer->Stat = DIMMER_STAT_S1ING;

                // 设置窗帘状态为停止
                pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;

                return SUCCESS;
            }

            tmpMs = VirtualAddr_DimmerGetSecFromT1T3(pDimmer->WorkTim.RunTim1);
            tmpMs *= 1000;

            pDimmer->WorkTim.WorkTimTick = tmpMs;

            // 判断窗帘总执行时间 是否为0
            if(tmpMs == 0)
            {
                Tmm_Clr(pDimmer->WorkTim.RunTmmId);

                pDimmer->Stat = DIMMER_STAT_S1ING;

                // 设置窗帘状态为停止
                pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;

                return SUCCESS;
            }
            else
            {
                Tmm_LoadMs(pDimmer->WorkTim.RunTmmId, tmpMs);
                pDimmer->Stat = DIMMER_STAT_S1ING;
            }
        }
    }
    else
    {
        if(pDimmer->CtAttr.IsCt == SET)
        {
            if(pDimmer->Trend == DIMMER_TREND_UP)
            {
                pDimmer->WorkTim.RunTim1    = pDimmer->CtAttr.OpenTimes;

                // 设置窗帘状态为开窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_OPEN;
            }
            else if(pDimmer->Trend == DIMMER_TREND_DOWN)
            {
                pDimmer->WorkTim.RunTim1    = pDimmer->CtAttr.CloseTimes;

                // 设置窗帘状态为关窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_CLOSE;
            }
            else
            {
                return ERROR;
            }
        }

        tmpMs = VirtualAddr_DimmerGetSecFromT1T3(pDimmer->WorkTim.RunTim1);
        tmpMs *= 1000;

        tmpMs >>= 8;
        pDimmer->WorkTim.WorkTimTick = tmpMs;

        if(tmpMs == 0)
        {
            Tmm_Clr(pDimmer->WorkTim.RunTmmId);

            pDimmer->LevelStat.RealLevel = pDimmer->SetLevel.SetLevel1;

            pDimmer->Stat = DIMMER_STAT_S1END;

            if(pDimmer->CtAttr.IsCt == SET)
            {
                // 设置窗帘状态为停止
                pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;
            }

            return SUCCESS;
        }
        else
        {
            Tmm_LoadMs(pDimmer->WorkTim.RunTmmId, tmpMs);
            pDimmer->Stat = DIMMER_STAT_S1ING;
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerS1ING
 功能描述  : 执行L1调光
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerS1ING(DimmerAttrTypedef *pDimmer)
{
    if(pDimmer == NULL)
    {
        return ERROR;
    }

    if(Tmm_IsON(pDimmer->WorkTim.RunTmmId) != SET)
    {
        if(pDimmer->Trend == DIMMER_TREDN_EQUAL)
        {
            pDimmer->Stat = DIMMER_STAT_S1END;
        }
        else
        {
            Tmm_LoadMs(pDimmer->WorkTim.RunTmmId, pDimmer->WorkTim.WorkTimTick);

            if(pDimmer->Trend == DIMMER_TREND_DOWN)
            {
                pDimmer->LevelStat.RealLevel--;

                if((pDimmer->LevelStat.RealLevel) <= (pDimmer->SetLevel.SetLevel1))
                {
                    pDimmer->Stat = DIMMER_STAT_S1END;
                }
            }
            else if(pDimmer->Trend == DIMMER_TREND_UP)
            {
                pDimmer->LevelStat.RealLevel++;

                if((pDimmer->LevelStat.RealLevel) >= (pDimmer->SetLevel.SetLevel1))
                {
                    pDimmer->Stat = DIMMER_STAT_S1END;
                }
            }
            else
            {
            }
        }

    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerS1END
 功能描述  : L1调光结束
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerS1END(DimmerAttrTypedef *pDimmer)
{
    TmmExp_ParaTypedef tmpTmmExp;

    if(pDimmer == NULL)
    {
        return ERROR;
    }

    pDimmer->Trend = DIMMER_TREDN_EQUAL;

    if(pDimmer->CtAttr.IsCt == SET)
    {
        // 设置窗帘状态为停止
        pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;
    }

    if(pDimmer->WorkTim.RunTim2 == 0xFFFF)
    {
        VirtualAddr_DimmerSetIdle(pDimmer);
    }
    else
    {
        tmpTmmExp.TmmSecond  = VirtualAddr_DimmerGetParaFromT2(DIMMER_T2_PARA_SECOND , pDimmer->WorkTim.RunTim2);
        tmpTmmExp.TmmMintue  = VirtualAddr_DimmerGetParaFromT2(DIMMER_T2_PARA_MINUTE , pDimmer->WorkTim.RunTim2);
        tmpTmmExp.TmmHour    = VirtualAddr_DimmerGetParaFromT2(DIMMER_T2_PARA_HOUR , pDimmer->WorkTim.RunTim2);

        TmmExp_Load(pDimmer->WorkTim.RunTmmExpId, tmpTmmExp);

        pDimmer->Stat = DIMMER_STAT_S2ING;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerS2ING
 功能描述  : 执行T2保持
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerS2ING(DimmerAttrTypedef *pDimmer)
{
    if(pDimmer == NULL)
    {
        return ERROR;
    }

    if(TmmExp_IsON(pDimmer->WorkTim.RunTmmExpId) != SET)
    {
        pDimmer->Stat = DIMMER_STAT_S2END;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerS2END
 功能描述  : T2保持时间结束
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerS2END(DimmerAttrTypedef *pDimmer)
{
    u32 tmpMs;

    if(pDimmer == NULL)
    {
        return ERROR;
    }

    if(pDimmer->LevelStat.RealLevel > pDimmer->SetLevel.SetLevel2)
    {
        pDimmer->Trend = DIMMER_TREND_DOWN;
    }
    else if(pDimmer->LevelStat.RealLevel < pDimmer->SetLevel.SetLevel2)
    {
        pDimmer->Trend = DIMMER_TREND_UP;
    }
    else
    {
        pDimmer->Trend = DIMMER_TREDN_EQUAL;
    }

    if(pDimmer->Trend == DIMMER_TREDN_EQUAL)
    {
        // 判断是否不为窗帘回路
        if(pDimmer->CtAttr.IsCt != SET)
        {
            Tmm_Clr(pDimmer->WorkTim.RunTmmId);

            pDimmer->Stat = DIMMER_STAT_S3ING;

            return SUCCESS;
        }
        else
        {
            // 判断是否为绝对开窗帘命令
            if(pDimmer->SetLevel.SetLevel2 == 0xFF)
            {
                pDimmer->WorkTim.RunTim3    = pDimmer->CtAttr.OpenTimes;

                // 设置窗帘状态为开窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_OPEN;
            }
            // 判断是否为绝对关窗帘命令
            else if(pDimmer->SetLevel.SetLevel2 == 0x00)
            {
                pDimmer->WorkTim.RunTim3    = pDimmer->CtAttr.CloseTimes;

                // 设置窗帘状态为关窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_CLOSE;
            }
            else
            {
                Tmm_Clr(pDimmer->WorkTim.RunTmmId);

                pDimmer->Stat = DIMMER_STAT_S3ING;

                // 设置窗帘状态为停止
                pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;

                return SUCCESS;
            }

            tmpMs = VirtualAddr_DimmerGetSecFromT1T3(pDimmer->WorkTim.RunTim3);
            tmpMs *= 1000;

            pDimmer->WorkTim.WorkTimTick = tmpMs;

            // 判断窗帘总执行时间 是否为0
            if(tmpMs == 0)
            {
                Tmm_Clr(pDimmer->WorkTim.RunTmmId);

                pDimmer->Stat = DIMMER_STAT_S3ING;

                // 设置窗帘状态为停止
                pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;

                return SUCCESS;
            }
            else
            {
                Tmm_LoadMs(pDimmer->WorkTim.RunTmmId, tmpMs);
                pDimmer->Stat = DIMMER_STAT_S3ING;
            }
        }
    }
    else
    {
        if(pDimmer->CtAttr.IsCt == SET)
        {
            if(pDimmer->Trend == DIMMER_TREND_UP)
            {
                pDimmer->WorkTim.RunTim3    = pDimmer->CtAttr.OpenTimes;

                // 设置窗帘状态为开窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_OPEN;
            }
            else if(pDimmer->Trend == DIMMER_TREND_DOWN)
            {
                pDimmer->WorkTim.RunTim3    = pDimmer->CtAttr.CloseTimes;

                // 设置窗帘状态为关窗帘
                pDimmer->CtAttr.CtStat      = DIMMER_CT_STAT_CLOSE;
            }
            else
            {
                return ERROR;
            }
        }

        tmpMs = VirtualAddr_DimmerGetSecFromT1T3(pDimmer->WorkTim.RunTim3);
        tmpMs *= 1000;

        tmpMs >>= 8;
        pDimmer->WorkTim.WorkTimTick = tmpMs;

        if(tmpMs == 0)
        {
            Tmm_Clr(pDimmer->WorkTim.RunTmmId);

            pDimmer->LevelStat.RealLevel = pDimmer->SetLevel.SetLevel2;

            pDimmer->Stat = DIMMER_STAT_S3END;

            if(pDimmer->CtAttr.IsCt == SET)
            {
                // 设置窗帘状态为停止
                pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;
            }

            return SUCCESS;
        }
        else
        {
            Tmm_LoadMs(pDimmer->WorkTim.RunTmmId, tmpMs);
            pDimmer->Stat = DIMMER_STAT_S3ING;
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerS3ING
 功能描述  : 执行L2调光
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerS3ING(DimmerAttrTypedef *pDimmer)
{
    if(pDimmer == NULL)
    {
        return ERROR;
    }

    if(Tmm_IsON(pDimmer->WorkTim.RunTmmId) != SET)
    {
        if(pDimmer->Trend == DIMMER_TREDN_EQUAL)
        {
            pDimmer->Stat = DIMMER_STAT_S3END;
        }
        else
        {
            Tmm_LoadMs(pDimmer->WorkTim.RunTmmId, pDimmer->WorkTim.WorkTimTick);

            if(pDimmer->Trend == DIMMER_TREND_DOWN)
            {
                pDimmer->LevelStat.RealLevel--;

                if((pDimmer->LevelStat.RealLevel) <= (pDimmer->SetLevel.SetLevel2))
                {
                    pDimmer->Stat = DIMMER_STAT_S3END;
                }
            }
            else if(pDimmer->Trend == DIMMER_TREND_UP)
            {
                pDimmer->LevelStat.RealLevel++;

                if((pDimmer->LevelStat.RealLevel) >= (pDimmer->SetLevel.SetLevel2))
                {
                    pDimmer->Stat = DIMMER_STAT_S3END;
                }
            }
            else
            {
            }
        }

    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerS3END
 功能描述  : 执行L2调光结束
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerS3END(DimmerAttrTypedef *pDimmer)
{
    if(pDimmer == NULL)
    {
        return ERROR;
    }

    pDimmer->Trend = DIMMER_TREDN_EQUAL;

    if(pDimmer->CtAttr.IsCt == SET)
    {
        // 设置窗帘状态为停止
        pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;
    }

    VirtualAddr_DimmerSetIdle(pDimmer);

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_VersionIteration
 功能描述  : 调光执行器 版本更迭
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年12月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_VersionIteration(DimmerAttrTypedef *pDimmer)
{
    if(pDimmer->Stat == DIMMER_STAT_IDLE)
    {
        /* 判断版本号是否为 初始化态 轮转态 未使用态 */
        if((pDimmer->LevelStat.Version == AG_MTC_VERSION_STAT_INIT)
                ||(pDimmer->LevelStat.Version == AG_MTC_VERSION_STAT_ACIRCLE)
                ||(pDimmer->LevelStat.Version == AG_MTC_VERSION_STAT_UNUSED))
        {
            pDimmer->LevelStat.Version = AG_MTC_VERSION_STAT_START;
        }
        /* 判断版本号是否为 最大的轮转值 */
        else if(pDimmer->LevelStat.Version == AG_MTC_VERSION_MAX_ACIRCLE)
        {
            pDimmer->LevelStat.Version = AG_MTC_VERSION_STAT_ACIRCLE;
        }
        /* 判断版本号是否在 正常迭代范围内 */
        else if((pDimmer->LevelStat.Version >= AG_MTC_VERSION_STAT_START)
                && (pDimmer->LevelStat.Version < AG_MTC_VERSION_MAX_ACIRCLE))
        {
            pDimmer->LevelStat.Version += 1;
        }
        else
        {
            return ERROR;
        }

        pDimmer->LevelStat.VersionUpdate = SET;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerSetIdle
 功能描述  : 调光状态设置为空闲
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerSetIdle(DimmerAttrTypedef *pDimmer)
{
    if(pDimmer == NULL)
    {
        return ERROR;
    }

    if(pDimmer->Stat != DIMMER_STAT_IDLE)
    {
        pDimmer->LevelStat.StableLevel  = pDimmer->LevelStat.RealLevel;
        pDimmer->Trend                  = DIMMER_TREDN_EQUAL;
        pDimmer->Stat                   = DIMMER_STAT_IDLE;
        pDimmer->CmdStat                = DIMMER_CMD_STAT_IDEL;

        if(pDimmer->CtAttr.IsCt == SET)
        {
            // 设置窗帘状态为停止
            pDimmer->CtAttr.CtStat  = DIMMER_CT_STAT_STOP;
        }

        VirtualAddr_VersionIteration(pDimmer);
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_DimmerPoll
 功能描述  : 调光执行函数
 输入参数  : DimmerAttrTypedef *pDimmer
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus VirtualAddr_DimmerPoll(DimmerAttrTypedef *pDimmer)
{
    if(pDimmer != NULL)
    {
        switch(pDimmer->Stat)
        {
        case DIMMER_STAT_IDLE:

            break;

        case DIMMER_STAT_LOAD:
            pDimmer->Stat = DIMMER_STAT_START;
            break;

        case DIMMER_STAT_START:
            VirtualAddr_DimmerStart(pDimmer);
            break;

        case DIMMER_STAT_S1ING:
            VirtualAddr_DimmerS1ING(pDimmer);
            break;

        case DIMMER_STAT_S1END:
            VirtualAddr_DimmerS1END(pDimmer);
            break;

        case DIMMER_STAT_S2ING:
            VirtualAddr_DimmerS2ING(pDimmer);
            break;

        case DIMMER_STAT_S2END:
            VirtualAddr_DimmerS2END(pDimmer);
            break;

        case DIMMER_STAT_S3ING:
            VirtualAddr_DimmerS3ING(pDimmer);
            break;

        case DIMMER_STAT_S3END:
            VirtualAddr_DimmerS3END(pDimmer);
            break;

        default:
            break;
        }

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_CheckVersionUpdate
 功能描述  : 检查状态反馈更新
 输入参数  : VirtualAtrrTypedef *chkVirtualAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年12月9日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus VirtualAddr_CheckVersionUpdate(VirtualAtrrTypedef *chkVirtualAddr)
{
    AddrAttrTypedef sAddr;
    MtcStatAttrTypedef   mtcStat;

    /* 判断版本是否有更新 */
    if(chkVirtualAddr->Dimmer.LevelStat.VersionUpdate == SET)
    {
        /* 复位标志位 */
        chkVirtualAddr->Dimmer.LevelStat.VersionUpdate = RESET;

        /* 载入地址信息 */
        sAddr.Type = CMD_ADDR_TYPE_APP;
        sAddr.AddrBuff[0] = chkVirtualAddr->WorkAddr.App;
        sAddr.AddrBuff[1] = chkVirtualAddr->WorkAddr.Group;

        /* 载入状态信息 */
        mtcStat.Version     = chkVirtualAddr->Dimmer.LevelStat.Version;
        mtcStat.StableLevel = chkVirtualAddr->Dimmer.LevelStat.RealLevel;

        // 载入稳定状态到红外状态中
        IrsLogic_LoadCmd(chkVirtualAddr->WorkAddr, chkVirtualAddr->Dimmer.LevelStat.RealLevel); 

        // 发送命令到RF
        // RF_LoadCmd(chkVirtualAddr->WorkAddr, chkVirtualAddr->Dimmer.LevelStat.RealLevel);

        /* 调用接口发送 */
        return StatLogic_ResponseStat(sAddr, mtcStat);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : VirtualAddr_Poll
 功能描述  : 虚拟地址轮巡函数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月7日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void VirtualAddr_Poll(void)
{
    u8 i;

    // 遍历整个虚拟地址
    for(i=0; i<VIRTUAL_ADDR_LEN; i++)
    {
        // 判断地址是否激活
        if(VirtualAttr[i].Activation == ENABLE)
        {
            // 执行调光
            VirtualAddr_DimmerPoll(&VirtualAttr[i].Dimmer);

            // 检查是否有状态反馈需要发送
            VirtualAddr_CheckVersionUpdate(&VirtualAttr[i]);
        }
    }
}



