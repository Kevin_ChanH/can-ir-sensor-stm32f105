/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Tmm.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : Tmm.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __TMM_H__
#define __TMM_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    TMM_STAT_OFF = 0,
    TMM_STAT_ON = 1
} Tmm_Stat;

typedef enum
{
    TMM_TIMER_EXP = 0,
    TMM_LED_FLASH,          /* LED闪烁时间 */

    TMM_LED_INDICATOR_FLASH,/* LED指示灯闪烁时间 */
    //TMM_RELAY_CNL,          /* 继电器定时时间 */
    
    TMM_CC1101_RX_MONITOR,   /* 监控CC1101接收 */
    
    TMM_CSMA_BACKOFF,       /* CSMA的退避定时器 */

    TMM_RF_APP_SEND,        /* 无线应用发送定时器 */

    TMM_TONGJI,             /* 统计时间 */

    TMM_CAN_OPT_WAIT,       /* CAN操作等待时间 */
    
    TMM_CAN_SEND_STAT,      /* CAN的发送状态统计时间 */
    
    TMM_CAN_SEND,           /* CAN 发送时间 */
    
    TMM_CAN_PACKET,         /* CAN 的数据包发送时间 */

    TMM_AG_STAT_REGET,      /* 获取AG状态 */

    TMM_BH1621_SAMPLING,
    /* 按键状态捕获定时器 */
    //TMM_KEY_START,

    /* 按键状态 */
    //TMM_VIRTUAL_START = (TMM_KEY_START + MCU_KEY_CHANNEL_NUM),
    TMM_INDICATOR_LED_START,
    
    TMM_VIRTUAL_START = (TMM_INDICATOR_LED_START + MCU_LED_INDICATOR_CHANNEL_NUM),

    /* 串口3的接收成帧时间 */
    TMM_UART3_RX_PACK = (TMM_VIRTUAL_START + VIRTUAL_ADDR_LEN),
    /* 串口3的发送间隔时间 */
    TMM_UART3_TX_FC,

    TMM_RF_SEND_QUEUE_START,

    MAX_TMM_NUM = (TMM_RF_SEND_QUEUE_START + MCU_ITF_RF_EXT_QUEUE_SIZE),
} Tmm_ID;

typedef struct
{
    Tmm_Stat TmmStat;
    u32 TmmTick;
} Tmm_TypeDef;

#define TMM_IT_ENABLE       SYSTICK_IT_ENABLE
#define TMM_IT_DISABLE      SYSTICK_IT_DISABLE



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

extern  ErrorStatus Tmm_LoadMs              (Tmm_ID tId, u32 ms);
extern  ErrorStatus Tmm_LoadBaseTick        (Tmm_ID tId, u32 ticks);
extern  ErrorStatus Tmm_Copy                (Tmm_ID bTid, Tmm_ID nTid);
extern  FlagStatus  Tmm_IsON                (Tmm_ID tId);
extern  ErrorStatus Tmm_Clr                 (Tmm_ID tId);
extern  void        Tmm_Init                (void);
extern  void        Tmm_Poll                (void);
extern  void        Tmm_IRQ                 (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif /* __TMM_H__ */

