/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : TmmCnt.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年11月3日
  最近修改   :
  功能描述   : TmmCnt.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年11月3日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __TMMCNT_H__
#define __TMMCNT_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    TMM_CNT_STAT_OFF = 0,
    TMM_CNT_STAT_ON,
    TMM_CNT_STAT_PAUSE,
    TMM_CNT_STAT_CONTINUE,
} TmmCnt_Stat;

typedef enum
{
    TMM_CNT_IRS_HAS_START = 0,
    TMM_CNT_MAX_NUM = (TMM_CNT_IRS_HAS_START + MCU_IRS_CHANNEL_NUM),
}TmmCnt_ID;

typedef struct
{
    TmmCnt_Stat TmmCntStat;
    u32         TmmCntTick;
} TmmCnt_TypeDef;




#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  FlagStatus              TmmCnt_IsId                                     (TmmCnt_ID tmmCntId);

extern  ErrorStatus             TmmCnt_ClrCntTime                               (TmmCnt_ID tmmCntId);

extern  ErrorStatus             TmmCnt_SetStat                                  (TmmCnt_ID tmmCntId, TmmCnt_Stat sStat);
extern  TmmCnt_Stat             TmmCnt_GetStat                                  (TmmCnt_ID tmmCntId);

extern  ErrorStatus             TmmCnt_GetCntTick                               (TmmCnt_ID tmmCntId, u32 *gTick);
extern  ErrorStatus             TmmCnt_GetCntMs                                 (TmmCnt_ID tmmCntId, u32 *gMs);

extern  void                    TmmCnt_Init                                     (void);

extern  void                    TmmCnt_Poll                                     (u32 tickCnt);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __TMMCNT_H__ */
