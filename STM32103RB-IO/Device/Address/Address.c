/******************************************************************************

                  版权所有 (C), 2013-2014, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Address.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年2月27日
  最近修改   :
  功能描述   : 地址操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : Addr_SetUnitAddr
 功能描述  : 设置单元地址
 输入参数  : u8 sAddr[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Addr_SetUnitAddr(u8 sAddr[])
{
    u16 tmpUnitAddr = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sAddr));

    // 组合单元地址
    tmpUnitAddr  = (u16)((sAddr[0]) << 8);
    tmpUnitAddr |= (u16)((sAddr[1]) & 0xFF);

    // 判断取值范围
    if((tmpUnitAddr < UNIT_MAX_VALUE)
     &&(tmpUnitAddr >= UNIT_MIN_VALUE))
    {
        // 设置单元地址
        return AddrMmy_SetUnitAddrParaToRam(sAddr);
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : Addr_GetUnitAddr
 功能描述  : 获取单元地址
 输入参数  : u16 *gUnitAddrValue
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Addr_GetUnitAddr(u16 *gUnitAddrValue)
{
    u16 tmpAddr;
    u8  tmpAddrByte[UNIT_MMY_PARA_LEN] = {0};

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gUnitAddrValue));

    // 获取单元地址
    if(AddrMmy_GetUnitAddrParaFromRam(tmpAddrByte) == SUCCESS)
    {
        tmpAddr  = (u16)((tmpAddrByte[0]) << 8);
        tmpAddr |= (u16)((tmpAddrByte[1]) & 0xFF);

        tmpAddr &= UNIT_MAX_VALUE;

        // 载入单元地址值
        *gUnitAddrValue = tmpAddr;

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : Addr_SetSnAddr
 功能描述  : 设置SN地址
 输入参数  : uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Addr_SetSnAddr(uc8 sParaBuff[])
{
    u8  sIndex = 0;
    u16 tmpMfrs = 0;
    u32 tmpDeviceType = 0;
    u16 tmpDeviceNum = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));

    // 组合产地
    tmpMfrs  = (u16)(sParaBuff[sIndex++] << 8);
    tmpMfrs |= (u16)(sParaBuff[sIndex++] & 0xFF);

    // 组合设备类型
    tmpDeviceType  = (u32)(sParaBuff[sIndex++] << 24);
    tmpDeviceType |= (u32)(sParaBuff[sIndex++] << 16);
    tmpDeviceType |= (u32)(sParaBuff[sIndex++] << 8);
    tmpDeviceType |= (u32)(sParaBuff[sIndex++] & 0xFF);

    // 越过时间
    sIndex += 2;

    // 组合生产序号
    tmpDeviceNum  = (u16)(sParaBuff[sIndex++] << 8);
    tmpDeviceNum |= (u16)(sParaBuff[sIndex++] & 0xFF);

    // 判断生产地址否合法
    if((tmpMfrs > ADDR_MFRS_NUM_MIN)
     &&((tmpMfrs < ADDR_MFRS_NUM_MAX)))
    {
    }
    else
    {
        return ERROR;
    }

    // 判断设备类型是否与本机的设备类型匹配
    if(tmpDeviceType == SYSTEM_DEVICE_TYPE)
    {
    }
    else
    {
        return ERROR;
    }

    if((tmpDeviceNum > ADDR_DEVICE_NUM_MIN)
     &&(tmpDeviceNum < ADDR_DEVICE_NUM_MAX))
    {
    }
    else
    {
        return ERROR;
    }

    // 设置SN地址
    return AddrMmy_SetSnAddrParaToRam(sParaBuff);
}

/*****************************************************************************
 函 数 名  : Addr_GetSnAddr
 功能描述  : 获取SN地址
 输入参数  : u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus Addr_GetSnAddr(u8 gParaBuff[])
{
    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));

    // 获取SN地址
    if(AddrMmy_GetSnAddrParaFromRam(gParaBuff) == SUCCESS)
    {
        //if((gParaBuff[0] != 0xFF)
        // ||(gParaBuff[1] != 0xFF))
        //{
            // SN的第一字节 - 主版本号
        //    gParaBuff[0] = SYSTEM_FIRMWARE_MAIN;
            // SN的第二字节 - 子版本号
        //    gParaBuff[1] = SYSTEM_FIRMWARE_SUB;
        //}

        return SUCCESS;
    }

    return ERROR;
}


/*****************************************************************************
 函 数 名  : Addr_IsSnNotCfg
 功能描述  : 判断SN地址是否未初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus Addr_IsSnNotCfg(void)
{
    u8 tmpSnBuff[SN_MMY_PARA_LEN] = {0};
    u8 i;

    // 获取SN地址
    if(Addr_GetSnAddr(tmpSnBuff) == SUCCESS)
    {
        for(i=0; i<SN_MMY_PARA_LEN; i++)
        {
            // 判断地址是否为 - 未初始化
            if(tmpSnBuff[i] != 0xFF)
            {
                // 返回 已配置
                return RESET;
            }
        }

        // 返回未配置
        return SET;
    }

    // 获取失败 - 返回未配置
    return SET;
}

/*****************************************************************************
 函 数 名  : Addr_IsSnMatch
 功能描述  : 判断SN地址是否匹配
 输入参数  : u8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus Addr_IsSnMatch(u8 sParaBuff[])
{
    u8 i;
    u8 tmpSnBuff[SN_MMY_PARA_LEN] = {0};

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));

    // 获取SN地址 - 判断失败
    if(Addr_GetSnAddr(tmpSnBuff) != SUCCESS)
    {
        // 返回不匹配
        return RESET;
    }

    // 轮询整个SN
    for(i=0; i<SN_MMY_PARA_LEN; i++)
    {
        // 判断地址是否不匹配
        if(tmpSnBuff[i] != sParaBuff[i])
        {
            // 返回不匹配
            return RESET;
        }
    }

    return SET;
}

/*****************************************************************************
 函 数 名  : Addr_IsUnitMatch
 功能描述  : 判断单元地址是否匹配
 输入参数  : uc16 sUnit
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus Addr_IsUnitMatch(uc16 sUnit)
{
    u16 tmpUnitAddr = 0;

    // 获取单元地址 - 判断失败
    if(Addr_GetUnitAddr(&tmpUnitAddr) != SUCCESS)
    {
        // 返回不匹配
        return RESET;
    }

    // 判断地址是否不匹配
    if(sUnit != tmpUnitAddr)
    {
        // 返回不匹配
        return RESET;
    }

    return SET;
}
