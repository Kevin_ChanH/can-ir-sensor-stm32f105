/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Address_MMY.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年2月27日
  最近修改   :
  功能描述   : Address_MMY.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __ADDRESS_MMY_H__
#define __ADDRESS_MMY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/* Unit Addr的最大值 */
#define     UNIT_MAX_VALUE              0x01FF
#define     UNIT_MIN_VALUE              0x0001


/* SN Addr 存储参数 占10Byte */
#define     SN_MMY_PARA_LEN             SYSTEM_SN_APP_LEN               /* 占用10Bytes配置字节 */
#define     SN_MMY_DATA_TYPE            MMY_DATA_TYPE_MID               /* 存储数据类型为中等类型 */
#define     SN_MMY_CFG_MSG_SIZE         MCU_MMY_CFG_MSG_MID_SIZE        /* 存储最大字节数为32Byte */


/* Unit Addr 存储参数 占2Byte */
#define     UNIT_MMY_PARA_LEN           SYSTEM_UNIT_ADDR_LEN            /* 占用10Bytes配置字节 */
#define     UNIT_MMY_DATA_TYPE          MMY_DATA_TYPE_MID               /* 存储数据类型为中等类型 */
#define     UNIT_MMY_CFG_MSG_SIZE       MCU_MMY_CFG_MSG_MID_SIZE        /* 存储最大字节数为32Byte */






#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus     AddrMmy_GetSnAddrParaFromMMY            (u8 gParaBuff[]);
extern  ErrorStatus     AddrMmy_SetSnAddrParaToMMY              (uc8 sParaBuff[]);
extern  ErrorStatus     AddrMmy_GetSnAddrParaFromRam            (u8 gParaBuff[]);
extern  ErrorStatus     AddrMmy_SetSnAddrParaToRam              (uc8 sParaBuff[]);

extern  ErrorStatus     AddrMmy_GetUnitAddrParaFromMMY          (u8 gParaBuff[]);
extern  ErrorStatus     AddrMmy_SetUnitAddrParaToMMY            (uc8 sParaBuff[]);
extern  ErrorStatus     AddrMmy_GetUnitAddrParaFromRam          (u8 gParaBuff[]);
extern  ErrorStatus     AddrMmy_SetUnitAddrParaToRam            (uc8 sParaBuff[]);

extern  void            AddrMmy_RunParaInit                     (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __ADDRESS_MMY_H__ */
