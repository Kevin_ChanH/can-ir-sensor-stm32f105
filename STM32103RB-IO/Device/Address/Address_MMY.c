/******************************************************************************

                  版权所有 (C), 2013-2014, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : Address_MMY.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2017年2月27日
  最近修改   :
  功能描述   : ADDR运行参数存储操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static u8 SnAddrMmyPara[SN_MMY_PARA_LEN]     = {0};
static u8 UnitAddrMmyPara[UNIT_MMY_PARA_LEN] = {0};

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
static uc8 SnAddrMmyParaDefault[SN_MMY_PARA_LEN] =
{0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

static uc8 UnitAddrMmyParaDefault[UNIT_MMY_PARA_LEN] =
{0xFF, 0xFF};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : AddrMmy_GetSnAddrParaFromMMY
 功能描述  : 从FLASH中获取SN地址
 输入参数  : u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus AddrMmy_GetSnAddrParaFromMMY(u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr = MCU_MMY_MAC_ADDR;
    u8          i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));


    /* 载入存储类型 */
    gMmyDat.Type = SN_MMY_DATA_TYPE;

    /* 获取地址下标 */
    gMmyAddr += ((0) * SN_MMY_CFG_MSG_SIZE);

    /* 获取FLASH数据 */
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<SN_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : AddrMmy_SetSnAddrParaToMMY
 功能描述  : 设置SN地址到FLASH中
 输入参数  : uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus  AddrMmy_SetSnAddrParaToMMY(uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr = MCU_MMY_MAC_ADDR;
    u8          i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));


    /* 载入存储类型 */
    sMmyDat.Type = SN_MMY_DATA_TYPE;

    /* 获取地址下标 */
    sMmyAddr += ((0) * SN_MMY_CFG_MSG_SIZE);

    /* 载入数据 */
    for(i = 0; i<SN_MMY_CFG_MSG_SIZE; i++)
    {
        if(i < SN_MMY_PARA_LEN)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    /* 将数据载入到FLASH内 */
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : AddrMmy_GetSnAddrParaFromRam
 功能描述  : 从RAM中获取SN地址到
 输入参数  : u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus AddrMmy_GetSnAddrParaFromRam(u8 gParaBuff[])
{
    u8 i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));

    /* 复制数据 */
    for(i = 0; i<SN_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = SnAddrMmyPara[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : AddrMmy_SetSnAddrParaToRam
 功能描述  : 设置SN地址参数到RAM中
 输入参数  : uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus AddrMmy_SetSnAddrParaToRam(uc8 sParaBuff[])
{
    u8          i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));

    /* 载入数据 */
    for(i = 0; i<SN_MMY_PARA_LEN; i++)
    {
        SnAddrMmyPara[i] = sParaBuff[i];
    }

    return AddrMmy_SetSnAddrParaToMMY(sParaBuff);
}

/*****************************************************************************
 函 数 名  : AddrMmy_GetUnitAddrParaFromMMY
 功能描述  : 从FLASH中获取单元地址
 输入参数  : u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus AddrMmy_GetUnitAddrParaFromMMY(u8 gParaBuff[])
{
    MmyDataBuff gMmyDat;
    u32         gMmyAddr = MCU_MMY_UNIT_ADDR;
    u8          i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));


    /* 载入存储类型 */
    gMmyDat.Type = UNIT_MMY_DATA_TYPE;

    /* 获取地址下标 */
    gMmyAddr += ((0) * UNIT_MMY_CFG_MSG_SIZE);

    /* 获取FLASH数据 */
    if(MMY_McuGetFlashData(gMmyAddr, &gMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    /* 复制数据 */
    for(i = 0; i<UNIT_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = gMmyDat.Buff[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : AddrMmy_SetUnitAddrParaToMMY
 功能描述  : 设置单元地址到FLASH中
 输入参数  : uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus  AddrMmy_SetUnitAddrParaToMMY(uc8 sParaBuff[])
{
    MmyDataBuff sMmyDat;
    u32         sMmyAddr = MCU_MMY_UNIT_ADDR;
    u8          i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));


    /* 载入存储类型 */
    sMmyDat.Type = UNIT_MMY_DATA_TYPE;

    /* 获取地址下标 */
    sMmyAddr += ((0) * UNIT_MMY_CFG_MSG_SIZE);

    /* 载入数据 */
    for(i = 0; i<UNIT_MMY_CFG_MSG_SIZE; i++)
    {
        if(i < UNIT_MMY_PARA_LEN)
        {
            sMmyDat.Buff[i] = sParaBuff[i];
        }
        else
        {
            sMmyDat.Buff[i] = 0;
        }
    }

    /* 将数据载入到FLASH内 */
    if(MMY_McuSetFlashData(sMmyAddr, sMmyDat) != SUCCESS)
    {
        return ERROR;
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : AddrMmy_GetUnitAddrParaFromRam
 功能描述  : 从RAM中获取单元地址
 输入参数  : u8 gParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus AddrMmy_GetUnitAddrParaFromRam(u8 gParaBuff[])
{
    u8 i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(gParaBuff));

    /* 复制数据 */
    for(i = 0; i<UNIT_MMY_PARA_LEN; i++)
    {
        gParaBuff[i] = UnitAddrMmyPara[i];
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : AddrMmy_SetUnitAddrParaToRam
 功能描述  : 设置单元地址到RAM中
 输入参数  : uc8 sParaBuff[]
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus AddrMmy_SetUnitAddrParaToRam(uc8 sParaBuff[])
{
    u8          i = 0;

    assert_app_param(IS_PARAM_POINTER_NOT_NULL(sParaBuff));

    /* 载入数据 */
    for(i = 0; i<UNIT_MMY_PARA_LEN; i++)
    {
        UnitAddrMmyPara[i] = sParaBuff[i];
    }

    return AddrMmy_SetUnitAddrParaToMMY(sParaBuff);
}

/*****************************************************************************
 函 数 名  : AddrMmy_RunParaInit
 功能描述  : 初始化地址运行参数
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2017年2月27日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void AddrMmy_RunParaInit(void)
{
    // 获取SN 地址
    if(AddrMmy_GetSnAddrParaFromMMY(SnAddrMmyPara) != SUCCESS)
    {
        AddrMmy_SetSnAddrParaToRam(SnAddrMmyParaDefault);
    }

    // 获取Unit 地址
    if(AddrMmy_GetUnitAddrParaFromMMY(UnitAddrMmyPara) != SUCCESS)
    {
        AddrMmy_SetUnitAddrParaToRam(UnitAddrMmyParaDefault);
    }
}
