#include "includes.h"

static CC1101_CCA_STATUS CC1101_GetCcaStat       (void);
static void              CC1101_QueueInit        (void);
static void              CC1101_GpioInit         (void);
static void              CC1101_NvicInit         (uc8 prePrio, uc8 subPrio, FunctionalState nvicCmd);
static void              CC1101_Strobe           (uc8 sData);
static void              CC1101_WriteReg         (u8 address, uc8 sDataBuff[], uc8 len);
static void              CC1101_ReadReg          (u8 address, u8 sDataBuff[], uc8 len);
static void              CC1101_PowerOn          (void);
static void              CC1101_SetRegDefault    (void);
static void              CC1101_ItCmd            (uc8 itSelect, FunctionalState itCmd);
static void              CC1101_MonitorRx        (void);
static ErrorStatus       CC1101_StartRx          (RFSFrame *rxFrame);
static void              CC1101_StartTx          (RFSFrame *txFrame);
static void              CC1101_SimulateInit     (void);

 RFSQueue CC1101RxQ;
 RFSQueue CC1101TxQ;

static CC1101_StatusTypedef CC1101_Stat;

static u8 CC1101RxBuff[CC1101_RX_QUEUE_SIZE][CC1101_RX_BUFF_SIZE] = {0};
static u8 CC1101TxBuff[CC1101_TX_QUEUE_SIZE][CC1101_TX_BUFF_SIZE] = {0};

static RFSFrame CC1101RxSFrame[CC1101_RX_QUEUE_SIZE];
static RFSFrame CC1101TxSFrame[CC1101_TX_QUEUE_SIZE];

u8 CC1101RxFIFOLenth = 0;
u8 LQIValue = 0;
s16 RSSIValue = 0;
u8 CC1101DeviceStat = 0;

// CC1101的默认寄存器长度
#define CC1101_DEFAULT_REG_LENGTH   0x2F

#define RF_BAUDRATE_64

// CC1101的默认寄存器配置表
const u8 CC1101DefaultReg[CC1101_DEFAULT_REG_LENGTH] =
{
    0x06,    //  0x00   IOCFG2    Asserts when sync word has been sent / received, and de-asserts at the end of the packet.
    0x2E,    //  0x01   IOCFG1    GDO1，SO，
    0x09,//0x0E    //  0x02   IOCFG0    0x0E:Carrier sense. High if RSSI level is above threshold. Cleared when entering IDLE mode.
    0x47,    //  0x03   FIFOTHR   // S
    0xD3,    //  0x04   SYNC1     Default
    0x91,    //  0x05   SYNC0     Default
    0xFF,    //  0x06   PKTLEN    Default
    0x0C,    //  0x07   PKTCTRL1  Auto Flash,Append Status; S  ->08 TAG1
    0x05,    //  0x08   PKTCTRL0  Whitening off,Normal Mode,CRC Enable,Variable packet length; S
    0x00,    //  0x09   ADDR      Default
    0x4B,    //  0x0A   CHANNR    Channel Num=75   S
    0x06,    //  0x0B   FSCTRL1   S
    0x00,    //  0x0C   FSCTRL0   S
    0x0B,    //  0x0D   FREQ2     S
    0x89,    //  0x0E   FREQ1     S
    0xD8,    //  0x0F   FREQ0     S
    0xC8,    //  0x10   MDMCFG4   S
    0x93,    //  0x11   MDMCFG3   S
    0x03,    //  0x12   MDMCFG2   S
    0x22,    //  0x13   MDMCFG1   S
    0xF8,    //  0x14   MDMCFG0   S
    0x34,    //  0x15   DEVIATN   S
    0x07,    //  0x16   MCSM2    Default
    0x30,    //  0x17   MCSM1    0x3C:RX OFF Stay in rx, 0X30:IDLE
    0x18,    //  0x18   MCSM0    XX
    0x16,    //  0x19   FOCCFG   S
    0x6C,    //  0x1A   BSCFG    S
    0x07,    //  0x1B   AGCCTRL2  S (0x43->0x07)42 dB
    0x47,    //  0x1C   AGCCTRL1  S 4dB above MAGN_TARGET setting
    0x91,    //  0x1D   AGCCTRL0  S
    0x87,    //  0x1E   WOREVT1   Default
    0x6B,    //  0x1F   WOREVT0   Default
    0xF8,    //  0x20   WORCTRL   Default
    0x56,    //  0x21   FREND1    S
    0x10,    //  0x22   FREND0    S
    0xE9,    //  0x23   FSCAL3    S
    0x2A,    //  0x24   FSCAL2    S
    0x00,    //  0x25   FSCAL1    S
    0x1F,    //  0x26   FSCAL0    S
    0x41,    //  0x27   RCCTRL1   Default
    0x00,    //  0x28   RCCTRL0   Default
    0x59,    //  0x29   FSTEST    S
    0x7F,    //  0x2A   PTEST     Default
    0x3F,    //  0x2B   AGCTEST   Default
    0x81,    //  0x2C   TEST2     s
    0x35,    //  0x2D   TEST1     s
    0x0B,    //  0x2E   TEST0     s
};


#define CC1101_FRAME_LEN_MIN        8
#define CC1101_FRAME_LEN_MAX        62

#define CC1101_REG_PATABLE          0x3E
#define CC1101_REG_FIFO             0x3F
#define CC1101_REG_RESTART          0x30
#define CC1101_REG_SRX              0x34
#define CC1101_REG_STX              0x35
#define CC1101_REG_SIDEL            0x36
#define CC1101_REG_SFRX             0x3A
#define CC1101_REG_SFTX             0x3B

#define CC1101_RSSI_READ            0xF4
#define CC1101_MARC_STAT_READ       0xF5
#define CC1101_PKTSTAT_READ         0xF8
#define CC1101_RX_BYTES_STAT_READ   0xFB
#define CC1101_PATABLE_STAT_READ    0xFE

FlagStatus CC1101_CrcStat = RESET;
FlagStatus CC1101_CsStat = RESET;
FlagStatus CC1101_CcaStat = RESET;
FlagStatus CC1101_Gdo2Stat = RESET;
FlagStatus CC1101_Gdo0Stat = RESET;

/*****************************************************************************
 函 数 名  : CC1101_GetCcaStat
 功能描述  : 获取CCA状态
 输入参数  : void  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月25日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static CC1101_CCA_STATUS CC1101_GetCcaStat(void)
{
    //CC1101_CCA_STATUS tmpCcaStat = CC1101_CCA_CLR;
    // 关闭 GDO0 中断
    //CC1101_ItCmd(CC1101_IT_GDO0, DISABLE);

    CC1101_CcaIRQ();

    //tmpCcaStat = CC1101_Stat.CCA_Stat;

    // 开启 GDO0 中断
    //CC1101_ItCmd(CC1101_IT_GDO0, ENABLE);

    // 返回状态
    //return tmpCcaStat;
    return CC1101_Stat.CCA_Stat;
}

/*****************************************************************************
 函 数 名  : CC1101_IsRfChannelClr
 功能描述  : 判断信道是否空闲
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月25日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus CC1101_IsRfChannelClr(void)
{
    if(CC1101_GetCcaStat() == CC1101_CCA_SET)
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : CC1101_QueueInit
 功能描述  : 初始化队列
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月25日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_QueueInit(void)
{
    u32 i = 0;

    CC1101RxQ.QSize = CC1101_RX_QUEUE_SIZE;
    CC1101RxQ.QHead = 0;
    CC1101RxQ.QTail = 0;
    CC1101RxQ.QStat = RF_STAT_IDEL;

    CC1101RxQ.sFrame = CC1101RxSFrame;
    for(i = 0; i<CC1101_RX_QUEUE_SIZE; i++)
    {
        CC1101RxQ.sFrame[i].buff  = &CC1101RxBuff[i][0];
        CC1101RxQ.sFrame[i].size  = CC1101_RX_BUFF_SIZE;
        CC1101RxQ.sFrame[i].len   = 0;
        CC1101RxQ.sFrame[i].index = 0;
        CC1101RxQ.sFrame[i].trmitPower = CC1101_TRANSMIT_POWER_NORMAL;
    }

    CC1101TxQ.QSize = CC1101_TX_QUEUE_SIZE;
    CC1101TxQ.QHead = 0;
    CC1101TxQ.QTail = 0;
    CC1101TxQ.QStat = RF_STAT_IDEL;

    CC1101TxQ.sFrame = CC1101TxSFrame;
    for(i = 0; i<CC1101_TX_QUEUE_SIZE; i++)
    {
        CC1101TxQ.sFrame[i].buff  = &CC1101TxBuff[i][0];
        CC1101TxQ.sFrame[i].size  = CC1101_TX_BUFF_SIZE;
        CC1101TxQ.sFrame[i].len   = 0;
        CC1101TxQ.sFrame[i].index = 0;
        CC1101TxQ.sFrame[i].trmitPower = CC1101_TRANSMIT_POWER_NORMAL;
    }
}

/*****************************************************************************
 函 数 名  : CC1101_GpioInit
 功能描述  : 初始化GPIO
 输入参数  : void  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月25日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_GpioInit(void)
{
    #if ((CC1101_GDO0_IS_ENABLE == ENABLE) || (CC1101_GDO2_IS_ENABLE == ENABLE))

    GPIO_InitTypeDef GPIO_InitStructure;
    u16              gpioPin = 0;

    RCC_APB2PeriphClockCmd(CC1101_GPIO_PERIPH, ENABLE);

    if(CC1101_GDO0_IS_ENABLE == ENABLE)
    {
        gpioPin |= CC1101_PIN_GDO0;
    }

    if(CC1101_GDO2_IS_ENABLE == ENABLE)
    {
        gpioPin |= CC1101_PIN_GDO2;
    }

    SPI2_GpioInit();

    GPIO_InitStructure.GPIO_Pin = gpioPin ;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;       /* 最大风险点 变成上拉输入风险最大 */
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(CC1101_PORT, &GPIO_InitStructure);

    CC1101_NvicInit(CC1101_CFG_GDO_NVIC_PRE, CC1101_CFG_GDO_NVIC_SUB, ENABLE);
    #endif
}

/*****************************************************************************
 函 数 名  : CC1101_NvicInit
 功能描述  : 初始化向量中断
 输入参数  : uc8 prePrio              
             uc8 subPrio              
             FunctionalState nvicCmd  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月25日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_NvicInit(uc8 prePrio, uc8 subPrio, FunctionalState nvicCmd)
{
    NVIC_InitTypeDef NVIC_InitStruCture;
    EXTI_InitTypeDef EXTI_InitStructure;

    if(CC1101_GDO0_IT_IS_ENABLE == ENABLE)
    {
        GPIO_EXTILineConfig(CC1101_EXIT_SOURCE_PORT,CC1101_GDO0_EXIT_PIN_SOURCE);
        EXTI_ClearITPendingBit(CC1101_GDO0_EXIT_LINE);

        EXTI_InitStructure.EXTI_Line = CC1101_GDO0_EXIT_LINE;
        EXTI_InitStructure.EXTI_Mode = CC1101_GDO0_EXIT_MODE;
        EXTI_InitStructure.EXTI_Trigger = CC1101_GDO0_EXIT_TRIG;
        EXTI_InitStructure.EXTI_LineCmd = ENABLE;
        EXTI_Init(&EXTI_InitStructure);

        NVIC_InitStruCture.NVIC_IRQChannel = EXTI0_IRQn;
        NVIC_InitStruCture.NVIC_IRQChannelPreemptionPriority = prePrio;
        NVIC_InitStruCture.NVIC_IRQChannelSubPriority = subPrio;
        NVIC_InitStruCture.NVIC_IRQChannelCmd = nvicCmd;
        NVIC_Init(&NVIC_InitStruCture);
    }


    if(CC1101_GDO2_IT_IS_ENABLE == ENABLE)
    {
        GPIO_EXTILineConfig(CC1101_EXIT_SOURCE_PORT,CC1101_GDO2_EXIT_PIN_SOURCE);
        EXTI_ClearITPendingBit(CC1101_GDO2_EXIT_LINE);

        EXTI_InitStructure.EXTI_Line = CC1101_GDO2_EXIT_LINE;
        EXTI_InitStructure.EXTI_Mode = CC1101_GDO2_EXIT_MODE;
        EXTI_InitStructure.EXTI_Trigger = CC1101_GDO2_EXIT_TRIG;
        EXTI_InitStructure.EXTI_LineCmd = ENABLE;
        EXTI_Init(&EXTI_InitStructure);

        NVIC_InitStruCture.NVIC_IRQChannel = EXTI1_IRQn;
        NVIC_InitStruCture.NVIC_IRQChannelPreemptionPriority = prePrio;
        NVIC_InitStruCture.NVIC_IRQChannelSubPriority = subPrio;
        NVIC_InitStruCture.NVIC_IRQChannelCmd = nvicCmd;
        NVIC_Init(&NVIC_InitStruCture);
    }

}

/*****************************************************************************
 函 数 名  : CC1101_Strobe
 功能描述  : SPI发送单字节
 输入参数  : uc8 sData  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月25日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_Strobe(uc8 sData)
{
    SPI2_CS_L;

    SPI2_SendByte(sData);

    SPI2_CS_H;
}

/*****************************************************************************
 函 数 名  : CC1101_WriteReg
 功能描述  : CC1101写入寄存器
 输入参数  : u8 address       
             uc8 sDataBuff[]  
             uc8 len          
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_WriteReg(u8 address, uc8 sDataBuff[], uc8 len)
{
    u8  i = 0;

    if(len == 0)
    {
        return;
    }

    SPI2_CS_L;

    while(SPI2_MISO_GET);

    address &= 0x3F;
    if(len > 1)
    {
        address |= 0x40;
    }

    SPI2_SendByte(address);

    for(i = 0; i<len; i++)
    {
        SPI2_SendByte(sDataBuff[i]);
    }

    SPI2_CS_H;
}

/*****************************************************************************
 函 数 名  : CC1101_ReadReg
 功能描述  : CC1101读取寄存器
 输入参数  : u8 address      
             u8 sDataBuff[]  
             uc8 len         
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_ReadReg(u8 address, u8 sDataBuff[], uc8 len)
{
    u8  i = 0;

    if(len == 0)
    {
        return;
    }

    SPI2_CS_L;

    while(SPI2_MISO_GET);

    address &= 0x3F;
    address |= 0x80;

    if(len > 1)
    {
        address |= 0x40;
    }

    SPI2_SendByte(address);

    for(i = 0; i<len; i++)
    {
        sDataBuff[i] = SPI2_SendByte(0x00);
    }

    SPI2_CS_H;
}

/*****************************************************************************
 函 数 名  : CC1101_ReadStat
 功能描述  : CC1101读取状态
 输入参数  : u8 statAddr  
             u8 gStat[]   
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus CC1101_ReadStat(u8 statAddr, u8 gStat[])
{
    u8 i;
    u8 GrStatBuff[8] = {0};

    for(i=0; i<8; i++)
    {
        SPI2_CS_L;

        while(SPI2_MISO_GET);

        SPI2_SendByte(statAddr);

        GrStatBuff[i] = SPI2_SendByte(0x00);

        SPI2_CS_H;
    }

    gStat[0] = GrStatBuff[0];

    for(i=0; i<7; i++)
    {
        if(GrStatBuff[i] != GrStatBuff[i+1])
        {
            return ERROR;
        }
    }

    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : CC1101_PowerOn
 功能描述  : CC1101上电时序
 输入参数  : void  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_PowerOn(void)
{
    SPI2_CLK_H;

    SPI2_MOSI_L;

    SPI2_CS_H;

    Delay_us(30);

    SPI2_CS_L;
    Delay_us(30);

    SPI2_CS_H;
    Delay_us(45);

    CC1101_Strobe(CC1101_REG_RESTART);
    Delay_us(500);
}

/*****************************************************************************
 函 数 名  : CC1101_SetRegDefault
 功能描述  : CC1101设置默认配置
 输入参数  : void  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_SetRegDefault(void)
{
    CC1101_WriteReg(0x00, CC1101DefaultReg, CC1101_DEFAULT_REG_LENGTH);
}

// 
/*****************************************************************************
 函 数 名  : CC1101_ItCmd
 功能描述  : CC1101中断开关
 输入参数  : uc8 itSelect           
             FunctionalState itCmd  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_ItCmd(uc8 itSelect, FunctionalState itCmd)
{
    if(CC1101_GDO0_IT_IS_ENABLE == ENABLE)
    {
        if((itSelect & CC1101_IT_GDO0) == CC1101_IT_GDO0)
        {
            if(itCmd == ENABLE)
            {
                //EXTI_ClearFlag(CC1101_GDO0_EXIT_LINE);
                //CC1101_GDO0_IT_ENABLE;
                SYSTEM_ENABLE_ALL_IRQ;
            }
            else
            {
                SYSTEM_DISABLE_ALL_IRQ;
                //EXTI_ClearFlag(CC1101_GDO0_EXIT_LINE);
                //CC1101_GDO0_IT_DISABLE;
            }
        }
    }

    if(CC1101_GDO2_IT_IS_ENABLE == ENABLE)
    {
        if((itSelect & CC1101_IT_GDO2) == CC1101_IT_GDO2)
        {
            if(itCmd == ENABLE)
            {
                SYSTEM_ENABLE_ALL_IRQ;
                //EXTI_ClearFlag(CC1101_GDO2_EXIT_LINE);
                //CC1101_GDO2_IT_ENABLE;
            }
            else
            {
                SYSTEM_DISABLE_ALL_IRQ;
                //EXTI_ClearFlag(CC1101_GDO2_EXIT_LINE);
                //CC1101_GDO2_IT_DISABLE;
            }
        }
    }
}

/*****************************************************************************
 函 数 名  : CC1101_MonitorRx
 功能描述  : CC1101监控RX
 输入参数  : void  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_MonitorRx(void)
{
    u8  getStat = 0;
    static vu8 rxErrCnt = 0;
    static vu8 rxStatErrCnt = 0;

    // 判断RX监控时间是否到达
    if(Tmm_IsON(TMM_CC1101_RX_MONITOR) != SET)
    {
        // 重新载入监控时间
        Tmm_LoadMs(TMM_CC1101_RX_MONITOR, CC1101_MONITOR_TICK_MS);

        // 读取MARC状态 并判断是否失败
        if(CC1101_ReadStat(CC1101_MARC_STAT_READ, &getStat) != SUCCESS)
        {
            // 失败次数+1
            rxStatErrCnt++;

            // 载入重新监控时间
            Tmm_LoadMs(TMM_CC1101_RX_MONITOR, 50*rxStatErrCnt);

            // 判断错误次数是否连续累计到10
            if(rxStatErrCnt >= 10)
            {
                // 错误次数清零
                rxStatErrCnt = 0;

                // 关闭GDO2中断
                CC1101_ItCmd(CC1101_IT_GDO2, DISABLE);

                // 重新复位整个CC1101
                CC1101_PowerOn();
                CC1101_SetRegDefault();

                CC1101_Strobe(CC1101_REG_SIDEL);
                CC1101_Strobe(CC1101_REG_SFRX);
                CC1101_Strobe(CC1101_REG_SRX);

                // 开启GDO2中断
                CC1101_ItCmd(CC1101_IT_GDO2, ENABLE);
            }

            return;
        }
        else
        {
            // 判断错误计数是否大于0
            if(rxStatErrCnt > 0)
            {
                // 错误计数-1
                rxStatErrCnt--;
            }
        }

        // 载入设备状态
        CC1101DeviceStat = getStat;

        // 判断设备状态是否超出正常范围
        if((getStat < 13) || (getStat > 15))
        {
            // 接收错误次数+1
            rxErrCnt++;

            // 判断接收错误次数是否大于等于15次
            if(rxErrCnt >= 15)
            {
                // 接收错误次数归零
                rxErrCnt = 0;

                // 关闭GDO2中断
                CC1101_ItCmd(CC1101_IT_GDO2, DISABLE);

                // 重新复位整个CC1101
                CC1101_PowerOn();
                CC1101_SetRegDefault();

                CC1101_Strobe(CC1101_REG_SIDEL);
                CC1101_Strobe(CC1101_REG_SFRX);
                CC1101_Strobe(CC1101_REG_SRX);

                // 开启GDO2中断
                CC1101_ItCmd(CC1101_IT_GDO2, ENABLE);
            }

            // 判断接收错误次数是否大于等于5次
            if(rxErrCnt >= 5)
            {
                // 关闭GDO2中断
                CC1101_ItCmd(CC1101_IT_GDO2, DISABLE);

                CC1101_Strobe(CC1101_REG_SIDEL);
                CC1101_Strobe(CC1101_REG_SFRX);
                CC1101_Strobe(CC1101_REG_SRX);

                // 开启GDO2中断
                CC1101_ItCmd(CC1101_IT_GDO2, ENABLE);
            }

            if(rxErrCnt > 0)
            {
                // 载入等待时间
                Tmm_LoadMs(TMM_CC1101_RX_MONITOR, (50*rxErrCnt));
            }
        }
        else
        {
            // 判断接收错误次数
            if(rxErrCnt > 0)
            {
                // 接收错误次数-1
                rxErrCnt--;
            }
        }
    }
}

/*****************************************************************************
 函 数 名  : CC1101_TransmitToRSSI
 功能描述  : CC1101转换RSSI
 输入参数  : u8 gdat     
             s16 *gRSSI  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_TransmitToRSSI(u8 gdat, s16 *gRSSI)
{
    if(gdat >= 128)
    {
        gRSSI[0] = (s16)((s16)(gdat - 256) / 2) - 74;
    }
    else
    {
        gRSSI[0] = (gdat / 2) - 74;
    }
}

/*****************************************************************************
 函 数 名  : CC1101_StartRx
 功能描述  : CC1101开始进入RX状态
 输入参数  : RFSFrame *rxFrame  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static ErrorStatus CC1101_StartRx(RFSFrame *rxFrame)
{
    u8 len = 0;
    u8 addr = 0;
    u8 gRssiDat = 0;

    ErrorStatus res;

    // 读取数据长度
    CC1101_ReadStat(CC1101_RX_BYTES_STAT_READ, &CC1101RxFIFOLenth);

    // 获取数据内容长度
    CC1101_ReadReg(CC1101_REG_FIFO, &len, 1);

    // 判断数据长度是否在范围内
    if((len > CC1101_FRAME_LEN_MIN) && (len <= CC1101_FRAME_LEN_MAX))
    {
        // 获取实际内容长度
        rxFrame->len = len-1;

        // 读取地址字节
        CC1101_ReadReg(CC1101_REG_FIFO, &addr, 1);

        // 读取数据内容
        CC1101_ReadReg(CC1101_REG_FIFO, rxFrame->buff, rxFrame->len);

        // 获取RSSI
        CC1101_ReadReg(CC1101_REG_FIFO, &gRssiDat, 1);

        // 获取LQI
        CC1101_ReadReg(CC1101_REG_FIFO, &LQIValue, 1);

        //CC1101_TransmitToRSSI(gRssiDat, &RSSIValue);

        res = SUCCESS;
    }
    else
    {
        res = ERROR;
    }

    // 关闭GDO2中断
    CC1101_ItCmd(CC1101_IT_GDO2, DISABLE);

    CC1101_Strobe(CC1101_REG_SIDEL);
    CC1101_Strobe(CC1101_REG_SFRX);
    CC1101_Strobe(CC1101_REG_SRX);

    // 清除接收标识
    CC1101_Stat.RxIntFlag = CC1101_IT_FLAG_CLR;

    // 打开GDO2中断
    CC1101_ItCmd(CC1101_IT_GDO2, ENABLE);

    // 重新载入RX监控的定时时间
    Tmm_LoadMs(TMM_CC1101_RX_MONITOR, 100);

    return res;
}

/*****************************************************************************
 函 数 名  : CC1101_StartTx
 功能描述  : CC1101开始进入TX状态
 输入参数  : RFSFrame *txFrame  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_StartTx(RFSFrame *txFrame)
{
    u8 addr = 0;
    u8 txLen = 0;
    u8 transmitPower = 0xC6;    //  +8.5

    // 判断数据长度是否合法
    if(((txFrame->len) == 0) || ((txFrame->len + 2) > CC1101_FRAME_LEN_MAX))
    {
        return;
    }

    // 发送功率为+10.6dB
    if(txFrame->trmitPower == CC1101_TRANSMIT_POWER_HIGHT)
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x12;      //  -29.8
        txPaTable[index++] = 0x0D;      //  -20.0
        txPaTable[index++] = 0x6E;      //  -8.9
        txPaTable[index++] = 0x50;      //  +1.7
        txPaTable[index++] = 0xC6;      //  +8.5
        txPaTable[index++] = 0xC2;      //  +9.9
        txPaTable[index++] = 0xC0;      //  +10.6
        */
        transmitPower = 0xC0;     //  +10.6
    }
    // 发送功率为+8.5dB
    else if(txFrame->trmitPower == CC1101_TRANSMIT_POWER_NORMAL)
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x12;      //  -29.8
        txPaTable[index++] = 0x0D;      //  -20.0
        txPaTable[index++] = 0x34;      //  -9.9
        txPaTable[index++] = 0x51;      //  +0.1
        txPaTable[index++] = 0xC7;      //  +8.2
        txPaTable[index++] = 0xC3;      //  +9.6
        txPaTable[index++] = 0xC2;      //  +9.9
        */
        transmitPower = 0xC6;     //  +8.5
    }
    // 发送功率为+5.0dB
    else if(txFrame->trmitPower == CC1101_TRANSMIT_POWER_MID)
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x12;      //  -29.8
        txPaTable[index++] = 0x0D;      //  -20.0
        txPaTable[index++] = 0x34;      //  -9.9
        txPaTable[index++] = 0x51;      //  +0.1
        txPaTable[index++] = 0xC7;      //  +8.2
        txPaTable[index++] = 0xC3;      //  +9.6
        txPaTable[index++] = 0xC2;      //  +9.9
        */
        transmitPower = 0x85;     //  +5.0
    }
    // 发送功率为+0.1dB
    else if(txFrame->trmitPower == CC1101_TRANSMIT_POWER_AVERAGE)
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x20;      //  -41.3
        txPaTable[index++] = 0x11;      //  -34.6
        txPaTable[index++] = 0x03;      //  -31.7
        txPaTable[index++] = 0x05;      //  -27.7
        txPaTable[index++] = 0x08;      //  -24.0
        txPaTable[index++] = 0x31;      //  -21.3
        txPaTable[index++] = 0x0D;      //  -20.0
        */
        transmitPower = 0x51;     // +0.1dB
    }
    // 发送功率为-5.5dB
    else if(txFrame->trmitPower == CC1101_TRANSMIT_POWER_FLAT)
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x03;      //  -31.7
        txPaTable[index++] = 0x08;      //  -24.0
        txPaTable[index++] = 0x1C;      //  -15.2
        txPaTable[index++] = 0x34;      //  -9.9
        txPaTable[index++] = 0x6B;      //  -6.1
        txPaTable[index++] = 0x8F;      //  -4.2
        txPaTable[index++] = 0x51;      //  +0.1
        */
        transmitPower = 0x6A;     // -5.5dB
    }
    // 发送功率为-9.9dB
    else if(txFrame->trmitPower == CC1101_TRANSMIT_POWER_MINOR)
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x03;      //  -31.7
        txPaTable[index++] = 0x08;      //  -24.0
        txPaTable[index++] = 0x1C;      //  -15.2
        txPaTable[index++] = 0x34;      //  -9.9
        txPaTable[index++] = 0x6B;      //  -6.1
        txPaTable[index++] = 0x8F;      //  -4.2
        txPaTable[index++] = 0x51;      //  +0.1
        */
        transmitPower = 0x34;     // -9.9dB
    }
    // 发送功率为-20.0dB
    else if(txFrame->trmitPower == CC1101_TRANSMIT_POWER_LOW)
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x03;      //  -31.7
        txPaTable[index++] = 0x08;      //  -24.0
        txPaTable[index++] = 0x1C;      //  -15.2
        txPaTable[index++] = 0x34;      //  -9.9
        txPaTable[index++] = 0x6B;      //  -6.1
        txPaTable[index++] = 0x8F;      //  -4.2
        txPaTable[index++] = 0x51;      //  +0.1
        */
        transmitPower = 0x0D;     // -20.0dB
    }
    // 发送功率为+8.5dB
    else
    {
        /*
        txPaTable[index++] = 0x00;      //  -63.8
        txPaTable[index++] = 0x12;      //  -29.8
        txPaTable[index++] = 0x0D;      //  -20.0
        txPaTable[index++] = 0x34;      //  -9.9
        txPaTable[index++] = 0x51;      //  +0.1
        txPaTable[index++] = 0x85;      //  +5.0
        txPaTable[index++] = 0xCB;      //  +6.9
        txPaTable[index++] = 0xC6;      //  +8.5
        */
        transmitPower = 0xC6;     //  +8.5
    }

    // 关闭GDO2中断
    CC1101_ItCmd(CC1101_IT_GDO2, DISABLE);

    CC1101_Strobe(CC1101_REG_SIDEL);
    CC1101_Strobe(CC1101_REG_SFTX);

    // 写入发送功率
    CC1101_WriteReg(CC1101_REG_PATABLE, &transmitPower, 1);

    txLen = txFrame->len + 1;

    // 写入发送长度
    CC1101_WriteReg(CC1101_REG_FIFO, &txLen, 1);
    // 写入发送地址
    CC1101_WriteReg(CC1101_REG_FIFO, &addr, 1);
    // 发送数据内容
    CC1101_WriteReg(CC1101_REG_FIFO, txFrame->buff, txFrame->len);

    // 标识CC1101进入发送状态
    CC1101_Stat.RF_Stat = CC1101_STAT_TX;
    // 标识为等待发送状态
    CC1101_Stat.TxIntFlag = CC1101_IT_FLAG_MARK;

    // 进入发送状态
    CC1101_Strobe(CC1101_REG_SIDEL);
    CC1101_Strobe(CC1101_REG_STX);

    // 开启GDO2中断
    CC1101_ItCmd(CC1101_IT_GDO2, ENABLE);
}

/*****************************************************************************
 函 数 名  : CC1101_SimulateInit
 功能描述  : CC1101模拟初始化
 输入参数  : void  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static void CC1101_SimulateInit(void)
{
    // 重启CC1101
    CC1101_PowerOn();
    CC1101_SetRegDefault();

    // 进入RX状态
    CC1101_Strobe(CC1101_REG_SIDEL);
    CC1101_Strobe(CC1101_REG_SFRX);
    CC1101_Strobe(CC1101_REG_SRX);

    // 设置为RX状态
    CC1101_Stat.RF_Stat = CC1101_STAT_RX;
    CC1101_Stat.RxIntFlag = CC1101_IT_FLAG_CLR;
    CC1101_Stat.CCA_Stat = CC1101_CCA_SET;

    // RX监控等待时间 100ms
    Tmm_LoadMs(TMM_CC1101_RX_MONITOR, 100);
}

/*****************************************************************************
 函 数 名  : CC1101_Init
 功能描述  : CC1101初始化
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_Init(void)
{
    // CC1101队列初始化
    CC1101_QueueInit();
    // CC1101的GPIO管脚
    CC1101_GpioInit();
    // 延时10ms
    Delay_us(10000);
    // CC1101模拟初始化
    CC1101_SimulateInit();
}

/*****************************************************************************
 函 数 名  : CC1101_UpdateRSSI
 功能描述  : CC1101更新RSSI
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_UpdateRSSI(void)
{
    u8 getRssiBuff[20] = {0};
    u8 i = 0;
    u32 rssiValue = 0;
    u8 rssiHex = 0;

    for(i=0; i<20; i++)
    {
        CC1101_ReadStat(CC1101_RSSI_READ, getRssiBuff+i);
    }

    for(i=0; i<20; i++)
    {
        rssiValue += getRssiBuff[i];
    }

    rssiValue /= 20;
    rssiHex = (u8)(rssiValue);

    CC1101_TransmitToRSSI(rssiHex, &RSSIValue);

}

/*****************************************************************************
 函 数 名  : CC1101_UpdatePKTStat
 功能描述  : 读取PKT状态
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_UpdatePKTStat(void)
{
    u8 pktStat = 0;

    if(CC1101_ReadStat(CC1101_PKTSTAT_READ, &pktStat) == SUCCESS)
    {
        if((pktStat & 0x80) != 0x00)
        {
            CC1101_CrcStat = SET;
        }
        else
        {
            CC1101_CrcStat = RESET;
        }

        if((pktStat & 0x40) != 0x00)
        {
            CC1101_CsStat = SET;
        }
        else
        {
            CC1101_CsStat = RESET;
        }

        if((pktStat & 0x10) != 0x00)
        {
            CC1101_CcaStat = SET;
        }
        else
        {
            CC1101_CcaStat = RESET;
        }

        if((pktStat & 0x04) != 0x00)
        {
            CC1101_Gdo2Stat = SET;
        }
        else
        {
            CC1101_Gdo2Stat = RESET;
        }


        if((pktStat & 0x01) != 0x00)
        {
            CC1101_Gdo0Stat = SET;
        }
        else
        {
            CC1101_Gdo0Stat = RESET;
        }
    }
}

/*****************************************************************************
 函 数 名  : CC1101_TxCheck
 功能描述  : CC1101发送检测
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_TxCheck(void)
{
    CSMA_STATUS tmpCsmaStat = CSMA_GetStat();

    static u8 rfTxFrameBuff[CC1101_TX_BUFF_SIZE];
    static RFSFrame rfTxFrame = {0,
                               0,
                               CC1101_TX_BUFF_SIZE,
                               rfTxFrameBuff,
                               CC1101_TRANSMIT_POWER_NORMAL};

    // 判断CSMA状态
    switch(tmpCsmaStat)
    {
        // CSMA 初始化
        case CSMA_INIT:
            
            // 开始CSMA
            CSMA_Start(FRAME_TYPE_USER_DAT);
            
        break;

        // CSMA完成
        case CSMA_DONE:

            // 获取发送帧
            if(RFQueue_Pull(&CC1101TxQ, &rfTxFrame) == SUCCESS)
            {
                // 开始发送数据帧
                CC1101_StartTx(&rfTxFrame);

                // 通讯灯闪烁一下
                LedIndicator_BlinkHoldSet(LED_ID_RF);
            }
            else
            {
                // 初始化CSMA
                CSMA_SetStat(CSMA_INIT);
            }

        break;

        // CSMA 失败
        case CSMA_FAILURE:
            
            // 初始化CSMA
            CSMA_SetStat(CSMA_INIT);
            
        break;

        default:
            break;
    }
}

/*****************************************************************************
 函 数 名  : CC1101_Impl
 功能描述  : CC1101数据循环
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_Impl(void)
{
    static u8 rfFrameBuff[CC1101_RX_BUFF_SIZE];
    static RFSFrame rfFrame = {0,
                             0,
                             CC1101_RX_BUFF_SIZE,
                             rfFrameBuff,
                             CC1101_TRANSMIT_POWER_NORMAL};

    // 判断是否处于接收模式
    if(CC1101_Stat.RF_Stat == CC1101_STAT_RX)
    {
        // 判断是否已经接收到数据
        if(CC1101_Stat.RxIntFlag == CC1101_IT_FLAG_MARK)
        {
            // 获取RF接收到的数据
            if(CC1101_StartRx(&rfFrame) == SUCCESS)
            {
                // 将接收到的数据保存到队列中
                RFQueue_Push(&CC1101RxQ, &rfFrame);

                // 通讯灯闪烁一下
                LedIndicator_BlinkHoldSet(LED_ID_RF);
                //Queue_Push(&Uart1TxQ, &rfFrame);
            }
        }
        else
        {
            // 判断发送队列是否有数据要发送
            if(RFQueue_IsNotEmpty(&CC1101TxQ) == SET)
            {
                // 做发送检测
                CC1101_TxCheck();
            }

            // 判断RF状态是否为接收状态
            if(CC1101_Stat.RF_Stat == CC1101_STAT_RX)
            {
                // 监控RX
                CC1101_MonitorRx();
            }
        }
    }
    // 判断是否处于发送模式
    else if(CC1101_Stat.RF_Stat == CC1101_STAT_TX)
    {
        // 判断标识是否清除
        if(CC1101_Stat.TxIntFlag == CC1101_IT_FLAG_CLR)
        {
            // 关闭GDO2中断
            CC1101_ItCmd(CC1101_IT_GDO2, DISABLE);

            CC1101_Strobe(CC1101_REG_SIDEL);
            CC1101_Strobe(CC1101_REG_SFRX);
            CC1101_Strobe(CC1101_REG_SRX);

            // 进入接收模式
            CC1101_Stat.RF_Stat = CC1101_STAT_RX;
            // 清除接收标识
            CC1101_Stat.RxIntFlag = CC1101_IT_FLAG_CLR;

            // 开启GDO2中断
            CC1101_ItCmd(CC1101_IT_GDO2, ENABLE);

            // 载入RX监控时间100ms
            Tmm_LoadMs(TMM_CC1101_RX_MONITOR, 100);
            // 初始化CSMA
            CSMA_SetStat(CSMA_INIT);
        }
    }
    else
    {
    }

    //if(CC1101_Stat.RF_Stat == CC1101_STAT_RX)
    //{
    //    CC1101_UpdateRSSI();
    //    CC1101_UpdatePKTStat();
    //}
}

/*****************************************************************************
 函 数 名  : CC1101_TxIRQ
 功能描述  : CC1101发送中断服务函数
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_TxIRQ(void)
{
    if(CC1101_Stat.RF_Stat == CC1101_STAT_TX)
    {
        CC1101_Stat.TxIntFlag = CC1101_IT_FLAG_CLR;
    }
}

/*****************************************************************************
 函 数 名  : CC1101_RxIRQ
 功能描述  : CC1101接收中断服务函数
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_RxIRQ(void)
{
    if(CC1101_Stat.RF_Stat == CC1101_STAT_RX)
    {
        CC1101_Stat.RxIntFlag = CC1101_IT_FLAG_MARK;
    }
}

/*****************************************************************************
 函 数 名  : CC1101_CcaIRQ
 功能描述  : CC1101_CCA中断服务函数
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月24日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CC1101_CcaIRQ(void)
{
    // 判断CCA状态
    if(CC1101_GET_GDO0_STAT == SET)
    {
        CC1101_Stat.CCA_Stat = CC1101_CCA_SET;
    }
    else
    {
        CC1101_Stat.CCA_Stat = CC1101_CCA_CLR;
    }
}


