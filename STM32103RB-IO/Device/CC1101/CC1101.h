#ifndef __CC1101_H__
#define __CC1101_H__

typedef enum
{
    CC1101_STAT_RX = 0,
    CC1101_STAT_TX,
} CC1101_STATUS;

typedef enum
{
    CC1101_CCA_CLR = 0,
    CC1101_CCA_SET,
} CC1101_CCA_STATUS;

typedef enum
{
    CC1101_IT_FLAG_CLR,
    CC1101_IT_FLAG_MARK,
} CC1101_IT_FLAG;

typedef struct
{
    volatile CC1101_IT_FLAG RxIntFlag;
    volatile CC1101_IT_FLAG TxIntFlag;
    volatile CC1101_CCA_STATUS CCA_Stat;
    CC1101_STATUS RF_Stat;
} CC1101_StatusTypedef;

 extern     RFSQueue      CC1101RxQ;
 extern     RFSQueue      CC1101TxQ;

#define     CC1101_PIN_GDO0             GPIO_Pin_0
#define     CC1101_PIN_GDO2             GPIO_Pin_1
#define     CC1101_PORT                 GPIOB

#define     CC1101_GPIO_PERIPH          RCC_APB2Periph_GPIOB

#define     CC1101_GDO0_EXIT_LINE       EXTI_Line0
#define     CC1101_GDO2_EXIT_LINE       EXTI_Line1

#define     CC1101_GDO0_EXIT_PIN_SOURCE GPIO_PinSource0
#define     CC1101_GDO2_EXIT_PIN_SOURCE GPIO_PinSource1
#define     CC1101_EXIT_SOURCE_PORT     GPIO_PortSourceGPIOB

#define     CC1101_GDO0_IT_DISABLE      (EXTI->IMR &= (~CC1101_GDO0_EXIT_LINE));
#define     CC1101_GDO0_IT_ENABLE       (EXTI->IMR |= CC1101_GDO0_EXIT_LINE);
#define     CC1101_GDO2_IT_DISABLE      (EXTI->IMR &= (~CC1101_GDO2_EXIT_LINE));
#define     CC1101_GDO2_IT_ENABLE       (EXTI->IMR |= CC1101_GDO2_EXIT_LINE);

#define     CC1101_GDO0_CLR_FLAG        (EXTI_ClearFlag(CC1101_GDO0_EXIT_LINE))
#define     CC1101_GDO2_CLR_FLAG        (EXTI_ClearFlag(CC1101_GDO2_EXIT_LINE))

#define     CC1101_GDO0_CLR_PENDING     (EXTI_ClearITPendingBit(CC1101_GDO0_EXIT_LINE))
#define     CC1101_GDO2_CLR_PENDING     (EXTI_ClearITPendingBit(CC1101_GDO2_EXIT_LINE))

#define     CC1101_GET_GDO0_STAT        (GPIO_ReadInputDataBit(CC1101_PORT, CC1101_PIN_GDO0))
#define     CC1101_GET_GDO2_STAT        (GPIO_ReadInputDataBit(CC1101_PORT, CC1101_PIN_GDO2))


#define     CC1101_TX_FC_TIM_MS         10

#define     CC1101_IT_GDO0              0x01
#define     CC1101_IT_GDO2              0x02
#define     CC1101_IT_BOTH              (CC1101_IT_GDO0 | CC1101_IT_GDO2)

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

extern  FlagStatus  CC1101_IsRfChannelClr   (void);
extern  void        CC1101_Init             (void);
extern  void        CC1101_Impl             (void);
extern  void        CC1101_TxIRQ            (void);
extern  void        CC1101_RxIRQ            (void);
extern  void        CC1101_CcaIRQ           (void);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif

#endif /* __CC1101_H__ */

