/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : StatMtc_Logic.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年12月9日
  最近修改   :
  功能描述   : 状态维护逻辑 操作文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年12月9日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/
extern  CAN_StatParaTypedef     CanStatPara;
extern  VirtualAtrrTypedef      VirtualAttr[VIRTUAL_ADDR_LEN];

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


ErrorStatus StatLogic_ResponseStat(AddrAttrTypedef resAddr, MtcStatAttrTypedef resMtcStat)
{
    FrameTypedef    sFrame;
    CAN_DataBase    sDb;
    u8              sBuff[FRAME_SINGLE_DATA_LEN] = {0};


    /* 载入状态信息 */
    sBuff[0] = resMtcStat.Version;
    sBuff[1] = resMtcStat.StableLevel;

    // 获取反馈帧
    if(Frame_GetSingleFeedbackCnlFrame(resAddr, sBuff, 2, &sFrame) == SUCCESS)
    {
        // 获取CAN控制帧
        if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
        {
            // 载入控制帧到发送队列
            return CAN_SendDbMsg(&sDb);
        }
    }
    return ERROR;
}

ErrorStatus StatLogic_ResponseOneAgStat(AddrAttrTypedef resAddr)
{
    u8 i = 0;
    MtcStatAttrTypedef   mtcStat;

    if(resAddr.Type != CMD_ADDR_TYPE_APP)
    {
        return ERROR;
    }

    if((resAddr.AddrBuff[0] == 0)
       || (resAddr.AddrBuff[1] == 0))
    {
        return ERROR;
    }

    // 遍历所有的虚拟地址
    for(i=0; i<VIRTUAL_ADDR_LEN; i++)
    {
        // 判断虚拟地址是否激活
        if(VirtualAttr[i].Activation == ENABLE)
        {
            // 判断地址是否匹配
            if((VirtualAttr[i].WorkAddr.App == resAddr.AddrBuff[0])
               && (VirtualAttr[i].WorkAddr.Group == resAddr.AddrBuff[1]))
            {
                // 载入版本号
                mtcStat.Version     = VirtualAttr[i].Dimmer.LevelStat.Version;
                // 载入实际值
                mtcStat.StableLevel = VirtualAttr[i].Dimmer.LevelStat.StableLevel;

                // 发送状态
                return StatLogic_ResponseStat(resAddr, mtcStat);
            }
        }
    }

    return ERROR;
}

ErrorStatus StatLogic_ResponseAgStat(AddrAttrTypedef resAddr)
{
    u8 i = 0;

    if(resAddr.Type != CMD_ADDR_TYPE_APP)
    {
        return ERROR;
    }

    if(resAddr.AddrBuff[0] == 0)
    {
        return ERROR;
    }

    // 轮询所有Group
    if(resAddr.AddrBuff[1] == 0)
    {
        for(i=0; i<0xFF; i++)
        {
            // 载入Group
            resAddr.AddrBuff[1] = (i+1);

            // 反馈一个AG地址的状态
            StatLogic_ResponseOneAgStat(resAddr);
        }
    }
    else
    {
        return StatLogic_ResponseOneAgStat(resAddr);
    }

    return ERROR;
}

ErrorStatus StatLogic_ReloadOneStat(RunAddrTypedef rlAddr, DimmerAttrTypedef *reDimmer, MtcStatAttrTypedef newMtcStat)
{
    reDimmer->LevelStat.Version     = newMtcStat.Version;
    reDimmer->LevelStat.StableLevel = newMtcStat.StableLevel;
    reDimmer->LevelStat.RealLevel   = newMtcStat.StableLevel;

    // 载入稳定状态到红外状态中
    IrsLogic_LoadCmd(rlAddr, reDimmer->LevelStat.RealLevel); 
    
    return SUCCESS;
}

ErrorStatus StatLogic_UpdateOneAgStat(AddrAttrTypedef resAddr, MtcStatAttrTypedef resMtcStat)
{
    u8 i = 0;

    if(resAddr.Type != CMD_ADDR_TYPE_APP)
    {
        return ERROR;
    }

    if((resAddr.AddrBuff[0] == 0)
       || (resAddr.AddrBuff[1] == 0))
    {
        return ERROR;
    }

    // 判断状态是否为未使用
    if(resMtcStat.Version == AG_MTC_VERSION_STAT_UNUSED)
    {
        return ERROR;
    }

    // 遍历所有的虚拟地址
    for(i=0; i<VIRTUAL_ADDR_LEN; i++)
    {
        // 判断虚拟地址是否激活
        if(VirtualAttr[i].Activation == ENABLE)
        {
            // 判断地址是否匹配
            if((VirtualAttr[i].WorkAddr.App == resAddr.AddrBuff[0])
               && (VirtualAttr[i].WorkAddr.Group == resAddr.AddrBuff[1]))
            {

                // 判断状态是否为空闲 - 空闲状态才进行状态更新比较
                if(VirtualAttr[i].Dimmer.CmdStat == DIMMER_CMD_STAT_IDEL)
                {
                    // 判断状态是否为未使用
                    if(VirtualAttr[i].Dimmer.LevelStat.Version == AG_MTC_VERSION_STAT_UNUSED)
                    {
                        // 更新状态
                        return StatLogic_ReloadOneStat(VirtualAttr[i].WorkAddr, &(VirtualAttr[i].Dimmer), resMtcStat);
                    }
                    // 判断是否为初始化态
                    else if(VirtualAttr[i].Dimmer.LevelStat.Version == AG_MTC_VERSION_STAT_INIT)
                    {
                        // 判断是否在正常范围内 或者 处于轮转态
                        if(((resMtcStat.Version > AG_MTC_VERSION_STAT_INIT) && (resMtcStat.Version <= AG_MTC_VERSION_MAX_ACIRCLE))
                           ||(resMtcStat.Version == AG_MTC_VERSION_STAT_ACIRCLE))
                        {
                            // 更新状态
                            return StatLogic_ReloadOneStat(VirtualAttr[i].WorkAddr, &(VirtualAttr[i].Dimmer), resMtcStat);
                        }
                    }
                    // 判断是否为轮转状态
                    else if(VirtualAttr[i].Dimmer.LevelStat.Version == AG_MTC_VERSION_STAT_ACIRCLE)
                    {
                        /* 判断新版本状态小于轮转边界状态 且 新版本的状态大于初始化状态 */
                        if((resMtcStat.Version < AG_MTC_VERSION_LIMIT_ACIRCLE)
                           &&(resMtcStat.Version > AG_MTC_VERSION_STAT_INIT))
                        {
                            // 更新状态
                            return StatLogic_ReloadOneStat(VirtualAttr[i].WorkAddr, &(VirtualAttr[i].Dimmer), resMtcStat);
                        }
                        // 反馈
                        else
                        {
                            if((resMtcStat.Version >= AG_MTC_VERSION_LIMIT_ACIRCLE)
                               &&(resMtcStat.Version <= AG_MTC_VERSION_MAX_ACIRCLE))
                            {
                                // 载入新版本
                                resMtcStat.Version      = VirtualAttr[i].Dimmer.LevelStat.Version;
                                resMtcStat.StableLevel  = VirtualAttr[i].Dimmer.LevelStat.StableLevel;
                                // 反馈更正新版本
                                return StatLogic_ResponseStat(resAddr, resMtcStat);
                            }
                        }
                    }
                    // 判断状态是否为正常运行状态
                    else if((VirtualAttr[i].Dimmer.LevelStat.Version > AG_MTC_VERSION_STAT_INIT)
                            && (VirtualAttr[i].Dimmer.LevelStat.Version <= AG_MTC_VERSION_MAX_ACIRCLE))
                    {
                        // 判断新版本是否大于旧版本
                        if(resMtcStat.Version > VirtualAttr[i].Dimmer.LevelStat.Version)
                        {
                            // 更新状态
                            return StatLogic_ReloadOneStat(VirtualAttr[i].WorkAddr, &(VirtualAttr[i].Dimmer), resMtcStat);
                        }
                        else if((resMtcStat.Version == AG_MTC_VERSION_STAT_ACIRCLE)
                                && (VirtualAttr[i].Dimmer.LevelStat.Version >= AG_MTC_VERSION_LIMIT_ACIRCLE))
                        {
                            // 更新状态
                            return StatLogic_ReloadOneStat(VirtualAttr[i].WorkAddr, &(VirtualAttr[i].Dimmer), resMtcStat);
                        }
                        else if(resMtcStat.Version < VirtualAttr[i].Dimmer.LevelStat.Version)
                        {
                            // 载入新版本
                            resMtcStat.Version      = VirtualAttr[i].Dimmer.LevelStat.Version;
                            resMtcStat.StableLevel  = VirtualAttr[i].Dimmer.LevelStat.StableLevel;
                            // 反馈更正新版本
                            return StatLogic_ResponseStat(resAddr, resMtcStat);
                        }
                        else
                        {
                            return ERROR;
                        }
                    }
                    else
                    {
                        return ERROR;
                    }
                }
            }
        }
    }

    return ERROR;
}

ErrorStatus StatLogic_SendReadCmd(AddrAttrTypedef rAddr)
{
    FrameTypedef    sFrame;
    CAN_DataBase    sDb;
    u8              sBuff[FRAME_SINGLE_DATA_LEN] = {0};

    if(rAddr.Type != CMD_ADDR_TYPE_APP)
    {
        return ERROR;
    }

    if((rAddr.AddrBuff[0] == 0x00)
    || (rAddr.AddrBuff[0] == 0xFF)
    || (rAddr.AddrBuff[1] == 0x00)
    || (rAddr.AddrBuff[1] == 0xFF))
    {
        return ERROR;
    }

    if(Frame_GetSingleReadCnlFrame(rAddr, sBuff, 0, &sFrame) == SUCCESS)
    {
        // 获取CAN控制帧
        if(Frame_SingleFrameGetCanDb(sFrame, &sDb) == SUCCESS)
        {
            // 载入控制帧到发送队列
            return CAN_SendDbMsg(&sDb);
        }
    }

    return ERROR;
}

void StatLogic_StatReGetPoll(void)
{
    static FlagStatus IsFinish          = RESET;
    static FlagStatus FirstTmmLoadFinish = RESET;
    static u8         ReGetVirtualAddr  = 0;
    AddrAttrTypedef gAddr;
    u32    tmpTick = 0;

    if(IsFinish != SET)
    {
        // 判断是否未载入定时时间 且 未执行过地址状态获取
        if((FirstTmmLoadFinish == RESET)
         &&(ReGetVirtualAddr == 0))
        {
            // 设置已载入时间完成状态
            FirstTmmLoadFinish = SET;

            // 获取随机时间
            tmpTick = Ramdom_GetRandom(AG_MTC_REGET_TMM_TICK_MAX, AG_MTC_REGET_TMM_TICK_MIN);

            // 载入时间
            Tmm_LoadBaseTick(TMM_AG_STAT_REGET, tmpTick);
        }

        if(Tmm_IsON(TMM_AG_STAT_REGET) != SET)
        {
            // 判断是否在线
            if(CAN_IsOnline() == SET)
            {
                while(ReGetVirtualAddr < VIRTUAL_ADDR_LEN)
                {
                    if((VirtualAttr[ReGetVirtualAddr].Activation == ENABLE)
                       &&(VirtualAttr[ReGetVirtualAddr].WorkAddr.App > 0)
                       &&(VirtualAttr[ReGetVirtualAddr].WorkAddr.Group> 0)
                       &&(VirtualAttr[ReGetVirtualAddr].Dimmer.CmdStat == DIMMER_CMD_STAT_IDEL)
                       &&(VirtualAttr[ReGetVirtualAddr].Dimmer.LevelStat.Version == AG_MTC_VERSION_STAT_INIT))
                    {
                        gAddr.Type = CMD_ADDR_TYPE_APP;
                        gAddr.AddrBuff[0] = VirtualAttr[ReGetVirtualAddr].WorkAddr.App;
                        gAddr.AddrBuff[1] = VirtualAttr[ReGetVirtualAddr].WorkAddr.Group;

                        StatLogic_SendReadCmd(gAddr);

                        tmpTick = Ramdom_GetRandom(AG_MTC_REGET_TMM_TICK_MAX, AG_MTC_REGET_TMM_TICK_MIN);

                        Tmm_LoadBaseTick(TMM_AG_STAT_REGET, tmpTick);

                        ReGetVirtualAddr++;

                        return;
                    }
                    else
                    {
                        ReGetVirtualAddr++;
                    }
                }
                IsFinish = SET;
            }
        }
    }
}

