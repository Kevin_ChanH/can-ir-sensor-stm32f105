/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : StatMtc_Logic.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年12月9日
  最近修改   :
  功能描述   : StatMtc_Logic.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年12月9日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __STATMTC_LOGIC_H__
#define __STATMTC_LOGIC_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef struct
{
    u8 StableLevel;
    s8 Version;
} MtcStatAttrTypedef;


/* 状态码 */
#define     AG_MTC_VERSION_STAT_UNUSED      ((s8)(-128))
#define     AG_MTC_VERSION_STAT_INIT        ((s8)(0))
#define     AG_MTC_VERSION_STAT_START       ((s8)(1))
#define     AG_MTC_VERSION_STAT_ACIRCLE     ((s8)(-1))
#define     AG_MTC_VERSION_MAX_ACIRCLE      ((s8)(127))

#define     AG_MTC_VERSION_LIMIT_ACIRCLE    ((s8)(100))

#define     AG_MTC_INIT_VERSION_STAT        AG_MTC_VERSION_STAT_INIT
#define     AG_MTC_INIT_LEVEL_VALUE         0


#define     AG_MTC_INIT_REGET_TMM_MS_DEFAULT    10
#define     AG_MTC_REGET_TMM_MS_DEFAULT         5


#define     AG_MTC_REGET_TMM_TICK_MAX       50000
#define     AG_MTC_REGET_TMM_TICK_MIN       5000



#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* __cplusplus */

extern  ErrorStatus             StatLogic_ResponseStat                          (AddrAttrTypedef resAddr, MtcStatAttrTypedef resMtcStat);
extern  ErrorStatus             StatLogic_ResponseOneAgStat                     (AddrAttrTypedef resAddr);
extern  ErrorStatus             StatLogic_ResponseAgStat                        (AddrAttrTypedef resAddr);
extern  ErrorStatus             StatLogic_UpdateOneAgStat                       (AddrAttrTypedef resAddr, MtcStatAttrTypedef resMtcStat);

extern  ErrorStatus             StatLogic_SendReadCmd                           (AddrAttrTypedef rAddr);
extern  void                    StatLogic_StatReGetPoll                         (void);


#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __STATMTC_LOGIC_H__ */
