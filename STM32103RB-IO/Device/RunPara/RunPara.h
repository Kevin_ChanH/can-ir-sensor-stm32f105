/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : RunPara.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月5日
  最近修改   :
  功能描述   : RunPara.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __RUNPARA_H__
#define __RUNPARA_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


typedef enum
{
    RUN_OUT_THRES_H = 0,
    RUN_OUT_THRES_L,
} RunParaOutThresTypedef;

typedef enum
{
    RUN_IO_MODE_TOGGLE = 0,
    RUN_IO_MDOE_OFF,
    RUN_IO_MDOE_ON,
    RUN_IO_MDOE_DIMMER,
    RUN_IO_MDOE_CTHALF,
    RUN_IO_MDOE_CTFLIP,
    RUN_IO_MDOE_SCENE,
    RUN_IO_MDOE_RAMP2,
} RunParaIoModeTypedef;


typedef enum
{
    RUN_LED_MODE_FLIP = 0,
    RUN_LED_MODE_ON,
    RUN_LED_MODE_OFF,
    RUN_LED_MODE_FFLIP,
} RunParaLedModeTypedef;


typedef enum
{
    RUN_POWER_OUT_ON_DISABLE = 0,
    RUN_POWER_OUT_ON_ENABLE,
} RunParaPowerOutModeTypedef;




#define     RUNPARA_CFG_MAX_NUM         MCU_INPUT_OUTPUT_CHANNEL
#define     RUNPARA_CFG_USER_NUM        MCU_INPUT_OUTPUT_CHANNEL
#define     RUNPARA_ID_LIMIT_START      1
#define     RUNPARA_ID_LIMIT_END        RUNPARA_CFG_USER_NUM

#define     SYSTEM_RUN_PARA_LEN         10



#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */

extern  FlagStatus                  RunPara_IsId                (uc8 rId);
extern  void                        RunPara_Init                (void);
extern  ErrorStatus                 RunPara_GetAddr             (uc8 gId, RunAddrTypedef *gAddr);
extern  u8                          RunPara_GetOutThres         (uc8  gId, RunParaOutThresTypedef gTh);
extern  RunParaIoModeTypedef        RunPara_GetIoMode           (uc8 gId);
extern  RunParaLedModeTypedef       RunPara_GetLedMode          (uc8 gId);
extern  u8                          RunPara_GetTotalTime        (uc8 gId);
extern  u8                          RunPara_GetSceneValue       (uc8 gId);
extern  RunParaPowerOutModeTypedef  RunPara_GetPowerOutMode     (uc8 gId);
extern  FlagStatus                  RunPara_OutPutIsSwitch      (RunParaIoModeTypedef rMode);
extern  FlagStatus                  RunPara_OutPutIsDimmer      (RunParaIoModeTypedef rMode);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __RUNPARA_H__ */
