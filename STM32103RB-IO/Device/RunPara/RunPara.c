/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : RunPara.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月5日
  最近修改   :
  功能描述   : 运行参数
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/
static u8  SystemIoPara[RUNPARA_CFG_USER_NUM][SYSTEM_RUN_PARA_LEN] = {0};

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/
static uc8 SystemIoParaDefault[RUNPARA_CFG_USER_NUM][SYSTEM_RUN_PARA_LEN] =
{
    {100, 1, 255, 0, 3, 0, 5, 0, 0, 0},  /* Ap=0,Gp=0,TH=255,TL=0,KeyType=Flip,Led=Fflip,rt=5s,Sv=0,ct=Long2,closeRF=None */
    {100, 2, 255, 0, 3, 0, 5, 0, 0, 0},  /* Ap=0,Gp=0,TH=255,TL=0,KeyType=Flip,Led=Fflip,rt=5s,Sv=0,ct=Long2,closeRF=None */
    {100, 3, 255, 0, 3, 0, 5, 0, 0, 0},  /* Ap=0,Gp=0,TH=255,TL=0,KeyType=Flip,Led=Fflip,rt=5s,Sv=0,ct=Long2,closeRF=None */
    {100, 4, 255, 0, 3, 0, 5, 0, 0, 0},  /* Ap=0,Gp=0,TH=255,TL=0,KeyType=Flip,Led=Fflip,rt=5s,Sv=0,ct=Long2,closeRF=None */
};

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/


/*****************************************************************************
 函 数 名  : RunPara_IsId
 功能描述  : 判断是否为运行参数ID
 输入参数  : uc8 rId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus RunPara_IsId(uc8 rId)
{
    if((rId >= RUNPARA_ID_LIMIT_START)
       &&(rId <= RUNPARA_ID_LIMIT_END))
    {
        return SET;
    }
    else
    {
        return RESET;
    }
}

/*****************************************************************************
 函 数 名  : RunPara_Init
 功能描述  : 运行参数初始化
 输入参数  : void
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
void RunPara_Init(void)
{
    u8 idIndex = 0;
    u8 paraIndex = 0;

    // 遍历所有 运行参数回路
    for(idIndex=0; idIndex<RUNPARA_CFG_USER_NUM; idIndex++)
    {
        // 获取一帧配置参数
        for(paraIndex=0; paraIndex<SYSTEM_RUN_PARA_LEN; paraIndex++)
        {
            // 循环载入默认数据
            SystemIoPara[idIndex][paraIndex] = SystemIoParaDefault[idIndex][paraIndex];
        }
    }
}

/*****************************************************************************
 函 数 名  : RunPara_GetAddr
 功能描述  : 获取地址
 输入参数  : uc8 gId
             RunParaAddrTypedef gAddr
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus RunPara_GetAddr(uc8 gId, RunAddrTypedef *gAddr)
{
    if((RunPara_IsId(gId) == SET)
        && (gAddr != NULL))
    {
        // 载入App
        gAddr->App   = SystemIoPara[gId-1][0];
        // 载入Group
        gAddr->Group = SystemIoPara[gId-1][1];

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : RunPara_GetOutThres
 功能描述  : 获取输出阀值
 输入参数  : uc8  gId
             RunParaOutThresTypedef gTh
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u8 RunPara_GetOutThres(uc8  gId, RunParaOutThresTypedef gTh)
{
    if(RunPara_IsId(gId) == SET)
    {
        // 判断是否为获取 最高阀值
        if(gTh == RUN_OUT_THRES_H)
        {
            // 返回最高阀值
            return SystemIoPara[gId-1][2];
        }
        // 判断是否为获取 最低阀值
        else if(gTh == RUN_OUT_THRES_L)
        {
            // 返回最低阀值
            return SystemIoPara[gId-1][3];
        }
        else
        {
            return 0;
        }
    }

    return 0;
}

/*****************************************************************************
 函 数 名  : RunPara_GetIoMode
 功能描述  : 获取IO的运行模式
 输入参数  : uc8 gId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
RunParaIoModeTypedef RunPara_GetIoMode(uc8 gId)
{
    if(RunPara_IsId(gId) == SET)
    {
        if(SystemIoPara[gId-1][4] == 0)
        {
            return RUN_IO_MODE_TOGGLE;
        }
        else if(SystemIoPara[gId-1][4] == 1)
        {
            return RUN_IO_MDOE_OFF;
        }
        else if(SystemIoPara[gId-1][4] == 2)
        {
            return RUN_IO_MDOE_ON;
        }
        else if(SystemIoPara[gId-1][4] == 3)
        {
            return RUN_IO_MDOE_DIMMER;
        }
        else if(SystemIoPara[gId-1][4] == 4)
        {
            return RUN_IO_MDOE_CTHALF;
        }
        else if(SystemIoPara[gId-1][4] == 5)
        {
            return RUN_IO_MDOE_CTFLIP;
        }
        else if(SystemIoPara[gId-1][4] == 6)
        {
            return RUN_IO_MDOE_SCENE;
        }
        else if(SystemIoPara[gId-1][4] == 7)
        {
            return RUN_IO_MDOE_RAMP2;
        }
        else
        {
            return RUN_IO_MODE_TOGGLE;
        }
    }

    return RUN_IO_MODE_TOGGLE;
}

/*****************************************************************************
 函 数 名  : RunPara_GetLedMode
 功能描述  : 获取LED的运行模式
 输入参数  : uc8 gId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
RunParaLedModeTypedef RunPara_GetLedMode(uc8 gId)
{
    if(RunPara_IsId(gId) == SET)
    {
        if(SystemIoPara[gId-1][5] == 0)
        {
            return RUN_LED_MODE_FLIP;
        }
        else if(SystemIoPara[gId-1][5] == 1)
        {
            return RUN_LED_MODE_ON;
        }
        else if(SystemIoPara[gId-1][5] == 2)
        {
            return RUN_LED_MODE_OFF;
        }
        else if(SystemIoPara[gId-1][5] == 3)
        {
            return RUN_LED_MODE_FFLIP;
        }
        else
        {
            return RUN_LED_MODE_FLIP;
        }
    }

    return RUN_LED_MODE_FLIP;
}

/*****************************************************************************
 函 数 名  : RunPara_GetTotalTime
 功能描述  : 获取总运行时间
 输入参数  : uc8 gId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u8 RunPara_GetTotalTime(uc8 gId)
{
    if(RunPara_IsId(gId) == SET)
    {
        // 返回总运行时间
        return SystemIoPara[gId-1][6];
    }

    return 0;
}

/*****************************************************************************
 函 数 名  : RunPara_GetSceneValue
 功能描述  : 获取场景值
 输入参数  : uc8 gId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
u8 RunPara_GetSceneValue(uc8 gId)
{
    if(RunPara_IsId(gId) == SET)
    {
        // 返回场景值
        return SystemIoPara[gId-1][7];
    }

    return 0;
}

/*****************************************************************************
 函 数 名  : RunPara_GetPowerOutMode
 功能描述  : 获取上电后的输出状态
 输入参数  : uc8 gId
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月5日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
RunParaPowerOutModeTypedef RunPara_GetPowerOutMode(uc8 gId)
{
    if(RunPara_IsId(gId) == SET)
    {
        // 判断上电 是否不输出
        if(((SystemIoPara[gId-1][8]>>4) & 0x01) == 0)
        {
            // 返回上电输出
            return RUN_POWER_OUT_ON_DISABLE;
        }
        else
        {
            // 返回上电输出
            return RUN_POWER_OUT_ON_ENABLE;
        }
    }

    return RUN_POWER_OUT_ON_DISABLE;
}

/*****************************************************************************
 函 数 名  : RunPara_OutPutIsSwitch
 功能描述  : 判断输出是否为开关
 输入参数  : RunParaLedModeTypedef rMode
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  RunPara_OutPutIsSwitch(RunParaIoModeTypedef rMode)
{
    // 判断是否为开关模式
    if((rMode == RUN_IO_MODE_TOGGLE)
        || (rMode == RUN_IO_MDOE_OFF)
        || (rMode == RUN_IO_MDOE_ON))
    {
        return SET;
    }

    return RESET;
}

/*****************************************************************************
 函 数 名  : RunPara_OutPutIsDimmer
 功能描述  : 判断输出是否为调光
 输入参数  : RunParaLedModeTypedef rMode
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月8日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
FlagStatus  RunPara_OutPutIsDimmer(RunParaIoModeTypedef rMode)
{
    // 判断是否为调光模式
    if(rMode == RUN_IO_MDOE_DIMMER)
    {
        return SET;
    }

    return RESET;
}



