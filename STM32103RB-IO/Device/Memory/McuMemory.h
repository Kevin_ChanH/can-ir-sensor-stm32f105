/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : McuMemory.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月18日
  最近修改   :
  功能描述   : McuMemory.c 的头文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月18日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __MCUMEMORY_H__
#define __MCUMEMORY_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

typedef enum
{
    MMY_DATA_TYPE_BIG = 0,          /* 存储数据 大数据64Byte */
    MMY_DATA_TYPE_MID,              /* 存储数据 中数据32Byte */
    MMY_DATA_TYPE_SML,              /* 存储数据 小数据16Byte */
}MmyDataType;


// 配置一个参数需要的数据大小
#define     MCU_MMY_CFG_MSG_BIG_SIZE            64
#define     MCU_MMY_CFG_MSG_MID_SIZE            32
#define     MCU_MMY_CFG_MSG_SML_SIZE            16


typedef struct
{
    u8 Len;
    u8 Buff[MCU_MMY_CFG_MSG_BIG_SIZE];
    MmyDataType Type;
} MmyDataBuff;



#define     MCU_MMY_LIMIT_ADDR_START            MCU_FLASH_DATA_START_ADDR
#define     MCU_MMY_LIMIT_ADDR_END              (MCU_FLASH_DATA_START_ADDR + MCU_FLASH_DATA_SIZE_BYTE)

#define     IS_MCU_MMY_ADDR(MMYR_ADDR)          (((MMYR_ADDR) >= MCU_MMY_LIMIT_ADDR_START) &&   \
                                                ((MMYR_ADDR) <=  MCU_MMY_LIMIT_ADDR_END))       \
                                                ? SET: RESET


#define     IS_ALIGN_4BYTE(AL_ADDR)             (((AL_ADDR) & 0x00000003) == 0x00)




// MCU 存储的基础地址
#define     MCU_MMY_BASE_ADDR                   MCU_FLASH_DATA_START_ADDR


/* 系统及设备配置信息  预留4KByte空间 */
/* 配置波特率 */
// 串口 波特率的地址偏移
#define     MCU_MMY_UART_BTR_ADDR_OFFSET        0
// 串口 波特率的地址            占64Byte
#define     MCU_MMY_UART_BTR_ADDR               (MCU_MMY_BASE_ADDR + MCU_MMY_UART_BTR_ADDR_OFFSET)
/* 485波特率 */
// 485 波特率的地址偏移
#define     MCU_MMY_485_BTR_ADDR_OFFSET         MCU_MMY_CFG_MSG_BIG_SIZE
// 485 波特率的地址             占64Byte
#define     MCU_MMY_485_BTR_ADDR                (MCU_MMY_BASE_ADDR + MCU_MMY_485_BTR_ADDR_OFFSET)
/* CAN1波特率 */
// CAN1 波特率的地址偏移
#define     MCU_MMY_CAN1_BTR_ADDR_OFFSET        (MCU_MMY_CFG_MSG_BIG_SIZE * 2)
// CAN1 波特率的地址            占64Byte
#define     MCU_MMY_CAN1_BTR_ADDR               (MCU_MMY_BASE_ADDR + MCU_MMY_CAN1_BTR_ADDR_OFFSET)
/* CAN2波特率 */
// CAN2 波特率的地址偏移
#define     MCU_MMY_CAN2_BTR_ADDR_OFFSET        (MCU_MMY_CFG_MSG_BIG_SIZE * 3)
// CAN2 波特率的地址            占64Byte
#define     MCU_MMY_CAN2_BTR_ADDR               (MCU_MMY_BASE_ADDR + MCU_MMY_CAN2_BTR_ADDR_OFFSET)

/* 设备标签 */
// 设备 设备标签 地址偏移
#define     MCU_MMY_DEVICE_TAG_ADDR_OFFSET      (MCU_MMY_CFG_MSG_BIG_SIZE * 4)
// 设备 设备标签 地址           占64Byte
#define     MCU_MMY_DEVICE_TAG_ADDR             (MCU_MMY_BASE_ADDR + MCU_MMY_DEVICE_TAG_ADDR_OFFSET)

/* MAC地址 */
// 设备 设备的MAC地址 地址偏移
#define     MCU_MMY_MAC_ADDR_OFFSET             (MCU_MMY_CFG_MSG_BIG_SIZE * 5)
// 设备 设备的MAC地址 地址      占64Byte
#define     MCU_MMY_MAC_ADDR                    (MCU_MMY_BASE_ADDR + MCU_MMY_MAC_ADDR_OFFSET)
/* UNIT地址 */
// 设备 设备的UNIT地址 地址偏移
#define     MCU_MMY_UNIT_ADDR_OFFSET            (MCU_MMY_CFG_MSG_BIG_SIZE * 6)
// 设备 设备的UNIT地址 地址     占64Byte
#define     MCU_MMY_UNIT_ADDR                   (MCU_MMY_BASE_ADDR + MCU_MMY_UNIT_ADDR_OFFSET)

/* 设备 安全层 */
// 设备安全层组 个数
#define     MCU_SECURITY_GROUP_NUM              8
// 设备安全层组 偏移地址        每个占64Byte
#define     MCU_MMY_SECURITY_GROUP_ADDR_OFFSET  (MCU_MMY_CFG_MSG_BIG_SIZE * 7)
// 设备安全层组 地址
#define     MCU_MMY_SECURITY_GROUP_ADDR         (MCU_MMY_BASE_ADDR + MCU_MMY_SECURITY_GROUP_ADDR_OFFSET)

/* 按键运行参数 */
// 按键运行参数 偏移地址
#define     MCU_MMY_KEY_PARA_ADDR_OFFSET        (MCU_MMY_CFG_MSG_BIG_SIZE * (7 + MCU_SECURITY_GROUP_NUM))
// 按键运行参数 地址            占64Byte
#define     MCU_MMY_KEY_PARA_ADDR               (MCU_MMY_BASE_ADDR + MCU_MMY_KEY_PARA_ADDR_OFFSET)




/* 接口 - CAN 信息配置 预留4KByte空间 */
// ITF_CAN  偏移地址 从第4K开始
#define     MCU_ITF_CAN_ADDR_OFFSET             0x1000
// ITF_CAN  存储地址
#define     MCU_ITF_CAN_ADDR                    (MCU_MMY_BASE_ADDR + MCU_ITF_CAN_ADDR_OFFSET)

/* 接口 - RF 信息配置 预留2KByte空间 */
// ITF_RF  偏移地址 从第8K开始
#define     MCU_ITF_RF_ADDR_OFFSET             0x2000
// ITF_CAN  存储地址
#define     MCU_ITF_RF_ADDR                     (MCU_MMY_BASE_ADDR + MCU_ITF_RF_ADDR_OFFSET)

/* 接口 - RF 安全层 预留512Byte空间 */
// ITF RF - SECURITY 偏移地址 从第5.5K开始
#define     MCU_ITF_RF_SEC_ADDR_OFFSET          0x2800
// ITF RF - SECURITY 存储地址
#define     MCU_ITF_RF_SEC_ADDR                 (MCU_MMY_BASE_ADDR + MCU_ITF_RF_SEC_ADDR_OFFSET)


/* IRS 信息配置 预留2KByte空间 */
// IRS 偏移地址 从第20K开始
#define     MCU_IRS_ADDR_OFFSET                 0x5000
// IRS 存储地址
#define     MCU_IRS_ADDR                        (MCU_MMY_BASE_ADDR + MCU_IRS_ADDR_OFFSET)

/* BEAM 信息配置 预留2K空间 */
// BEAM 偏移地址 从第24K开始
#define     MCU_BEAM_ADDR_OFFSET                0x6000
// BEAM 存储地址
#define     MCU_BEAM_ADDR                       (MCU_MMY_BASE_ADDR + MCU_BEAM_ADDR_OFFSET)


#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */


extern  ErrorStatus             MMY_McuFlashAddrCheck                           (uc32 fmAddr, u8 len);
extern  ErrorStatus             MMY_McuFlashReadBuff                            (uc32 fmStartAddr, u8 gBuff[], u32 gLen);
extern  ErrorStatus             MMY_McuFlashWriteBuff                           (u32 fmStartAddr, u8 sBuff[], u32 sLen);
extern  ErrorStatus             MMY_McuGetFlashData                             (uc32 gAddr, MmyDataBuff *gBuff);
extern  ErrorStatus             MMY_McuSetFlashData                             (uc32 sAddr, const MmyDataBuff sBuff);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __MCUMEMORY_H__ */
