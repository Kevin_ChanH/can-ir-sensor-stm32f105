/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : McuMemory.c
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年9月18日
  最近修改   :
  功能描述   : MCU内部数据存储
  函数列表   :
  修改历史   :
  1.日    期   : 2016年9月18日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/
#include "Includes.h"

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/*****************************************************************************
 函 数 名  : MMY_McuFlashAddrCheck
 功能描述  : Flash地址检查
 输入参数  : uc32 fmAddr
             u8 len
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus MMY_McuFlashAddrCheck(uc32 fmAddr, u8 len)
{
    if((IS_MCU_MMY_ADDR(fmAddr) == SET)
       && (IS_MCU_MMY_ADDR(fmAddr+len) == SET))
    {
        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : MMY_McuFlashReadBuff
 功能描述  : 获取Flash数据
 输入参数  : uc32 fmStartAddr
             u8   gBuff[]
             u32  gLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus MMY_McuFlashReadBuff(uc32 fmStartAddr, u8 gBuff[], u32 gLen)
{
    u32 gDwordLen = 0;
    u32 i;

    u32 fmReadAddr = 0;
    u32 *fmReadBuff;


    // 判断指针是否为空
    if(gBuff == NULL)
    {
        return ERROR;
    }

    // 判断地址是否合法
    if(MMY_McuFlashAddrCheck(fmStartAddr, gLen) != SUCCESS)
    {
        return ERROR;
    }

    // 判断数据长度是否为4字节对齐
    if((IS_ALIGN_4BYTE(gLen) != SET) || (gLen == 0))
    {
        return ERROR;
    }

    // 将数据长度 转换成4Byte的数据长度
    gDwordLen = gLen >> 2;

    // 将缓存指针类型转换为 u32 指针类型
    fmReadBuff = ((u32 *)(gBuff));
    // 将起始地址 赋予为读取地址
    fmReadAddr = fmStartAddr;

    // 循环读取数据 u32类型
    for(i=0; i<gDwordLen; i++)
    {
        // 获取数据 u32类型
        fmReadBuff[i] = (*(vu32*)fmReadAddr);

        // 地址指向下4个Byte
        fmReadAddr += 4;
    }

    // 返回数据读取成功
    return SUCCESS;
}


/*****************************************************************************
 函 数 名  : MMY_McuFlashWriteBuff
 功能描述  : 写入Flash数据
 输入参数  : u32 fmStartAddr
             u8 sBuff[]
             u32 sLen
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus MMY_McuFlashWriteBuff(u32 fmStartAddr, u8 sBuff[], u32 sLen)
{
    u32 i;

    u32 fmWriteBlockAddr  = 0;          /* 块地址 */
    u32 fmWriteAddrOffset = 0;          /* 地址偏移量 */
    u32 fmWriteAddr = 0;                /* 写入地址 */
    u32 *fmWriteBuff;                   /* 写入缓存 */

    /* 写入数据缓存 */
    u8  fmDataWriteBuff[MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE];

    // 判断指针是否为空
    if(sBuff == NULL)
    {
        return ERROR;
    }

    // 判断地址是否合法
    if(MMY_McuFlashAddrCheck(fmStartAddr, sLen) != SUCCESS)
    {
        return ERROR;
    }

    // 判断数据长度是否为4Byte操作
    if((IS_ALIGN_4BYTE(sLen) != SET) || (sLen == 0))
    {
        return ERROR;
    }

    // 获取块地址
    fmWriteBlockAddr  = (fmStartAddr & (~((u32)MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE -1)));
    // 获取地址偏移量
    fmWriteAddrOffset = (fmStartAddr & (MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE -1));

    // 判断本次写入的数据内容是否超出了整个Block
    if((fmWriteAddrOffset + sLen) > MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE)
    {
        return ERROR;
    }

    // 读取数据缓存 整块数据缓存
    if(MMY_McuFlashReadBuff(fmWriteBlockAddr, fmDataWriteBuff, MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE) != SUCCESS)
    {
        return ERROR;
    }

    // 写入需要修改的部分
    for(i=0; i<sLen; i++)
    {
        fmDataWriteBuff[fmWriteAddrOffset++] = sBuff[i];
    }

    // 解锁FLASH
    FLASH_Unlock();
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);

    // 擦除块
    if(FLASH_ErasePage(fmWriteBlockAddr) != FLASH_COMPLETE)
    {
        // 锁定FLASH
        FLASH_Lock();

        // 返回错误
        return ERROR;
    }

    // 获取块地址
    fmWriteAddr = fmWriteBlockAddr;
    // 获取数据缓存
    fmWriteBuff = ((u32 *)(fmDataWriteBuff));

    // 循环写入数据 整块 以4Byte为单元
    for(i=0; i<MCU_FLASH_CFG_BLOCK_SIZE_4_BYTE; i++)
    {
        FLASH_ProgramWord(fmWriteAddr, fmWriteBuff[i]);
        fmWriteAddr += 4;
    }

    // 锁定FLASH
    FLASH_Lock();

    // 返回成功
    return SUCCESS;
}

/*****************************************************************************
 函 数 名  : MMY_McuGetFlashData
 功能描述  : 获取配置数据
 输入参数  : uc32 gAddr
             MmyDataBuff *gBuff
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus MMY_McuGetFlashData(uc32 gAddr, MmyDataBuff *gBuff)
{
    u8 gLen = 0;
    u16 crc = 0;

    if(gBuff == NULL)
    {
        return ERROR;
    }

    if(gBuff->Type == MMY_DATA_TYPE_BIG)
    {
        gLen = MCU_MMY_CFG_MSG_BIG_SIZE;
    }
    else if(gBuff->Type == MMY_DATA_TYPE_MID)
    {
        gLen = MCU_MMY_CFG_MSG_MID_SIZE;
    }
    else if(gBuff->Type == MMY_DATA_TYPE_SML)
    {
        gLen = MCU_MMY_CFG_MSG_SML_SIZE;
    }
    else
    {
        return ERROR;
    }

    if(MMY_McuFlashReadBuff(gAddr, gBuff->Buff, gLen) != SUCCESS)
    {
        return ERROR;
    }

    crc = gBuff->Buff[gLen-1];
    crc <<= 8;
    crc += gBuff->Buff[gLen-2];

    if(crc == CRC_GetCcitt(gBuff->Buff, gLen - 2))
    {
        gBuff->Len = (gLen - 2);

        return SUCCESS;
    }

    return ERROR;
}

/*****************************************************************************
 函 数 名  : MMY_McuSetFlashData
 功能描述  : 写入配置数据
 输入参数  : uc32 sAddr
             const MmyDataBuff sBuff
 输出参数  : 无
 返 回 值  :
 调用函数  :
 被调函数  :

 修改历史      :
  1.日    期   : 2016年9月20日
    作    者   : 陈宏鸿
    修改内容   : 新生成函数

*****************************************************************************/
ErrorStatus MMY_McuSetFlashData(uc32 sAddr, MmyDataBuff sBuff)
{
    u8 sLen = 0;
    u16 crc = 0;

    if(sBuff.Type == MMY_DATA_TYPE_BIG)
    {
        sLen = MCU_MMY_CFG_MSG_BIG_SIZE;
    }
    else if(sBuff.Type == MMY_DATA_TYPE_MID)
    {
        sLen = MCU_MMY_CFG_MSG_MID_SIZE;
    }
    else if(sBuff.Type == MMY_DATA_TYPE_SML)
    {
        sLen = MCU_MMY_CFG_MSG_SML_SIZE;
    }
    else
    {
        return ERROR;
    }

    crc = CRC_GetCcitt(sBuff.Buff, sLen-2);

    sBuff.Buff[sLen-2] = (u8)(crc & 0xFF);
    sBuff.Buff[sLen-1] = (u8)((crc>>8) & 0xFF);


    return MMY_McuFlashWriteBuff(sAddr, sBuff.Buff, sLen);
}




