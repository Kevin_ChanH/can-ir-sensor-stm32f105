#ifndef __CSMA_H__
#define __CSMA_H__




typedef enum
{
    CSMA_CHANNEL_IDEL = 0,
    CSMA_CHANNEL_BUSY,
} CSMA_CHANNEL_STATUS;

typedef enum
{
    CSMA_INIT = 0,
    CSMA_START,
    CSMA_SILENT_WAIT,
    CSMA_BEB_ON,
    CSMA_DONE,
    CSMA_FAILURE,
    CSMA_UNKNOWN,
} CSMA_STATUS;

typedef enum
{
    FRAME_TYPE_USER_DAT = 0,
    FRAME_TYPE_SYN,
    FRAME_TYPE_NACK,
    FRAME_TYPE_ACK,
    FRAME_TYPE_RTS,
    FRAME_TYPE_CTS,
} CSMA_FRAME_TYPE;


typedef struct
{
    CSMA_STATUS Stat;                   // CSMA 状态
    CSMA_CHANNEL_STATUS ChannelStat;    // CSMA 通道状态
    CSMA_FRAME_TYPE FrameType;          // CSMA 帧类型
    u8 Nb;                              // CSMA 二进制指数
    u8 Be;                              // CSMA 退避次数
} CSMA_ATTRIBUTE;                       // CSMA 属性


#define     CSMA_RANDOM_DEFAULT_SEED    987




#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

extern  CSMA_STATUS             CSMA_GetStat                    (void);
extern  void                    CSMA_Start                      (CSMA_FRAME_TYPE sType);
extern  void                    CSMA_SetStat                    (CSMA_STATUS stat);
extern  void                    CSMA_Init                       (void);
extern  void                    CSMA_Impl                       (void);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif


#endif /* __CSMA_H__ */

