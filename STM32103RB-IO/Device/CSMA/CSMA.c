
#include "Includes.h"



//Please refer to 802.11b CSMS/CA algorithm for following definitions. All units are microsecond(uS).
//The use of PN(pseudo-random noise) sequence to represent each symbol.
//A symbol duration 63uS = (1s/baud rate)*4, here baud rate is 64kbps.
//Follow constants is from 802.15.4 protocol
//aBaseSlotDuration := 60 symbols                       基础间隙时间
//aMinLIFSPeriod    := 40 symbols
//aMaxBE            := 5                                最大的二进制指数
//aMaxFrameResponseTime := 1220 symbols                 最大的帧响应时间
//aMaxFrameRetries  := 3                                最大的帧重发次数
//aResponseWaitTime := 32*aBaseSuperframeDuration       一个响应等待时间
//aUnitBackoffPeriod:= 20 symbols                       一个单位的退避期
//macAckWaitDuration:= 120 symbols                      物理ACK等待时间
//macMinBE(range is 0~3) := 3                           物理层-最小的二进制指数
//aMaxSIFSFrameSize := 18 Octets                        最大的短帧帧间隙-帧长度
//aMinSIFSPeriod    := 12 symbols.                      最小的短帧帧间隙-时间



#define ST   20   //ST: Slot Time.                      间隙时间
#define SIFS 50//10   //SIFS: Short Inter Frame Space       短帧间间隙
#define DIFS 100//50   //DIFS = SIFS + 2*ST                  分布式帧间间隙

#define CSMA_TICK_PERIOD         10                     // CSMA节拍时间 10us
#define CSMA_BACKOFF_TIME_PERIOD (ST/CSMA_TICK_PERIOD)  // CSMA退避时间

#define MAC_MAX_BE               10                     // 最大的二进制指数
#define SILENT_BE                5                      // 静默退避指数
#define MAC_MIN_BE               1                      // 最小的二进制指数
#define UNIT_BACKOFF_PERIOD      20                     // 一个单位的退避期
#define MAC_MAX_BACKOFF_TIMS     14                     // 最大的回退次数为14次
#define MAC_ACK_WAIT_DURATION    120                    // 最大的ACK等待时间


// 获取CSMA随机属性
static      u16         CSMA_GetRandomBackoffTim        (u8 cwBe);

// CSMA属性
CSMA_ATTRIBUTE CsmaAttribute;                           // CSMA 属性

/*****************************************************************************
 函 数 名  : CSMA_GetRandomBackoffTim
 功能描述  : CSMA获取随机退避时间
 输入参数  : u8 cwBe  
 输出参数  : 无
 返 回 值  : static
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
static u16 CSMA_GetRandomBackoffTim(u8 cwBe)
{
    // 窗口时间
    u16 tmpCw = (u16)((1 << cwBe) - 1);
    // 在窗口时间内  获取随机数
    u16 tmpBackoffTim = ((Random_Rand()%tmpCw)*CSMA_BACKOFF_TIME_PERIOD);

    return tmpBackoffTim;
}

/*****************************************************************************
 函 数 名  : CSMA_GetStat
 功能描述  : 获取CSMA的状态
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
CSMA_STATUS CSMA_GetStat(void)
{
    return CsmaAttribute.Stat;
}

/*****************************************************************************
 函 数 名  : CSMA_Start
 功能描述  : 开始启用CSMA
 输入参数  : CSMA_FRAME_TYPE sType  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CSMA_Start(CSMA_FRAME_TYPE sType)
{
    CsmaAttribute.FrameType = sType;        // 载入帧类型
    CsmaAttribute.Nb = 0;                   // 清零回退次数
    CsmaAttribute.Be = MAC_MIN_BE;          // 赋值最小二进制指数
    CSMA_SetStat(CSMA_START);               // 设置开始CSMA
}

/*****************************************************************************
 函 数 名  : CSMA_SetStat
 功能描述  : 设置CSMA状态
 输入参数  : CSMA_STATUS stat  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CSMA_SetStat(CSMA_STATUS stat)
{
    u16 tmpBackoff = 0;
    CsmaAttribute.Stat = stat;

    switch(CsmaAttribute.Stat)
    {
        // CSMA 静默等待状态
        case CSMA_SILENT_WAIT:
            if(CsmaAttribute.Be == MAC_MIN_BE)
            {
                // 获取随机退避时间
                tmpBackoff += CSMA_GetRandomBackoffTim(SILENT_BE);

                if(CsmaAttribute.FrameType <= FRAME_TYPE_SYN)
                {
                    if(tmpBackoff < (DIFS/CSMA_TICK_PERIOD))
                    {
                        tmpBackoff += (DIFS/CSMA_TICK_PERIOD);
                    }

                }
                else
                {
                    if(tmpBackoff < (SIFS/CSMA_TICK_PERIOD))
                    {
                        tmpBackoff += (SIFS/CSMA_TICK_PERIOD);
                    }
                }
            }
            else
            {
                if(CsmaAttribute.FrameType <= FRAME_TYPE_SYN)
                {
                    // 分布式帧间隙等待 50us
                    tmpBackoff = DIFS/CSMA_TICK_PERIOD;
                }
                else
                {
                    // 短帧间隙 10us
                    tmpBackoff = SIFS/CSMA_TICK_PERIOD;
                }
            }

            // 载入退避时间
            Tmm_LoadBaseTick(TMM_CSMA_BACKOFF, tmpBackoff);
        break;


        // 二进制指数 退避状态
        case CSMA_BEB_ON:
            tmpBackoff = CSMA_GetRandomBackoffTim(CsmaAttribute.Be++);      // 获取随机退避时间
            Tmm_LoadBaseTick(TMM_CSMA_BACKOFF, tmpBackoff);             // 载入随机退避时间
            CsmaAttribute.Nb++;                                         // 回退次数+1

            // 将二进制范围控制在最大数以内
            if(CsmaAttribute.Be > MAC_MAX_BE)
            {
                CsmaAttribute.Be = MAC_MAX_BE;
            }
        break;

        default :
        break;
    }
}

/*****************************************************************************
 函 数 名  : CSMA_Init
 功能描述  : 初始化CSMA
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CSMA_Init(void)
{

    CsmaAttribute.Be = MAC_MIN_BE;                      // 初始化 二进制指数
    CsmaAttribute.Nb = 0;                               // 清零回退次数
    CsmaAttribute.FrameType = FRAME_TYPE_USER_DAT;      // 设置帧类型为 - 用户数据

    // 设置为初始化状态
    CSMA_SetStat(CSMA_INIT);
}

/*****************************************************************************
 函 数 名  : CSMA_Impl
 功能描述  : CSMA轮询
 输入参数  : void  
 输出参数  : 无
 返 回 值  : 
 调用函数  : 
 被调函数  : 
 
 修改历史      :
  1.日    期   : 2018年4月28日
    作    者   : KelvinChan
    修改内容   : 新生成函数

*****************************************************************************/
void CSMA_Impl(void)
{
    switch(CsmaAttribute.Stat)
    {
        case CSMA_START:
            // 判断RF信号是否空闲
            if(CC1101_IsRfChannelClr() == SET)
            {
                // 等待帧间隙时间
                CSMA_SetStat(CSMA_SILENT_WAIT);
            }
            else
            {
                // 进入二进制指数等待时间
                CSMA_SetStat(CSMA_BEB_ON);
            }
        break;

        // 等待帧间隙时间
        case CSMA_SILENT_WAIT:

            // 判断RF是否空闲
            if(CC1101_IsRfChannelClr() == SET)
            {
                // 判断帧间隙时间是否到达
                if(Tmm_IsON(TMM_CSMA_BACKOFF) != SET)
                {
                    // CSMA完成 - 进入发送
                    CSMA_SetStat(CSMA_DONE);
                }
            }
            else
            {
                // 开启二进制指数退避
                CSMA_SetStat(CSMA_BEB_ON);
            }
        break;

        case CSMA_BEB_ON:

            // 判断随机退避时间是否到达
            if(Tmm_IsON(TMM_CSMA_BACKOFF) != SET)
            {
                // 判断RF是否空闲
                if(CC1101_IsRfChannelClr() == SET)
                {
                    // 等待帧间隙时间
                    CSMA_SetStat(CSMA_SILENT_WAIT);
                }
                else if(CsmaAttribute.Nb < MAC_MAX_BACKOFF_TIMS)
                {
                    // 再一次执行二进制退避算法
                    CSMA_SetStat(CSMA_BEB_ON);
                }
                else
                {
                    // CSMA 失败
                    CSMA_SetStat(CSMA_FAILURE);
                }
            }
        break;

        default :
        break;
    }
}


