/******************************************************************************

                  版权所有 (C), 2013-2016, 深圳市云居科技开发有限公司

 ******************************************************************************
  文 件 名   : DriversConfig.h
  版 本 号   : 初稿
  作    者   : 陈宏鸿
  生成日期   : 2016年8月31日
  最近修改   :
  功能描述   : 设备配置文件
  函数列表   :
  修改历史   :
  1.日    期   : 2016年8月31日
    作    者   : 陈宏鸿
    修改内容   : 创建文件

******************************************************************************/
#ifndef __DRIVES_CONFIG_H__
#define __DRIVES_CONFIG_H__

/*----------------------------------------------*
 * 包含头文件                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部变量说明                                 *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 外部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 内部函数原型说明                             *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 全局变量                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 模块级变量                                   *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 常量定义                                     *
 *----------------------------------------------*/

/*----------------------------------------------*
 * 宏定义                                       *
 *----------------------------------------------*/

/* MCU 类型 */
#define     MCU_TYPE_STM32F103          0
#define     MCU_TYPE_STM32F105          1

#define     MCU_TYPE                    MCU_TYPE_STM32F105
/* 应用代码调试接口 */
//#define USE_APP_FULL_ASSERT         1

/* 发送ID */
#define   SEND_PACKET_ID_NUM         0x00880000

/* 接收ID */
#define   RECEIVE_PACKET_ID_NUM      0x00770000

/* 发送时间间隔 */
#define   PACKET_TIME_SEC            3


/* 系统时钟 */
// 系统时钟分频为 10us
#define SYSTICKS_PER_SEC          100000


#define CC1101_CFG_GDO_NVIC_PRE 0
#define CC1101_CFG_GDO_NVIC_SUB 0
#define CC1101_GDO0_IS_ENABLE   ENABLE
#define CC1101_GDO2_IS_ENABLE   ENABLE
#define CC1101_GDO0_IT_IS_ENABLE    DISABLE
#define CC1101_GDO2_IT_IS_ENABLE    ENABLE
#define CC1101_GDO0_EXIT_MODE   EXTI_Mode_Interrupt
#define CC1101_GDO2_EXIT_MODE   EXTI_Mode_Interrupt
#define CC1101_GDO0_EXIT_TRIG   EXTI_Trigger_Rising_Falling
#define CC1101_GDO2_EXIT_TRIG   EXTI_Trigger_Falling
#define CC1101_MISO_EEROR_CNT   5000000
#define CC1101_MONITOR_TICK_MS  100
#define CC1101_RX_BUFF_SIZE     64
#define CC1101_TX_BUFF_SIZE     64
#define CC1101_RX_QUEUE_SIZE    64
#define CC1101_TX_QUEUE_SIZE    64




/* MAC地址调整为10Byte */
#define     SYSTEM_SN_APP_LEN           10
/* UNIT地址调整为2Byte */
#define     SYSTEM_UNIT_ADDR_LEN        2
/* APP地址调整为2Byte */
#define     SYSTEM_APP_ADDR_LEN         2



/* MCU内部存储 */
// 存储的起始地址 从FLASH的64K开始
#define     MCU_FLASH_DATA_START_ADDR           0x08010000
// 整个存储的大小
#define     MCU_FLASH_DATA_SIZE_BYTE            0x10000

#if (MCU_TYPE == MCU_TYPE_STM32F103)

// 数据块 4Byte 以256个为一块
#define     MCU_FLASH_CFG_BLOCK_SIZE_4_BYTE     256
// 数据块 1Byte 以1024个为一块
#define     MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE     1024

#elif (MCU_TYPE == MCU_TYPE_STM32F105)

// 数据块 4Byte 以256个为一块
#define     MCU_FLASH_CFG_BLOCK_SIZE_4_BYTE     512
// 数据块 1Byte 以1024个为一块
#define     MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE     2048

#else

// 数据块 4Byte 以256个为一块
#define     MCU_FLASH_CFG_BLOCK_SIZE_4_BYTE     256
// 数据块 1Byte 以1024个为一块
#define     MCU_FLASH_CFG_BLOCK_SIZE_1_BYTE     1024

#endif

// 输入输出通道 最大值
#define     MCU_INPUT_OUTPUT_MAX_CHANNEL        0
// 输出输出通道 个数
#define     MCU_INPUT_OUTPUT_CHANNEL            0

// PWM通道      个数
//#define     MCU_PWM_CHANNEL_NUM                 MCU_INPUT_OUTPUT_CHANNEL
// 继电器通道   个数
//#define     MCU_RELAY_CHANNEL_NUM               MCU_INPUT_OUTPUT_CHANNEL
// 按键通道     个数
//#define     MCU_KEY_CHANNEL_NUM                 MCU_INPUT_OUTPUT_CHANNEL
// LED通道      个数
//#define     MCU_LED_CHANNEL_NUM                 MCU_INPUT_OUTPUT_CHANNEL

// LED指示灯    个数
#define     MCU_LED_INDICATOR_CHANNEL_NUM       3
// IRS通道
#define     MCU_IRS_CHANNEL_NUM                 1
#define     MCU_IRS_FORMULA_NUM                 16

// 照度传感器
#define     MCU_BEAM_CHANNEL_NUM                1
#define     MCU_BEAM_FORMULA_NUM                16

// CAN转RF逻辑参数个数
#define     MCU_CAN_TO_RF_LOGIC_NUM             16
// RF转CAN逻辑参数个数
#define     MCU_RF_TO_CAN_LOGIC_NUM             16

// 
#define     MCU_ITF_RF_EXT_QUEUE_SIZE           32


// 输入输出通道 边界 起始
//#define     MCU_INPUT_OUTPUT_LIMIT_START        1
// 输入输出通道 边界 结束
//#define     MCU_INPUT_OUTPUT_LIMIT_END          MCU_INPUT_OUTPUT_CHANNEL

// MCU默认存储基地址
#define     MCU_DEFAULT_MMY_BASE_ADDR           0x08000000
// MCU默认单元地址存储地址
#define     MCU_UNIT_ADDR_DEFAULT_MMY_ADDR      (MCU_DEFAULT_MMY_BASE_ADDR + 0x1C00)
// MCU默认的CAN1波特率存储地址
#define     MCU_CAN1_BTR_DEFAULT_MMY_ADDR       (MCU_DEFAULT_MMY_BASE_ADDR + 0x1D00)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_CATCH_DEFAULT_MMY_ADDR      (MCU_DEFAULT_MMY_BASE_ADDR + 0x1000)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_AE_DEFAULT_MMY_ADDR         (MCU_DEFAULT_MMY_BASE_ADDR + 0x1040)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_E_DEFAULT_MMY_ADDR          (MCU_DEFAULT_MMY_BASE_ADDR + 0x1080)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_MO_DEFAULT_MMY_ADDR         (MCU_DEFAULT_MMY_BASE_ADDR + 0x10C0)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_ME_DEFAULT_MMY_ADDR         (MCU_DEFAULT_MMY_BASE_ADDR + 0x1100)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_O_DEFAULT_MMY_ADDR          (MCU_DEFAULT_MMY_BASE_ADDR + 0x1140)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_AO_DEFAULT_MMY_ADDR         (MCU_DEFAULT_MMY_BASE_ADDR + 0x1180)
// 红外捕获默认值存储的起始地址
#define     MCU_IRS_FORMULA_DEFAULT_MMY_ADDR    (MCU_DEFAULT_MMY_BASE_ADDR + 0X11C0)
// 固件信息默认值存储地址
#define     MCU_FIRMWARE_DEFAULT_MMY_START_ADDR (MCU_DEFAULT_MMY_BASE_ADDR + 0x1600)

// 照度传感器默认存储的起始地址
#define     MCU_BEAM_FORMULA_DEFAULT_MMY_ADDR   (MCU_DEFAULT_MMY_BASE_ADDR + 0x2000)

// 接口 CAN 默认存储参数
#define     MCU_ITF_CAN_DEFAULT_MMY_ADDR        (MCU_DEFAULT_MMY_BASE_ADDR + 0x2400)
// 接口 RF 默认存储参数
#define     MCU_ITF_RF_DEFAULT_MMY_ADDR         (MCU_DEFAULT_MMY_BASE_ADDR + 0x3000)

/* UART3 运行参数 */
// 定义了UART3的中断配置
#define     UART3_CFG_IT            (USART_IT_RXNE)
// 定义了UART3的波特率
#define     UART3_CFG_BAU           115200
// 定义了UART3的中断主优先级
#define     UART3_CFG_NVIC_PRE      0
// 定义了UART3的中断从优先级
#define     UART3_CFG_NVIC_SUB      0
// 定义了UART3的接收Buff大小
#define     UART3_RX_BUFF_SIZE      64
// 定义了UART3的发送Buff大小
#define     UART3_TX_BUFF_SIZE      64
// 定义了UART3的接收队列个数
#define     UART3_RX_QUEUE_SIZE     10
// 定义了UART3的发送队列个数
#define     UART3_TX_QUEUE_SIZE     10
// 定义了UART3接收成帧的时间
#define     UART3_RX_PACK_MS        10
// 定义了UART3发送的间隔时间
#define     UART3_TX_PACK_MS        15



/* CAN 配置 */
// CAN最大收发数据 1280Byte
#define     CAN_BUF_MAX_LEN         560
// 控制帧在队列中的数据长度
#define     CAN_FRAME_MAX_LEN       8
// CAN 发送队列
#define     CAN_TX_QUEUE_SIZE       100
// CAN 接收队列
#define     CAN_RX_QUEUE_SIZE       100



/* 系统 中断控制 */
// 关闭所有中断
#define     SYSTEM_DISABLE_ALL_IRQ          __set_PRIMASK(1)
// 使能所有中断
#define     SYSTEM_ENABLE_ALL_IRQ           __set_PRIMASK(0);



// 定义CPU状态寄存器类型
typedef     u32     CPU_SR;

// 定义并初始化 CPU状态暂存器
#define     CPU_SR_ALLOC()      CPU_SR cpu_sr = (CPU_SR)0

// 保存CPU状态寄存器 且 关闭总中断
static __INLINE CPU_SR   CPU_SR_Save(void)
{
    CPU_SR tmp_cpu_sr = 0;

    // 保存CPU寄存器
    tmp_cpu_sr = __get_PRIMASK();

    // 关闭所有中断
    __set_PRIMASK(1);

    // 返回CPU寄存器
    return tmp_cpu_sr;
}

// 恢复CPU状态
static __INLINE void  CPU_SR_Restore(CPU_SR tmp_cpu_sr)
{
    __set_PRIMASK(tmp_cpu_sr);
}

// 关闭总中断 且 暂存CPU寄存器状态
#define     CPU_INT_DIS()       do { cpu_sr = CPU_SR_Save(); } while (0)
// 恢复CPU寄存器状态
#define     CPU_INT_EN()        do { CPU_SR_Restore(cpu_sr); } while (0)


#define     VIRTUAL_ADDR_LEN        25


/* 设备类型标号 */
// 接口卡设备类型代码
#define     SYSTEM_DEVICE_INTERFACE     0x00010001

// 6路IO设备类型
#define     SYSTEM_DEVICE_IO6           0x00020001
// 3路按键设备类型
#define     SYSTEM_DEVICE_KEY3          0x00020002
// 16路IO设备类型
#define     SYSTEM_DEVICE_IO16          0x00020003

// 红外传感器设备类型
#define     SYSTEM_DEVICE_IR_SENSOR     0x00020F01

// 红外传感器 - 综合设备类型 - 基础人体红外 + RF转发 + 照度计
#define     SYSTEM_DEVICE_IR_SYNTHESIS  0x00020F11
// 照度传感器 - 独立设备类型 - 独立照度计
#define     SYSTEM_DEVICE_ILLUMINANCE   0x00021F01

// 4路16A调光器设备类型
#define     SYSTEM_DEVICE_DIMMER4_16    0x00030001

// 4路16A继电器设备类型
#define     SYSTEM_DEVICE_RELAY4_16A    0x00030101
// 4路10A继电器设备类型
#define     SYSTEM_DEVICE_RELAY4_10A    0x00030102

// 32路10A继电器设备类型
#define     SYSTEM_DEVICE_RELAY32_10A   0x00030132

// 4路窗帘设备类型
#define     SYSTEM_DEVICE_CURTAIN4      0x00030201

// 红外遥控设备类型代码
#define     SYSTEM_DEVICE_IR_REMOTE     0x00030F01

// 设备类型设置为 接口卡设备类型
#define     SYSTEM_DEVICE_TYPE          SYSTEM_DEVICE_IR_SYNTHESIS



/* 固件版本号 */
// 主版本号
#define     SYSTEM_VERSION_MAIN             0
// 子版本号
#define     SYSTEM_VERSION_SUB              3
// 正式发布号
#define     SYSTEM_VERSION_FINE             3



// 固件主版本号 - 跟随SN输出
#define     SYSTEM_FIRMWARE_MAIN            3
// 固件子版本号 - 跟随SN输出
#define     SYSTEM_FIRMWARE_SUB             3



#define     SYSTEM_UNIT_DEFAULT_VALUE       500

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif /* __cplusplus */



#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* __cplusplus */


#endif /* __DRIVES_CONFIG_H__ */

